#!/bin/bash

set -e

# These are set programmatically from create.sh
SSH_KEY=
PACMANSERVER=
PACKAGES_REMOVE=

echo -e "\nStarting setup.sh\n"

# confiugre /dev/sda/ to DOS
fdisk /dev/sda << EOF

n
p



w
EOF

# format partition
mkfs.ext4 /dev/sda1

# mount partition
mount /dev/sda1 /mnt

# Configure pacman mirrors for the live environment to bootstrap
echo "Server = ${PACMANSERVER}" > /etc/pacman.d/mirrorlist
# Make curl allow the self signed cert
sed -i "/^\[options\][[:space:]]*$/a XferCommand = /usr/bin/curl -v -k -L -C - -f -o %o %u" /etc/pacman.conf
# Only cache in RAM
sed -i "s/^#CacheDir.*$/CacheDir = \/tmp\/pacman-cache\//" /etc/pacman.conf

# Update the live system's keyring (helps for older .isos to make new disk up to date all offline)
pacman -Sy --noconfirm archlinux-keyring

# Bootstrap all packages on the real disk
pacstrap /mnt base base-devel linux grub
#base-devel linux grub --ignore $PACKAGES_REMOVE

# Do the same for the pacman configs on real disk
echo "Server = ${PACMANSERVER}" > /mnt/etc/pacman.d/mirrorlist
sed -i "/^\[options\][[:space:]]*$/a XferCommand = /usr/bin/curl -k -L -C - -f -o %o %u" /mnt/etc/pacman.conf
sed -i "s/^#CacheDir.*$/CacheDir = \/tmp\/pacman-cache\//" /mnt/etc/pacman.conf

# Add custom AUR pacman repo to pacman.conf so VM can get AUR stuff from host
# SigLevel = Never is needed for some packages that are checked on host, but guest shouldn't need to import a key
echo "[aur-repo]" >> /mnt/etc/pacman.conf
echo "SigLevel = Never" >> /mnt/etc/pacman.conf
echo "Server = ${PACMANSERVER}/aur/" >> /mnt/etc/pacman.conf

# Change journald and pacman to write to ram
#sed -i 's/^[[:space:]]*#Storage[[:space:]]*=[[:space:]]*auto.*$/Storage=volatile/' /mnt/etc/systemd/journald.conf
sed -i 's/^[[:space:]]*#LogFile[[:space:]]*=.*$/LogFile=\/var\/run\/pacman.log/' /mnt/etc/pacman.conf

# genfstab
genfstab -U /mnt >> /mnt/etc/fstab


# init.sh run in the arch-chroot
echo """#!/bin/bash
# Set root password to something random. Can only login from SSH then
echo "root:$(/usr/bin/head /dev/urandom | /usr/bin/md5sum)" | chpasswd

pacman-key --init
pacman-key --populate archlinux

pacman --noconfirm -Sy archlinux-keyring

pacman --noconfirm -Su \
xorg xorg-xinit i3 dmenu mesa-utils \
openssh \
gnome-terminal bash-completion nano \
htop \
#tigervnc \

sed -i 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-install --target=i386-pc /dev/sda
mkinitcpio -p linux
grub-mkconfig -o /boot/grub/grub.cfg

# Create user and set user password
mkdir /home/user/
useradd -s /bin/bash user
echo "user:a" | chpasswd

# Enable autologin
mkdir -p /etc/systemd/system/getty@tty1.service.d/

echo '''
[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin user --noclear %I \$TERM
''' > /etc/systemd/system/getty@tty1.service.d/override.conf

systemctl enable getty@tty1

echo '''
#unset SESSION_MANAGER
#unset DBUS_SESSION_BUS_ADDRESS
export \$(dbus-launch)
exec i3
''' > /home/user/.xinitrc

echo '''
export TERMINAL=gnome-terminal
export GDK_SCALE=2
PATH=/home/user/.local/bin:\$PATH
# Clear firefox cache
rm -rdf /home/user/.cache/mozilla/

# Spin until magic file exists to startx
if [[ -z "\${DISPLAY}" && "\${XDG_VTNR}" -eq 1 ]]; then
  echo "Waiting for /tmp/startx to exist to start GUI..."
  while true; do
    if [ -f /tmp/startx ]; then
	  exec startx
      break
    fi
    sleep .1
  done
fi
''' > /home/user/.bash_profile

# Configure vncserver for user
#echo ':0=user' > /etc/tigervnc/vncserver.users
#mkdir /home/user/.vnc/
#echo -n "password" | vncpasswd -f > /home/user/.vnc/passwd
#chmod 600 /home/user/.vnc/passwd
#echo '''
#desktop=sandbox
#dpi=192
#geometry=3840x2160
#''' > /home/user/.vnc/config
#echo '''#!/bin/sh
#unset SESSION_MANAGER
#unset DBUS_SESSION_BUS_ADDRESS
#[ -r \$HOME/.Xresources ] && xrdb \$HOME/.Xresources
#export \$(dbus-launch)
#source ~/.bash_profile
#cd
#exec i3
#''' > /home/user/.vnc/xstartup
echo '''
Xft.dpi:       192
Xft.antialias: true
Xft.hinting:   true
Xft.rgba:      rgb
Xft.autohint:  true
Xft.hintstyle: hintslight
Xft.lcdfilter: lcddefault
Xcursor.theme: xcursor-breeze
Xcursor.size: 24
*.font: Mono:pixelsize=40
''' > /home/user/.Xresources
#systemctl enable vncserver@:0

# Enable ipv4 dhcp with systemd-networkd
echo '''
[Match]
Name=*
[Network]
DHCP=ipv4
''' > /etc/systemd/network/all.network

systemctl enable systemd-networkd

# Allow root ssh login
mkdir -p /root/.ssh/
echo "$SSH_KEY" > /root/.ssh/authorized_keys
systemctl enable sshd

echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen

# Create a systemd.path (inotify) to load a user.js file to all firefox profiles
# The services calls a script that finds all folders in ~/.mozilla/firefox/ and adds user.js from /root/ to them
mkdir -p /home/user/.mozilla/

echo '''
[Unit]
Description=Monitor user .mozilla folder for Firefox being started for the first time
[Path]
PathModified=/home/user/.mozilla/
Unit=mozilla-monitor.service
[Install]
WantedBy=multi-user.target
''' > /etc/systemd/system/mozilla-monitor.path

echo '''
[Unit]
Description=Add user.js to all Firefox profiles
[Service]
Type=oneshot
ExecStart=-/root/mozilla-monitor.sh
''' > /etc/systemd/system/mozilla-monitor.service

echo '''#!/bin/bash
/usr/bin/find /home/user/.mozilla/firefox/ -maxdepth 1 -type d -name \"*\\.*\" -print0 | while IFS= read -r -d \"\" file; do /usr/bin/cp -f /root/user.js \"\$file\" ; done
/usr/bin/chmod a+r /home/user/.mozilla/firefox/*/user.js
''' > /root/mozilla-monitor.sh
chmod u+x /root/mozilla-monitor.sh
chattr +i /root/mozilla-monitor.sh

systemctl daemon reload
systemctl enable mozilla-monitor.path

/usr/bin/chown user:user -R /home/user/

# Disable the mandb generation
systemctl disable man-db.timer

# Downloads custom aur-repo
pacman -Sy
""" > /mnt/init.sh

arch-chroot /mnt /bin/bash /init.sh
rm /mnt/init.sh
shutdown now
