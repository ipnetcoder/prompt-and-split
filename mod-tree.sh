#!/bin/bash
# Add Kconfig and Makefile options to all the right files in the kernel source tree
# Copy the code files into the right places automatically

[[ -z "$1" ]] && echo "Need to give a folder destination" && exit 1
dest="$1"

random="/tmp/junk_file_1"
random2="/tmp/junk_file_2"

# Add some code ($2) after a line ($1) in a file ($3)
function addFileAfterLine(){
	if ! grep -q -- "$2" "$3" ; then sed -i "/$1/a $2" "$3" ; fi
}

function AcontainsB(){
	python -c "
with open('$1', 'r') as a:
  with open('$2', 'r') as b:
    exit(0 if ''.join([c for c in a.read() if c.strip()]) in ''.join([c for c in b.read() if c.strip()]) else 1)"
return $?
}

# Look for lines in B that match A (disregarding spaces,tabs,newlines) and remove them from B
function removeAfromB(){
    python -c "
with open('$1', 'r') as a:
  new = []
  with open('$2', 'r') as b:
    remove = a.readlines()
    while not remove[-1].strip(): remove.pop(-1)
    while not remove[0].strip(): remove.pop(0)
    file = b.readlines()
    def match(line1, line2):
      return ''.join([c for c in line1 if c.strip()]) == ''.join([c for c in line2 if c.strip()])
    f_index = 0
    last_jump = False
    while f_index < len(file) - len(remove) + 1:
       if sum([match(remove[r_index], file[f_index+ r_index]) for r_index in range(0,len(remove))]) == len(remove):
         f_index += len(remove)
         last_jump = True
       else:
         new.append(file[f_index])
         f_index += 1
         last_jump = False
    if not last_jump: new.extend(file[-len(remove)+1:])
  with open('$2', 'w') as b:
    for line in new: b.write(line)
"
}

# Starting when line == $1 is hit, duplicate until line == $6 and replace the line == $1 with the string $2
# replace instances of string $3 with string $4 in all lines between line == $1 and line == $6
# $5 is the file to open
# $7 is 0 for don't print the original function, 1 is do print the original function
function functionDuplicate(){
	python -c "
with open('$5', 'r') as f:
  start = False
  lines = []
  final = []
  for line in f.readlines():

    if '$1' == line.strip():
      start = True

    # Only add original function to final if $7 is 1
    if start == False or (start == True and '$7' == \"1\"):
      final.append(line)

    if start == True:
      lines.append(line.replace('$3', '$4'))
      if '$6' == line.strip():
        start = False
        final.extend(['$2'+'\n'] + lines[1:])
with open('$5', 'w') as f:
  f.write(\"\".join(final))
"
}

# Read file $3 and put the contents of file $2 immediately before the line == $1 (line == $1 is in file $3)
function addFileBeforeLine(){
    python -c "
inFile = []
outFile = []
with open('$2', 'r') as f:
  inFile = [line for line in f.readlines()] # Just in case readlines is a generator
with open('$3', 'r') as f:
  for line in f.readlines():
    if '$1' == line.strip():
      outFile.extend(inFile)
    outFile.append(line)
with open('$3', 'w') as f:
  f.write(\"\".join(outFile))
"
}

# Add line $1 set to $2 to the .config file
function configAdd(){
	if ! grep -q -- "^CONFIG_${1}[[:space:]]*=" ${dest}/.config ; then
		echo "CONFIG_${1}=${2}" >> ${dest}/.config
	else
		sed -i "s/^CONFIG_${1}[[:space:]]*=.*/CONFIG_${1}=${2}/" ${dest}/.config
	fi
}

# Add the test driver folder to Makefile
! grep -q -- "obj-\$(CONFIG_TEST_MODULE) += test/" ${dest}/drivers/Makefile && echo "obj-y += test/" >> ${dest}/drivers/Makefile

# Source test driver folder in driver Kconfig
echo 'source "drivers/test/Kconfig"' > $random
# Add text before a line in the file
awk 'NR==FNR{bfile = bfile $0 RS; next} /^endmenu/{printf "%s", bfile} {print}' $random ${dest}/drivers/Kconfig > $random2
cat $random2 > ${dest}/drivers/Kconfig

# export access_remote_vm from mm/memory.c to be used in other modules
! grep -q -- "EXPORT_SYMBOL_GPL(access_remote_vm);" ${dest}/mm/memory.c && echo "EXPORT_SYMBOL_GPL(access_remote_vm);" >> ${dest}/mm/memory.c

# Create xt_PDLOG.c with a few changes from xt_LOG.c. No xt_PDLOG.h is needed
file=${dest}/net/netfilter/xt_PDLOG.c
if [ ! -f $file ]; then
	cp ${dest}/net/netfilter/xt_LOG.c $file
	# Replace the NF_LOG_TYPE
	sed -i 's/NF_LOG_TYPE_LOG/NF_LOG_TYPE_PDLOG/g' $file
	# Replace the .name = "LOG"
	sed -i 's/\"LOG\"/\"PDLOG\"/g' $file
	# Remove the original author, aliases, and description
	sed -i 's/^MODULE_AUTHOR(.*//g' $file
	sed -i 's/^MODULE_ALIAS(.*//g' $file
	sed -i 's/^MODULE_DESCRIPTION(.*//g' $file
	# Add a new description and aliases
	echo """
MODULE_DESCRIPTION(\"Xtables: IPv4/IPv6 userspace packet logging with process and DNS info\");
MODULE_ALIAS(\"ipt_PDLOG\");
MODULE_ALIAS(\"ip6t_PDLOG\");""" >> $file
fi

# Move 'struct nf_log_buf' definition to nf_log.h, since it is now needed outside the original .c file
# Add this def from ${dest}/net/netfilter/nf_log.c to include/net/netfilter/nf_log.h.
echo """#define S_SIZE (1024 - (sizeof(unsigned int) + 1))

struct nf_log_buf {
        unsigned int    count;
        char            buf[S_SIZE + 1];
};""" > $random

# First confirm it is still in original place to alert if it ever changes with new kernel ver and silently breaks something
if ! AcontainsB $random "${dest}/net/netfilter/nf_log.c" ; then
	echo -e "\nALERT If this is the first time running this on a clean tree:"
	echo -e "\tnf_log.c nf_log_buf has changed! Need to update the finder code here!"
	echo -e "Ignore if runnning a second time, then nf_log_buf definition is gone from nf_log.c\n"
#	exit 1
fi

# Remove struct nf_log_buf definition from the c file (after checking it hasn't changed above)
removeAfromB $random "${dest}/net/netfilter/nf_log.c"
# Add that struct to the h file instead
if ! AcontainsB $random ${dest}/include/net/netfilter/nf_log.h ; then
	sed -i "/^#define _NF_LOG_H/r $random" ${dest}/include/net/netfilter/nf_log.h
fi
# Add logger type to the nf_log.h enum definition
! grep -q -- "NF_LOG_TYPE_PDLOG" ${dest}/include/net/netfilter/nf_log.h && \
	sed -i 's/NF_LOG_TYPE_LOG[[:space:]]*=[[:space:]]*0[[:space:]]*,/NF_LOG_TYPE_LOG = 0,NF_LOG_TYPE_PDLOG,/g' ${dest}/include/net/netfilter/nf_log.h

# Add logger type to net/netfilter/nft_log.c because there is compiler warning that it is not checked
str="case NF_LOG_TYPE_PDLOG:break;"
! grep -q -- "$str" ${dest}/net/netfilter/nft_log.c && \
	sed -i "/case NF_LOG_TYPE_MAX:/i ${str}" ${dest}/net/netfilter/nft_log.c

# Change nf_log_buf_close() definition to have an arguemnt to printk. This allows pdlog to not printk, but default loggers will
for loc in "${dest}/net/netfilter/nf_log.c" "${dest}/include/net/netfilter/nf_log.h"; do
	if ! grep -q -- "void nf_log_buf_close(struct nf_log_buf \*m, bool print)" "$loc" ; then
		sed -i "s/\(void nf_log_buf_close(struct nf_log_buf \*m\))/\1, bool print)/g" "$loc"
	fi
done
# Make the printk in nf_log.c conditional on the argument
if ! grep -q -- "if (print) printk(\"%s.n\", m->buf);" ${dest}/net/netfilter/nf_log.c ; then
	sed -i 's/\(printk("%s\\n", m->buf);\)/if (print) \1/g' ${dest}/net/netfilter/nf_log.c
fi

# Go into nf_log_syslog.c, where IPv4 and IPv6 LOG targets have code, and copy it to nf_pdlog.c that will only use
# the IPv4 and IPv6 loggers, not ARP, netdev, or bridge
file=${dest}/net/netfilter/nf_log_syslog.c
if ! grep -q -- "err6" $file ; then

	# Add the proper includes to the top of the file
	addFileAfterLine "#include <net\/netfilter\/nf_log.h>" "#include <linux\/netfilter\/xt_dns.h>" $file
	addFileAfterLine "#include <net\/netfilter\/nf_log.h>" "#include <net\/netfilter\/nf_pdlog.h>" $file

	# Add the default_pdloginfo struct after default_loginfo
	functionDuplicate "static const struct nf_loginfo default_loginfo = {" "static const struct nf_loginfo default_pdloginfo = {" "NF_LOG_TYPE_LOG" "NF_LOG_TYPE_PDLOG" $file "};" "1"

	# Add the nf_ip_pdlogger

	# Add the unregisters to the exit functions.
	functionDuplicate "static void __exit nf_log_syslog_exit(void)" "static void __exit nf_log_syslog_exit(void)" "nf_log_unregister(&nf_ip_logger);" "nf_log_unregister(&nf_ip_logger);nf_log_unregister(&nf_ip_pdlogger);nf_log_unregister(&nf_ip6_pdlogger);" $file "}" "0"
	functionDuplicate "static void __net_exit nf_log_syslog_net_exit(struct net *net)" "static void __net_exit nf_log_syslog_net_exit(struct net *net)" "nf_log_unset(net, &nf_ip_logger);" "nf_log_unset(net, &nf_ip_logger);nf_log_unset(net, &nf_ip_pdlogger);nf_log_unset(net, &nf_ip6_pdlogger);" $file "}" "0"

	# Add the registering and errs in the init functions around the "return 0;" line
	functionDuplicate "static int __init nf_log_syslog_init(void)" "static int __init nf_log_syslog_init(void)" "return 0;" "ret = nf_log_register(NFPROTO_IPV4, &nf_ip_pdlogger); if (ret < 0) goto err6; ret = nf_log_register(NFPROTO_IPV6, &nf_ip6_pdlogger); if (ret < 0) goto err7; return 0; err7: nf_log_unregister(&nf_ip_pdlogger); err6: nf_log_unregister(&nf_bridge_logger);" $file "}" "0"
	functionDuplicate "static int __net_init nf_log_syslog_net_init(struct net *net)" "static int __net_init nf_log_syslog_net_init(struct net *net)" "return 0;" "ret = nf_log_set(net, NFPROTO_IPV4, &nf_ip_pdlogger); if (ret) goto err5; ret = nf_log_set(net, NFPROTO_IPV6, &nf_ip6_pdlogger); if (ret) goto err6; return 0; err6: nf_log_unset(net, &nf_ip_pdlogger); err5: nf_log_unset(net, &nf_bridge_logger);" $file "}" "0"

	# Copy the nf_ip6?_logger definition to be pdlogger by adding one above
	for VERNUM in "4" "6"; do
		VER="$VERNUM"
		[[ "$VERNUM" == "4" ]] && VER="" # IPv4 leaves off 4 in some places for ip, but 6 always is ip6

		echo """static struct nf_logger nf_ip${VER}_pdlogger __read_mostly = {
	.name           = \"nf_pdlog_ipv${VERNUM}\",
	.type           = NF_LOG_TYPE_PDLOG,
	.logfn          = nf_pdlog_ip${VER}_packet,
    .me             = THIS_MODULE,
};""" > $random
		addFileBeforeLine "static struct nf_logger nf_ip${VER}_logger __read_mostly = {" $random $file

		# Copy the nf_log_ip6?_packet functions to be pdlogger
		functionDuplicate "static void nf_log_ip${VER}_packet(struct net *net, u_int8_t pf," "static void nf_pdlog_ip${VER}_packet(struct net *net, u_int8_t pf," "default_loginfo" "default_pdloginfo" $file "}" "1"

		# Convert the nf_log_buf_close(m) to true in the pdlogger functions (different from the false in the ipv4 if on ipv6)
		functionDuplicate "static void nf_pdlog_ip${VER}_packet(struct net *net, u_int8_t pf," "static void nf_pdlog_ip${VER}_packet(struct net *net, u_int8_t pf," "nf_log_buf_close(m);" "nf_log_buf_close(m, true);" $file "}" "0"

		# IPv6 is not implemented yet in xt_dns, so skip changing the nf_pdlog_ip6_packet() for now
		if [[ "$VERNUM" == "4" ]]; then

			# Because only "nf_log_buf_close(m, true);" line now exists in the file, add code ABOVE it for PDLOG
	    	echo """nf_pdlog_send_buf(m, skb,
        #if IS_ENABLED(CONFIG_NETFILTER_XT_MATCH_DNS)
            xt_dns_hostname_from_ipv$VERNUM(ip_hdr(skb)->daddr, net),
        #else
            NULL,
        #endif
        net);""" > $random
			# Add the nf_pdlog_send_buf to that unique function before the buf_close
			awk 'NR==FNR{bfile = bfile $0 RS; next} /nf_log_buf_close\(m, true\);/{printf "%s", bfile} {print}' $random $file > $random2
    		cat $random2 > $file
		fi

		# Convert the true to a false, the final value for these pdlog functions that should not printk
		sed -i 's/nf_log_buf_close(m, true);/nf_log_buf_close(m, false);/g' $file
	done

	# Set all existing close() print with the new function. Then later our own code can call same function and not printk
	sed -i 's/nf_log_buf_close(\([a-zA-Z0-9_]*\))/nf_log_buf_close(\1, true)/g' $file

	# Add the aliases ( and MODULE_ALIAS_NF_LOGGER aliases? )
	echo 'MODULE_ALIAS("nf_pdlog_ipv4");' >> $file
	echo 'MODULE_ALIAS("nf_pdlog_ipv6");' >> $file
fi

# Add a new LSM hook
if ! grep -q -- "LSM_HOOK(void, LSM_RET_VOID, socket_release, struct socket \*sock, struct inode \*inode)" ${dest}/include/linux/lsm_hook_defs.h ; then
	echo "LSM_HOOK(void, LSM_RET_VOID, socket_release, struct socket *sock, struct inode *inode)" >> ${dest}/include/linux/lsm_hook_defs.h
fi

if ! grep -q -- "static inline void security_socket_release(struct socket \*sock, struct inode \*inode){}" ${dest}/include/linux/security.h ; then
	echo "static inline void security_socket_release(struct socket *sock, struct inode *inode){}" > $random
	# Add text before a line in the file
	awk 'NR==FNR{bfile = bfile $0 RS; next} /static inline void security_socket_create\(int family,/{printf "%s", bfile} {print}' $random ${dest}/include/linux/security.h > $random2
	cat $random2 > ${dest}/include/linux/security.h
fi

if ! grep -q -- "security_socket_release(sock, inode);" ${dest}/net/socket.c ; then
	sed -i '/^static void __sock_release(struct socket \*sock, struct inode \*inode)$/ {n;/^{$/a security_socket_release(sock, inode);'$'\n''}' ${dest}/net/socket.c
fi

if ! grep -q -- "void security_socket_release(" ${dest}/security/security.c ; then
	echo "void security_socket_release(struct socket *sock, struct inode *inode){ call_void_hook(socket_release, sock, inode); } EXPORT_SYMBOL(security_socket_release);" > $random
	sed -i "/^#ifdef CONFIG_SECURITY_NETWORK$/r $random" ${dest}/security/security.c
fi

# Add the Kconfig options and Makefile options. Append or look for a specific line and add before that
if ! grep -q -- CONFIG_SECURITY_SOCKET_PROCESS ${dest}/security/Makefile ; then
	echo "obj-\$(CONFIG_SECURITY_SOCKET_PROCESS) += socket_process/" >> ${dest}/security/Makefile
	echo "subdir-\$(CONFIG_SECURITY_SOCKET_PROCESS) += socket_process" >> ${dest}/security/Makefile
fi
if ! grep -q -- "source \"security/socket_process/Kconfig\"" ${dest}/security/Kconfig ; then
	sed -i '/^source "security\/selinux\/Kconfig"$/a source "security\/socket_process\/Kconfig"' ${dest}/security/Kconfig
fi
if ! grep -q -- "void security_socket_release(struct socket \*sock, struct inode \*inode);" ${dest}/include/linux/security.h ; then
	sed -i '/^int security_socket_create(int family, int type, int protocol, int kern);$/a void security_socket_release(struct socket *sock, struct inode *inode);' ${dest}/include/linux/security.h
fi

addFileAfterLine "obj-\$(CONFIG_NF_LOG_SYSLOG) += nf_log_syslog.o" "obj-\$(CONFIG_NF_PDLOG) += nf_pdlog.o" "${dest}/net/netfilter/Makefile"
addFileAfterLine "obj-\$(CONFIG_NETFILTER_XT_TARGET_LOG) += xt_LOG.o" "obj-\$(CONFIG_NETFILTER_XT_TARGET_PDLOG) += xt_PDLOG.o" "${dest}/net/netfilter/Makefile"
addFileAfterLine "obj-\$(CONFIG_NETFILTER_XT_MATCH_U32) += xt_u32.o" "obj-\$(CONFIG_NETFILTER_XT_MATCH_PROC) += xt_proc.o" "${dest}/net/netfilter/Makefile"
addFileAfterLine "obj-\$(CONFIG_NETFILTER_XT_MATCH_U32) += xt_u32.o" "obj-\$(CONFIG_NETFILTER_XT_MATCH_DNS) += xt_dns.o" "${dest}/net/netfilter/Makefile"

if ! grep -q -- "config NF_PDLOG" ${dest}/net/netfilter/Kconfig ; then
	sed -i 's/config NF_LOG_SYSLOG/config NF_PDLOG\n\ttristate\n\nconfig NF_LOG_SYSLOG/g' ${dest}/net/netfilter/Kconfig
fi

if ! grep -q -- "config NETFILTER_XT_TARGET_PDLOG" ${dest}/net/netfilter/Kconfig ; then
	echo """
	config NETFILTER_XT_TARGET_PDLOG
        tristate \"PDLOG target support\"
        select NF_PDLOG
        default m # if NETFILTER_ADVANCED=n
        help
          This option adds a 'PDLOG' target, which copies target LOG
          and adds process and DNS info for packets.
          This calls a generic netlink module to send the data
          to a custom program.

          To compile it as a module, choose M here.  If unsure, say N.
""" > $random
	# Add text before a line in the file
	awk -v text=$text 'NR==FNR{bfile = bfile $0 RS; next} /config NETFILTER_XT_TARGET_LOG/{printf "%s", bfile} {print}' $random ${dest}/net/netfilter/Kconfig > $random2
	cat $random2 > ${dest}/net/netfilter/Kconfig
fi

if ! grep -q -- "config NETFILTER_XT_MATCH_DNS" ${dest}/net/netfilter/Kconfig ; then
	echo """
config NETFILTER_XT_MATCH_DNS
        tristate '\"dns\" match support'
	default m
        depends on NETFILTER_ADVANCED
        help
          Packet dns matching allows you to match packets
          based on what hostname or domain they are connecting to.
          A table is built in kernel by watching the unencrypted DNS packets.
          The user specifies with the --watch option what kind of packets to watch.

config NETFILTER_XT_MATCH_PROC
        tristate '\"proc\" match support'
	default m
        depends on NETFILTER_ADVANCED
        help
          Socket process matching allows you to match packets
          based on what process created the socket: path and comm. It is also
          possible to specificy a path not existsing, like for kernel sockets
""" > $random
	# Add text after a line
	sed -i "/^comment \"Xtables matches\"$/r $random" ${dest}/net/netfilter/Kconfig
fi

# Add the socket_process to the start of the CONFIG_LSM kernel config option.
# sed does not have negative lookahead so need grep first
if ! grep -q -- '^CONFIG_LSM="socket_process' ${dest}/.config ; then
	sed -i 's/^CONFIG_LSM[[:space:]]*=[[:space:]]*"\([^"]*\)"$/CONFIG_LSM="socket_process,\1"/' ${dest}/.config
fi

for i in "NETFILTER_XT_TARGET_PDLOG" "NETFILTER_XT_MATCH_DNS" "NETFILTER_XT_MATCH_PROC" $(find tree/drivers/ -name "Kconfig" | xargs -I {} -r grep "^config [A-Z_]*" "{}"  | awk -F' ' '{print $2}') ;
do
	configAdd "$i" "m"
done

for i in "SECURITY_SOCKET_PROCESS" "DM_CRYPT" "TRUSTED_KEYS" "ENCRYPTED_KEYS"
do
	configAdd "$i" "y"
done

/usr/bin/rm --force "$random" "$random2"
echo "Done with $0"
