#include <grp.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>
#include <xtables.h>
#include <unistd.h>
#include <string.h>

// Need to include the header file from the kernel module
#include "/home/user/code/prompt-and-split/tree/include/uapi/linux/netfilter/xt_dns.h"

// Enum for the specified options on the command line
enum {
	O_WATCH_QUERIES	= 0,
    O_HOSTNAME,
    O_DOMAIN,
    O_DST,
    O_SRC,
};

static const struct xt_option_entry dns_mt_opts[] = {
	{
		.name = "watch-queries",
		.id = O_WATCH_QUERIES,
		.type = XTTYPE_NONE,
	},
	{
		.name = "hostname",
		.id = O_HOSTNAME,
		.type = XTTYPE_STRING,
		.flags = XTOPT_INVERT | XTOPT_PUT, XTOPT_POINTER(struct xt_dns_match_info, string)
	},
	{
		.name = "domain",
		.id = O_DOMAIN,
		.type = XTTYPE_STRING,
		.flags = XTOPT_INVERT | XTOPT_PUT, XTOPT_POINTER(struct xt_dns_match_info, string)
	},
	{
		.name = "dns-dst",
		.id = O_DST,
		.type = XTTYPE_NONE,
	},
	{
		.name = "dns-src",
		.id = O_SRC,
		.type = XTTYPE_NONE,
	},
	XTOPT_TABLEEND, 
};

static void dns_mt_help(void){
	printf(
"dns match options:\n"
"    --watch-queries     update this\n"
"[!] --hostname          string matches the entire hostname, like www.example.com\n"
"[!] --domain            string matches domain, so google.com matches *.example.com and example.com\n"
"    --dns-src           match the hostname string to the packet source\n"
"    --dns-dst           match the hostname string to the packet destination\n"
);
}

static void dns_mt_parse(struct xt_option_call *cb){
	struct xt_dns_match_info *info = cb->data;
	xtables_option_parse(cb);

	switch (cb->entry->id) {
		case O_WATCH_QUERIES:
			info->match |= XT_DNS_WATCH_QUERIES;
			break;
		case O_HOSTNAME:
			if (cb->invert)
				info->invert |= XT_DNS_HOSTNAME;
			info->match |= XT_DNS_HOSTNAME;
			break;
		case O_DOMAIN:
			if (cb->invert)
				info->invert |= XT_DNS_DOMAIN;
			info->match |= XT_DNS_DOMAIN;
			break;
		case O_SRC:
			info->match |= XT_DNS_SRC;
			break;
		case O_DST:
			info->match |= XT_DNS_DST;
			break;
	}
}

static void dns_mt_print_item(const struct xt_dns_match_info *info, const char *label, uint8_t flag, bool numeric){	
	if (!(info->match & flag)) return;
	if (info->invert & flag) printf(" !");

	printf(" %s", label);

	switch (info->match & flag) {
		case XT_DNS_DOMAIN:
		case XT_DNS_HOSTNAME:
			printf(" %.*s", MAX_HOSTNAME_LEN, info->string);
			break;
	}
}

static void dns_mt_print(const void *ip, const struct xt_entry_match *match, int numeric){
	const struct xt_dns_match_info *info = (void *)match->data;

	dns_mt_print_item(info, "--watch-queries",   XT_DNS_WATCH_QUERIES,  numeric);
	dns_mt_print_item(info, "--hostname",        XT_DNS_HOSTNAME,       numeric);
	dns_mt_print_item(info, "--domain",          XT_DNS_DOMAIN,         numeric);
	dns_mt_print_item(info, "--dns-src",         XT_DNS_SRC,            numeric);
	dns_mt_print_item(info, "--dns-dst",         XT_DNS_DST,            numeric);
}

static void dns_mt_save(const void *ip, const struct xt_entry_match *match){
	dns_mt_print(ip, match, true);
}

static void dns_mt_check(struct xt_fcheck_call *cb){

	if (cb->xflags == 0){
		xtables_error(PARAMETER_PROBLEM, "dns: At least one parameter is required\n");
	}
	if ((cb->xflags & XT_DNS_SRC) && (cb->xflags & XT_DNS_DST)){
		xtables_error(PARAMETER_PROBLEM, "dns: Cannot select --dns-src and --dns-dst together\n");
	}
	if ((cb->xflags & XT_DNS_HOSTNAME) && (cb->xflags & XT_DNS_DOMAIN)){
		xtables_error(PARAMETER_PROBLEM, "dns: Cannot select --hostname and --domain together\n");
	}
	// if ((cb->xflags & XT_DNS_WATCH_QUERIES) && (XT_DNS_WATCH_QUERIES != cb->xflags)){
	// 	xtables_error(PARAMETER_PROBLEM, "dns: Must only select --watch-queries\n");
	// }
	// if ((cb->xflags & XT_DNS_DST) && (XT_DNS_DST == cb->xflags)){
	// 	xtables_error(PARAMETER_PROBLEM, "dns: Must select more than just --dns-dst\n");
	// }
	// if ((cb->xflags & XT_DNS_SRC) && (XT_DNS_SRC == cb->xflags)){
	// 	xtables_error(PARAMETER_PROBLEM, "dns: Must select more than just --dns-src\n");
	// }
}

static int dns_mt_xlate(struct xt_xlate *xl, const struct xt_xlate_mt_params *params){ 
	printf("DNS_XLATE not implemented!\n");
	return 1;
}

static struct xtables_match dns_mt_reg[] = {
	{
		.version       = XTABLES_VERSION,
		.name          = "dns",
		.revision      = 1,
		.family        = NFPROTO_UNSPEC,
		.size          = XT_ALIGN(sizeof(struct xt_dns_match_info)),
		.userspacesize = XT_ALIGN(sizeof(struct xt_dns_match_info)),
		.help          = dns_mt_help,
		.print         = dns_mt_print,
		.save          = dns_mt_save,
		.xlate	       = dns_mt_xlate,
		.x6_parse      = dns_mt_parse,
		.x6_fcheck     = dns_mt_check,
		.x6_options    = dns_mt_opts,
	},
};

void _init(void){ 
	xtables_register_matches(dns_mt_reg, sizeof(dns_mt_reg)/sizeof(*dns_mt_reg));
}
