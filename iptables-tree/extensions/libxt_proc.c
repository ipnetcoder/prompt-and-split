#include <grp.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>
#include <xtables.h>
#include <unistd.h>
#include <string.h>
#include "/home/user/code/prompt-and-split/tree/include/uapi/linux/netfilter/xt_proc.h"

enum {
	O_PATH = 0,
	O_PID,
	O_NOPATH,
	O_ARG1,
};

static const struct xt_option_entry proc_mt_opts[] = {
	{.name = "path",        .id = O_PATH,              .type = XTTYPE_STRING, .flags = XTOPT_INVERT | XTOPT_PUT, XTOPT_POINTER(struct xt_proc_match_info, path)},
	{.name = "pid",         .id = O_PID,               .type = XTTYPE_STRING, .flags = XTOPT_INVERT},
	{.name = "no-path",     .id = O_NOPATH,            .type = XTTYPE_NONE,   .flags = XTOPT_INVERT},
	{.name = "arg1",        .id = O_ARG1,              .type = XTTYPE_STRING,   .flags = XTOPT_INVERT},
	XTOPT_TABLEEND,
};

static void proc_mt_help(void){
	printf(
"proc match options:\n"
"[!] --path         for example: /usr/bin/sshd\n"
"[!] --pid          for exmaple: 1234"
"[!] --no-path      sk_buff, sock, or socket doesn't exist.\n"
"                   Usually for kernel packets\n"
"                   or incoming to no listening socket.\n"
"[!] --arg1         Arg 1 of /proc/[pid]/cmdline"
);
}

static void proc_mt_parse(struct xt_option_call *cb){
	struct xt_proc_match_info *info = cb->data;
	
	uint32_t pid;
	xtables_option_parse(cb);

	switch (cb->entry->id) {
		case O_PATH:
			if (cb->invert){
				info->invert |= XT_PROC_PATH;
			}
			info->match |= XT_PROC_PATH;
			break;
		case O_NOPATH:
			if (cb->invert){
				info->invert |= XT_PROC_NOPATH;
			}
			info->match |= XT_PROC_NOPATH;
			break;
		case O_ARG1:
			if (cb->invert){
				info->invert |= XT_PROC_ARG1;
			}
			info->match |= XT_PROC_ARG1;
			break;
		
		case O_PID:
			if (!xtables_strtoui(cb->arg, NULL, &pid, 0, UINT32_MAX - 1)){
				xtables_param_act(XTF_BAD_VALUE, "pid", "--pid", cb->arg);
			}
		
			if (cb->invert){
				info->invert |= XT_PROC_PID;
			}
			info->match |= XT_PROC_PID;
			info->pid = pid;
			break;
	}
}

static void proc_mt_print_item(const struct xt_proc_match_info *info, const char *label, uint8_t flag, bool numeric){	
	if (!(info->match & flag)){
		return;
	}
	if (info->invert & flag){
		printf(" !");
	}

	printf(" %s", label);

	switch (info->match & flag) {
		case XT_PROC_PATH:
			printf(" %.*s", PATH_MAX, info->path);
			break;
		case XT_PROC_ARG1:
			printf(" %.*s", PATH_MAX, info->arg1);
			break;
		case XT_PROC_PID:
			printf(" %d", info->pid);
			break;
	}
}

static void proc_mt_print(const void *ip, const struct xt_entry_match *match, int numeric){
	const struct xt_proc_match_info *info = (void *)match->data;

	proc_mt_print_item(info, "--path", XT_PROC_PATH, numeric);
	proc_mt_print_item(info, "--pid", XT_PROC_PID, numeric);
	proc_mt_print_item(info, "--no-path", XT_PROC_NOPATH, numeric);
	proc_mt_print_item(info, "--arg1", XT_PROC_ARG1, numeric);
}

static void proc_mt_save(const void *ip, const struct xt_entry_match *match){
	proc_mt_print(ip, match, true);
}

static void proc_mt_check(struct xt_fcheck_call *cb){
	if (cb->xflags == 0){
		xtables_error(PARAMETER_PROBLEM, "proc: At least one of --path --pid --no-path --arg1 is required\n");
	}
}

static int proc_mt_xlate(struct xt_xlate *xl, const struct xt_xlate_mt_params *params){ 
	printf("PROC_XLATE NOT IMPLEMENTED!\n");
	return -1;
}

static struct xtables_match proc_mt_reg[] = {
	{
		.version       = XTABLES_VERSION,
		.name          = "proc",
		.revision      = 1,
		.family        = NFPROTO_UNSPEC,
		.size          = XT_ALIGN(sizeof(struct xt_proc_match_info)),
		.userspacesize = XT_ALIGN(sizeof(struct xt_proc_match_info)),
		.help          = proc_mt_help,
		.print         = proc_mt_print,
		.save          = proc_mt_save,
		.xlate	       = proc_mt_xlate,
		.x6_parse      = proc_mt_parse,
		.x6_fcheck     = proc_mt_check,
		.x6_options    = proc_mt_opts,
	},
};
void _init(void){ 
	xtables_register_matches(proc_mt_reg, sizeof(proc_mt_reg)/sizeof(*proc_mt_reg));
}
