#ifndef _NF_PDLOG_H
#define _NF_PDLOG_H

void nf_pdlog_send_buf(struct nf_log_buf *m, const struct sk_buff *skb, const char* hostname, struct net *net);

#endif /* _NF_PDLOG_H */
