#ifndef _SOCKET_PROCESS_LSM_H
#define _SOCKET_PROCESS_LSM_H

extern char* socket_process_lsm_get_path(struct socket *);
extern pid_t socket_process_lsm_get_pid(struct socket *);
extern char* socket_process_lsm_get_arg1(struct socket *);
extern bool socket_process_lsm_destined(const struct sk_buff *, char const *);
extern int socket_process_lsm_seq_show(struct seq_file *, void *);
extern void socket_process_set_feature(int, bool);

int socket_proces_lsm_security_socket_accept(struct socket *, struct socket *);
int socket_proces_lsm_security_socket_bind(struct socket *, struct sockaddr *, int);
int socket_proces_lsm_security_socket_post_create(struct socket *, int, int, int, int);
void socket_proces_lsm_security_socket_release(struct socket *, struct inode *);
#endif /* _SOCKET_PROCESS_LSM_H */
