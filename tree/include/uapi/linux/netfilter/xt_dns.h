#ifndef _XT_DNS_MATCH_H
#define _XT_DNS_MATCH_H

#include <linux/types.h>

/* In comments note that a DNS query [packet] is the packet that each process sends out to the DNS server.
 * DNS repsonse [packet] is the packet sent back.
 * A DNS packet is the same form for queries and responses.
 * A query should include 1 question, the hostname that the process wants tthe IP for and 0 answers.
 * The assocaited repsonse should have the same question with different types of answers, hopefully one being an IP.
 */

// Max length for a full hostname including dots
#define MAX_HOSTNAME_LEN 255

typedef uint32_t IPv4_TYPEDEF;

//Interface to get the hostname for an IP address so that NFLOG can ask for it to display
char* xt_dns_hostname_from_ipv4(__be32 ip, struct net* net);

char* xt_dns_hostname_from_ipv6(struct in6_addr *ip, struct net *net);

enum {
    XT_DNS_WATCH_QUERIES    = 1 << 0,
    XT_DNS_HOSTNAME 		= 1 << 1,
    XT_DNS_DOMAIN           = 1 << 2,
    XT_DNS_DST   			= 1 << 3,
    XT_DNS_SRC              = 1 << 4,
};

struct xt_dns_match_info {
	char string[MAX_HOSTNAME_LEN + 1]; //the string to match the domain or hostname to
	__u8 match, invert;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Packet Byte Counts and Values:

#define UDP_HEADERLEN                  8

//Bytes concerning DNS packets
#define ETHERNET_HEADER_LEN            0   //16 if it was captured by wireshark and had the "Linux Cooked Capture" start section
#define UDP_HEADER_LEN                 8
#define DNS_HEADER_BYTES_START         ETHERNET_HEADER_LEN + (4 * ip_header->ihl) + UDP_HEADER_LEN

#define DNS_HEADER_BYTES_BEFORE_QUERIES_SECTION                 12              //bytes in the dns header before the queries seciton
#define DNS_HEADER_BYTES_AFTER_QUERY_NAME                       4               //bytes after the query name to the next entry
#define DNS_HEADER_BYTES_BEFORE_QUESTION_COUNT                  4
#define DNS_HEADER_BYTES_BEFORE_ANSWER_COUNT                    DNS_HEADER_BYTES_BEFORE_QUESTION_COUNT + 2
#define DNS_HEADER_BYTES_BEFORE_AUTHORATATIVE_COUNT             DNS_HEADER_BYTES_BEFORE_QUESTION_COUNT + 4
#define DNS_HEADER_BYTES_BEFORE_ADDITIONAL_COUNT                DNS_HEADER_BYTES_BEFORE_QUESTION_COUNT + 6

#define DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION                 DNS_HEADER_BYTES_START + DNS_HEADER_BYTES_BEFORE_QUERIES_SECTION 
#define DNS_PACKET_BYTES_BEFORE_QUESTION_COUNT                  DNS_HEADER_BYTES_START + DNS_HEADER_BYTES_BEFORE_QUESTION_COUNT
#define DNS_PACKET_BYTES_BEFORE_ANSWER_COUNT                    DNS_HEADER_BYTES_START + DNS_HEADER_BYTES_BEFORE_ANSWER_COUNT
#define DNS_PACKET_BYTES_BEFORE_AUTHORATATIVE_COUNT             DNS_HEADER_BYTES_START + DNS_HEADER_BYTES_BEFORE_AUTHORATATIVE_COUNT
#define DNS_PACKET_BYTES_BEFORE_ADDITIONAL_COUNT                DNS_HEADER_BYTES_START + DNS_HEADER_BYTES_BEFORE_ADDITIONAL_COUNT

#define DNS_ANSWER_BYTES_BEFORE_TYPE                            2     //bytes before the type in each answer in the answer section
#define DNS_ANSWER_BYTES_BEFORE_ANSWER                          12    //bytes before the actual answer in each answer section
#define DNS_ANSWER_BYTES_BEFORE_LENGTH                          10    //bytes before the data len in the answer field of each answer in the answer section

//Values for DNS fields
#define DNS_QUERY_TYPE_A                                        1               //Answer type
#define DNS_QUERY_TYPE_CNAME                                    5               //CNAME type
#define DNS_QUERY_CLASS_INET                                    1

#endif /* _XT_DNS_MATCH_H */






