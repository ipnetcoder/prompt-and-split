#ifndef _XT_PROC_MATCH_H
#define _XT_PROC_MATCH_H

#include <linux/types.h>

enum {
    XT_PROC_PATH 			= 1 << 0,
    XT_PROC_NOPATH 			= 1 << 1,
    XT_PROC_PID             = 1 << 2,
    XT_PROC_ARG1            = 1 << 3,
};

#define XT_PROC_MASK (XT_PROC_PATH | XT_PROC_NOPATH | XT_PROC_PID | XT_PROC_ARG1)

struct xt_proc_match_info {
	char path[PATH_MAX + 1];
	char arg1[PATH_MAX + 1];
    pid_t pid;
	__u8 match, invert;
};
#endif /* _XT_PROC_MATCH_H */
