#include <net/sock.h>
#include <linux/atomic.h>
#include <linux/bitops.h>
#include <linux/interrupt.h>
#include <linux/netdevice.h>    /* for network interface checks */
#include <net/netlink.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/dccp.h>
#include <linux/sctp.h>
#include <net/sctp/structs.h>
#include <linux/quota.h>
#include <linux/un.h>           /* for Unix socket types */
#include <net/af_unix.h>        /* for Unix socket types */
#include <linux/parser.h>
#include <linux/nfs_mount.h>
#include <net/ipv6.h>
#include <linux/hugetlb.h>
#include <linux/personality.h>
#include <linux/audit.h>
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/posix-timers.h>
#include <linux/syslog.h>
#include <linux/user_namespace.h>
#include <linux/export.h>
#include <linux/msg.h>
#include <linux/shm.h>
#include <linux/bpf.h>
#include <linux/kernfs.h>
// #include <linux/stringhash.h>
#include <uapi/linux/mount.h>
#include <linux/errno.h>
#include <linux/sched/signal.h>
#include <linux/sched/task.h>
#include <linux/lsm_hooks.h>
#include <linux/xattr.h>
#include <linux/capability.h>
#include <linux/unistd.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/slab.h>
#include <linux/pagemap.h>
#include <linux/proc_fs.h>
#include <linux/swap.h>
#include <linux/spinlock.h>
#include <linux/syscalls.h>
#include <linux/dcache.h>
#include <linux/file.h>
#include <linux/fdtable.h>
#include <linux/namei.h>
#include <linux/mount.h>
#include <linux/fs_context.h>
#include <linux/fs_parser.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter_ipv6.h>
#include <linux/tty.h>
#include <net/icmp.h>
#include <net/ip.h>             /* for local_port_range[] */
#include <net/tcp.h>            /* struct or_callable used in sock_rcv_skb */
#include <net/inet_connection_sock.h>
#include <net/net_namespace.h>
#include <net/netlabel.h>
#include <linux/uaccess.h>
#include <asm/ioctls.h>
#include <linux/init.h>
#include <linux/kd.h>
#include <linux/kernel.h>

// #include <linux/tracehook.h>

#include <linux/kern_levels.h>
#include <linux/binfmts.h>
#include <linux/in6.h>
#include <net/ipv6.h>
#include <net/socket_process.h> // This is where other places can read the hashtable

#define SOCKET_PROCESS_LSM_DEBUG 0
#define SOCKET_PROCESS_LSM_ONLY_IPV4 1

//Structure to store socket -> process string is a Linked List.. Ideally it is not queried often enough to warrant being a hashed set 
//Another structure could be an array. 

// Hashtable of socket pointer to that pointer and a char* to the process
#include <linux/hashtable.h>
#include <linux/rwlock_types.h>
DEFINE_HASHTABLE(hashtable, 12);
DEFINE_RWLOCK(hashtable_rwlock_t);
struct entry_node {
    char *path_pointer;
    char *path;
    char *arg1_pointer; // currently storing this for bound sockets, but not setting it
    char *arg1;
    pid_t pid;
    bool listening; //when deleted, should we try to delete a listening socket from the linked list
    struct socket *sock;
    struct sockaddr *address;
    struct hlist_node hlist_node;
};

DEFINE_RWLOCK(listening_ll4_rwlock_t);
struct listening_ll4 {
    __be16 sin_port;
    __be32 s_addr; //store the fastest to access info for the address
    char *path;
    struct socket *sock;
    struct list_head myList;
};
struct list_head listening_ll4_head;

struct listening_ll6 {
    __be16 sin6_port;
    struct in6_addr sin6_addr;
    void *sock;
    struct list_head myList;
};
struct list_head listening_ll6_head;

// Should this LSM get arg1 of processes
bool socket_process_feature_arg1 = false;

extern int access_remote_vm(struct mm_struct *mm, unsigned long addr, void *buf, int len, unsigned int gup_flags);

// Both socket_process_lsm_get_mm_proctitle and socket_process_lsm_get_mm_cmdline are copied from fs/proc/base.c
// copy_to_user() is replaced with in kernel memcpy since we want arg1 or cmdline in a kernel string
static ssize_t socket_process_lsm_get_mm_proctitle(struct mm_struct *mm, char *buf, size_t count, unsigned long pos, unsigned long arg_start){
    char *page;
    int ret, got;

    if (pos >= PAGE_SIZE){
        return 0;
    }

    page = (char *)__get_free_page(GFP_KERNEL);
    if (!page){
        return -ENOMEM;
    }

    ret = 0;
    got = access_remote_vm(mm, arg_start, page, PAGE_SIZE, FOLL_ANON);
    if (got > 0) {
        int len = strnlen(page, got);

        /* Include the NUL character if it was found */
        if (len < got)
            len++;

        if (len > pos) {
            len -= pos;
            if (len > count)
                len = count;
            
            memcpy(buf, page+pos, len);
            ret = 0;
        }
    }
    free_page((unsigned long)page);
    return ret;
}

static ssize_t socket_process_lsm_get_mm_cmdline(struct mm_struct *mm, char *buf, size_t count, loff_t *ppos){
    unsigned long arg_start, arg_end, env_start, env_end;
    unsigned long pos, len;
    char *page, c;

    /* Check if process spawned far enough to have cmdline. */
    if (!mm->env_end){
        return 0;
    }

    spin_lock(&mm->arg_lock);
    arg_start = mm->arg_start;
    arg_end = mm->arg_end;
    env_start = mm->env_start;
    env_end = mm->env_end;
    spin_unlock(&mm->arg_lock);

    if (arg_start >= arg_end){
        return 0;
    }

    /*
     * We allow setproctitle() to overwrite the argument
     * strings, and overflow past the original end. But
     * only when it overflows into the environment area.
     */
    if (env_start != arg_end || env_end < env_start)
        env_start = env_end = arg_end;
    len = env_end - arg_start;

    /* We're not going to care if "*ppos" has high bits set */
    pos = *ppos;
    if (pos >= len){
        return 0;
    }
    if (count > len - pos)
        count = len - pos;
    if (!count){
        return 0;
    }

     /* Magical special case: if the argv[] end byte is not
     * zero, the user has overwritten it with setproctitle(3).
     *
     * Possible future enhancement: do this only once when
     * pos is 0, and set a flag in the 'struct file'.
     */
    if (access_remote_vm(mm, arg_end-1, &c, 1, FOLL_ANON) == 1 && c){
        return socket_process_lsm_get_mm_proctitle(mm, buf, count, pos, arg_start);
    }

    /*
     * For the non-setproctitle() case we limit things strictly
     * to the [arg_start, arg_end[ range.
     */
    pos += arg_start;
    if (pos < arg_start || pos >= arg_end){
        return 0;
    }
    if (count > arg_end - pos)
        count = arg_end - pos;

    page = (char *)__get_free_page(GFP_KERNEL);
    if (!page){
        return -ENOMEM;
    }

    len = 0;
    while (count) {
        int got;
        size_t size = min_t(size_t, PAGE_SIZE, count);

        got = access_remote_vm(mm, pos, page, size, FOLL_ANON);
        if (got <= 0)
            break;
        memcpy(buf, page, got);
        pos += got;
        buf += got;
        len += got;
        count -= got;
    }

    free_page((unsigned long)page);
    return len;
}

// Allocate and set pointers
static void socket_process_lsm_set_pointers(char** path_pointer, char** path, pid_t* pid, char** arg1_pointer, char** arg1){

    struct mm_struct* mm = current->mm;
    // struct mm_struct* mm = get_task_mm(current);
    // if (!mm) return 0;
    // mmput(mm);
    if (mm){
        down_read(&mm->mmap_lock);
        if (mm->exe_file){
            loff_t ppos;
            ssize_t ret;    
            char *tmp;
            #define SOCKET_PROCESS_ALLOC_FAIL { if (*path_pointer){ kfree(*path_pointer);}*path_pointer = 0;*path = 0;*arg1_pointer = 0;*arg1 = 0;*pid = 0; }

            if((*path_pointer = kmalloc(PATH_MAX, GFP_ATOMIC))){
                *path = d_path(&mm->exe_file->f_path, *path_pointer, PATH_MAX);
                *pid  = current->pid;
                // if path in list of cmdline checks (python):
                // read the cmdline from this mm and take the first argument and set that as the path
                if (true == socket_process_feature_arg1){
                    if ((tmp = kmalloc(PATH_MAX, GFP_ATOMIC))){
                        ret = socket_process_lsm_get_mm_cmdline(mm, tmp, PATH_MAX, &ppos);
                        *arg1_pointer = tmp;
                        *arg1 = strnlen(tmp, ret) < ret ? tmp + strnlen(tmp, ret) + 1 : NULL;
                    } else {
                        printk(KERN_ERR "socket_process_lsm: ERROR: kmalloc failed for arg1.\n");
                        SOCKET_PROCESS_ALLOC_FAIL
                    }
                } else {
                    *arg1_pointer = 0;
                    *arg1 = 0;
                }
            } else {
                printk(KERN_ERR "socket_process_lsm: ERROR: kmalloc failed for path.\n");
                SOCKET_PROCESS_ALLOC_FAIL
            }
        } else { // Never hit yet in a few months
            printk(KERN_ERR "socket_process_lsm: ERROR: no exe_file for %s\n", current->comm) ;
        }
        up_read(&mm->mmap_lock);
    } else { // This gets hit on boot for "swapper/0" and messes up terminal if KERN_ERR
        printk(KERN_ERR "socket_process_lsm: ERROR: no mm for %s\n", current->comm) ;
    }
}

#define MACRO_SET_PATH(PATH_POINTER, PATH, PID, ARG1_POINTER, ARG1) \
    char* PATH_POINTER = 0; \
    char* PATH = 0; \
    pid_t PID = 0; \
    char* ARG1_POINTER = 0; \
    char* ARG1 = 0; \
    socket_process_lsm_set_pointers(&PATH_POINTER, &PATH, &PID, &ARG1_POINTER, &ARG1);

#define SOCKET_PROCESS_LSM_GET_MEMBER(MEMBER, RETURN_VALUE) \
    struct entry_node* iter; \
    unsigned long hash_flags; \
    if(0 == sock){ \
        printk(KERN_WARNING "socket_process_lsm: Given NULL socket, returned %d. comm %s\n", RETURN_VALUE, current->comm); \
        return RETURN_VALUE; \
    } \
    read_lock_irqsave(&hashtable_rwlock_t, hash_flags); \
    hash_for_each_possible(hashtable, iter, hlist_node, (u64)sock){ \
        if (sock == iter->sock){ \
            read_unlock_irqrestore(&hashtable_rwlock_t, hash_flags); \
            return iter->MEMBER; \
        } \
    } \
    read_unlock_irqrestore(&hashtable_rwlock_t, hash_flags); \
    printk(KERN_ERR "socket_process_lsm: No element for sock, returning %d. comm %s\n", RETURN_VALUE, current->comm); \
    return RETURN_VALUE;

// Return the path from a socket. TODO: return allocated memory
char* socket_process_lsm_get_path(struct socket *sock){
    SOCKET_PROCESS_LSM_GET_MEMBER(path, NULL)
}
EXPORT_SYMBOL(socket_process_lsm_get_path);

char* socket_process_lsm_get_arg1(struct socket *sock){
    SOCKET_PROCESS_LSM_GET_MEMBER(arg1, NULL)
}
EXPORT_SYMBOL(socket_process_lsm_get_arg1);

pid_t socket_process_lsm_get_pid(struct socket *sock){
    SOCKET_PROCESS_LSM_GET_MEMBER(pid, 0)
}
EXPORT_SYMBOL(socket_process_lsm_get_pid);

// Add socket to the hashtable.
// Path is gotten ASAP because I've seen issues with 'current' macro, so be fast
// This function takes memory ownership of path_pointer and arg1_pointer
static void socket_process_lsm_update_socket(struct socket *sock, char *path_pointer, char *path, pid_t pid, char* arg1_pointer, char* arg1){
    //Go through the hash table and delete the old socket struct and add a new one
    struct entry_node *iter;
    unsigned long hash_flags;
    bool sock_in_table = false;
    
    // Search the hashtable for this socket pointer and update it if found
    read_lock_irqsave(&hashtable_rwlock_t, hash_flags);
    hash_for_each_possible(hashtable, iter, hlist_node, (u64)sock){
        if (sock == iter->sock){

            if(iter->path_pointer){
                kfree(iter->path_pointer);
            }
            if(iter->arg1_pointer){
                kfree(iter->arg1_pointer);
            }

            // SHOULD CHECK THE OTHER ITER PROPERTIES HERE LIKE LISTENING BOOL?
            iter->path_pointer = path_pointer;
            iter->path = path;
            iter->pid = pid;
            iter->arg1_pointer = arg1_pointer;
            iter->arg1 = arg1;

            sock_in_table = true;
            goto socket_process_lsm_update_socket_break;
        }
    }
    socket_process_lsm_update_socket_break:
    read_unlock_irqrestore(&hashtable_rwlock_t, hash_flags);

    // Did not find an entry matching this socket (iter is null or it's path wasn't set to the one passed in), so add a new one
    if (false == sock_in_table){
        hash_flags = 0;

        if(!(iter = kmalloc(sizeof(struct entry_node), GFP_ATOMIC))){
            printk(KERN_ERR "socket_process_lsm: ERROR allocating iter in update_socket for %s, %d\n", path, pid);
            if (path_pointer){
                kfree(path_pointer);
            }
            if (arg1_pointer){
                kfree(arg1_pointer);
            }
            return;
        }
        iter->path_pointer = path_pointer;
        iter->path = path;
        iter->pid  = pid;
        iter->arg1_pointer = arg1_pointer;
        iter->arg1 = arg1;
        iter->sock = sock;

        write_lock_irqsave(&hashtable_rwlock_t, hash_flags);
        hash_add(hashtable, &iter->hlist_node, (u64)sock);
        write_unlock_irqrestore(&hashtable_rwlock_t, hash_flags);
    }
}

// Locks linked list, then locks hashtable
// Is this skb destined for a listening/bound socket created by this path? skb->sk->sk_socket is not considered
// path can be NULL, but then socket owner must also be NULL
bool socket_process_lsm_destined(const struct sk_buff* skb, const char* path) {
    /* Paths both exist or are both null. !! reduces existance to 1 so bitwise XOR will work. */
    /* And if PATH1 is null, then PATH2 is null so match (this or protects strncmp from PATH2 being null) OR if the paths match */
    #define MACRO_PATH_LOGIC(CALLED_PATH, LISTEN_PATH, RET) \
    if (!(!!CALLED_PATH ^ !!LISTEN_PATH) && (0 == CALLED_PATH || 0 == strncmp(CALLED_PATH, LISTEN_PATH, PATH_MAX))) { \
        RET = true; \
    }

    if (skb->protocol == htons(ETH_P_IP)){
        unsigned long ll_flags;
        struct listening_ll4 *entry = 0;

        __be16 port;
        if(IPPROTO_TCP == ip_hdr(skb)->protocol){
            port = tcp_hdr(skb)->dest;
        } else if (IPPROTO_UDP == ip_hdr(skb)->protocol){
            port = udp_hdr(skb)->dest;
        } else {
            return false;
        }

        read_lock_irqsave(&listening_ll4_rwlock_t, ll_flags);
        list_for_each_entry(entry, &listening_ll4_head, myList) {
            // printk(KERN_INFO "dest: %d, %d, %s\n", iter->sin_port, iter->s_addr, path);
            //account for (listening on 0.0.0.0 or the addresses are the same)
            if (port == entry->sin_port && (0 == entry->s_addr || ip_hdr(skb)->daddr == entry->s_addr)) {
                bool ret = false;
                MACRO_PATH_LOGIC(path, entry->path, ret)
                if (ret) {
                    read_unlock_irqrestore(&listening_ll4_rwlock_t, ll_flags);
                    return ret;
                }
            }
        }
        read_unlock_irqrestore(&listening_ll4_rwlock_t, ll_flags);
    } else if (skb->protocol == htons(ETH_P_IPV6)) {
        // struct ipv6hdr *ip6_header = ipv6_hdr(skb);
        // const struct in6_addr daddr = ip6_header->daddr;
        // struct listening_ll6* iter = head6;
        // //FIX HERE
        // __be16 sport = 0;
        // while (iter){
        //     if (sport == iter->sin6_port &&
        //         (
        //          (
        //             //addrs match exactly
        //             daddr.s6_addr32[0] == iter->sin6_addr.s6_addr32[0] && daddr.s6_addr32[1] == iter->sin6_addr.s6_addr32[1] &&
        //             daddr.s6_addr32[2] == iter->sin6_addr.s6_addr32[2] && daddr.s6_addr32[3] == iter->sin6_addr.s6_addr32[3]
        //          )
        //         ||  //or
        //          (
        //             //listening on ::
        //             0 ==  iter->sin6_addr.s6_addr32[0] && 0 ==  iter->sin6_addr.s6_addr32[1] &&
        //             0 ==  iter->sin6_addr.s6_addr32[3] && 0 ==  iter->sin6_addr.s6_addr32[3]
        //          )
        //         )
        //     ) {
        //         char* iter_path = socket_process_lsm_get_path(iter->sock);
        //         MACRO_PATH_LOGIC(path, iter_path)
        //     }
        //     iter = iter->next;
        // }
    }
    return false;
}
EXPORT_SYMBOL(socket_process_lsm_destined);

int socket_process_lsm_seq_show(struct seq_file *m, void *v){
    struct listening_ll4 *entry;
    struct entry_node* iter;
    unsigned int counter;
    unsigned long ll_flags, hash_flags;

    seq_puts(m, "=== Start Feature Table ===\n");
    
    seq_printf(m, "arg1: %d\n", socket_process_feature_arg1);

    seq_puts(m, "=== End Feature Table ===\n\n=== Start Listening Table ===\n");
    
    read_lock_irqsave(&listening_ll4_rwlock_t, ll_flags);
    list_for_each_entry(entry, &listening_ll4_head, myList) {
        seq_printf(m, "%s %d:%d\n", entry->path ,entry->s_addr, entry->sin_port);
    }
    read_unlock_irqrestore(&listening_ll4_rwlock_t, ll_flags);
    
    seq_puts(m, "=== End Listening Table ===\n\n=== Start Socket Table ===\n");
    
    read_lock_irqsave(&hashtable_rwlock_t, hash_flags);
    hash_for_each(hashtable, counter, iter, hlist_node){
        if (true == socket_process_feature_arg1){
           seq_printf(m, "[%d] %s %s\n", iter->pid, iter->path, iter->arg1);
        } else {
           seq_printf(m, "[%d] %s\n", iter->pid, iter->path);
        }
    }
    read_unlock_irqrestore(&hashtable_rwlock_t, hash_flags);
    
    seq_puts(m, "=== End Socket Table ===\n");

    return 0;
}
EXPORT_SYMBOL(socket_process_lsm_seq_show);

// Set feature on or off. Enhance function later if more features are togglable
void socket_process_set_feature(int feature, bool state){
    if (0 == feature){
        socket_process_feature_arg1 = state;
    }
}
EXPORT_SYMBOL(socket_process_set_feature);

// Add *sock to the structure of created sockets with a string for its exe path
static int socket_process_lsm_security_socket_post_create(struct socket *sock, int family, int type, int protocol, int kern){
    /* type
    SOCK_DGRAM      = 1,
    SOCK_STREAM     = 2,
    SOCK_RAW        = 3,
    SOCK_RDM        = 4,
    SOCK_SEQPACKET  = 5,
    SOCK_DCCP       = 6,
    SOCK_PACKET     = 10, */
    // #define AF_UNSPEC       0
    // #define AF_UNIX         1       /* Unix domain sockets          */
    // #define AF_LOCAL        1       /* POSIX name for AF_UNIX       */
    // #define AF_INET         2       /* Internet IP Protocol         */
    // #define AF_AX25         3       /* Amateur Radio AX.25          */
    // #define AF_IPX          4       /* Novell IPX                   */
    // #define AF_APPLETALK    5       /* AppleTalk DDP                */
    // #define AF_NETROM       6       /* Amateur Radio NET/ROM        */
    // #define AF_BRIDGE       7       /* Multiprotocol bridge         */
    // #define AF_ATMPVC       8       /* ATM PVCs                     */
    // #define AF_X25          9       /* Reserved for X.25 project    */
    // #define AF_INET6        10      /* IP version 6                 */
    // #define AF_ROSE         11      /* Amateur Radio X.25 PLP       */
    // #define AF_DECnet       12      /* Reserved for DECnet project  */
    // #define AF_NETBEUI      13      /* Reserved for 802.2LLC project*/
    // #define AF_SECURITY     14      /* Security callback pseudo AF */
    // #define AF_KEY          15      /* PF_KEY key management API */
    // #define AF_NETLINK      16
    // #define AF_ROUTE        AF_NETLINK /* Alias to emulate 4.4BSD */
    // #define AF_PACKET       17      /* Packet family                */

    #if SOCKET_PROCESS_LSM_ONLY_IPV4 == 1
    if (AF_INET != family) {
        // printk(KERN_INFO "CREATE %s %d %d %d %d\n", family, type, protocol, kern);
        return 0;
    }
    #else
    if (AF_INET != family || AF_INET6 != family) {
        // printk(KERN_INFO "CREATE %s %d %d %d %d\n", family, type, protocol, kern);
        return 0;
    }
    #endif

    MACRO_SET_PATH(path_pointer, path, pid, arg1_pointer, arg1)

    socket_process_lsm_update_socket(sock, path_pointer, path, pid, arg1_pointer, arg1);

    #if SOCKET_PROCESS_LSM_DEBUG == 1
    printk(KERN_ERR "%s created socket %lu\n", path, sock);
    #endif
    return 0;
}

//add *sock to listening sockets list.
//accept may be used for TCP connetions, but bind is used for udp and tcp, so better to use
//need to look at accept's new socket for TCP stuff
static int socket_process_lsm_security_socket_bind(struct socket *sock, struct sockaddr *address, int addrlen){
    unsigned long hash_flags, ll_flags;
    struct entry_node* iter;
    bool contained_in_hashtable = false;
    MACRO_SET_PATH(path_pointer, path, pid, arg1_pointer, arg1)
    // CHECK THAT PATH IS ALLOCATED BEFORE PROCEEDING!!!

    // printk(KERN_ERR "family %d %d %d\n", address->sa_family, AF_INET, AF_INET6);
    if (AF_INET == address->sa_family) {
        struct listening_ll4 *next;
        char *get_path;
        if(!(next = kmalloc(sizeof(struct listening_ll4), GFP_ATOMIC))){
            printk(KERN_ERR "socket_process_lsm: ERROR allocating next for socket bind\n");
            if (path_pointer){
                kfree(path_pointer);
            }
            if (arg1_pointer){
                kfree(arg1_pointer);
            }
            return 0;
        }
        next->sin_port = ((struct sockaddr_in *)address)->sin_port; 
        next->s_addr = ((struct sockaddr_in *)address)->sin_addr.s_addr;
        next->sock = sock;
        
        if(!(get_path = socket_process_lsm_get_path(sock))){
            printk(KERN_ERR "socket_process_lsm: ERROR got NULL path in bind for address %d:%d comm %s\n", next->s_addr, next->sin_port, current->comm);
            if (path_pointer){
                kfree(path_pointer);
            }
            if (arg1_pointer){
                kfree(arg1_pointer);
            }
            return 0;
        } else {
            if(!(next->path = kstrndup(get_path, PATH_MAX, GFP_ATOMIC))){
                printk(KERN_ERR "socket_process_lsm: ERROR with kstrndup path for get_path. comm %s %s\n", current->comm, get_path);
                if (path_pointer){
                    kfree(path_pointer);
                }
                if (arg1_pointer){
                    kfree(arg1_pointer);
                }
                kfree(next);
                return 0;
            }
        }
        // else printk(KERN_ERR "socket_process_lsm: Failed to alloc next bind struct in ipv4 linked list\n");
        
        write_lock_irqsave(&listening_ll4_rwlock_t, ll_flags);
        list_add_tail(&(next->myList), &listening_ll4_head);
        write_unlock_irqrestore(&listening_ll4_rwlock_t, ll_flags);

        #if SOCKET_PROCESS_LSM_DEBUG == 1
        printk(KERN_INFO "socket_process_lsm: Bound to %d:%d\n", next->s_addr, next->sin_port);
        #endif
        
    } else if (AF_INET6 == address->sa_family) {
        return 0;
        // NEXT = kmalloc(sizeof(struct listening_ll6), GFP_ATOMIC)));
        //     NEXT->sin6_port = ((struct sockaddr_in6 *)address)->sin6_port;
        //     NEXT->sin6_addr = ((struct sockaddr_in6 *)address)->sin6_addr;
        //     NEXT->sock = sock; \
        // else printk(KERN_ERR "socket_process_lsm: Failed to alloc next bind struct in ipv6 linked list\n");
    } else {
        return 0;
    }

    // Search the hashtable for this socket pointer and set it to be listening.
    // If it is already there, the process binding must be the same that created it, right?
    // CHECK THAT QUESTION HERE
    read_lock_irqsave(&hashtable_rwlock_t, hash_flags);
    hash_for_each_possible(hashtable, iter, hlist_node, (u64)sock){
        if (sock == iter->sock){
            iter->listening = true;
            contained_in_hashtable = true;
            goto socket_process_lsm_security_socket_bind_break;
        }
    }
    socket_process_lsm_security_socket_bind_break:
    read_unlock_irqrestore(&hashtable_rwlock_t, hash_flags);

    // If it wasn't there, add a new iter to the table
    if (false == contained_in_hashtable){
        if(!(iter = kmalloc(sizeof(struct entry_node), GFP_ATOMIC))){
            // ADD A DELETE OF THE SOCK POINTER FROM THE LINKED LIST HERE?
            printk(KERN_ERR "socket_process_lsm: ERROR allocating iter for bound socket\n");
            if (path_pointer){
                kfree(path_pointer);
            }
            if (arg1_pointer){
                kfree(arg1_pointer);
            }
            return 0;
        }
        iter->path_pointer = path_pointer;
        iter->path = path;
        iter->pid  = pid;
        iter->arg1_pointer = arg1_pointer;
        iter->arg1 = arg1;
        iter->sock = sock;
        iter->listening = true;

        write_lock_irqsave(&hashtable_rwlock_t, hash_flags);
        hash_add(hashtable, &iter->hlist_node, (u64)sock);
        write_unlock_irqrestore(&hashtable_rwlock_t, hash_flags);
    }
    #if SOCKET_PROCESS_LSM_DEBUG == 1
    printk(KERN_ERR "%s %d bind socket %lu\n", path, pid, sock);
    #endif

    return 0;
}

// Add the newsock to the list of sockets and processes
static int socket_process_lsm_security_socket_accept(struct socket *sock, struct socket *newsock){
    MACRO_SET_PATH(path_pointer, path, pid, arg1_pointer, arg1)

    socket_process_lsm_update_socket(newsock, path_pointer, path, pid, arg1_pointer, arg1);

    #if SOCKET_PROCESS_LSM_DEBUG == 1
    printk(KERN_ERR "%s accpeted socket %lu to %lu\n", path, sock, newsock);
    #endif

    return 0;
}

// Locks hashtable, then potentially locks linked list
// Remove sock entry from hashtable and listening linked list (if it is there) 
static void socket_process_lsm_security_socket_release(struct socket *sock, struct inode *inode){
    struct entry_node* iter;
    struct hlist_node* hlist_node_tmp;
    unsigned long hash_flags;

    write_lock_irqsave(&hashtable_rwlock_t, hash_flags);
    hash_for_each_possible_safe(hashtable, iter, hlist_node_tmp, hlist_node, (u64)sock){
        if (sock == iter->sock){
            #if SOCKET_PROCESS_LSM_DEBUG == 1
            printk(KERN_INFO "%s releasing socket %lu\n", iter->path, sock);
            #endif

            // Remove the sock from the listening linked list
            if (iter->listening){
                struct listening_ll4 *entry, *listening_ll4_tmp;
                unsigned long ll_flags;

                write_lock_irqsave(&listening_ll4_rwlock_t, ll_flags);
                list_for_each_entry_safe(entry, listening_ll4_tmp, &listening_ll4_head, myList) {
                    if(sock == entry->sock){
                        if (entry->path){
                            kfree(entry->path);
                        }
                        list_del(&(entry->myList));
                        kfree(entry);
                        goto socket_process_lsm_security_socket_release_inner_break;
                    }
                }
                socket_process_lsm_security_socket_release_inner_break:
                write_unlock_irqrestore(&listening_ll4_rwlock_t, ll_flags);
            }

            if(iter->path_pointer){
                kfree(iter->path_pointer);
            }
            if(iter->arg1_pointer){
                kfree(iter->arg1_pointer);
            }
            hash_del(&iter->hlist_node);
            kfree(iter);
            goto socket_process_lsm_security_socket_release_break;
        }
    }
    #if SOCKET_PROCESS_LSM_DEBUG == 1
    printk(KERN_WARNING "socket_process_lsm: %s released socket %lu not in table.\n", current->comm, (u64)sock);
    #endif
    
    socket_process_lsm_security_socket_release_break:
    write_unlock_irqrestore(&hashtable_rwlock_t, hash_flags);
}

// int hugetlb_sysctl_handler(struct ctl_table *table, int write, void *buffer, size_t *length, loff_t *ppos){
//     return hugetlb_sysctl_handler_common(false, table, write, buffer, length, ppos);
// }

// static int hugetlb_sysctl_handler_common(bool obey_mempolicy,
//              struct ctl_table *table, int write,
//              void *buffer, size_t *length, loff_t *ppos)
// {
//     struct hstate *h = &default_hstate;
//     unsigned long tmp = h->max_huge_pages;
//     int ret;

//     if (!hugepages_supported())
//         return -EOPNOTSUPP;

//     ret = proc_hugetlb_doulongvec_minmax(table, write, buffer, length, ppos,
//                          &tmp);
//     if (ret)
//         goto out;

//     if (write)
//         ret = __nr_hugepages_store_common(obey_mempolicy, h,
//                           NUMA_NO_NODE, tmp, *length);
// out:
//     return ret;
// }  

// static struct ctl_table debug_table[] = {
// #ifdef CONFIG_SYSCTL_EXCEPTION_TRACE
//     {
//         .procname   = "exception-trace",
//         .data       = &show_unhandled_signals,
//         .maxlen     = sizeof(int),
//         .mode       = 0644,
//         .proc_handler   = proc_dointvec
//     },
// #endif
//     { }
// };
// DECLARE_SYSCTL_BASE(kernel, kern_table);
// DECLARE_SYSCTL_BASE(vm, vm_table);
// DECLARE_SYSCTL_BASE(debug, debug_table);
// DECLARE_SYSCTL_BASE(dev, dev_table);
// int __init sysctl_init_bases(void){
//     register_sysctl_base(kernel);
//     register_sysctl_base(vm);
//     register_sysctl_base(debug);
//     register_sysctl_base(dev);
//     return 0;
// }

static struct security_hook_list socket_process_lsm_hooks[] __lsm_ro_after_init = {
	LSM_HOOK_INIT(socket_post_create, socket_process_lsm_security_socket_post_create),
	LSM_HOOK_INIT(socket_bind, socket_process_lsm_security_socket_bind),
	LSM_HOOK_INIT(socket_accept, socket_process_lsm_security_socket_accept),
	LSM_HOOK_INIT(socket_release, socket_process_lsm_security_socket_release),
};
static __init int socket_process_lsm_init(void){
	printk(KERN_INFO "socket_process_lsm: Starting\n");
    
    INIT_LIST_HEAD(&listening_ll4_head);

//        default_noexec = !(VM_DATA_DEFAULT_FLAGS & VM_EXEC);
    security_add_hooks(socket_process_lsm_hooks, ARRAY_SIZE(socket_process_lsm_hooks), "socket_process");
// if (dummylsm_enforcing_boot) pr_debug("Starting in enforcing mode\n");
// else pr_debug("Starting in permissive mode\n");
    return 0;
}

int socket_process_enabled_boot __initdata = 1;
DEFINE_LSM(socket_process) = {
        .name = "socket_process",
//        .flags = LSM_FLAG_LEGACY_MAJOR | LSM_FLAG_EXCLUSIVE,
        .enabled = &socket_process_enabled_boot,
        .init = socket_process_lsm_init,
};