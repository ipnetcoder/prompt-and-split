#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/file.h>
#include <linux/cred.h>
#include <net/sock.h>
#include <net/inet_sock.h>

#include <linux/netfilter/x_tables.h>
#include <linux/netfilter/xt_proc.h>
#include <linux/fs.h>
#include <net/tcp.h>

extern char* socket_process_lsm_get_path(struct socket *sock);
extern pid_t socket_process_lsm_get_pid(struct socket *sock);
extern char* socket_process_lsm_get_arg1(struct socket *sock);
extern bool socket_process_lsm_destined(const struct sk_buff *skb, char const *path);

// Incoming packets don't have a process socket, but by tracking sockets that bind() 
// can match where the socket and its executable path the packet would go to. Maybe fails with lots of NAT?
#define XT_PROC_MATCH_DESTINED_SOCKET 1

static int proc_check(const struct xt_mtchk_param *par){
	struct xt_proc_match_info *info = par->matchinfo;

	if (info->match & ~XT_PROC_MASK){
		return -EINVAL;
	}

	// Only allow the common case where the userns of the writer matches the userns of the network namespace.
	if ((info->match & XT_PROC_MASK) && (current_user_ns() != par->net->user_ns)){
		return -EINVAL;
	}

	return 0;
}

static bool proc_mt(const struct sk_buff *skb, struct xt_action_param *par){

	//In order to get listening ips, ports, and process:
	// 1) kernelspace filp_open("/proc/net/tcp") always seems to fail
	// 2) Creating a struct seq_file needs a valid struct file inside it, so calling the seq functions is NOT EASY.
	//    These would iterate over the listening sks in tcp_ipv4.c, but the struct file not having an inode that I think points to /proc/net/tcp makes it fail
	//    I left the start of that failed attempt below.
	// struct seq_file * seq = kzalloc(sizeof(struct seq_file), GFP_ATOMIC);
	// seq->file = (struct file *)kzalloc(sizeof(struct file), GFP_ATOMIC);
	// loff_t *off = kzalloc(sizeof(loff_t), GFP_ATOMIC);
	// seq->private = (struct tcp_iter_state *st)kzalloc(sizeof(struct tcp_iter_state), GFP_ATOMIC);
	// void * a = tcp_seq_start(seq, off);
	// struct sock *b = (struct sock*)a;
	// if(a == SEQ_START_TOKEN) printk(KERN_INFO "First\n");
	// else printk(KERN_INFO "ino: %d\n", sock_i_ino(b));
	// a = tcp_seq_next(seq, a, off);
	// b = (struct sock*)a;
	// if(a != NULL) printk(KERN_INFO "ino: %d\n", sock_i_ino(b));

	char *path;
	char *arg1;
	struct sock *sk;

	const struct xt_proc_match_info *info = par->matchinfo;
	// printk(KERN_INFO "table: %s\n", par->table);
	// struct net *net = xt_net(par);

	// In the case of comm "swapper/0" there was skb == 0, kworker/0:1" had no socket
	// Need to investigate more, but it seems like these might die quickly and lose skbs?

	// In the case of kernel finding out about --no-path, but there being a path specified, 
	// check the list of listening ips and ports and if the destination, port, and process match, then return true

	// Immediately return whether the user specified --no-path and did not invert it.
	// If they did not specify --no-path then false is returned immediately
	if(!skb || !(sk = skb_to_full_sk(skb))){
		// printk(KERN_ERR "xt_proc: No skb for %s.\n", current->comm);
		// printk(KERN_ERR "xt_proc: No full sk for %s.\n", current->comm);

		if(info->match & XT_PROC_PATH){
			#if XT_PROC_MATCH_DESTINED_SOCKET == 1
			return !(socket_process_lsm_destined(skb, info->path) ^ !(info->invert & XT_PROC_PATH));
			#else
			return !(info->invert & XT_PROC_PATH);
			#endif
		}
		return (info->match & XT_PROC_NOPATH) && !(info->invert & XT_PROC_NOPATH);
	}
	// if(!(socket = sk->sk_socket)){
	// 	printk(KERN_ERR "xt_proc: No socket for %s.\n", current->comm);

	// 	if(info->match & XT_PROC_PATH)
	// 		#if XT_PROC_MATCH_DESTINED_SOCKET == 1
	// 		return !(nf_pdlog_destined(skb, info->path) ^ !(info->invert & XT_PROC_PATH));
	// 		#else
	// 		return !(info->invert & XT_PROC_PATH);
	// 		#endif
		
	// 	return (info->match & XT_PROC_NOPATH) && !(info->invert & XT_PROC_NOPATH);
	// }

	// if (!sk || !sk->sk_socket || !net_eq(net, sock_net(sk))) return (info->match ^ info->invert) == 0;
		 // Socket exists but user wanted ! --socket-exists.
	// else if (info->match & info->invert & XT_OWNER_SOCKET) return false;

	/* Check differnet operations on the path and no path option */
	if((path = socket_process_lsm_get_path(sk->sk_socket))){
		if (info->match & XT_PROC_PATH) {
			if ((0 == strncmp(path, info->path, PATH_MAX)) ^ !(info->invert & XT_PROC_PATH)){
				/* strcmp returns NULL if strs are same, otherwise return pointer where they differ
				 * if (paths match) is different than (not specifying inversion) return false  
				 */
				return false; 
			}

		} else if (info->match & XT_PROC_NOPATH){
			if (!(info->invert & XT_PROC_NOPATH)){
				//if user did NOT specify the inversion, then DON'T match since path exists
				return false;
			}
		}
		
	} else {
		if((info->match & XT_PROC_PATH) && !(info->invert & XT_PROC_PATH)){
		//no path && (user specified path or path-sub)       &&  (did not invert that requested path) return false;
			return false;
		}

		//check that user did "! --no-path" (any path exists), in which case this rule doesn't match since path doesn't exist
		if(info->match & info->invert & XT_PROC_NOPATH){
			return false;
		}
	}

	// TODO: clean up this check to not set arg1 first?
	if ((arg1 = socket_process_lsm_get_arg1(sk->sk_socket))){
		if (info->match & XT_PROC_ARG1) {
			if ((0 == strncmp(arg1, info->arg1, PATH_MAX)) ^ !(info->invert & XT_PROC_ARG1)){
				return false;
			}
		}
	} else if((info->match & XT_PROC_ARG1) && !(info->invert & XT_PROC_ARG1)){
		return false;
	}

	if (info->match & XT_PROC_PID) {
		if ((socket_process_lsm_get_pid(sk->sk_socket) == info->pid) ^ !(info->invert & XT_PROC_PID)){
			return false;
		}
	}

	return true;
}

static struct xt_match proc_mt_reg __read_mostly = {
	.name       = "proc",
	.revision   = 1,
	.family     = NFPROTO_UNSPEC,
	.checkentry = proc_check,
	.match      = proc_mt,
	.matchsize  = sizeof(struct xt_proc_match_info),
	.hooks      = (1 << NF_INET_PRE_ROUTING) |
                  (1 << NF_INET_POST_ROUTING) |
                  (1 << NF_INET_LOCAL_OUT) |
                  (1 << NF_INET_LOCAL_IN),
	.me         = THIS_MODULE,
};

static int __init proc_mt_init(void){
	int ret = xt_register_match(&proc_mt_reg);
	if (0 != ret){
		printk(KERN_ERR "xt_proc: ERROR %d registering matches\n", ret);
	} 
	return ret;
}

static void __exit proc_mt_exit(void){
	xt_unregister_match(&proc_mt_reg);
}

module_init(proc_mt_init);
module_exit(proc_mt_exit);
// MODULE_AUTHOR("Jan Engelhardt <jengelh@medozas.de>");
MODULE_DESCRIPTION("Xtables: struct socket process matching with a patched kernel");
MODULE_LICENSE("GPL");
MODULE_ALIAS("ipt_proc");
MODULE_ALIAS("ip6t_proc");
