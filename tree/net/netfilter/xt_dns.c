#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/file.h>
#include <linux/cred.h>
#include <net/sock.h>
#include <net/inet_sock.h>
#include <linux/hashtable.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/ip.h>
#include <net/sock.h>
#include <net/ip.h>
//#include <stdarg.h>
#include <linux/in.h>
#include <asm/bitops.h>
#include <linux/inet.h>
#include <linux/string.h>
#include <linux/hashtable.h>
#include <linux/rwlock_types.h>
#include <linux/netfilter/x_tables.h>
#include <linux/seq_file.h>
#include <linux/proc_fs.h>
#include <linux/mutex.h>

#include <net/socket_process.h>
#include <net/netfilter/nf_pdlog.h>
#include <linux/netfilter/xt_dns.h>

// Currently don't have full IPv6 support, so leave it compiled out for now
#define XT_DNS_USE_IP6 0
#define XT_DNS_DEBUG 0

// Struct for queue info
struct xt_dns_queue_struct {
	// One error byte is used instead of checking sizes and max values of uid and gid 
	bool error;
	uint16_t tid;
	pid_t pid;
	uid_t uid;
	gid_t gid;
	char path[PATH_MAX + 1];
};

// Array on stack. Previously used a kfifo and copied bytes to user. This is better for use with a seq file, which is needed from 5.11 -> 5.14 because open(/proc/) would BUG due to no llseek, but adding would only print the fifo until the nullbyte
#define XT_DNS_QUEUE_SIZE_4 10
struct xt_dns_queue_struct xt_dns_queue_4[XT_DNS_QUEUE_SIZE_4];
uint8_t xt_dns_queue_start_4 = 0;
static struct mutex xt_dns_queue_lock_4;

DEFINE_RWLOCK(xt_dns_hashtable_rwlock_t_4);
DEFINE_HASHTABLE(xt_dns_hashtable_4, 10); // support 2^10 = 1024 entries
struct xt_dns_entry_node4 {
	char *hostname;
	__be32 ip;
	struct hlist_node xt_dns_hlist_node_4;
};

#if XT_DNS_USE_IP6 == 1
#define XT_DNS_QUEUE_SIZE_6 10
struct xt_dns_queue_struct xt_dns_queue_6[XT_DNS_QUEUE_SIZE_6];
uint8_t xt_dns_queue_start_6 = 0;
static struct mutex xt_dns_queue_lock_6;

DEFINE_RWLOCK(xt_dns_hashtable_rwlock_t_6);
DEFINE_HASHTABLE(xt_dns_hashtable_6, 10);
struct xt_dns_entry_node6 {
    char *hostname;
    struct in6_addr ip;
    struct hlist_node xt_dns_hlist_node_6;
};
#endif

static bool ipv4_eq(const IPv4_TYPEDEF a, const IPv4_TYPEDEF b){ return a == b; }
static IPv4_TYPEDEF ipv4_hash(IPv4_TYPEDEF a){ return a; }

// Macro to match question so that the logic is same for ipv4 and ipv6
#define MACRO_MATCH_QUESTION(HASHTABLE, ENTRY_NODE, HLIST_NODE, IPADDRESS, HASH_FUNC, EQ_FUNC) \
struct ENTRY_NODE* iter; \
if(info->match & XT_DNS_HOSTNAME){ \
    /* Hash the IP to get the hostname/domain from the hashtable node. */ \
    /* Because 2 different hostnames can have the same IPs, need to loop over an array of hostnames for each IP and */ \
    /* find the domain/hostname that matches the one in the rule in order to check. If domain/hostname does not match, */ \
    /* then continue in the array until finding one that does or end with no match. */ \
    hash_for_each_possible(HASHTABLE, iter, HLIST_NODE, HASH_FUNC(IPADDRESS)){ \
        if (EQ_FUNC(IPADDRESS, iter->ip) && 0 == strncmp(iter->hostname, info->string, MAX_HOSTNAME_LEN)){ \
            return !(info->invert & XT_DNS_HOSTNAME); \
        } \
    } \
    return info->invert & XT_DNS_HOSTNAME; \
} else if (info->match & XT_DNS_DOMAIN) { \
	/* Same as above, but check info->string to be the right most section of the hostname, as the whole thing or with a dot before */ \
	/* Get length of each, then safely memcmp the right end of info->string */ \
	hash_for_each_possible(HASHTABLE, iter, HLIST_NODE, HASH_FUNC(IPADDRESS)){ \
        if (EQ_FUNC(IPADDRESS, iter->ip)){ \
        	size_t hostname_len = strnlen(iter->hostname, MAX_HOSTNAME_LEN); \
        	size_t domain_len = strnlen(info->string, MAX_HOSTNAME_LEN); \
        	if (hostname_len < domain_len) { \
        		/* Longer domain than hostname immediately fails */ \
	            return info->invert & XT_DNS_DOMAIN; \
        	} else if(hostname_len == domain_len){ \
        		/* Same length simply needs a string comparison */ \
        		if (0 == strncmp(iter->hostname, info->string, MAX_HOSTNAME_LEN)){ \
		            return !(info->invert & XT_DNS_DOMAIN); \
		        } \
			    return info->invert & XT_DNS_DOMAIN; \
        	} else { \
        		/* Check for a dot and that everything after that to the right mathces */ \
        		if ('.' == *(iter->hostname + (hostname_len - domain_len) - 1) && 0 == memcmp(iter->hostname + (hostname_len - domain_len), info->string, domain_len)){ \
		            return !(info->invert & XT_DNS_DOMAIN); \
		        } \
				return info->invert & XT_DNS_DOMAIN; \
        	} \
        } \
    } \
    return info->invert & XT_DNS_DOMAIN; \
}

// Look in the hashtable for hostname associated with supplied IP address.
// This function is meant to be used by other kernel modules to look at the global kernel DNS table.
char* xt_dns_hostname_from_ipv4(__be32 ip, struct net *net){
	struct xt_dns_entry_node4* iter;
	unsigned long flags;

	read_lock_irqsave(&xt_dns_hashtable_rwlock_t_4, flags);
	hash_for_each_possible(xt_dns_hashtable_4, iter, xt_dns_hlist_node_4, ip){
		if (ip == iter->ip){
			read_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_4, flags);
			return iter->hostname;
		}
	}
	read_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_4, flags);	
	return NULL;
}
EXPORT_SYMBOL(xt_dns_hostname_from_ipv4);

// #if XT_DNS_USE_IP6 == 1
// #include <linux/in6.h>
// #include <net/ipv6.h>
// // Convert 128 bits into 64 bits with XOR
// static uint64_t ipv6_hash(struct in6_addr *address){
//     return ((uint64_t)(address->s6_addr32[0] ^ address->s6_addr32[1]) << 32) | (address->s6_addr32[2] ^ address->s6_addr32[3]);
// }
// // Return 0 if the addresses are equal
// static bool ipv6_eq(const struct in6_addr *a, const struct in6_addr *b){
//     unsigned char i = 0;
//     for (; i < 4; ++i){
//         if (a->s6_addr32[i] != b->s6_addr32[i]) return false;//ntohl(a->s6_addr32[i]) < ntohl(b->s6_addr32[i]);
//     }
//     return true;
// }
char *xt_dns_hostname_from_ipv6(struct in6_addr *ip, struct net *net){
//     struct xt_dns_entry_node6* iter;
//     unsigned long flags;
//     read_lock_irqsave(&xt_dns_hashtable_rwlock_t_6, flags);
//     hash_for_each_possible(xt_dns_hashtable_6, iter, xt_dns_hlist_node_6, ipv6_hash(ip)){
//         if (ipv6_eq(ip, iter->ip)){
//             read_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_6, flags);
//             return iter->hostname;
//         }
//     }
//     read_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_6, flags);   
    return NULL;
}
// static bool xt_dns_mt6(const struct sk_buff *skb, struct xt_action_param *par){
//     const struct xt_dns_match_info *info = par->matchinfo;
    
//     struct ipv6hdr *ip6_header;

//     if (skb->protocol == htons(ETH_P_IPV6)) {
//         ipv6_header = ipv6_hdr(skb);
//     } else{
//         printk(KERN_WARNING "xt_dns: Encountered non IPv6 packet of protocol %d\n", skb->protocol);
//         return false;
//     }
//     // struct in6_addr addr = ipv6_header->daddr;
//     MACRO_MATCH_QUESTION(
//         xt_dns_hashtable_6, 
//         xt_dns_entry_node6, 
//         xt_dns_hlist_node_6,
//         (info->match & XT_DNS_DST ? ipv6_header->daddr : ip_headerv6->saddr), 
//         ipv6_hash, 
//         ipv6_eq
//     )
//     return false;
// }
// #endif
// static void xt_dns_PrintIPv4DNS(void){
// 	struct xt_dns_entry_node4* iter;
// 	unsigned int counter = 0;
// 	printk(KERN_INFO "DNS Info:\n"); 
// 	hash_for_each(xt_dns_hashtable_4, counter, iter, xt_dns_hlist_node_4) { 
// 		printk("Raw IP: %d\n", iter->ip);
// 		printk(KERN_INFO "%s %pI4\n", iter->hostname, &iter->ip);
// 	}
// }

static int xt_dns_check(const struct xt_mtchk_param *par){
	struct xt_dns_match_info *info = par->matchinfo;
	
	if (info->match && (current_user_ns() != par->net->user_ns)){
		printk(KERN_ERR "xt_dns: current_user_ns()\n");
        return -EINVAL;
	}

	return 0;
}

static bool xt_dns_mt(const struct sk_buff *skb, struct xt_action_param *par){
	const struct xt_dns_match_info *info = par->matchinfo;
	// const struct file *filp;
	// struct net *net = xt_net(par);
	// struct sock *sk = skb_to_full_sk(skb);
	// filp = sk->sk_socket->file;
	// if (filp == NULL) return ((info->match ^ info->invert) & (XT_OWNER_UID | XT_OWNER_GID)) == 0;
	// if (!sk || !sk->sk_socket || !net_eq(net, sock_net(sk))) return (info->match ^ info->invert) == 0;
    // Socket exists but user wanted ! --socket-exists.
	// else if (info->match & info->invert & XT_OWNER_SOCKET) return false;
	
	struct iphdr *ip_header;

	if (skb->protocol == htons(ETH_P_IP)) {
		ip_header = ip_hdr(skb);
	} else {
		printk(KERN_WARNING "xt_dns: Encountered non IP packet of protocol %d\n", skb->protocol);
		return false;
	}

	/* Passively watches queries and writes to /proc/dnsqueries so userspace can associte DNS queries with the asking process
	 * This option is first because if it is on, queries to the local DNS proxy are the first thing that ever happen meaning this will get hit the most.
	 * Make sure the iptables rule with --watch-queries has -p udp --dport 53 on it or this will get a lot of traffic
	 */
	if (info->match & XT_DNS_WATCH_QUERIES) {
		/*
        Sample code to perform hashing
		char *res = kzalloc(17, GFP_KERNEL);
        struct crypto_shash *shash = crypto_alloc_shash("md5", 0, 0);
        struct shash_desc *desc = kmalloc(sizeof(struct shash_desc) + crypto_shash_descsize(shash), GFP_ATOMIC);
        desc->tfm = shash;
        crypto_shash_init(desc);
		crypto_shash_update(desc, data, len);
		crypto_shash_final(desc, res);
		crypto_free_shash(desc->tfm);
		*/

		struct sock *sk;

		if (mutex_lock_interruptible(&xt_dns_queue_lock_4)){
			printk(KERN_ERR "xt_dns: Mutex failed to lock in xt_dns_mt\n");
	        return true;
	    }

		// Queue fills => match to likely DROP the dns query
		if (XT_DNS_QUEUE_SIZE_4 == xt_dns_queue_start_4){
            printk(KERN_WARNING "xt_dns: Queue_4 filled up\n");
			mutex_unlock(&xt_dns_queue_lock_4);
			return true;
		}

		// Copy in the path followed by a nullbyte, then the pid in ASCII followed by a nullbyte
		if(skb && (sk = skb_to_full_sk(skb)) && sk_fullsock(sk)){

			char *path;
        	read_lock_bh(&sk->sk_callback_lock);
			
			xt_dns_queue_4[xt_dns_queue_start_4].tid = ntohs(*((uint16_t* )(skb->data + (4 * ip_header->ihl) + UDP_HEADERLEN)));

            if ((path = socket_process_lsm_get_path(sk->sk_socket))){
            	// Dangerous assumption here that path is null terminated!
            	memcpy(xt_dns_queue_4[xt_dns_queue_start_4].path, path, strnlen(path, PATH_MAX) + 1);
            } else {
            	printk(KERN_WARNING "xt_dns: No path for socket %8ph, %s\n", sk, current->comm);
            	xt_dns_queue_4[xt_dns_queue_start_4].path[0] = 0;
            }

            // Check for error after releasing the sk_callback_lock since it is very unlikely
        	xt_dns_queue_4[xt_dns_queue_start_4].pid = socket_process_lsm_get_pid(sk->sk_socket);

	        if (sk->sk_socket && sk->sk_socket->file) {
                const struct cred *cred = sk->sk_socket->file->f_cred;
            	xt_dns_queue_4[xt_dns_queue_start_4].uid = from_kuid_munged(&init_user_ns, cred->fsuid);
            	xt_dns_queue_4[xt_dns_queue_start_4].gid = from_kgid_munged(&init_user_ns, cred->fsgid);

		        read_unlock_bh(&sk->sk_callback_lock);
	        } else {
		        read_unlock_bh(&sk->sk_callback_lock);

            	printk(KERN_WARNING "xt_dns: No file for socket %8ph, %s\n", sk, current->comm);
		        xt_dns_queue_4[xt_dns_queue_start_4].error = true;
	        }
	        
	        if (unlikely(0 == xt_dns_queue_4[xt_dns_queue_start_4].pid)){
	        	printk(KERN_WARNING "xt_dns: Error getting pid for socket %8ph, %s\n", sk, current->comm);
		        xt_dns_queue_4[xt_dns_queue_start_4].error = true;
	        }
            
        } else {
        	printk(KERN_WARNING "xt_dns: No skb for %s\n", current->comm);
	        xt_dns_queue_4[xt_dns_queue_start_4].error = true;
        }

        xt_dns_queue_start_4 += 1;

		mutex_unlock(&xt_dns_queue_lock_4);

		return false;
	}

    /* Matches outgoing queries against hostname/domain.
     * Not super useful since it requires kernel parsing of DNS, but code left for reference
     */
// 	#if USE_XT_DNS_QUERY == 1
// 	if (info->match & XT_DNS_QUERY) {
// 		uint16_t questions_count = ntohs(*(uint16_t* )(skb->data + DNS_PACKET_BYTES_BEFORE_QUESTION_COUNT));
// 		// uint16_t answers_count = ntohs(*(uint16_t* )(skb->data + DNS_PACKET_BYTES_BEFORE_ANSWER_COUNT));
// 		uint16_t question_type, question_class;
// 		char hostname_question[MAX_HOSTNAME_LEN];
// 		uint16_t hostname_length = 0; // total length into the hostname question so far. Later used to get past the hostname to the question type and class
// 		uint8_t label_length = *(skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION); //length of the next segment of the hostname. 1 byte in size.
// 		// Safety feature for non standard DNS or malicious DNS packets
// 		if(1 != questions_count){
// 			printk(KERN_ERR "xt_dns: Query packet does not have exactly 1 question. Not matching.\n");
// 			return false;
// 		}
// 		// Begin loop to get the hostname question
// // Need better checking here for out of bounds on the next label_length!!!
// 		while(0 != label_length && hostname_length < MAX_HOSTNAME_LEN){
// 			//copy the new section of query into the allocated buffer
// 			strncpy(hostname_question + hostname_length, skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION + hostname_length + 1, label_length);
// 			hostname_question[hostname_length + label_length] = '.'; //fill the place that is the length of the next domain section as a dot
// 			hostname_length += label_length + 1; //increase by the number of chars and 1 for the dot just read
// 			label_length = *(skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION + hostname_length); //set the length of the next section of the hostname query
// 		}
// 		// Safety feature for non standard DNS or malicious DNS packets
// 		if (hostname_length > MAX_HOSTNAME_LEN){
// 			printk(KERN_ALERT "xt_dns: Non-DNS or malformed DNS packet was read.\n");
// 			return false;
// 		}
// 		hostname_question[hostname_length - 1] = 0; //set nullbyte for safety
// 		hostname_length += sizeof(label_length); // increase the hostname_length by 1 for the terminating 0 byte at the end of the hostname question
// 		question_type = ntohs(*((uint16_t* )(skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION + hostname_length)));
// 		question_class = ntohs(*((uint16_t* )(skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION + hostname_length + sizeof(question_type))));
// 		if(question_type != DNS_QUERY_TYPE_A){
// 			printk(KERN_WARNING "xt_dns: Query packet does not have type A. Not matching.\n");
// 			return false;
// 		}
// 		if(question_class != DNS_QUERY_CLASS_INET){
// 			printk(KERN_WARNING "xt_dns: Query packet does not have class INET. Not matching.\n");
// 			return false;
// 		}
// 		// Start looking at the hostname or domain given by the user on the command line
// 		if(info->match & XT_DNS_HOSTNAME){
// 			if (0 == strncmp(hostname_question, info->string, MAX_HOSTNAME_LEN))
// 				return !(info->invert & XT_DNS_HOSTNAME);
// 			return info->invert & XT_DNS_HOSTNAME;
// 		} 
// 		return false; //something went wrong so rule does not match
// 	}
// 	#endif
	// --watch-responses is used to passively store the A record answers from DNS replies 
	// into a global hashtable for IP -> hostname.
	// if (info->match & XT_DNS_WATCH_RESPONSES) {
	// 	if (ip_header->protocol == IPPROTO_UDP) {
	// 		// struct udphdr *udp_h = (void *)(struct updhdr *)skb_transport_header(skb);
	// 		// if (udp_h->source == ntohs(DNS_SERVER_PORT)) {
	// 		uint16_t questions_count = ntohs(*(uint16_t* )(skb->data + DNS_PACKET_BYTES_BEFORE_QUESTION_COUNT));
	// 		uint16_t answers_count = ntohs(*(uint16_t* )(skb->data + DNS_PACKET_BYTES_BEFORE_ANSWER_COUNT));
	// 		// uint16_t authoratives_count = ntohs(*(uint16_t* )(skb->data + DNS_BEFORE_AUTHORATATIVES_COUNT));
	// 		// uint16_t additionals_count = ntohs(*(uint16_t* )(skb->data + DNS_BEFORE_ADDITIONALS_COUNT));
	// 		//len is the len of each domain in the hostname query
	// 		//at most 63 chars in a domain section, 2^8 size
	// 		uint8_t len = *(skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION);
	// 		//Is this answer the first A record in this packet? Then remove all previous entries in the hashtable for that hotname
	// 		bool first_valid_answer = true;
	// 		//Iterator for looping over answers. There are 2^16 possible answers based on the size of the length section, so uint32_t works as counter
	// 		uint32_t i = 0;	
	// 		//Write lock flags for the hashtable's irq save write lock
	// 		unsigned long dns_write_flags;
	// 		/*
	// 		 * The reason to copy the hostname query in a loop is that each section of a domain can be at max 63 chars,
	// 		 * meaning the byte where the length of the next section may fall in the range of acceptable ASCII
	// 		 * characters for a hostname. This means we cannot copy until nullbyte, then try to figure out what was
	// 		 * a valid char versus a length. 
	// 		 */
	// 		char *hostname_question = kmalloc(MAX_HOSTNAME_LEN + 1, GFP_ATOMIC);
	// 		//max be (253 ~= 2^8 bytes for 1 query)*(2^16 - 1 max number of type A answers) ~= 2^24 < 2^32 for unsigned int
	// 		uint32_t totallen = 0;				
	// 		uint64_t start_of_answers;		
	// 		if(len == 0){
	// 			printk(KERN_ERR "xt_dns: Len is 0!!!\n");
	// 			kfree(hostname_question);
	// 			return false;
	// 		}
	// 		if(questions_count > 1){
	// 			printk(KERN_ALERT "xt_dns: This module only supports 1 query per packet at the moment!\n");
	// 			kfree(hostname_question);
	// 			return false;
	// 		}
	// 		/* Loop over hostname query until getting the final len 0 byte, or hit MAX_HOSTNAME_LEN
	// 		 * Check for the totallen, starting at 0 before this, going over the max length of a hostname
	// 		 * because if any non-DNS packets are read, this while loop could be bad  
	// 		 */
	// 		while(len != 0 && totallen < MAX_HOSTNAME_LEN){
	// 			//copy the new section of query into the allocated buffer
	// 			strncpy(hostname_question + totallen, skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION + totallen + 1, len);
	// 			//fill the place that is the length of the next domain section as a dot
	// 			hostname_question[totallen + len] = '.';
	// 			//increase totallen by the chars and length/dot just read
	// 			totallen += len + 1;
	// 			//set what is the len of the next section of the hostname query
	// 			len = *(skb->data + DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION + totallen);
	// 		}
	// 		/* Similar to the reasoning for terminating if totallen goes over the max for a length of a hostname, 
	// 		 * check again here to be safe.
	// 		 */
	// 		if (totallen > MAX_HOSTNAME_LEN){
	// 			printk(KERN_ALERT "xt_dns: Non-DNS or malformed DNS packet was read.\n");
	// 			kfree(hostname_question);
	// 			return false;
	// 		}
	// 		/* Assuming all went well parsing the hostname question, set nullbyte for safety
	// 		 * and increase the totallen by 1 for the terminating 0 byte at the end of the hostname question
	// 		 */
	// 		hostname_question[totallen - 1] = 0;
	// 		totallen += 1;
	// 		#if XT_DNS_DEBUG == 1
	// 		printk(KERN_INFO "xt_dns Got %d answers for %s\n", answers_count, hostname_question);
	// 		#endif
	// 		//bytes for the start of the Answers section is a variable since question length was variable 
	// 		start_of_answers = DNS_PACKET_BYTES_BEFORE_QUERIES_SECTION + totallen + DNS_HEADER_BYTES_AFTER_QUERY_NAME;
	// 		//reset totallen here to use as counter for the next section: the Answers section
	// 		totallen = 0;
	// 		//First remove all previous entries of the same hostname as the queried one
	// 		//Then, add the hostname and answer ip to the array at the beginning.
	// 		//NOTE THAT MULTIPLE HOSTNAMES CAN GO TO 1 IP.
	// 		//ideally we need to access the mappings here from just the hostname and remove the old hostnames
	// 		//This takes linear time.
	// 		//Then in each packet processing we access in constant time with hashing based on IP
	// 		//In the case of multiple hostnames, we pick which one? That is a complex question, but probably should be for the 
	// 		//most recent query
	// 		/* Technically could lock inside the for loop, but the overhead of the lock and unlock each iteration could be more than locking here.
	// 		 * Because DNS queries are not frequent and this is meant to be run on the incoming answers from the internet, not a local cache,
	// 		 * performance based on lock placement in or out of a small loop isn't a convern.
	// 		 */
	// 		write_lock_irqsave(&xt_dns_hashtable_rwlock_t_4, dns_write_flags);
	// 		//look through for all answers that match and remove them. If thy dont exist, add it
	// 		for(; i < answers_count; i += 1){
	// 			if(DNS_QUERY_TYPE_A == ntohs(*((uint16_t* )(skb->data + start_of_answers + totallen + DNS_ANSWER_BYTES_BEFORE_TYPE)))){
	// 				struct xt_dns_entry_node4* iter;
	// 				/* For only the first A record, run through the hashtable and remove any entries that match
	// 				 * this hostname question, as an update. This is linear, and would need a double hashtable to not be.
	// 				 */
	// 				if(first_valid_answer){
	// 					struct hlist_node* tmp;
	// 					unsigned int counter;
	// 					char *hostname_to_free = NULL; //Same hostname for multiple IP answers is stored as one string. This char* avoids a double kfree
	// 					hash_for_each_safe(xt_dns_hashtable_4, counter, tmp, iter, xt_dns_hlist_node_4){
	// 						if(strcmp(iter->hostname, hostname_question) == 0){
	// 							#if XT_DNS_DEBUG == 1
	// 							printk(KERN_INFO "xt_dns: Removing %s\n", iter->hostname);
	// 							#endif
	// 							hostname_to_free = iter->hostname;
	// 							hash_del(&iter->xt_dns_hlist_node_4);
	// 							kfree(iter);
	// 						}
	// 					}
	// 					if (hostname_to_free)
	// 						kfree(hostname_to_free);
	// 					first_valid_answer = false;
	// 				}
	// 				#if XT_DNS_DEBUG == 1
	// 				printk(KERN_INFO "xt_dns: Adding %s\n", hostname_question);
	// 				#endif
	// 				//it would not be smart to remove any entries from this dns data because a prorgam could not 
	// 				//internally respect DNS TTL and keep connecting to a server through the lifetime of its connection.
	// 				//if the dns changes while a program is connecting to a server, then it gets flagged as making a new
	// 				//connection to a random ip (as it should)
	// 				/* Allocate a new bucket in the hashtable, based on the IP, for this hostname.
	// 				 * Each hostname is NOT a strcpy(), so in the loop above only one is freed.
	// 				 * This is accounted for this with char *hostname_to_free above.
	// 				 */
	// 				iter = (struct xt_dns_entry_node4*)kmalloc(sizeof(struct xt_dns_entry_node4), GFP_ATOMIC);
	// 				iter->hostname = hostname_question;
	// 				//might be faster to create a local ip variable above, since call iter->ip right below?
	// 				iter->ip = *((IPv4_TYPEDEF*)(skb->data + start_of_answers + totallen + DNS_ANSWER_BYTES_BEFORE_ANSWER));
	// 				hash_add(xt_dns_hashtable_4, &iter->xt_dns_hlist_node_4, iter->ip);
	// 			}
	// 			// Increase the counter totallen by the amount of bytes before the next section, I think
	// 			totallen += DNS_ANSWER_BYTES_BEFORE_ANSWER + ntohs(*((uint16_t* )(skb->data + start_of_answers + totallen + DNS_ANSWER_BYTES_BEFORE_LENGTH))); 
	// 		}
	// 		// If first_valid_answer is true, then never hit an A record, so never added hostname_question to the hashtable
	// 		if (first_valid_answer == true)
	// 			kfree(hostname_question);
	// 		write_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_4, dns_write_flags);
	// 	}
	// 	return false;
	// }
    MACRO_MATCH_QUESTION(
        xt_dns_hashtable_4, 
        xt_dns_entry_node4, 
        xt_dns_hlist_node_4,
        (info->match & XT_DNS_DST ? ip_header->daddr : ip_header->saddr),
        ipv4_hash, 
        ipv4_eq
    )
    
	return false;
}

static struct xt_match dns_mt_reg[] __read_mostly = {
	{
        .name       = "dns",
    	.revision   = 1,
    	.family     = NFPROTO_IPV4,
    	.checkentry = xt_dns_check,
    	.match      = xt_dns_mt,
    	.matchsize  = sizeof(struct xt_dns_match_info),
    	.hooks      = (1 << NF_INET_PRE_ROUTING) |
                      (1 << NF_INET_POST_ROUTING) |
                      (1 << NF_INET_LOCAL_OUT) |
                      (1 << NF_INET_LOCAL_IN),
    	.me         = THIS_MODULE,
    },
#if XT_DNS_USE_IP6 == 1
    {
        .name       = "dns",
        .revision   = 1,
        .family     = NFPROTO_IPV6,
        .checkentry = xt_dns_check,
        .match      = xt_dns_mt6,
        .matchsize  = sizeof(struct xt_dns_match_info),
        .hooks      = (1 << NF_INET_PRE_ROUTING) |
                      (1 << NF_INET_POST_ROUTING) |
                      (1 << NF_INET_LOCAL_OUT) |
                      (1 << NF_INET_LOCAL_IN),
        .me         = THIS_MODULE,
    }
#endif
};

static int xt_dns_show_dnsqueries(struct seq_file *m, void *v){
    
    uint8_t i = 0;

    if (mutex_lock_interruptible(&xt_dns_queue_lock_4)){
		printk(KERN_ERR "xt_dns: Mutex failed to lock in xt_dns_read\n");
        return -ERESTARTSYS;
    }
    for ( ; i < xt_dns_queue_start_4 ; i += 1){

		seq_printf(m, "%d\n", xt_dns_queue_4[i].error);
		seq_printf(m, "%d\n", xt_dns_queue_4[i].tid);
		seq_printf(m, "%d\n", xt_dns_queue_4[i].pid);
		seq_printf(m, "%d\n", xt_dns_queue_4[i].uid);
		seq_printf(m, "%d\n", xt_dns_queue_4[i].gid);
		seq_printf(m, "%s\n", xt_dns_queue_4[i].path);
		xt_dns_queue_4[i].error = false; // This is the only place this gets reset
    }

	xt_dns_queue_start_4 = 0;

	mutex_unlock(&xt_dns_queue_lock_4);
	return 0;
}

/* Userspace writes DNS answers here to update kernel table.
 * This prevents kernel having to parse DNS packets, although original is left commented out for reference
 */
ssize_t xt_dns_table_write(struct file *file, const char __user *ubuf, size_t count, loff_t *ppos){
	// Data must be in the form "my.hostname.com,192.168.001.001192.168.002.001"
    // Do not do rigorous sanitization, but it is decent
	struct xt_dns_entry_node4* iter;
	unsigned long dns_write_flags;
	char *string_original; // Pointer to the original allocated char *string to free later
    char *string;
    char *temp_ip; // Used to hold the ip address from string, since kstrtouint doesn't specify length
	int ubuf_counter;
	int hostname_length = 0;
	bool first_ip = true;

	if (count > 1024) {
		printk(KERN_WARNING "xt_dns: Wrote buffer > 1024 bytes to \"/proc/dnsadd\".\n");
		return count;
	}
	if (!(string = kmalloc(count + 1, GFP_ATOMIC))){
		printk(KERN_ERR "xt_dns: Error allocating string in xt_dns_table_write\n");
		return count;
	}
	if (copy_from_user(string, ubuf, count)) {
        printk(KERN_ERR "xt_dns: Error reading ubuf from \"/proc/dnsadd\"\n");
        kfree(string);
        return count;
    }
    string[count] = 0; // Nullbyte terminate just in case

    // Check if user wrote "flush", which clears the whole cache
    if (0 == strncmp(string, "flush", 5)){
		struct xt_dns_entry_node4* iter;
		struct hlist_node* tmp;
		unsigned int counter;

    	printk(KERN_INFO "xt_dns: Flushing IPv4 dnstable\n");

		write_lock_irqsave(&xt_dns_hashtable_rwlock_t_4, dns_write_flags);	
    	hash_for_each_safe(xt_dns_hashtable_4, counter, tmp, iter, xt_dns_hlist_node_4){
			hash_del(&iter->xt_dns_hlist_node_4);
			kfree(iter->hostname);
			kfree(iter);
		}
		write_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_4, dns_write_flags);
		
    	return count;
    }

	// Using string as the hostname is smart because otherwise for each hostname we would allocate 256 bytes (max length)
	// but probably most ubufs will be well below that, so we end up not wasting as much space saving hostnames
	string_original = string;

	while(',' != *string && *string && hostname_length++ < MAX_HOSTNAME_LEN + 1){
		string++; // Increment to the comma after hostname
	}

    // Jump to right after the comma, since this is done with (++string) below
	ubuf_counter = hostname_length + 1;

	// Next char is not comma || after comma is NULL || length isn't multiple of 10, meaning IP answers weren't entered properly
	if (',' != *string || 0 == *(++string) || 0 != strnlen(string, count - ubuf_counter) % 10){
		
		// UPDATE HERE to print the offending string
        // User did not give ip addresses in the right form or one was cut off
		if (MAX_HOSTNAME_LEN + 1 == hostname_length){
			printk(KERN_WARNING "xt_dns: Hostname written to \"/proc/dnsadd\" was too long.\n");
		} else {
			printk(KERN_WARNING "xt_dns: Didn't supply the proper format of DNS table entry to \"/proc/dnsadd\". %s\n", string_original);
		}

		kfree(string_original);
		return count;
	}

	*(string - 1) = 0; // Set the comma to null so using string as the hostname string is nullbyte terminated
	
	if (!(temp_ip = kmalloc(11, GFP_ATOMIC))){
        printk(KERN_ERR "xt_dns: Error allocating temp_ip.\n");
        kfree(string_original);
        return count;
    }

    // CHECK HERE FOR BETTER FREEING ORIGINAL_STRING
	temp_ip[10] = 0;
	write_lock_irqsave(&xt_dns_hashtable_rwlock_t_4, dns_write_flags);	
	while(*string){
		if (first_ip){
			struct hlist_node* tmp;
			unsigned int counter;
			hash_for_each_safe(xt_dns_hashtable_4, counter, tmp, iter, xt_dns_hlist_node_4){
				if(0 == strcmp(iter->hostname, string_original)){
					#if XT_DNS_DEBUG == 1
					printk(KERN_INFO "xt_dns: Removing %s\n", iter->hostname);
					#endif
					
					hash_del(&iter->xt_dns_hlist_node_4);
					kfree(iter->hostname);
					kfree(iter);
				}
			}
			first_ip = false;
		}

        // CHECK HERE FOR MALLOC FAIL
		iter = kmalloc(sizeof(struct xt_dns_entry_node4), GFP_ATOMIC);
		iter->hostname = kstrdup(string_original, GFP_ATOMIC);
		// Used to set all iter->hostname = string_original, but that makes a table flush slightly more complicated
		// because hostnames could popout in all orders and we must avoid a double free

		memcpy(temp_ip, string, 10);
		if (kstrtouint(temp_ip, 10, &iter->ip)) {
			printk(KERN_ERR "xt_dns: Error with kstrtouint_from_user\n");
			kfree(iter->hostname);
			kfree(iter);

            goto xt_dns_table_write_broke;
		}
		#if XT_DNS_DEBUG == 1
		printk(KERN_INFO "xt_dns: Adding %s %pI4\n", string_original, &iter->ip);
		#endif
		hash_add(xt_dns_hashtable_4, &iter->xt_dns_hlist_node_4, iter->ip);

		if((ubuf_counter += 10) >= count){
			break;
		}
		string += 10;
	}

	xt_dns_table_write_broke:

	write_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_4, dns_write_flags);
	kfree(string_original);
	kfree(temp_ip);
	return count;
}

static struct proc_dir_entry *xt_dns_proc_dnstable;
static struct proc_dir_entry *xt_dns_proc_dnsqueries;

static struct proc_dir_entry *xt_dns_proc_dnsadd; 
static const struct proc_ops xt_dns_proc_dnsadd_fops __ro_after_init = {
	.proc_write  = xt_dns_table_write,
	// This noop_llseek needs to be here or kernel BUGs on python f.write(). Same for read, but proc_create_single() handles it.
	.proc_lseek  = noop_llseek,
};

static int xt_dns_seq_show_dnstable(struct seq_file *m, void *v){
    struct xt_dns_entry_node4* iter;
    unsigned long hash_flags;
    unsigned int counter;

    seq_puts(m, "=====Start DNS Table=====\n");

    read_lock_irqsave(&xt_dns_hashtable_rwlock_t_4, hash_flags);
    hash_for_each(xt_dns_hashtable_4, counter, iter, xt_dns_hlist_node_4) { 
        seq_printf(m, "%s %pI4\n", iter->hostname, &iter->ip);
    }
    read_unlock_irqrestore(&xt_dns_hashtable_rwlock_t_4, hash_flags);

    seq_puts(m, "=====End DNS Table=====\n");

    return 0;
}

static int __init xt_dns_mt_init(void){
	int ret;
    printk(KERN_INFO "xt_dns: Loading.\n");

    // Displays dnstable in the kernel
    if (NULL == (xt_dns_proc_dnstable = proc_create_single("dnstable", 0444, NULL, xt_dns_seq_show_dnstable))) {
        printk(KERN_ERR "xt_dns: couldn't create /proc/dnstable\n");
        return -1;
    }

    // Where to supply data for the table
	if(NULL == (xt_dns_proc_dnsadd = proc_create("dnsadd", 0222, NULL, &xt_dns_proc_dnsadd_fops))) {
        printk(KERN_ERR "xt_dns: couldn't create /proc/dnsadd\n");
        proc_remove(xt_dns_proc_dnstable);
        return -1;
    }

    // Displays process, pid and tid
	if(NULL == (xt_dns_proc_dnsqueries = proc_create_single("dnsqueries", 0444, NULL, xt_dns_show_dnsqueries))) {
        printk(KERN_ERR "xt_dns: couldn't create /proc/dnsqueries\n");
        proc_remove(xt_dns_proc_dnstable);
        proc_remove(xt_dns_proc_dnsadd);
        return -1;
    }

	// printk(KERN_INFO "xt_dns: /proc/dnsadd and /proc/dnsqueries created succesfully\n");
   
    mutex_init(&xt_dns_queue_lock_4);
	hash_init(xt_dns_hashtable_4);
    
    #if XT_DNS_USE_IP6 == 1
    mutex_init(&xt_dns_queue_lock_6);
    hash_init(xt_dns_hashtable_6);
    #endif

	if((ret = xt_register_matches(dns_mt_reg, ARRAY_SIZE(dns_mt_reg)))){
		printk(KERN_ERR "xt_dns: Error %d registering matches\n", ret);

        proc_remove(xt_dns_proc_dnstable);
        proc_remove(xt_dns_proc_dnsadd);
        proc_remove(xt_dns_proc_dnsqueries);

	    mutex_destroy(&xt_dns_queue_lock_4);

        #if XT_DNS_USE_IP6 == 1
	    mutex_destroy(&xt_dns_queue_lock_6);
        #endif

		return ret;
	}
    printk(KERN_INFO "xt_dns: Loaded.\n");
    return 0;
}

static void __exit xt_dns_mt_exit(void)
{
    printk(KERN_INFO "xt_dns: Exitting.\n");

    proc_remove(xt_dns_proc_dnstable);
    proc_remove(xt_dns_proc_dnsadd);
    proc_remove(xt_dns_proc_dnsqueries);

    mutex_destroy(&xt_dns_queue_lock_4);

    #if XT_DNS_USE_IP6 == 1
    mutex_destroy(&xt_dns_queue_lock_6);
    #endif

	xt_unregister_matches(dns_mt_reg, ARRAY_SIZE(dns_mt_reg));
    printk(KERN_INFO "xt_dns: Exitted.\n");
}

module_init(xt_dns_mt_init);
module_exit(xt_dns_mt_exit);
MODULE_DESCRIPTION("Xtables: Creates a table and matches IP to commandline hostname");
MODULE_LICENSE("GPL");
MODULE_ALIAS("ipt_dns");
#if XT_DNS_USE_IP6 == 1
MODULE_ALIAS("ip6t_dns");
#endif