#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>

#include <linux/skbuff.h>
#include <net/protocol.h>
#include <net/netfilter/nf_log.h> //Called by nf_pdlog.h
#include <net/netfilter/nf_pdlog.h>
#include <linux/ip.h> //needed for ip_hdr(skb)
#include <linux/proc_fs.h>
// #include <linux/mutex.h>

// Needed for getting the process path from a struct socket
#include <net/socket_process.h>
// For the MAX_HOSTNAME_LEN
#include <linux/netfilter/xt_dns.h>


/* Internal logging interface, copied mostly from net/netfilter/nf_log.c, which relies on the real PDLOG target modules.
 * nf_log_ipv[46].c are mostly copied to nf_pdlog_ipv[46].c to register new loggers
 *
 * This file's function, nf_pdlog_buf_close(), will serve as the meeting place between the nf_pdlog_ipv[46].c loggers
 * Only nf_log_buf_close is rewritten and other nf_log_.* functions rely on the original net/netfilter/nf_log.c
 * net/netfilter/nf_pdlog_ipv[46].c are nearly idenitical to nf_log_ipv[46].c, except they call the pdlog close now.
 * An even lighter solution might rewrite nf_log_ipv[46].c to export some of their currently static functions, 
 * however this is more of a drop in files and compile solution.
 *
 * This file is a regular kernel module that registers a genetlink family and will send data to userspace as nf_pdlog_buf_close() is called.
 * The new logger macro type is defined in include/net/netfilter/nf_log.h as NF_LOG_TYPE_PDLOG
 */
// #define S_SIZE (1024 - (sizeof(unsigned int) + 1))

struct nf_pdlog_queue_struct {
    // Size comes from nf_log_buf
    char log[S_SIZE + 1];
    char path[PATH_MAX + 1];
    char hostname[MAX_HOSTNAME_LEN + 1];
    pid_t pid;
};

#define NF_PDLOG_QUEUE_SIZE 100
static struct nf_pdlog_queue_struct nf_pdlog_queue[NF_PDLOG_QUEUE_SIZE];
static struct mutex nf_pdlog_lock;
static uint8_t nf_pdlog_queue_start = 0;


static int nf_pdlog_show(struct seq_file *m, void *v){
    uint8_t i = 0;
    
    if (mutex_lock_interruptible(&nf_pdlog_lock)){
        printk(KERN_ERR "nf_pdlog: Mutex failed to lock in pdlog_read\n");
        return -ERESTARTSYS;
    }

    for ( ; i < nf_pdlog_queue_start ; i += 1){
        seq_printf(m, "%s\n", nf_pdlog_queue[i].log);
        seq_printf(m, "%s\n", nf_pdlog_queue[i].hostname);
        seq_printf(m, "%s\n", nf_pdlog_queue[i].path);
        seq_printf(m, "%d\n", nf_pdlog_queue[i].pid);
    }

    nf_pdlog_queue_start = 0;

    mutex_unlock(&nf_pdlog_lock);

    return 0;
}

// This is called by the ipv4 and ipv6 logger functions. Hostname is guarenteed to ull terminate in them?
void nf_pdlog_send_buf(struct nf_log_buf *m, const struct sk_buff *skb, const char* hostname, struct net *net) { 

	struct sock *sk;
    char *path;
    pid_t pid;

    m->buf[m->count] = 0;

    if((sk = skb_to_full_sk(skb))){
        path = socket_process_lsm_get_path(sk->sk_socket);
        pid = socket_process_lsm_get_pid(sk->sk_socket);
    } else {
        // printk(KERN_WARNING "nf_pdlog: %s skb_to_full_sk(skb) is null\n", current->comm);
        path = NULL;
        pid = 0;
    }

    // Level used to indicate group!
    //nf_log.c adds a KERN_SOH 0x01 char, then '0' + log-level to the m->buf before the prefix, so skip 2 chars ahead of log
    if(m->buf){
        if (mutex_lock_interruptible(&nf_pdlog_lock)){
            printk(KERN_ERR "nf_dlog: Mutex failed to lock in nf_pdlog_send_buf\n");
            return;
        }

        if (NF_PDLOG_QUEUE_SIZE == nf_pdlog_queue_start){
            printk(KERN_WARNING "nf_pdlog: Queue filled up\n");
            mutex_unlock(&nf_pdlog_lock);
            return;
        }

        // Add the log string, first byte of which is the --log-level value
        memcpy(nf_pdlog_queue[nf_pdlog_queue_start].log, (char*)m->buf + 1, strnlen((char*)m->buf + 1, S_SIZE) + 1);
       
        if(hostname){
            memcpy(nf_pdlog_queue[nf_pdlog_queue_start].hostname, hostname, strnlen(hostname, MAX_HOSTNAME_LEN) + 1);
        } else {
            nf_pdlog_queue[nf_pdlog_queue_start].hostname[0] = 0;
        }

        if(path){
            memcpy(nf_pdlog_queue[nf_pdlog_queue_start].path, path, strnlen(path, PATH_MAX) + 1);
        } else {
            nf_pdlog_queue[nf_pdlog_queue_start].path[0] = 0;
        }

        nf_pdlog_queue[nf_pdlog_queue_start].pid = pid;

        nf_pdlog_queue_start += 1;
        mutex_unlock(&nf_pdlog_lock);
    } else {
        printk(KERN_ERR "nf_pdlog: Got NULL m->buf for %s, %s, %d\n", hostname, path, pid);
    }
}
EXPORT_SYMBOL_GPL(nf_pdlog_send_buf);

static struct proc_dir_entry *pdlog_proc_entry; 

static int __init nf_pdlog_init(void){ 
    printk(KERN_INFO "nf_pdlog: Loading.\n");

    mutex_init(&nf_pdlog_lock);

    if(NULL == (pdlog_proc_entry = proc_create_single("pdlog", 0444, NULL, nf_pdlog_show))) {
        printk(KERN_ERR "nf_pdlog: Couldn't create proc entry\n");
        mutex_destroy(&nf_pdlog_lock);
        return -1;
    }

    printk(KERN_INFO "nf_pdlog: Loaded.\n");
    return 0;
}

static void __exit nf_pdlog_exit(void){
    printk(KERN_INFO "nf_pdlog: Exitting.\n");

    proc_remove(pdlog_proc_entry);
    mutex_destroy(&nf_pdlog_lock);

    printk(KERN_INFO "nf_pdlog: Exitted.\n");
}

module_init(nf_pdlog_init);
module_exit(nf_pdlog_exit);

MODULE_ALIAS("ipt_PDLOG");
MODULE_ALIAS("ip6t_PDLOG");
MODULE_LICENSE("GPL");