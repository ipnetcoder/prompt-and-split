/* 
 * This is an xtables target that uses in kernel to communicate packet log, process, and dns info to userspace.
 * This code is a duplicate of net/netfilter/xt_LOG.c except for some obvious
 * changes indicated by "Modification" and "End Modification" comments.
 * 
 * Because this is meant to function exactly as xt_LOG, excetp for the different logger type, NF_LOG_TYPE_PDLOG.
 * This calls the ipv[46] loggers defined in net/ipv[46]/netfilter/nf_pdlog_ipv[46].c
 * The type macro was added in include/net/netfilter/nf_log.h
 *
 * Note there is no xt_PDLOG.h, as the original xt_LOG.h is sufficient.
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/skbuff.h>
#include <linux/if_arp.h>
#include <linux/ip.h>
#include <net/ipv6.h>
#include <net/icmp.h>
#include <net/udp.h>
#include <net/tcp.h>
#include <net/route.h>

#include <linux/netfilter.h>
#include <linux/netfilter/x_tables.h>
#include <linux/netfilter/xt_LOG.h>
#include <linux/netfilter_ipv6/ip6_tables.h>
#include <net/socket_process.h>
#include <net/netfilter/nf_log.h>

//Modification
#define NF_LOG_TYPE_LOG NF_LOG_TYPE_PDLOG
//End Modification

static unsigned int
log_tg(struct sk_buff *skb, const struct xt_action_param *par)
{
	const struct xt_log_info *loginfo = par->targinfo;
	struct net *net = xt_net(par);
	struct nf_loginfo li;

	li.type = NF_LOG_TYPE_LOG;
	li.u.log.level = loginfo->level;
	li.u.log.logflags = loginfo->logflags;

	nf_log_packet(net, xt_family(par), xt_hooknum(par), skb, xt_in(par),
		      xt_out(par), &li, "%s", loginfo->prefix);
	return XT_CONTINUE;
}

static int log_tg_check(const struct xt_tgchk_param *par)
{
	const struct xt_log_info *loginfo = par->targinfo;

	if (par->family != NFPROTO_IPV4 && par->family != NFPROTO_IPV6)
		return -EINVAL;

	//Modification
	if (loginfo->level >= 256){
		pr_debug("level %u >= 256\n", loginfo->level);
	//End Modification
		return -EINVAL;
	}

	if (loginfo->prefix[sizeof(loginfo->prefix)-1] != '\0') {
		pr_debug("prefix is not null-terminated\n");
		return -EINVAL;
	}

	return nf_logger_find_get(par->family, NF_LOG_TYPE_LOG);
}

static void log_tg_destroy(const struct xt_tgdtor_param *par)
{
	nf_logger_put(par->family, NF_LOG_TYPE_LOG);
}

static struct xt_target log_tg_regs[] __read_mostly = {
	{
		//Modification
		.name		= "PDLOG",
		//End Modification
		.family		= NFPROTO_IPV4,
		.target		= log_tg,
		.targetsize	= sizeof(struct xt_log_info),
		.checkentry	= log_tg_check,
		.destroy	= log_tg_destroy,
		.me		= THIS_MODULE,
	},
#if IS_ENABLED(CONFIG_IP6_NF_IPTABLES)
	{
		//Modification
		.name		= "PDLOG",
		//End Modification
		.family		= NFPROTO_IPV6,
		.target		= log_tg,
		.targetsize	= sizeof(struct xt_log_info),
		.checkentry	= log_tg_check,
		.destroy	= log_tg_destroy,
		.me		= THIS_MODULE,
	},
#endif
};

static int __init log_tg_init(void)
{
	return xt_register_targets(log_tg_regs, ARRAY_SIZE(log_tg_regs));
}

static void __exit log_tg_exit(void)
{
	xt_unregister_targets(log_tg_regs, ARRAY_SIZE(log_tg_regs));
}

module_init(log_tg_init);
module_exit(log_tg_exit);

MODULE_LICENSE("GPL");
//Modification
MODULE_DESCRIPTION("Xtables: IPv4/IPv6 userspace packet logging with process and DNS info");
MODULE_ALIAS("ipt_PDLOG");
MODULE_ALIAS("ip6t_PDLOG");
//End Modification
