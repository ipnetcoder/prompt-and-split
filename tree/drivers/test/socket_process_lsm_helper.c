// Helper module for socket_process_lsm LSM to create a proc file to display information
// Also a proc file to toggle features on and off
#include <linux/module.h>
#include <linux/seq_file.h>
#include <linux/proc_fs.h>

#include <net/socket_process.h>

#if IS_ENABLED(CONFIG_SECURITY_SOCKET_PROCESS)
static struct proc_dir_entry *socket_process_lsm_helper_proc_debug;
static struct proc_dir_entry *socket_process_lsm_helper_proc_features;

static int socket_process_lsm_helper_seq_show(struct seq_file *m, void *v){
	return socket_process_lsm_seq_show(m, v);
}

ssize_t socket_process_lsm_helper_write(struct file *file, const char __user *ubuf, size_t count, loff_t *ppos){
	// Data must be in the form "feature=0" or "feature=1"
	
	char *string;

	if (0 == count){
		return 0;
	}

	if (count > 1024) {
		printk(KERN_WARNING "socket_process_lsm_helper: Wrote buffer > 1024 bytes to \"/proc/socket_process_features\".\n");
		return count;
	}
	if (!(string = kmalloc(count + 1, GFP_ATOMIC))){
		printk(KERN_ERR "socket_process_lsm_helper: Error allocating string in %s\n", __func__);
		return count;
	}
	if (copy_from_user(string, ubuf, count)) {
        printk(KERN_ERR "socket_process_lsm_helper: Error reading ubuf from \"/proc/socket_process_features\"\n");
        kfree(string);
        return count;
    }
    string[count] = 0; // Nullbyte terminate just in case

    // Update later to parse strings for multiple features
    if ('0' == string[0]){
    	socket_process_set_feature(0, false);
    } else if ('1' == string[0]) {
    	socket_process_set_feature(0, true);
    }

    return count;
}

static const struct proc_ops socket_process_lsm_helper_proc_features_fops __ro_after_init = {
	.proc_write  = socket_process_lsm_helper_write,
	// This noop_llseek needs to be here or kernel BUGs on python f.write(). Same for read, but proc_create_single() handles it.
	.proc_lseek  = noop_llseek,
};

#endif

static int __init socket_process_lsm_helper_mt_init(void){
	#if IS_ENABLED(CONFIG_SECURITY_SOCKET_PROCESS)
		if (NULL == (socket_process_lsm_helper_proc_debug = proc_create_single("socket_process_lsm_debug", 0400, NULL, socket_process_lsm_helper_seq_show))) {
	        printk(KERN_ERR "socket_process_lsm_helper: Failed to add proc debug entry");
	        return -1;
	    }
	    if (NULL == (socket_process_lsm_helper_proc_features = proc_create("socket_process_lsm_features", 0200, NULL, &socket_process_lsm_helper_proc_features_fops))) {
	        printk(KERN_ERR "socket_process_lsm_helper: Failed to add proc features entry");
			proc_remove(socket_process_lsm_helper_proc_debug);
	        return -1;
	    }
	    printk(KERN_INFO "socket_process_lsm_helper: Loaded.");
    #else
    	printk(KERN_INFO "socket_process_lsm_helper: Socket Process LSM is not enabled. No need for this modules");
    #endif

	return 0;
}

static void __exit socket_process_lsm_helper_mt_exit(void){
	#if IS_ENABLED(CONFIG_SECURITY_SOCKET_PROCESS)
		proc_remove(socket_process_lsm_helper_proc_debug);
		proc_remove(socket_process_lsm_helper_proc_features);		
	#endif
}

module_init(socket_process_lsm_helper_mt_init);
module_exit(socket_process_lsm_helper_mt_exit);

MODULE_DESCRIPTION("socket_process_lsm_helper: Creates proc entry to read data socket_process_lsm stores");
MODULE_LICENSE("GPL");