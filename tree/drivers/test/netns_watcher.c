#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/file.h>
#include <linux/cred.h>
#include <net/sock.h>
#include <net/inet_sock.h>
#include <linux/hashtable.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/ip.h>
#include <net/sock.h>
#include <net/ip.h>
#include <linux/in.h>
#include <asm/bitops.h>
#include <linux/inet.h>
#include <linux/string.h>
#include <linux/rwlock_types.h>
#include <linux/netfilter/x_tables.h>
#include <net/net_namespace.h>

static __net_init int netns_watcher_proc_init_net(struct net *net){
    printk(KERN_INFO "Creating netns %d\n", net->hash_mix);
    return 0;
}
static __net_exit void netns_watcher_proc_exit_net(struct net *net){
    printk(KERN_INFO "Destroying netns %d\n", net->hash_mix);
}

static __net_initdata struct pernet_operations netns_watcher_proc_ops = {
    .init = netns_watcher_proc_init_net,
    .exit = netns_watcher_proc_exit_net,
};

static int __init netns_watcher_mt_init(void){
	int ret;
	if ((ret = register_pernet_subsys(&netns_watcher_proc_ops))){
		printk(KERN_ERR "netns_watcher: Error %d registering pernet subsys\n", ret);
	}
	return ret;
}

static void __exit netns_watcher_mt_exit(void){
    unregister_pernet_subsys(&netns_watcher_proc_ops);
}

module_init(netns_watcher_mt_init);
module_exit(netns_watcher_mt_exit);
MODULE_LICENSE("GPL");
MODULE_ALIAS("netns_watcher");
