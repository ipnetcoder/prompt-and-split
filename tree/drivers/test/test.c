// All copied from fs/proc/base.c

#include <linux/uaccess.h>

#include <linux/errno.h>
#include <linux/time.h>
#include <linux/proc_fs.h>
#include <linux/stat.h>
#include <linux/task_io_accounting_ops.h>
#include <linux/init.h>
#include <linux/capability.h>
#include <linux/file.h>
#include <linux/fdtable.h>
#include <linux/generic-radix-tree.h>
#include <linux/string.h>
#include <linux/seq_file.h>
#include <linux/namei.h>
#include <linux/mnt_namespace.h>
#include <linux/mm.h>
#include <linux/swap.h>
#include <linux/rcupdate.h>
#include <linux/kallsyms.h>
#include <linux/stacktrace.h>
#include <linux/resource.h>
#include <linux/module.h>
#include <linux/mount.h>
#include <linux/security.h>
#include <linux/ptrace.h>
//#include <linux/tracehook.h>
#include <linux/printk.h>
#include <linux/cache.h>
#include <linux/cgroup.h>
#include <linux/cpuset.h>
#include <linux/audit.h>
#include <linux/poll.h>
#include <linux/nsproxy.h>
#include <linux/oom.h>
#include <linux/elf.h>
#include <linux/pid_namespace.h>
#include <linux/user_namespace.h>
#include <linux/fs_struct.h>
#include <linux/slab.h>
#include <linux/sched/autogroup.h>
#include <linux/sched/mm.h>
#include <linux/sched/coredump.h>
#include <linux/sched/debug.h>
#include <linux/sched/stat.h>
#include <linux/posix-timers.h>
#include <linux/time_namespace.h>
#include <linux/resctrl.h>
#include <linux/cn_proc.h>
#include <trace/events/oom.h>
//#include "internal.h"
//#include "fd.h"

/*
 * Decrement the use count and release all resources for an mm.
 */
// void mmput(struct mm_struct *mm)
// {
// 	might_sleep();

// 	if (atomic_dec_and_test(&mm->mm_users))
// 		__mmput(mm);
// }
// EXPORT_SYMBOL_GPL(mmput);

/**
 * access_remote_vm - access another process' address space
 * @mm:		the mm_struct of the target address space
 * @addr:	start address to access
 * @buf:	source or destination buffer
 * @len:	number of bytes to transfer
 * @gup_flags:	flags modifying lookup behaviour
 *
 * The caller must hold a reference on @mm.
 */
// int access_remote_vm(struct mm_struct *mm, unsigned long addr,
// 		void *buf, int len, unsigned int gup_flags)
// {
// 	return __access_remote_vm(mm, addr, buf, len, gup_flags);
// }

// extern int access_remote_vm(struct mm_struct *mm, unsigned long addr, void *buf, int len, unsigned int gup_flags);

// static ssize_t test_get_mm_proctitle(struct mm_struct *mm, char *buf, size_t count, unsigned long pos, unsigned long arg_start){
// 	char *page;
// 	int ret, got;

// 	if (pos >= PAGE_SIZE){
//         printk(KERN_INFO "Bad 0\n");
// 		return 0;
//     }

// 	page = (char *)__get_free_page(GFP_KERNEL);
// 	if (!page){
//         printk(KERN_INFO "Bad -1\n");
// 		return -ENOMEM;
//     }

// 	ret = 0;
// 	got = access_remote_vm(mm, arg_start, page, PAGE_SIZE, FOLL_ANON);
// 	if (got > 0) {
// 		int len = strnlen(page, got);

// 		/* Include the NUL character if it was found */
// 		if (len < got)
// 			len++;

// 		if (len > pos) {
// 			len -= pos;
// 			if (len > count)
// 				len = count;
// 			len -= copy_to_user(buf, page+pos, len);
// 			if (!len)
// 				len = -EFAULT;
// 			ret = len;
// 		}
// 	}
// 	free_page((unsigned long)page);
// 	return ret;
// }

// static ssize_t test_get_mm_cmdline(struct mm_struct *mm, char *buf, size_t count, loff_t *ppos){
//     unsigned long arg_start, arg_end, env_start, env_end;
//     unsigned long pos, len;
//     char *page, c;

//     /* Check if process spawned far enough to have cmdline. */
//     if (!mm->env_end){
//         printk(KERN_INFO "Bad 1\n");
//         return 0;
//     }

//     spin_lock(&mm->arg_lock);
//     arg_start = mm->arg_start;
//     arg_end = mm->arg_end;
//     env_start = mm->env_start;
//     env_end = mm->env_end;
//     spin_unlock(&mm->arg_lock);

//     if (arg_start >= arg_end){
//         printk(KERN_INFO "Bad 2\n");
//         return 0;
//     }

//     /*
//      * We allow setproctitle() to overwrite the argument
//      * strings, and overflow past the original end. But
//      * only when it overflows into the environment area.
//      */
//     if (env_start != arg_end || env_end < env_start)
//         env_start = env_end = arg_end;
//     len = env_end - arg_start;

//     /* We're not going to care if "*ppos" has high bits set */
//     pos = *ppos;
//     if (pos >= len){
//         printk(KERN_INFO "Bad 3\n");
//         return 0;
//     }
//     if (count > len - pos)
//         count = len - pos;
//     if (!count){
//         printk(KERN_INFO "Bad 4\n");
//         return 0;
//     }

//      /* Magical special case: if the argv[] end byte is not
//      * zero, the user has overwritten it with setproctitle(3).
//      *
//      * Possible future enhancement: do this only once when
//      * pos is 0, and set a flag in the 'struct file'.
//      */
//     if (access_remote_vm(mm, arg_end-1, &c, 1, FOLL_ANON) == 1 && c){
//         printk(KERN_INFO "Bad 5\n");
//         return test_get_mm_proctitle(mm, buf, count, pos, arg_start);
//     }

//     /*
//      * For the non-setproctitle() case we limit things strictly
//      * to the [arg_start, arg_end[ range.
//      */
//     pos += arg_start;
//     if (pos < arg_start || pos >= arg_end){
//         printk(KERN_INFO "Bad 6\n");
//         return 0;
//     }
//     if (count > arg_end - pos)
//         count = arg_end - pos;

//     page = (char *)__get_free_page(GFP_KERNEL);
//     if (!page){
//         printk(KERN_INFO "Bad 7\n");
//         return -ENOMEM;
//     }

//     len = 0;
//     while (count) {
//         int got;
//         size_t size = min_t(size_t, PAGE_SIZE, count);

//         got = access_remote_vm(mm, pos, page, size, FOLL_ANON);
//         printk(KERN_INFO "%s|%s| %d %d %d %d %d\n", page, page+9, *(page+8), mm->arg_end - mm->arg_start, got, size, count);
//         if (got <= 0)
//             break;
//         memcpy(buf, page, got);
//         pos += got;
//         buf += got;
//         len += got;
//         count -= got;
//     }

//     free_page((unsigned long)page);
//     return len;
// }

// void print_proc(void) {
// 	size_t count = 100;
// 	loff_t ppos = 0;
// 	char *b = kzalloc(count, GFP_ATOMIC);
//     bool all_zero = true;
//     int i = 0;
// 	struct mm_struct *mm;
//     ssize_t ret;
//     if (!(mm = get_task_mm(current))){
//         printk(KERN_INFO "mm 0\n");
//         return;
//     }
//     ret = test_get_mm_cmdline(mm, b, count, &ppos);
//     printk(KERN_INFO "ret: %d\n", ret);

//     mmput(mm);
//     for (; i < count ; i += 1){
//         if ( 0 != *(b+i)) all_zero = false;
//     }
//     printk(KERN_INFO "proc %s %d\n", current->comm, all_zero);
//     if (false == all_zero)
//         printk(KERN_INFO "%s %s\n", b, b + strnlen(b,ret)+1);

//     kfree(b);
// }

static int xt_dns_seq_show_dnstable(struct seq_file *m, void *v){
    seq_puts(m, "Hello\n");
    // print_proc();
    return 0;
}

static struct proc_dir_entry *xt_dns_proc_dnstable;

static int __init test_mt_init(void){
    // print_proc();
    xt_dns_proc_dnstable = proc_create_single("test", 0444, NULL, xt_dns_seq_show_dnstable);

	return 0;
}

static void __exit test_mt_exit(void){
    proc_remove(xt_dns_proc_dnstable);
}

module_init(test_mt_init);
module_exit(test_mt_exit);

MODULE_LICENSE("GPL");
