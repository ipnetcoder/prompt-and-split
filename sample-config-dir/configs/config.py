# Turn off if you run a server on 0.0.0.0:443, but want traffic from PREROUTING which is routed to Tor to work properly with the rules.
USE_LISTENING_PROCESS_GUESS_FOR_IPTABLES = True

DNS_REMOTE = ("127.0.0.1", 9053)

# When user adds a allow DNS lookup rule, add IP accept rule(s) with these protocols and ports:
DNS_YES_IMPLIES_IP_YES_PROTO_PORTS = [("tcp",443),("tcp",80),("udp",443)]
# Make list empty if you don't want this (will get a separate prompt when process tried to send a packet to that qname)
#DNS_YES_IMPLIES_IP_YES_PROTO_PORTS = []

# NEEDS TO BE UPDATED:
# Similar to above, delete a -p tcp --dport 443 for the deleted DNS rule
#DNS_DEL_IMPLIES_IP_DEL = True
# Similar, delete an iptables rule that matches the DNS_YES_IMPLIES_IP_YES stuff deletes the dns rule too. Not implemented yet?
# This should also be the case for the dns custom routing, but will require more work to do
#IP_DEL_IMPLIES_DNS_DEL = True

# File that acts like /etc/hosts for group string and group number
GROUP_NUMBERS_CONFIG_FILE = "/run/prompt-and-split/groups.txt"

PACKET_PROMPTER_LOG_FILE = "/var/log/prompt-and-split/prompter.log"

DNS_CACHE_TTL = 300
DNS_LOG_FILE = "/var/log/prompt-and-split/prompter_dns.log"
DNS_FOREVER_FILE = "/etc/prompt-and-split/rules/dns.txt"
IP_FOREVER_FILE = "/etc/prompt-and-split/rules/ip.txt"

# Seconds between each proc read
PROC_READ_SLEEP = 1
# Check the listening sockets at PROC_READ_SLEEP time this. counter not yet added
PROC_WRITE_MULTIPLIER = 1

# tid to process mappings are no longer relevant after 10 seconds. This will stop any funny business from filling up map forever
# Remeber that this is networking and something may never come, be corrupted, be late, or fail to follow up
TID_EXPIRE_SECONDS = 10

# Size of the buffer to read from kernel
BUF_SIZE = 65536

# Length of the chain name used in iptables, seen with iptables-save or ipables -vnL
IPTABLES_CHAIN_MAX = 11 #28

# These are hard coded in the nf_pdlog.c file
PROC_PATH = "/proc/pdlog"

DNS_TABLE_PATH = "/proc/dnsadd"

# File to read to get updates for mapping DNS query packets' transaction IDs to process path.
# File format is 2 bytes for transaction id, then the process path in ASCII followed by a nullbyte. Repeat.
TID_PROC_FILE = "/proc/dnsqueries"
