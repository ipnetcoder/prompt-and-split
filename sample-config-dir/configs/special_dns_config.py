# Sort by process, then uid, then list of dicts with 'qname' regex and 'addr' to that query send to

# Route certain program and user's DNS queries to an IP address different than the system default
# TODO: Need to make this changeable from an outside script call
DNS_SPECIAL_ROUTING_FILE = "/run/prompt-and-split/dns_ip"
DNS_SPECIAL_ROUTING_IP = ""
with open(DNS_SPECIAL_ROUTING_FILE, "rt") as f:
	DNS_SPECIAL_ROUTING_IP = f.read().strip()

# Processes that don't create IP rules for dns answer. These only query and don't need packet rules
DNS_DONT_CREATE_IP_RULE = ("/usr/bin/dig", "/usr/bin/nslookup", "/usr/bin/drill", "/usr/bin/ping")

# Route DN based on process, uid, qname, or source to a final "addr"
DNS_SPECIAL_ROUTING = {
	# Allow pacman (uid 0) connecting to any site to query VPN, not Tor.
	"/usr/bin/pacman": {
		0: [{"qname": ".*", "source": "localhost", "addr": (DNS_SPECIAL_ROUTING_IP, 53)}, ],
	},
	# curl (uid 0) checking IP not over Tor shoul query the VPN
	"/usr/bin/curl": {
		0: [{"qname": "^somesite$", "source": "localhost", "addr": (DNS_SPECIAL_ROUTING_IP, 53)}, ],
	},
	# VMs querying from interface br-$GROUP
	# see special_dns_config_groups_to_funcs() for mapping source to the configured @ group
	"(no path)": {
		-1: [{"qname": ".*", "source": "@vm", "addr": ("*-DNS", 1053)}, ],
	},
}

# Similar to above, but checked separately
# Normally a program in group 0 asks 127.0.0.1:53 (this script) directly for DNS info, but for different groups need this:
DNS_YES_IMPLIES_IP_YES_CUSTOM = {
	"(no path)": {
		"path": "(no path)",
		"direction": "in",
		# "*" is filled in based on iif minus the "br-" prefix
		"group": "*",
		"proto_ports": [("tcp",443), ("tcp", 80)],
	},
}
