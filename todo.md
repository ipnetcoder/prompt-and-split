#### To Do Items (no order):
- when a process queries for a chained hostname, make the IP prompt resolve the IP to use the queried one!
- make build script run in the background on the server, in a screen or tmux or something
- better support for changing python binary since /usr/bin/pythonX.Y is the real name caught in PDLOG rules
- add sharing not encrypted and already existing host directories ith SMB to a VM
- make startVM read config file in one go rather than calling a function for each config string key
- Add local dns cache for non prompting --namespaces
- add level of preference for LANInterface, pick eth0 first if exist, then wlan0 if exists and wpa_cli or iwd is running
- pass key strokes to qemu programmatically to make runsetup root passwd automatic
- make the PACKAGES_REMOVE variable dynamic based on what is on the current host system and what is in pkgIgnore (so doesn't have the latest version)
- add sgn and vm scripts to startvpn.sh or git repo, also make notification for sgn unified
- make vm shutdown govorned by the startvpn.sh lock because sometimes the sleeper will break when startvpn.sh is called elsewhere (/etc/hosts io)
- fix VM first startup autostart firefox profile not loading
- change ssh key given to VM to be a unique one for each (no leaks) and set ssh config accordingly
- consolidate startvpn.sh code for DNS in netNS when starting --namespace and --group
- kernel cmdline parsing
- make a dummy no internet namespace/group and allow option for required_group to go to that even if it is set
- Fix all exit 1 to be return to not lose root shell on error
- Make --vm work with root, but su user
- Disconnect from wifi and rules allows packets out
- Check journal for "read UDPv4 [EHOSTUNREACH]: No route to host (code=113)" to stop VPN
- Fix the ${namespace}_route_via to use dynamic IPs, not 10.0.0.1/30, for better chaining
- Make option to prevent stopping the namespace that is _route_via while the other is up
- --lxc and --vm no internet option, except for local pacman, smb, etc?
- Set HostRulesForGroup() to remove specific rules to IP
- LXC temp disk needs to delete itself
- Implement _prompting option for namespaces
- Fix iptables to read cmdline arg1 for processes
- Process execution directory: python can connect to pypi.org if in the ~/ directory only
- Finish code for running VM to "reattach" to group when it goes back up
- Fully implement groups in DNS
- Check for hostname reuse in VMs as well as in file names
- Option to embed VPN regex into vpn file so restart of the service may change the IP
- Parse an AUTH_FAIL from openvpn, or any failure to start
	- Also tcpdump snoop for IP failed to route or bad port
- Convert openvpn logs from syslog to a file (for faster parsing?)
- Convert all VMs from X to Wayland
- Kernel:
	- Turn off syslog netns restrictions for PDLOG only
	- -m proc hash option:
		- Hash at first hit, or commandline provided
	- Fix incoming (on a bridge interface) packet to a listening port on host being assumed for that host process
		- Kernel shouldn't have job of picking out local IP destinations, so userspace prompter.py should
- Add the tutorial for LUKS1 /boot and LUKS2 / elsewhere

- Create the script code for a Tor group, sent to a port on localhost
- Fix traped EXIT and return values
- Add multiple mount places so that config files can be loaded (like for the DE)
- Convert all sed delimiters from slash to remove picket fencing
- Add vnc option to VM configs and remove default setup from setup.sh 
- Create a namespace category for storing ip rules, rather than just group
	- Things with no source and != default group are loaded with namespace?
- When create namespace without persist, disregard OpenVPN stuff
- Fix problem with namespace_persist generating the nsip when it sees it existing on system already
	- persist start at the very beginning 
- Remove pid from being a dictionary key in dns rules since I so rarely use it?
- Check for programs running in a namespace before letting it stop
- Add vm/lxc name and group to the running file
	- Remove vm/lxc name and group from the running file somehow. Polling for activity?
- Create one function to check that group/namespace/etc is up/down
- Fix the kmalloc erroring in socket_process_lsm_set_pointers
- bash function to set key value pairs
- what is rcu_read_lock() in nf_log.c?
- Update method of getting and setting LANIP and LANGateway
- Fix --lxc base to update the base properly
- Fix --lxc config option disk = temp to clone base to /tmp/

- Hide IP rules created from the DNS_YES_IMPLIES to make it cleaner
- Make it so the LANIP could change and iptables rules are updated for it or don't use in the first place
- SELinux or AppArmor QEMU, or run it as a different user than default user (view w/ SPICE) in small chance of a VM escape
- Convert all dnsmasq over to systemd services to use dynamic users and better permissions controls
- Make ov_interface randomly generated
- convert all the pdlog and xt_dns studd over to per namespace so that it will work in --namespace

- Something to clean up dead QEMU tap interfaces
- Convert prompter.py into having zenity prompts so they popup and focus over other apps

- Add some NAT for privacy on all bridges. Always use 192.168.1.1/24 somehow externally?
- Does stopping tun101 try to delete tun101?
- del_ip_rule also deletes DNS rule for simple and custom cases
- DNS until restart corresponds to IP until restart

- Add profile to DNS and IP rules based on what WiFi network is connected to
- Add a smarter VM option to take a just created VM and save a "snapshot" of it for fast new creations
- Should PDLOG be a terminating target (to allow dropping)?
- Create startstopgroup rule to prevent igmp (from dnsmasq)?

- Should (no listening process) for incoming be listed as (no path) rather than any?
- Fix a rule that is 'modified' in the ip editor being the same cannot be added for Existing, but then is removed

- If a rule is added and for some reason it already exists, but the process chain has 0 references, make sure to add it to the --path chain

- If create/accept a DNS rule that is exists, but disabled, simply enable it
- Fix and standardize all the comment line regexes
- Turn all $() into using a global variable (like registers) similar to config[] to do better error handling!
- Convert newVar regex calls into a standard global variables
	- Similarly globalize br-$group name for bridge
- Add protection rule for local IPs don't get routed over tun0
- Fix all the error checking in startvpn.sh functions
- Make --vm option see what VMs are already running
- Pull all Tor IPs from cache file to add to ipset for TBB VMs
- IPv6 support... not inclined to do this until ipv4 is feature complete
- Fix openvpn running as root in namespace to use systemd namespacing
- socket_process_lsm way to print the socket table stored in the LSM
- Add dns option to --hostapd-* so client doesnt set its own server or similar
- Fix prompter.py to always accept input, not just on prompt pop ups. Perhaps separate the prompt from the input?
- Add gid support to DNS rules. although this seems to be gotten wrongly from the kernel, or groups are random?
- Make some simple switch condition for rules based on string like connected WiFi network or some user specified
- Fix remote-add.sh run as root part to a simpler root command
- Convert build server builds to run in tmux, then display output when done
	- Add new modules and LSM prompts to add to .config automatically to allow no interaction with build server
- prompter.py DNS parser needs to account for CNAMEs better
	- Add option to all --tap-* for what DNS to use (e.g. for VPN use VPN's or start some specific program at a port)
	- Add option to create a vpn tun based on exit location
- Add option to toggle fast but possibly leaky hosts file reading and responding
- Add timing/cache management to kernel DNS cache
- Improve QEMU performance options
- Make iptables.routes option read IP from a file with conditions. So get IP from 'remote' section of an OpenVPN config
- Create better dynamic rules
	- Depending on time or place I can create rules for a VM and specify namespace at start, rather than static definition at the start
- Add a network log to see when NEW connections are made and to where
- Make 'DNS chaining' feature, so DNS are allowed in a chain
	- If something calls some unique domain first, then a CDN immediately after, the CDN is allowed automatically, but only after the first request
- Similar to DNS chaining, if DNS query is allowed, then immediately a request for that hostname and the rule is not the DNS_IMPLIES_... rule, remove the DNS_IMPLIES_... rule
	- Example is ping a hostname, don't want to then have a -p tcp --dport 443 rule AND a icmp rule
- Add SNAT option to createRoutedNetNS()
- hostname set support in kernel
- Separate DNS tables in kernel based on group or something so VMs can use different tables than system
- Create a completely separate WiFi virtual NIC to connect clean VMs to (with separate MAC so the router thinks different device)?

#### Completed from above:
x Create rotating Exit IP from a list
x Add tap-tun option for routing to a generic tun
x Add more group support to kernel because running out in userspace
x Make enable/disable for packet rules, similar to the DNS rules
x Update tap groups to create a bridge and multiple TAPs for multiple VMs
x Update ipExists to check for larger prefixes, like don't create a /24 in a /16
x Commands to run when a --namespace is created
x Change Openvpn systemd service to require the ss-local service. This causes no leaks right???
x paths on chain pdlog-wlan0-in are being reported as path any by print_packet_rules. Update GROUP_REGEX and all similar things
x Add --print-rules-on-disk option to prompter.py to display forever and later expiring rules
x Add cleaner --unload-rules type functionality for stopping group
x Fix the until restart in iptables rules presentation
x do bind-dynamic to a wlan0-$group-ap? Would this allow DHCP to work on a later created AP iface?
	- bind-interface prevented DHCP reply to return to the DHCP relay, so 10.3.0.10 would send to 10.3.0.1, but the reply would fail to send
x look at tap interfaces that have that intreface as their master!
x Convert local DHCP on bridge to x.y.z.0/25 and on hostapd x.y.z.128/25?
x Need to add what IPs dnsmasq can give based on source of the query, so hostapd get upper half
x stop --reload-rules from creating too many chains
x Add a microsocks switch to startvpn.sh for ifaceRouting, so create a socks proxy to tun0 with microsocks-tun0
x Add a clearDNS option to use for tun0 instead of DoH
x Look for avaialble suitable taps abandoned by previous shutdowns, or hash in the paramters to get same named tap?
x Make prompter.py show what VM is asking dns
x VM script with FS watcher to create drop in user.js when a FF profile is created
x Add --stop-running option to stop all things in running file
x Make dns_editor to parse group for ip rule creation, look at source?
x After change from adding IP rule before DNS rule, check for "Exists" as not an error and disregard it
x Move the DNS source resolution to the prompter so it displays properly
x Make 3 VM options in config files: mount (for browser profile), hostname (for network rules), disk (for temp or other permanent)
x Add a system group to modify all files instead of all users
x distinguish between namespace started with VPN and started without (the dontStartVPN option)
x --services-stop should stop all hostapd, groups, namespaces. Where to keep track of all that stuff? File or list interfaces?
x Make --group-stop prompt to stop hostapd
x Create better reader and writer to ips.txt
x Add /etc/hosts support to storing rules with different groups in ip.txt
x Make sure process 'any' rules come BEFORE all other paths? What should be the order for accept/deny for that?
x Update the writing reading and writing of ips.txt to fix DNS for subnets
x Fixed DNS error with line_num
x Fix needing root ssh password for the keyfiles for sudo login
x Create some sweeper to remove orphaned 0 reference chains, although should fix root causes
x FIX THE DEADLOCK IN KERNEL LSM
	- This didn't ome up until started running -m dns --watch-queries in second namespace, so multi-threads now. 
	- Started saving process path in the listening linked list instead of calling get_path() for the socket
x Compile on 5.15.7
x VMs install and start spice-vdagent for copy paste
x Make a better way to turn --namespace option into a --group option. 2 Steps: 
x add packages option to VM to always make sure those packages are installed and up to date on boot
x Prevent stopping --namespace when a --group requires it
x VM temp disk to be a clone in /tmp/
x Make LXC work just like how VMs work
x Make the upstream IP configurable to the tun0 gateway not hardcoded
x Fixed DNS add line_num setter
	- Remember second VPN instances are in their own netNS because VPN expects only one, so routing conflicts
	1) Setup iptables rules in namespace, for single processes on this machine run in there:
	- dnsmasq on inner lo upstreams to 10.0.X.1:53 out of NS where prompter listens, and upstreams back inside to 10.0.X.2:53
		- Proc path should be caught with TID and --watch-queries inside
	2) For bridge and qemu outside:
	- Same setup with ifaceRoute, but to the veth-nontorX interface
x Make NAMESPACE_ov_port and ov_ip be able to be multiple values that then are picked randomly
x Removed all vpn services' names from script and use config[] options VPN1, VPN2
x make groups.conf CLEARDNSUPSTREAM dynamic to get from the running --namespace
x Add -o for connections to an IP address
x Fix /etc/fstab for VMs only if mount_option exists. Set to mount with the ssh after boot, no fstab anymore
x Add a spice/qxl/sdl option to VM configs
x Fixed packet editor
x Create a "required group" for a VM or LXC, so it is never accidentally used with the wrong desintation. e.g. bind bank VM to home IP tunnel
x Make DNS rules stored by source, not just process and pid
	- fullmatch hostnames are added only using process and pid, so a different source is re-prompted and overwrites old one
	- Convert this to [process][pid][0 or 2][pattern] = list of rules for that pattern
x Reason that --overwrite-dns... was keeping old rules was FromCLI=True meant process_hostname_acl was read into memory before deleting file
x Unify LXC and VM ip rule save_file location
x Convert all systemd services to /run/systemd/
x Fix VM mount point getting recursively created and so owned by root: Recursive dir creation with awk and install
x VM resolution set in config. Startx doesn't exec until first ssh in
x Fix VM hostname saved for permanent IP rule
x VM sound
x Add better support for multiple VPNS at once
x Possible to send namespace's VPN over other namespaces
x --namespace options should be tied to a VPN string
x Expose localhost port of vms to outside with /etc/hosts entry
x Convert DNS to dict based on process. Checks done on full hostname, then *.domain1.tld, then *.domain2.domain1.tld
x Completely simplify DNS:
	-editor completely rewrites old rules
	-remove line_num
	-everything displayed on single lines
x Removed socker_process LSM as a major LSM
x Track running VMs by pid and 'tail --pid' to remove them from a file and notify-send on shutdown
x LUKS encrypt mount options
x Check the modified time of the temp disk before using the CoW disk still existing in /tmp/ (shutdown VM, update temp disk, restart tries to use CoW in /tmp/ is bad)
x Change default VM journald to disk since there have been some panics
x Remove prompter.py line_num and store on disk in readable form.
x Add way to install headers locally
x --lxc gets added to running file, but removed when group goes down
x Userspace /proc/*/cmdline parsing so that python3.9 can be identified by what script is running and location
x VM downloads folder samba mounted to host
x When tor-browser AUR pacage updates
x Convert all iptables to iptables -w
x X session autostart processes
x Create IPSET from extracted IPs from tor cache file
x LXC mounts with encryption like VMs
x DNS to IP rules are added on demand?
x Fix --vm temp to update the temp base properly
x Add a namespace watcher kernel module
x LUKS encrypt VM disks.
x Update DNS server so that a program firing two identical queries at once (multithread pacman) will only send 1 upstream, not both
x Make generic samba service for each VM to mount, with system for host to set permissions for each
x Create LXC without rootfs fails to set pacman Xfer command (fix create LXC from scratch)
x Check if qemu process dies while sshspin
x Sort dns list by pattern after source
x only add user to dns rule if they are permanent user (in /etc/passwd) and not a systemd dynamic user
