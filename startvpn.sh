#!/bin/bash
DEFAULT_NAMESPACE=default

# Make sure a default namespace exists so 'ip netns exec default' works
[ ! -f /run/netns/${DEFAULT_NAMESPACE} ] && /usr/bin/mkdir /run/netns/ 2>/dev/null ; /usr/bin/touch /run/netns/${DEFAULT_NAMESPACE} 2>/dev/null && /usr/bin/mount --bind /proc/1/ns/net /run/netns/${DEFAULT_NAMESPACE}
[[ "$(/usr/bin/ip netns identify)" != "${DEFAULT_NAMESPACE}" ]] && echo "This program is meant to be run from the default namespace!" && exit 1

for exe in "ipcalc" "ip_overlap" ; do
	! which $exe >/dev/null 2>&1 && echo "Install ${exe} or add it to PATH" && exit 1
done

COMMENT_REGEX="^[[:space:]]*\($\|#\)"
CONFIG_DIR="/etc/prompt-and-split"

# Read config file and set default of no sslocal and openvpn pairs. Bad config may cause unknown errors later!
declare -A config=()
for file in "${CONFIG_DIR}/configs/namespaces.conf" "${CONFIG_DIR}/configs/groups.conf" "${CONFIG_DIR}/configs/config.conf" ; do
	while IFS= read -r line ; do
		if [[ $(echo $line | /usr/bin/grep -v "$COMMENT_REGEX" | /usr/bin/grep -o -F "=" ) ]]; then
			key=$(echo $line | /usr/bin/cut -d '=' -f 1 | /usr/bin/tr -d ' ')
			# Remove outer spaces and quotations
			val=$(echo $line | /usr/bin/cut -d '=' -f 2- | /usr/bin/sed 's/^[[:space:]]*//;s/[[:space:]]*$//' | /usr/bin/sed 's/^\"\(.*\)\"/\1/g')
			config[$key]=$val
		fi
	done < "$file"
done

TMP_DIR="/run/prompt-and-split"
if /usr/bin/mkdir "$TMP_DIR" 2>/dev/null ; then
	/usr/bin/chown 0:"${config[FS_GROUP]}" "$TMP_DIR" 2>/dev/null
	/usr/bin/chmod 0777 "$TMP_DIR" 2>/dev/null
fi

LOCK="${TMP_DIR}/lock"
[[ -f "$LOCK" ]] && echo "Failed to run (lock file exists)" && LOCK= && exit 1 # trapped EXIT will not delete LOCK if it was unset here

# LANInterface option can be multiple interfaces separated by spaces. First from left to right that exist is set as the config value
for iface in ${config[LANInterface]} ; do /usr/bin/ip link show dev "$iface" >/dev/null 2>&1 && config[LANInterface]="$iface" && break ; done
(echo ${config[LANInterface]} | /usr/bin/grep -q -- " " || ! /usr/bin/ip link show dev "$iface" >/dev/null 2>&1) && echo "Option LANInterface does not exist!" && exit 1

RUNNING_FILE="${TMP_DIR}/running" # File stores when user starts a group, namespace, hostapd, etc.
/usr/bin/touch "$LOCK" "$RUNNING_FILE"
/usr/bin/chown 0:"${config[FS_GROUP]}" "$LOCK" "$RUNNING_FILE" 2>/dev/null
/usr/bin/chmod 0660 "$RUNNING_FILE" "$LOCK" 2>/dev/null

# Needed
[ $UID -eq 0 ] && /usr/bin/sysctl --quiet --write net.ipv4.ip_forward=1

IPV4_REGEX="([0-9]{1,3}\.){3}[0-9]{1,3}"
PREFIX_REGEX="(/[0-9]{1,2})"
SET_DNS_FILE="${TMP_DIR}/set_dns"
CUSTOM_NOTIFY_SCRIPT="/etc/my_scripts_root/notify-send_all_users" #UPDATE to incorporate this
debug=1
FORCE=0 # Prevent any user prompts for an LXC or VM running when trying to stop a group
USE_SAFETY=1

MARK_FILE="${TMP_DIR}/marks.txt"
GROUP_FILE="${TMP_DIR}/groups.txt"
IP_FILE="${TMP_DIR}/ips.txt"
NSIP_FILE="${TMP_DIR}/nsips.txt"
TABLE_FILE="${TMP_DIR}/tables.txt"

CHAIN_PREFIX="pdlog-"
DEFAULT_IN="${CHAIN_PREFIX}${DEFAULT_NAMESPACE}-in"
DEFAULT_OUT="${CHAIN_PREFIX}${DEFAULT_NAMESPACE}-out"

# If a group starts and there is a running VM with an orphaned 'qemu-*' interface that was in that group, add it to /etc/hosts, load the rules for it and set the bridge as master for the interface
UPDATE_ORPHANED_RUNNING_VMS=1

# Error printed to stderr by qemu to not see 
QEMU_ERROR_TO_REMOVE="qemu-system-x86_64: warning: spice: no gl-draw-done within one second\|qemu-system-x86_64: warning: console: no gl-unblock within one second"

MODULES="nf_log_syslog nf_pdlog xt_PDLOG xt_dns xt_proc"

LUKS_PASSWORD_FILE="${CONFIG_DIR}/rules/luks.txt" # Passwords stored for VM and LXC mounts
if [[ ! -f "$LUKS_PASSWORD_FILE" ]]; then
	/usr/bin/mkdir -p "${CONFIG_DIR}/rules/"
	/usr/bin/chown root:"${config[FS_GROUP]}" "${CONFIG_DIR}/rules/"
	/usr/bin/touch "$LUKS_PASSWORD_FILE"
	/usr/bin/chattr +a "$LUKS_PASSWORD_FILE"
fi

# This is used to create and pass variables betwen functions based on the name
declare -A global=()
# Service names that must be restarted with making the initial VPN connection.
declare -A INITIAL_SERVICES=()

# Replace the lastoctet of an IPv4 with the second string
function lastOctet(){
	local ip=$(echo "$1" | /usr/bin/awk -v r="$2" -F '.' '{print $1"."$2"."$3"."r}')
	if [[ $1 =~ ^${IPV4_REGEX}${PREFIX_REGEX}$ ]]; then
		echo "$ip"/$(echo "$1" | /usr/bin/cut -d'/' -f2)
	else
		echo "$ip"
	fi
}

function BcontainsA(){
	python -c "
with open('$1', 'r') as a:
  with open('$2', 'r') as b:
    exit(0 if ''.join([c for c in a.read() if c.strip()]) in ''.join([c for c in b.read() if c.strip()]) else 1)"
	return $?
}

# Is $1 in $2 ? Good for checking if '1' is in '1 2 3 4 5'
function isIn(){
	[[ $2 =~ (^|[[:space:]])$1($|[[:space:]]) ]] && echo 1 || echo 0
}

function ifaceAddress(){
	/usr/bin/ip address show dev "$1" | /usr/bin/grep --only-matching -E "^[[:space:]]*inet ${IPV4_REGEX}" | head -1 | /usr/bin/awk -F ' ' '{print $2}'
}

function openPort(){
	local ports=$(/usr/bin/ss --listening --tcp --udp --numeric | /usr/bin/grep -v "^Netid " | /usr/bin/awk '{print $5}' | /usr/bin/cut -d':' -f2 | /usr/bin/awk '!x[$0]++' | sort -h)
	for port in $(seq $(echo 2^15 + 1 | bc) $(echo 2^16 - 1  | bc) | shuf)
	do
		if [[ 0 == $(isIn $port $ports) ]]; then
			echo $port
			return
		fi
	done
}

# Get the -I value for iptables to add a rule $1 above the bottom
function fromBottom(){
	local n="$1" ; [[ ! "$n" =~  ^[0-9]{1,}$ ]] && return
	local table="$2" ; [[ -z "$table" ]] && return
	local chain="$3" ; [[ -z "$chain" ]] && return

	echo "$(( $(iptables -t "$table" -nL "$chain" | wc -l) - 1 - "$n" ))"
}

# Poll "ip address" for an interface with a timeout
function waitForIface() {
	local iface="$1"
	local timeout="$2"
	while true
	do
		echo
	done
		
}

# Create new service files for namespace index $2, VPN service $1
function writeServicesForName(){
	local readOrWrite="$1"
	local name="$2"
	local vpn="${config[${name}_vpn_provider]}"
	local vpn_len=$(echo $(echo ${vpn} | wc -c) + 1 | bc)

	# UPDATE HERE to support dynamic users
	local openvpn_user="openvpn"

	# Set default values if they are not present in config
	# if key is _{vpn}_KEY then use instead of the _KEY, which is set after if nothing set it here first
	for key in "${!config[@]}" ; do
		if [[ "$key" =~ ^_"${vpn}" ]]; then 
			local newkey=$(echo ${key} | /usr/bin/cut -c"${vpn_len}"-)
			[[ -z "${config[${name}${newkey}]}" ]] && config[${name}${newkey}]=${config[${key}]}
		fi
	done
	for key in "${!config[@]}" ; do
		[[ "$key" =~ ^_ && ! "$key" =~ ^_"${vpn}" && -z "${config[${name}${key}]}" ]] && config[${name}${key}]=${config[${key}]}
	done

	local vpnAcct="${config[${name}_vpn_account]}"
	if [[ -z "${config[${name}_vpn_provider]}" || -z "${config[${name}_vpn_account]}" ]]; then
		echo "Namespace \"${name}\" doesn't exist in config file. Exitting."
		exit 1
	fi
	
	# Generate nsip (used for --group and --namespace) for any potential namespace not "default". Make sure to pass readOrWrite properly
	if [[ "$DEFAULT_NAMESPACE" != "$name" ]]; then
		local reg="10\.([0-9]{1,3}\.){2}1"
		config[${name}_veth_nsip]=$(newVar "$readOrWrite" "$name" "${TMP_DIR}/nsips.txt" "$reg" "/30")
		! echo "${config[${name}_veth_nsip]}" | /usr/bin/grep -q -E -- "^${reg}$" && echo "ERROR Getting nsip for ${name}" && exit 1
	fi

	# Set syslog to include group name if not specified
	# [[ "${config[${name}_ov_syslog]}" == "${config[_ov_syslog]}" ]] && config[${name}_ov_syslog]="${config[${name}_ov_syslog]}-${name}"
	[[ -z "${config[${name}_ov_syslog]}" ]] && config[${name}_ov_syslog]="${vpn}-${name}"

	if [[ 1 == "${config[${name}_ss_use]}" ]]; then
		config[${name}_ss_service]="sslocal-${vpn}-${name}".service

		local sslocal_service="${config[${name}_ss_service]}"
		local sslocal_service_path="/run/systemd/system/${sslocal_service}"
		local sslocal_config="/run/${sslocal_service}.conf"
	fi

	config[${name}_ov_service]="openvpn-${vpn}-${name}".service

	local openvpn_service="${config[${name}_ov_service]}"
	local openvpn_service_path="/run/systemd/system/${openvpn_service}"
	local openvpn_config_dir="/etc/openvpn/client-${vpn}-${vpnAcct}/"
	[[ ! -d "$openvpn_config_dir" ]] && echo "ERROR: openvpn config dir ${openvpn_config_dir} does not exist. Exitting." && exit 1
	/usr/bin/chown "${openvpn_user}" -R -- "$openvpn_config_dir"

	local openvpn_config_file="${openvpn_config_dir}/${config[${name}_ov_syslog]}.conf"
	[[ 0 == "$readOrWrite" && ! -f "$openvpn_config_file" ]] && echo "ERROR: openvpn config file ${openvpn_config_file} does not exist. Exitting." && exit 1
	
	# ovX_regex option means pick an IP from RANDFILE whose line starts with the ovX_regex string
	# Pick a new IP for writing the service file, or read from the existent one
	if [[ -n "${config[${name}_ov_regex]}" ]]; then
		if [[ 1 == "$readOrWrite" ]]; then
			local ipFile="${CONFIG_DIR}/vpn-ips/${vpn}.txt"
			! /usr/bin/grep -q -E -- " ${IPV4_REGEX} (tcp|udp) ([0-9]{1,}[ ]?){1,}$" "$ipFile" && echo "ERROR: ${ipFile} not formatted correctly. Exitting." && exit 1
			
			# If config specifies proto, make sure IP and port match it
			if [[ "" != "${config[${name}_ov_proto]}" ]]; then
				local tmp=$(awk -vr="${config[${name}_ov_regex]}" -vp="${config[${name}_ov_proto]}" '$1 ~ "^"r && $3 == p {$1="" ; print $0}' "$ipFile" | shuf --head 1)
			else
				local tmp=$(grep "^${config[${name}_ov_regex]}" "$ipFile" | /usr/bin/awk {$1="" ; print $0} | shuf --head 1)
			fi
			config[${name}_ov_ip]=$(echo $tmp | awk '{print $1}')
			config[${name}_ov_proto]=$(echo $tmp | awk '{print $2}')
			config[${name}_ov_port]=$(echo $tmp | awk '$1="" ; $2="" ; {print $0}')
		else
			# TODO: what if there are more than one remote lines in the conf? ovpn picks a random one, but this gets last
			local tmp=$(grep "^remote " "$openvpn_config_file" | tail -1)
			config[${name}_ov_ip]=$(echo $tmp | /usr/bin/awk '{print $2}')
			config[${name}_ov_port]=$(echo $tmp | /usr/bin/awk '{print $3}')
			config[${name}_ov_proto]=$(grep "^proto " "$openvpn_config_file" | tail -1 | grep -oE -- "tcp|udp")
		fi
	fi

	# If _ov_port or _ov_ip has multiple options, pick one, or read from the config file.
	# This is overwritten by _ov_regex being set, which picks an ip and port line based on that regex run on first column
	local k=( "_ov_port"   "_ov_ip" )
	local v=( '{print $3}' '{print $2}' )
	for i in "${!k[@]}"; do
		if [[ "${config[${name}${k[${i}]}]}" =~ " " ]]; then
			if [[ 1 == "$readOrWrite" ]]; then
				config[${name}${k[${i}]}]=$(echo ${config[${name}${k[${i}]}]} | tr ' ' '\n'  | shuf --head 1)
			else
				config[${name}${k[${i}]}]=$(grep "^remote " "$openvpn_config_file" | tail -1 | /usr/bin/awk "${v[${i}]}")
			fi
		fi
	done

	if [[ -z ${config[${name}_ov_ip]} || -z ${config[${name}_ov_port]} || -z ${config[${name}_ov_proto]} ]]; then
		echo "OpenVPN IP or port or proto is empty:"
		echo "IP:    ${config[${name}_ov_ip]}"
		echo "Port:  ${config[${name}_ov_port]}"
		echo "Proto: ${config[${name}_ov_proto]}"
		exit 1
	fi

	# Pick a free port to listen on or read previous one
	# if [[ -z "${config[${name}_ss_lport]}" || "free" == "${config[${name}_ss_lport]}" ]]; then
	# 	if [[ 1 == "$readOrWrite" ]]; then
	# 		# if [[ 1 == "${config[${name}_ss_use_default]}" && "$DEFAULT_NAMESPACE" != "$name" ]]; then
	# 		for port in `seq 1080 65535`; do
	# 			if [[ "ok" == $(/usr/bin/ip netns exec "$name" ss --listening --family inet --tcp --numeric | /usr/bin/awk -v p="$port" '$4 ~ ":"p"$" {print "ok"}') ]]; then
	# 				config[${name}_ss_lport]="$port"
	# 				break
	# 			fi
	# 		done
	# 	else
	# 		config[${name}_ss_lport]=$(grep "^socks-proxy " "$openvpn_config_file" | tail -1 | /usr/bin/awk '{print $3}')
	# 	fi
	# fi

	# Make sure to bind openvpn service to the proper sslocal service. Don't PropagateStop of netNS OpenVPN to default ss-local
	local openvpnPropagateComment="#"
	local openvpnBindComment="#"
	local bind_service=

	# Set the first hop (fh) IP address info for allowing that through the firewall
	if [[ "${config[VPN1]}" == "$vpn" ]]; then
		if [[ 1 == "${config[${name}_ss_use]}" ]]; then
			openvpnBindComment=""

			config[${name}_fh_ip]="${config[${name}_ss_ip]}"
			config[${name}_fh_port]="${config[${name}_ss_rport]}"
			config[${name}_fh_proto]="${config[${name}_ss_proto]}"
			config[${name}_fh_path]="${config[${name}_ss_path]}"
			config[${name}_fh_owner]="${config[${name}_ss_owner]}"
		else
			config[${name}_fh_ip]="${config[${name}_ov_ip]}"
			config[${name}_fh_port]="${config[${name}_ov_port]}"
			config[${name}_fh_proto]="${config[${name}_ov_proto]}"
			config[${name}_fh_path]="/usr/bin/openvpn"
			config[${name}_fh_owner]="${openvpn_user}"
		fi
			
	elif [[ "${config[VPN2]}" == "$vpn" ]]; then
		config[${name}_fh_ip]="${config[${name}_ov_ip]}"
		config[${name}_fh_port]="${config[${name}_ov_port]}"
		config[${name}_fh_proto]="${config[${name}_ov_proto]}"
		# config[${name}_fh_path]="${config[${name}_ss_path]}"
		# config[${name}_fh_owner]="${config[${name}_ss_owner]}"
	fi

	if [[ 1 == "$readOrWrite" ]]; then	
		# UPDATE HERE to conform to multiple VPN providers
		# Setup openvpn config and service files
		/usr/bin/cp -f "${openvpn_config_dir}/default.conf" "$openvpn_config_file"
		/usr/bin/chown "${openvpn_user}":openvpn "$openvpn_config_file"
		echo """
syslog ${config[${name}_ov_syslog]}
dev ${config[${name}_ov_interface]}
proto ${config[${name}_ov_proto]}${config[${name}_ov_protoNumber]}
remote ${config[${name}_ov_ip]} ${config[${name}_ov_port]}
pull-filter ignore ${config[${name}_ov_ip]}""" >> "$openvpn_config_file"
	
		if [[ "${config[VPN1]}" == "$vpn" && 1 == "${config[${name}_ss_use]}" ]]; then
			# Use the default netNS's SOCKS proxy instead of starting a new process in the namespace
			bind_service=${sslocal_service}
			if [[ 1 == "${config[${name}_ss_use_default]}" && "$DEFAULT_NAMESPACE" != "$name" ]]; then
				writeServicesForName 0 "$DEFAULT_NAMESPACE"
				bind_service="${config[${DEFAULT_NAMESPACE}_ss_service]}"
				# Do not propogate the stop of this VPN service to the default namespace's ss-local service
				openvpnPropagateComment="#"
				echo "socks-proxy $(lastOctet ${config[${name}_veth_nsip]} 2) ${config[${DEFAULT_NAMESPACE}_ss_lport]}" >> "$openvpn_config_file"
			else
				echo "socks-proxy 127.0.0.1 ${config[${name}_ss_lport]}" >> "$openvpn_config_file"
			
# \"user\": \"${config[${name}_ss_owner]}\",
				echo """{
\"server\": \"${config[${name}_ss_ip]}\",
\"server_port\": \"${config[${name}_ss_rport]}\",
\"local_port\": \"${config[${name}_ss_lport]}\",
\"password\": \"${config[${name}_ss_password]}\",
\"method\": \"${config[${name}_ss_cipher]}\",
}""" > "$sslocal_config"
				/usr/bin/chmod a+r "$sslocal_config"

				# TODO: update this
				/usr/bin/useradd ss-local
				echo """[Unit]
Description=ss-local namespaced instance
AssertPathExists=/run/netns/${name}
[Service]
Type=exec
NetworkNamespacePath=/run/netns/${name}
User=ss-local
Group=ss-local
ExecStart=/usr/bin/ss-local -v -c ${sslocal_config}

NoNewPrivileges=true
NonBlocking=true
MemoryDenyWriteExecute=true
ProtectHostname=true
ProtectControlGroups=true
ProtectKernelModules=true
ProtectKernelTunables=true
LockPersonality=true
RestrictSUIDSGID=true
SystemCallArchitectures=native

PrivateDevices=true
ProtectControlGroups=true
ProtectHome=true
ProtectKernelTunables=true
# RestrictSUIDSGID=true

ProtectClock=yes
ProtectHostname=yes

ProtectKernelLogs=yes
ProtectKernelModules=yes

# ProtectSystem=strict
PrivateTmp=true

RestrictAddressFamilies=AF_INET AF_NETLINK
RestrictRealtime=true
RestrictNamespaces=true
SystemCallArchitectures=native

ProtectProc=noaccess
# ProcSubset=true

SystemCallFilter=@system-service

InaccessiblePaths=/etc/
# InaccessiblePaths=/usr/bin/at
# InaccessiblePaths=/usr/bin/cron
# InaccessiblePaths=/usr/bin/zsh
InaccessiblePaths=/usr/bin/bash
InaccessiblePaths=/usr/bin/sh
# InaccessiblePaths=/usr/bin/wget
InaccessiblePaths=/usr/bin/curl
InaccessiblePaths=/usr/bin/ssh
# InaccessiblePaths=/usr/bin/scp
InaccessiblePaths=/usr/bin/python
InaccessiblePaths=/usr/bin/perl
InaccessiblePaths=/usr/local/
# MemoryDenyWriteExecute=true
# ProtectHostname=true
# ProtectControlGroups=true
# ProtectKernelModules=true
# ProtectKernelTunables=true
# LockPersonality=true
# RestrictSUIDSGID=true
# SystemCallArchitectures=native

# CapabilityBoundingSet=~CAP_SYS_TIME
# AmbientCapabilities=
# ReadOnlyPaths=/proc/
# RestrictNamespaces=~user
# RestrictNamespaces=~pid
# RestrictNamespaces=~net
# RestrictNamespaces=~uts
# RestrictNamespaces=~mnt
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#SystemCallFilter=@system-service @chown
#SystemCallFilter=~@resources @privileged
[Install]
WantedBy=multi-user.target
""" > "$sslocal_service_path"
				/usr/bin/chmod a+r "$sslocal_service_path"
			fi
		fi

		echo """[Unit]
Description=OpenVPN tunnel
Wants=sd-notifer@%n.service
# Man page says to use BindsTo (not Requires) so that other service is guarenteed in Active state 
${openvpnBindComment}BindsTo=${bind_service}
After=syslog.target ${bind_service}
# TODO: investigate why systemd 253 makes this invalid:
# Requested transaction contains unmergeable jobs: Transaction contains conflicting jobs 'stop' and 'restart' for openvpn-${VPN1}-default.service. Probably contradicting requirement dependencies configured.
#${openvpnPropagateComment}PropagatesStopTo=${bind_service}
[Service]
Type=notify
PrivateTmp=true
User=${openvpn_user}
WorkingDirectory=${openvpn_config_dir}

NetworkNamespacePath=/run/netns/${name}
ExecStart=/usr/bin/openvpn --suppress-timestamps --nobind --config ${openvpn_config_file}

AmbientCapabilities=CAP_IPC_LOCK CAP_NET_ADMIN CAP_NET_RAW CAP_SETGID CAP_SETUID CAP_SYS_CHROOT CAP_DAC_OVERRIDE
CapabilityBoundingSet=CAP_IPC_LOCK CAP_NET_ADMIN CAP_NET_RAW CAP_SETGID CAP_SETUID CAP_SYS_CHROOT CAP_DAC_OVERRIDE
LimitNPROC=10
DeviceAllow=/dev/null rw
DeviceAllow=/dev/net/tun rw
ProtectSystem=true
ProtectHome=true
KillMode=process
[Install]
WantedBy=multi-user.target""" > "$openvpn_service_path"
		/usr/bin/chmod a+r "$openvpn_service_path"
		/usr/bin/systemctl daemon-reload
	fi

	[[ "$DEFAULT_NAMESPACE" == "$name" ]] && INITIAL_SERVICES=( "ulogd.service" "${config[${DEFAULT_NAMESPACE}_ov_service]}" ) # "systemd-timesyncd.service"
}

NFLOGOUTPUTGROUP="1"
NFLOGINGROUP="3"

NFLOGNONTORGROUP="7" #marks for non-tor routed connections
NFLOGTORGROUP="8" #marks for Tor routed connections

#TODO:
	# Allow creating all the rules without a VPN connection active
	# When stop namespace, offer user the choice to remove firewall rules if the service is already stopped, since 
		# another with its ips or interface may have started?
		# Add comment as the hash of the service? So if anything changed it would safely remove command!
	# add proto and port to protect()
	# Add expire date for initial wlan0 sslocal connection?
	# Better /usr/bin/systemctl error and exit handling to stop all before services

# Clear 0 reference tables at the end of each run
trap clearZeroRefChains EXIT
function clearZeroRefChains() {
	local ret=$?
	/usr/bin/rm --force $LOCK
	[[ 0 != $EUID ]] && exit $ret
	iptables -w -t mangle -vnL | /usr/bin/grep "^Chain [^ ]* (0 references)$" | /usr/bin/cut -d' ' -f2 | /usr/bin/grep "^[io][ad]" | xargs -I {} -r /bin/bash -c 'iptables -w -t mangle -F "{}"; iptables -w -t mangle -X "{}";' ''
	exit $ret
}

function clearEverything() {
	for chain in "OUTPUT" "INPUT" "FORWARD" ; do iptables -w -P "$chain" DROP ; done
	for table in "mangle" "filter" "nat" ; do iptables -w -t "$table" -F ; iptables -w -t "$table" -X ; done
	for mod in $MODULES ; do rmmod "$mod" ; done
	conntrack -F 2>/dev/null
	/usr/bin/rm --force "$SET_DNS_FILE"
}

function restartServices(){
	for service in "$@"; do
		if ! /usr/bin/systemctl restart $service ; then
			echo "$service failed to start"
			/usr/bin/systemctl stop "$service"
			exit 1
		fi
	done
}

function startStopHttpProxy(){
	local start="$1"
	local namespace="$2"
	local listenAddress="$3"

	local comment="${namespace} HTTP proxy"
	local service_name="tinyproxy-${namespace}.service"
	local service_path="/run/systemd/system/${service_name}"
	local service_config_path="${TMP_DIR}/tinyproxy-${namespace}.conf"
	local port=1234
	local inPort=1080

	local PROXY_RULES=(
		"PREROUTING  -t nat    -i ${config[LANInterface]} -d ${LANIP}         -p tcp --dport ${inPort}                            -j DNAT --to-destination ${listenAddress}:${port}"
		"FORWARD     -t filter -i veth-${namespace}       -s ${listenAddress} -p tcp --sport ${port}   -o ${config[LANInterface]} -j ACCEPT"
		"FORWARD     -t filter -i ${config[LANInterface]} -d ${listenAddress} -p tcp --dport ${port}   -o veth-${namespace}       -j ACCEPT"
		"POSTROUTING -t nat    -o veth-${namespace}       -d ${listenAddress} -p tcp --dport ${port}                              -j MASQUERADE"
	)

	if [[ 1 == "$start" ]]; then
		echo """
# User tinyproxy
# Group tinyproxy
Port ${port}

Listen ${listenAddress}
# Bind: This allows you to specify which interface will be used for
# outgoing connections.  This is useful for multi-home'd machines where
# you want all traffic to appear outgoing from one particular interface.
#Bind 192.168.0.1

# BindSame: If enabled, tinyproxy will bind the outgoing connection to the ip address of the incoming connection.
#BindSame yes

Timeout 600
DefaultErrorFile \"/dev/null\"
StatHost \"$(/usr/bin/head /dev/urandom | /usr/bin/md5sum | /usr/bin/cut -d' ' -f1).stats\"
StatFile \"/dev/null\"
#LogFile \"/dev/null\"
Syslog Off
# Critical, Error, Warning, Notice, Connect, Info
LogLevel Info
#PidFile \"/var/run/tinyproxy/tinyproxy.pid\"

#  # no upstream proxy for internal websites and unqualified hosts
#  upstream none ".internal.example.com"
#  upstream none "www.example.com"
#  upstream none "10.0.0.0/8"
#  upstream none "192.168.0.0/255.255.254.0"
#  upstream none "."
# The LAST matching rule wins the route decision.  As you can see, you
# can use a host, or a domain:
#  name     matches host exactly
#  .name    matches any host in domain "name"
#  .        matches any host with no domain (in 'empty' domain)
#  IP/bits  matches network/mask
#  IP/mask  matches network/mask

MaxClients 100
#Allow 127.0.0.1
#BasicAuth user password
# ViaProxyName \"tinyproxy\"
DisableViaHeader Yes
# Because prompt-and-split has dns based filtering, use that instead of a separate file
#Filter \"/etc/tinyproxy/filter\"
""" | /usr/bin/sudo /usr/bin/tee "${service_config_path}" 1>/dev/null && /usr/bin/sudo /usr/bin/chmod +r "${service_config_path}"

		echo """[Unit]
Description=Tinyproxy Web Proxy Server In Network Namespace ${namespace}
After=network.target
AssertPathExists=/var/run/netns/${namespace}/

[Service]
NetworkNamespacePath=/var/run/netns/${namespace}/
AmbientCapabilities=
CapabilityBoundingSet=
Type=forking
DynamicUser=yes
LimitNOFILE=1048576
ExecStart=/usr/bin/tinyproxy -c ${service_config_path}
# PIDFile=//tinyproxy.pid

PrivateDevices=yes
ProtectSystem=strict

[Install]
WantedBy=multi-user.target
""" | /usr/bin/sudo /usr/bin/tee "${service_path}" 1>/dev/null && /usr/bin/sudo /usr/bin/chmod +r "${service_path}"
		
		/usr/bin/systemctl daemon-reload
		/usr/bin/systemctl restart "$service_name"

		for i in "${PROXY_RULES[@]}" ; do
			iptables -w -I $i -m comment --comment "$comment" || echo $i
		done
	else
		for i in "${PROXY_RULES[@]}" ; do
			iptables -w -D $i -m comment --comment "$comment" || echo $i
		done
		/usr/bin/systemctl stop "$service_name" 2>/dev/null
	fi
}

function startStopDnsmasq(){
	local start="$1"
	local namespace="$2"
	local interface="$3" # Keeping separate from args helps distinguish services. Use has since some use multiple (uses comma)
	local args="$4"

	local service="dnsmasq-${namespace}-$(echo $interface | md5sum | /usr/bin/cut -c -10).service"
# LockPersonality=yes
# MemoryDenyWriteExecute=true
# NonBlocking=true
# NoNewPrivileges=true
# ReadOnlyPaths=/proc/
# PrivateDevices=true
# ProtectControlGroups=yes
# ProtectHome=yes
# ProtectHostname=yes
# ProtectKernelLogs=yes
# ProtectKernelModules=yes
# ProtectKernelTunables=yes
# ProtectSystem=strict
# RestrictAddressFamilies=AF_INET AF_INET6 AF_NETLINK
# SystemCallArchitectures=native
# #SystemCallFilter=@system-service @chown
# # SystemCallFilter=~@resources @privileged
	if [[ 1 == "$start" ]]; then
		echo """[Unit]
Description=dnsmasq from prompter.py
After=network.target
Before=nss-lookup.target
Wants=nss-lookup.target
AssertPathExists=/run/netns/${namespace}

[Service]
Type=forking
ExecStart=/usr/bin/dnsmasq --log-facility=/dev/null --pid-file --conf-file=/dev/null --interface ${interface} ${args}
ExecReload=/bin/kill -HUP \$MAINPID

NetworkNamespacePath=/run/netns/${namespace}

DynamicUser=yes

TemporaryFileSystem=/run/:ro
BindPaths=/run/prompt-and-split/

#ReadWritePaths=/run/prompt-and-split/

NoNewPrivileges=yes
PrivateTmp=yes
PrivateDevices=yes
ProtectSystem=strict
ProtectHome=yes
ProtectHostname=yes
ProtectKernelLogs=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes
RestrictRealtime=yes

# RestrictAddressFamilies=AF_INET AF_NETLINK
AmbientCapabilities=CAP_NET_BIND_SERVICE CAP_NET_ADMIN
CapabilityBoundingSet=CAP_NET_BIND_SERVICE CAP_NET_ADMIN

[Install]
WantedBy=multi-user.target""" > "/run/systemd/system/${service}"
		
		/usr/bin/systemctl daemon-reload
		/usr/bin/systemctl restart "$service"
	else
		/usr/bin/systemctl stop "$service" 2>/dev/null
	fi
}

# Start or stop dns-over-https client with a new config in a namespace
function startStopDoH(){
	local start="$1"
	local namespace="$2"
	local listen="$3"
	local conf="/run/doh-client-${namespace}.conf" # Needs to be readable to systemd dynamic user
	local service="doh-client-${namespace}.service"

	if [[ 1 == "$start" ]]; then
		# UPDATE HERE to cut out multiline 'listen = [ ... ]'
		# Copy the config with the new listening address and port
		/usr/bin/sed "s/^[[:space:]]*listen[[:space:]]*=.*/listen=${listen}/" /etc/dns-over-https/doh-client.conf > "$conf"

		echo """[Unit]
Description=DNS-over-HTTPS Client
After=network.target
Before=nss-lookup.target
Wants=nss-lookup.target
AssertPathExists=/run/netns/${namespace}

[Service]
AmbientCapabilities=
CapabilityBoundingSet=
NetworkNamespacePath=/run/netns/${namespace}
ExecStart=/usr/bin/doh-client -conf ${conf}
LimitNOFILE=1048576
Restart=always
RestartSec=3
Type=simple
DynamicUser=yes

ReadOnlyPaths=/proc/
MemoryDenyWriteExecute=true
LockPersonality=yes
ProtectControlGroups=yes
NoNewPrivileges=true
PrivateDevices=true
ProtectHome=yes
ProtectHostname=yes
ProtectKernelLogs=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes
ProtectSystem=strict
RestrictAddressFamilies=AF_INET
RestrictNamespaces=true
RestrictRealtime=true

[Install]
WantedBy=multi-user.target
""" > "/run/systemd/system/${service}"

		/usr/bin/systemctl restart "$service"
	else
		/usr/bin/systemctl stop "$service" 2>/dev/null
	fi
}

# Return if an IP range exists on an interface or route
function ipExists(){
	local ip="$1"
	if ! echo "$ip" | /usr/bin/grep -q -E -- "^${IPV4_REGEX}${PREFIX_REGEX}?$" ; then
		echo 0
		return
	fi

	# If IP has host bit set, change the host bit down, so 10.5.0.1/24 -> 10.5.0.0/24
	# UPDATE to be generalized and not just 0 the last octet, OR set this in the python script to fix!
	ip_overlap $(/usr/bin/ip route show table all | /usr/bin/grep --only-matching -E "^${IPV4_REGEX}${PREFIX_REGEX}?" | /usr/bin/grep --invert-match -E "(0\.){3}0/1|128(\.0){3}/1") "$(lastOctet $ip 0)" && echo 1 && return
	echo 0
}

# Create a systemd path script to update an ipset with tor IPs
file=/etc/systemd/scripts/tor-ipset.sh
[[ ! -f $file ]] && echo '''#!/bin/bash
name="tor"
tmp_name="tor-temp"
file="/var/lib/tor/cached-microdesc-consensus"
IPV4_REGEX="([0-9]{1,3}\.){3}[0-9]{1,3}"

/usr/bin/ipset create "$name" hash:ip,port 2>/dev/null

/usr/bin/ipset create "$tmp_name" hash:ip,port
for ipport in $(/usr/bin/grep -- "^r " "$file" | /usr/bin/grep -Eo -- "${IPV4_REGEX} [0-9]{1,}" | /usr/bin/sed "s/ /,/"); do
    /usr/bin/ipset add "$tmp_name" $ipport -exist
done

# Add the directory port and the OR port
IFS=$'\''\n'\''
for ipport in $(/usr/bin/grep -- "^dir-source " "$file" | /usr/bin/grep -Eo -- "${IPV4_REGEX} [0-9]{1,} [0-9]{1,}"); do
    /usr/bin/ipset add "$tmp_name" $(echo $ipport | /usr/bin/awk '\''{print $1","$2}'\'') -exist
    /usr/bin/ipset add "$tmp_name" $(echo $ipport | /usr/bin/awk '\''{print $1","$3}'\'') -exist
done

/usr/bin/ipset swap "$tmp_name" "$name"
/usr/bin/ipset destroy "$tmp_name"''' > $file && chmod +x $file

file=/etc/systemd/system/tor-ipset.service
[[ ! -f $file ]] && echo '''[Unit]
Description=Add Tor IPs to ipset
[Service]
ExecStart=/etc/systemd/scripts/tor-ipset.sh''' > $file

file=/etc/systemd/system/tor-ipset.path
[[ ! -f $file ]] && echo '''[Unit]
Description=Monitor the tor consensus cache and update ipset
[Path]
PathModified=/var/lib/tor/cached-microdesc-consensus
Unit=tor-ipset.service
[Install]
WantedBy=multi-user.target''' > $file && /usr/bin/systemctl enable --now $(basename $file)

# Add or replace an entry in /etc/hosts
hostsEditScript=${TMP_DIR}/hostsEditScript.sh
[[ ! -f "$hostsEditScript" ]] && echo '''#!/bin/bash
IPV4_REGEX='"$IPV4_REGEX"'
add="$1"
name="$2"
ip="$3"
file="$4" ; [[ -z "$file" ]] && file="/etc/hosts"
reverse="$5"

privs=/usr/bin/sudo
# Do not need sudo privs/logs for this if already root
[[ $UID == 0 ]] && privs=""

if [[ 1 != $add ]]; then
	${privs} /usr/bin/sed -i "s/^\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\} ${name}$//;/^[[:space:]]*$/d" "$file"
	exit
fi

if [[ 1 != "$reverse" ]]; then
	if /usr/bin/grep -q -E -- "^${IPV4_REGEX} ${name}$" "$file" ; then
		${privs} /usr/bin/sed -i "s/^\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\} ${name}$/${ip} ${name}/" "$file"
	else
		${privs} /bin/bash -c "echo ${ip} ${name} >> $file"
	fi
else
	if /usr/bin/grep -q -E -- "^${name} ${IPV4_REGEX}$" "$file" ; then
		${privs} /usr/bin/sed -i "s/^${name} \([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}$/${name} ${ip}/" "$file"
	else
		${privs} /bin/bash -c "echo ${name} ${ip} >> $file"
	fi
fi''' | /usr/bin/sudo /usr/bin/tee "$hostsEditScript" 1>/dev/null && /usr/bin/sudo /usr/bin/chmod +rx "$hostsEditScript"

# This is needed in a delayed /usr/bin/sudo bash -c 'stuff && stuff ...', so it must be in a separate script to be called
removeAddScript=${TMP_DIR}/removeAddScript.sh
[[ ! -f "$removeAddScript" ]] && echo '''#!/bin/bash
removeAdd="$1"
line="$2" ; [[ -z "$line" ]] && exit
if [[ 1 == "$removeAdd" ]]; then
	echo "$line" >> "$3"
elif [[ -n "$removeAdd" ]]; then
 	/usr/bin/sed -i "s/^${line}$//g;/^[[:space:]]*$/d" "$3"
fi''' | /usr/bin/sudo /usr/bin/tee "$removeAddScript" 1>/dev/null && /usr/bin/sudo /usr/bin/chmod +rx "$removeAddScript"

# Generate an unused variable (mark,group,ip,or nsip) for this group. Basically a 1:1 map in a file
# TODO: Add the special logic to prevent creating a table that already exists!
function newVar(){
	local write="$1" # 1 is writing, != 1 is only reading
	local group="$2" ; [[ -z "$group" ]] && echo "ERROR: no group as a key given" && exit 1
	local file="$3" ; [[ ! -f "$file" ]] && /usr/bin/touch "$file" && /usr/bin/chmod 0666 "$file"
	local varRegex="$4"
	local available="$5"
	local prefix="" #Used when available is changed to a list of IP addresses

	local varsInFile=$(/usr/bin/awk '{print $2}' "$file" | tr '\n' ' ')

	# If $available is an ip prefix, update $available
	case "$available" in
		"/24")
			prefix="$available"
			available=$(for a in $(seq -s ' ' 3 250); do echo "10.${a}.0.1" ; done | tr '\n' ' ')
		;;
		"/30")
			prefix="$available"
			available=$(for a in $(seq -s ' ' 3 250); do echo "10.0.${a}.1" ; done | tr '\n' ' ')
		;;
	esac
	# echo $write $group $file $varRegex $prefix $varsInFile && exit
	
	# This $group is not in $file, so generate a new var that is not used already
	if ! /usr/bin/grep -q -E -- "^${group} ${varRegex}$" "$file" ; then
		if [[ 1 == "$write" ]]; then
			for i in $(echo "$available"); do
				# For IP: Check that i does not exist on system in routes or ifaces
				if [[ 0 == $(isIn "$i" "$varsInFile") && 0 == $(ipExists "${i}${prefix}") ]]; then
					"$hostsEditScript" 1 "$group" "$i" "$file" "1"
					echo "$i"
					return
				fi
			done
		fi
		echo "Error generating new var for ${file}, group ${group}. Wanted to read, but nothing was in ${file}."
		# echo $available
		# echo $varsInFile
		return

	# This $group is already in the file, so need to extract the var from it
	else
		local ret=$(grep -E "^${group} ${varRegex}$" "$file" | /usr/bin/cut -d' ' -f2)
		
		# If nothing available or meant to only read, then we don't care if the IP exists or not and don't want to check
		if [[ -z "$available" || 1 != "$write" ]]; then
			echo "$ret"
			return
		fi

		# IP in the file is not used on the system (or exists on a veth interface already for --namepsace with persist options) so return immediately (ipExists will return 0 if $ret is not an IP address)
		if [[ 0 == $(ipExists "${ret}${prefix}") || $(/usr/bin/ip address show veth-${group} | /usr/bin/grep "^[[:space:]]*inet") ]]; then
			echo "$ret"

		# Get a new IP from $available, make sure it is not in $file already or in use on the system
		# In use on the system MIGHT be OK if it is a "persist" option for --namespace
		else
			for i in $(echo "$available"); do
				# For IP: Check that i does not exist on system in routes or ifaces
				#(some other task in the meantime used that IP since last `ip addr del`)
				# Remove the used one from the file and replace with the fresh one
				if [[ 0 == $(isIn "$i" "$varsInFile") && 0 == $(ipExists "${i}${prefix}") ]]; then
					"$hostsEditScript" 1 "$group" "$i" "$file" "1"
					echo "$i"
					return
				fi
			done
			echo "Error getting new var from $file!"
			return
		fi
	fi
}

function createNetNS(){
	local start="$1"
	local namespace="$2"
	local vethIface="$3"
	local vethIfaceIP="$4"
	local vethIfaceNS="$5"
	local vethIfaceNSIP="$6"
	local metric="$7" ; [[ -z "$metric" ]] && metric="0"

	if [[ 1 == "$start" ]]; then
		if ! ip netns list | /usr/bin/awk '{print $1}' | /usr/bin/grep -q -- "^${namespace}$" ; then
			/usr/bin/ip netns add "$namespace"
		    /usr/bin/ip link add "$vethIface" type veth peer name "$vethIfaceNS"
		    /usr/bin/ip link set "$vethIfaceNS" netns "$namespace"
		    /usr/bin/ip netns exec "$namespace" /usr/bin/ip link set lo up
		    /usr/bin/ip netns exec "$namespace" /usr/bin/ip link set "$vethIfaceNS" up
		    /usr/bin/ip netns exec "$namespace" /usr/bin/ip address add "$vethIfaceNSIP"/30 dev "$vethIfaceNS"
		    /usr/bin/ip netns exec "$namespace" /usr/bin/ip route add default via "$vethIfaceIP" dev "$vethIfaceNS" src "$vethIfaceNSIP" metric "$metric"
		    /usr/bin/ip link set "$vethIface" up
		    /usr/bin/ip address add "$vethIfaceIP"/30 dev "$vethIface"
		else
			echo "NetNS ${namespace} already exists."
		fi
	else
		/usr/bin/ip netns delete "$namespace"

		/usr/bin/ip link delete "$vethIface"
	fi
}

# Create dnsmasq user as well as copy dnsmasq binary
function createUser(){
	/usr/bin/useradd --no-create-home --shell /usr/bin/nologin "$1" 2>/dev/null
}

function parseOpenvpnSyslog(){
	# Check if service is running, and if so scrape from journalctl, else return nothing since it is not running
	local syslog="$1"
	local service="$2"
	local stopOnError="$3" # Stop that service if failed to parse syslog?

	if ! /usr/bin/systemctl is-active --quiet "$service" ; then
		echo "Error OpenVPN service not running"
		exit 1
	fi

	# Loop until "Initializtion Sequence Completed"
	local timeout=60 # Wait a total of 60 seconds
	local sleep=.25  # Poll every .25 seconds
	local pid=
	# echo $(seq 1 $(echo "${timeout} / ${sleep}" | bc))
	# echo $service
	# echo $syslog
	# exit

	for i in $(seq 1 $(echo "${timeout} / ${sleep}" | bc)); do
		pid=$(journalctl --boot --unit "$service" --no-pager | /usr/bin/grep "]: Initialization Sequence Completed$" | /usr/bin/tail -1 | /usr/bin/sed "s/.* ${syslog}\[\([0-9]*\)\]: .*/\1/")
        [[ -n "$pid" && "$pid" =~ ^[0-9]*$ ]] && break
	    sleep "$sleep"
    done

    if [[ -n "$pid" ]]; then
    	# Most recent PUSH info and interface
    	local info=$(journalctl --boot --unit "$service" --no-pager --grep "PUSH: " | tail -1)
		local interface=$(journalctl --boot --unit "$service" --no-pager --grep "^TUN/TAP device [^ ]* opened$" | tail -1 | /usr/bin/awk -F ' ' '{print $(NF-1)}')
		local localIP=$(echo $info | /usr/bin/grep -oE "ifconfig ${IPV4_REGEX} ${IPV4_REGEX}" | /usr/bin/cut -d ' ' -f2)
		local prefix=$(ipcalc 0.0.0.0/$(echo $info | /usr/bin/grep -oE "ifconfig ${IPV4_REGEX} ${IPV4_REGEX}" | /usr/bin/cut -d ' ' -f3) | /usr/bin/grep "^Netmask: .* = [0-9]* " | /usr/bin/awk -F ' = ' '{print $2}' | /usr/bin/cut -d ' ' -f1)
		local gateway=$(echo $info | /usr/bin/grep -oE "route-gateway ${IPV4_REGEX}" | /usr/bin/cut -d ' ' -f2)
		local dns=$(echo $info | /usr/bin/grep -oE "dhcp-option DNS ${IPV4_REGEX}" | /usr/bin/cut -d ' ' -f3)

		echo "$interface" "$localIP" "$prefix" "$gateway" "$dns"
    else
		echo "ERROR: Unable to get Initialization Sequence Completed from OpenVPN"
		[[ 1 == "$stopOnError" ]] && /usr/bin/systemctl stop "$service"
		exit 1
    fi
}

function makeMarkers(){
	iptables -w -t mangle -N mangle-out-MARK 2>/dev/null
	if iptables -w -t mangle -N nontor-marker 2>/dev/null ; then
		iptables -w -t mangle -A nontor-marker ! -d 10.0.0.0/8 -j NFLOG --nflog-group "$NFLOGNONTORGROUP" --nflog-threshold 1 --nflog-prefix "X Tor "
		iptables -w -t mangle -A nontor-marker ! -d 192.168.0.0/16 -j NFLOG --nflog-group "$NFLOGNONTORGROUP" --nflog-threshold 1 --nflog-prefix "X Tor "
		iptables -w -t mangle -A nontor-marker -j MARK --set-mark "${config[NONTORMARK]}"
	fi
	if iptables -w -t mangle -N tor-marker 2>/dev/null ; then
		
		iptables -w -t mangle -A tor-marker -j NFLOG --nflog-group "$NFLOGTORGROUP" --nflog-threshold 1 --nflog-prefix "Tor "
		iptables -w -t mangle -A tor-marker -j MARK --set-mark "${config[TORMARK]}"
	fi
}

function protect(){
	# Safety rules added to filter and mangle. Openvpn starts. Real routing and blocking rules are added. Safety rules removed
	# They are added with -I so that they remove the DROP first to not drop anything while being removed
	PROTECT_RULES=(
	"OUTPUT -j DROP"
	"INPUT  -j DROP"

	"OUTPUT -j LOG --log-uid --log-prefix PROTECT-LEAK "
	"INPUT  -j LOG --log-uid --log-prefix PROTECT-LEAK "

	"OUTPUT -o lo -j ACCEPT"
	"INPUT  -i lo -j ACCEPT"

	# "OUTPUT -p udp --dport 123 -o ${config[LANInterface]} -s ${LANIP} -d $LANGateway -m owner --uid-owner systemd-timesync -m proc --path /usr/lib/systemd/systemd-timesyncd -j ACCEPT"
	# "INPUT  -p udp --sport 123 -i ${config[LANInterface]} -d ${LANIP} -s $LANGateway -j ACCEPT"

	"OUTPUT -o ${config[LANInterface]} -s ${LANIP} -d ${config[${DEFAULT_NAMESPACE}_fh_ip]} -p ${config[${DEFAULT_NAMESPACE}_fh_proto]} -j ACCEPT"
	"OUTPUT -o ${config[LANInterface]} -s ${LANIP} -d ${config[${DEFAULT_NAMESPACE}_fh_ip]} -p icmp -j ACCEPT"
	"INPUT  -i ${config[LANInterface]} -d ${LANIP} -s ${config[${DEFAULT_NAMESPACE}_fh_ip]} -p ${config[${DEFAULT_NAMESPACE}_fh_proto]} -j ACCEPT"
	"INPUT  -i ${config[LANInterface]} -d ${LANIP} -s ${config[${DEFAULT_NAMESPACE}_fh_ip]} -p icmp -j ACCEPT"
	)

	for table in "filter" "mangle" ; do
		for i in "${PROTECT_RULES[@]}" ; do
			if ! iptables -w $1 $i -t $table -m comment --comment "PROTECT_RULES"; then
				echo "Protective failed for $1 $i ! Exitting."
				exit 1
			fi
		done
	done
}

function add_safety() {
	#rules to protect tun dropping and routing table going over wlan0
	iptables -w -t "$1" -N safety
		ipset destroy safety-"$1"
		ipset create safety-"$1" hash:net
		ipset add safety-"$1" "${config[${DEFAULT_NAMESPACE}_fh_ip]}"
		ipset add safety-"$1" "$LANSubnet"
	iptables -w -t "$1" -A safety -m set --match-set safety-"$1" dst -j RETURN
	iptables -w -t "$1" -A safety -j LOG --log-prefix "SAFETY-$1 "
	iptables -w -t "$1" -A safety -j DROP
	iptables -w -t "$1" -A "$2" -o "${config[LANInterface]}" -j safety
}

# Dynamically return an ip route table that isn't in use yet. Count up from 0 looking for a table not in the routes yet.
function getTable() {
	declare -A in_use=()
	for table in $(/usr/bin/ip route list table all | /usr/bin/grep -o "table [^ ]* " | /usr/bin/cut -d' ' -f2 | uniq) ; do
		if [[ ! "$table" =~ ^[0-9]*$ ]]; then
			id=$(cat /etc/iproute2/rt_tables  | /usr/bin/grep "^[0-9]\{1,\}[[:space:]]*${table}$"  | /usr/bin/awk -F ' ' '{print $1}')
			in_use["$id"]=1
		else
			in_use["$table"]=1
		fi
	done
	ret=""
	for i in `seq 1 255` ; do
		if [[ -z "${in_use[${i}]}" ]]; then
			ret="$i"
			break
		fi
	done
	echo "$ret"
}

# Create 'ip rule' to send an fwmark over a table which routes that over an interface
function routeMarkInterface(){
	local start="$1"     # 1 to start, 0 to stop
	#local tableFile="$2" # File to store the table number in 
	local iface="$3"     #
	local gateway="$4"   # 
	local mark="$5"      # 
	local group="$6"
	local table=""

	if [[ -z "$table" ]]; then
		local reg="[0-9]{1,}"
		table=$(newVar "$start" "$group" "$TABLE_FILE" "$reg" "$(seq -s ' ' 3 250)")
		! echo "$table" | /usr/bin/grep -q -E -- "^${reg}$" && echo "$table" && exit 1
	fi

	if [[ 1 == "$start" ]]; then

		if [[ -z "$gateway" ]]; then
			echo "You should manually give a gateway address for 'ip route via $gateway ...' to be accurate!"
			gateway=$(/usr/bin/ip route show | /usr/bin/grep --only-matching -E "via ${IPV4_REGEX} dev ${iface}" | head -1 | /usr/bin/cut -d' ' -f2)
			if [[ -z "$gateway" ]]; then
				echo "Could not get gateway from a 'via IP dev IFACE'"
				gateway=$(lastOctet $(ifaceAddress ${iface}) "1")
			fi
			echo "Using $gateway as gateway address"
		fi

		/usr/bin/ip route add default via "$gateway" dev "$iface" table "$table"

		[[ -z "$(/usr/bin/ip rule list fwmark ${mark} table ${table})" ]] && /usr/bin/ip rule add fwmark "$mark" table "$table"
	else
		# if [[ ! -f "$tableFile" ]]; then
			# echo "State file $tableFile does not exist! I don't know what ip rule to delete. Check your routes manually!"
		# else
		# table="$(cat "$tableFile")"
		# /usr/bin/rm --force "$tableFile"
		/usr/bin/ip rule del fwmark "$mark" table "$table"
		/usr/bin/ip route flush table "$table"
		# fi
	fi
}

function createRoutedNetNS(){
	local start="$1"       # 1 for start, 0 for stop
	local routedIface="$2" # iface to route packets out of the netNS to
	local namespace="$3"   # name for the netNS
	local mark="$4"        # mark to use for routing
	local vethIface="$5"   # veth in default netNS
	local vethIfaceIP="$6" # IP for that veth
	local vethIfaceNS="$7" # veth in the netNS
	local vethIfaceNSIP="$8" # IP for that veth
	local comment="$9"     # comment for the iptables rules, helps to avoid some duplicates in caller function

	RULES=(
		# Route internet destined traffic (since local is covered by ip route table local)
		"PREROUTING -t mangle -i $vethIface -j MARK --set-mark $mark"
		# Allow traffic in and out
		"FORWARD -t filter -o ${vethIface} -i ${routedIface} -j ACCEPT"
		# -i and -o are needed in addition to --mark because if routedIface goes down, we do not want traffic exitting over whatever becomes the default iface
		"FORWARD -t filter -i ${vethIface} -o ${routedIface} -m mark --mark $mark -j ACCEPT"

		# MASQ out the LANInterface from the clean netNS veth
		# Could create option for SNAT, but this is only used for DNS at this point I think so its fine
		"POSTROUTING -t nat -o ${routedIface} -s ${vethIfaceNSIP} -m mark --mark $mark -j MASQUERADE"

		# Allow through the mangle safety
		"safety -t mangle -m mark --mark $mark -j RETURN"
	)

	if [[ 1 == "$start" ]]; then
		# Set the DNS for the namespace. No harm in doing this since it's not needed at the moment and could stop a leak later
        mkdir -p "/etc/netns/${namespace}/"
        echo "nameserver 127.0.0.1" > "/etc/netns/${namespace}/resolv.conf"
        chmod a+r "/etc/netns/${namespace}/resolv.conf"

		createNetNS "$start" "$namespace" "$vethIface" "$vethIfaceIP" "$vethIfaceNS" "$vethIfaceNSIP"

		for i in "${RULES[@]}" ; do iptables -w -C $i -m comment --comment $comment 2>/dev/null || iptables -w -I $i -m comment --comment $comment ; done

	else

		for i in "${RULES[@]}" ; do iptables -w -D $i -m comment --comment $comment 2>/dev/null; done

		createNetNS "$start" "$namespace" "$vethIface" "$vethIfaceIP" "$vethIfaceNS" "$vethIfaceNSIP"
	fi
}

# TODO: Make timer for failed to connect to vpn better
# Note the option ssX_use_default means route the tun into the default netNS socks proxy
function startStopNamespace(){
	local start="$1"
	local namespace="$2"
	[[ "$namespace" =~ "-" ]] && echo "Namespace name cannot contain dashes. Exitting." && exit 1

	# This option is meant to go along with the "pairX_persist" option, meant to keep the netNS and veth pair always up
	# So running with this option on creates the NS, then with it off will start or stop the VPN
	# If starting, don't start the VPN, but only create the namespace and veth pair.
	# If stopping, only stop the VPN and remove its rules, but don't tear down the namespace and veth pair
	local dontStartVPN="$3"
	local persist="$4" # This will keep netNS,veth pair, and rules/routes for it up when stopping
	local group="$namespace"

	# The predefined half of the veth pair moved into the namespace
	local vethIfaceNS="veth9999"
	local vethIfaceNSIP=${config[${group}_veth_nsip]}

	# Half of the veth pair that stays in the default namespace	
	local vethIface="veth-${namespace}"
	local vethIfaceIP=$(lastOctet $vethIfaceNSIP 2)

	# Used for vpn provider "$VPN2" to route its namespace via another namespace
	local otherNetNS="${config[${namespace}_route_via]}"
	local otherNetNSIface="${namespace}-${otherNetNS}"
	
	local rules= # Used at the bottom to create or delete iptables rules from the namespace

	setLANinfo

	[[ -z "$namespace" ]] && echo "Need to give a namespace string name for this" && exit 1
	if [[ 1 == "$start" ]]; then
		/usr/bin/grep -q -- "^namespace ${namespace}$" "$RUNNING_FILE" && echo "ERROR: Namespace ${namespace} looks already started" && exit 1 
		/usr/bin/systemctl is-active --quiet "${config[${group}_ov_service]}" && echo "ERROR: The openVPN service for namespace ${namespace} is already running" && exit 1
		/usr/bin/systemctl is-active --quiet "${config[${group}_ss_service]}" && echo "ERROR: The ss-local service for namespace ${namespace} is already running" && exit 1
		[[ 1 != "$persist" ]] && /usr/bin/ip netns list | /usr/bin/awk '{print $1}' | /usr/bin/grep -q -- "^${namespace}$" && echo "ERROR: The namespace for this group is already running" && exit 1
		[[ -n "${config[${namespace}_route_via]}" && "$DEFAULT_NAMESPACE" != "${config[${namespace}_route_via]}" ]] && ! /usr/bin/grep -q -- "^namespace ${config[${namespace}_route_via]}$" "$RUNNING_FILE" && echo "ERROR: Required netNS ${config[${namespace}_route_via]} is not active" && exit 1
	else
		[[ -n "${config[$(echo ${group} | tr '[:lower:]' '[:upper:]')_REQUIREDNS]}" ]] && /usr/bin/grep -q -- "^group ${group}$" "$RUNNING_FILE" && echo "ERROR: Group ${group} requires this namespace to run." && exit 1 
		# TODO: check that these processes are really only the ones started by startvpn.sh
		if [[ $(/usr/bin/lsns --type net | /usr/bin/awk '{if ($7 ==  "/run/netns/${namespace}") print $3}') -gt 2 ]]; then
			read -p "Some processes running in this netNS. Are you sure you want to stop it?" -n 1 -r
			echo ""
    		[[ ! "$REPLY" =~ ^[Yy]$ ]] && exit 0
    	fi
	fi

	startStopDnsmasq 0 "$namespace" "$vethIfaceNS"
	startStopDoH 0 "$namespace"

	local NAMESPACE_RULES
	# Set the route localnet and add the iptables rule to route to the port for vpn tha uses sslocal proxy
	if [[ "${config[VPN1]}" == "${config[${namespace}_vpn_provider]}" ]]; then
		if [[ 1 == "${config[${namespace}_ss_use_default]}" ]]; then
			# Need to load the info or default_ss_proto and lport, $2=0 means only read
			writeServicesForName 0 "${DEFAULT_NAMESPACE}"
			NAMESPACE_RULES=(
				"PREROUTING -t nat -i ${vethIface} -s ${vethIfaceNSIP} -d ${vethIfaceIP} -p ${config[${DEFAULT_NAMESPACE}_ss_proto]} --dport ${config[${DEFAULT_NAMESPACE}_ss_lport]} -j DNAT --to-destination 127.0.0.1"
			)
		else
			# Forward needs to ip checks because there were some UDP leaks. Not sure how with the routing, but it happened.
			NAMESPACE_RULES=( # Rules that go up before the VPN is active. Forward and route first hop ip to internet
				"FORWARD -t filter -o ${vethIface} -i ${config[LANInterface]} -s ${config[${namespace}_fh_ip]} -d ${vethIfaceNSIP} -j ACCEPT"
		        "FORWARD -t filter -o ${config[LANInterface]} -i ${vethIface} -s ${vethIfaceNSIP} -d ${config[${namespace}_fh_ip]} -j ACCEPT"
				"POSTROUTING -t nat -o ${config[LANInterface]} -d ${config[${namespace}_fh_ip]} -s ${vethIfaceNSIP} -j MASQUERADE"
			)
		fi
	fi
	# UPDATE this IP to be generated dynamically
	if [[ -n "${config[${namespace}_route_via]}" ]]; then
		writeServicesForName 0 "$otherNetNS"

		local netNSIP=10.0.0.1
		# Get vpn interface used in the other netNS
		local ROUTE_VIA_RULES=(
			"POSTROUTING -t nat    -o ${config[${otherNetNS}_ov_interface]}                       -s ${netNSIP} -j MASQUERADE"
			"FORWARD     -t filter -o ${config[${otherNetNS}_ov_interface]} -i ${otherNetNSIface} -s ${netNSIP} -j ACCEPT"
			"FORWARD     -t filter -i ${config[${otherNetNS}_ov_interface]} -o ${otherNetNSIface} -d ${netNSIP} -j ACCEPT"
		)
	fi

	# Restart the OpenVPN and bridge services
	if [[ 1 == "$start" ]]; then
		makeMarkers
		createNetNS "$start" "$namespace" "$vethIface" "$vethIfaceIP" "$vethIfaceNS" "$vethIfaceNSIP" "1024"
		/usr/bin/mkdir -p -- "/etc/netns/${namespace}/"
		echo "nameserver 127.0.0.1" > "/etc/netns/${namespace}/resolv.conf"
		/usr/bin/chmod a+r -- "/etc/netns/${namespace}/resolv.conf"
		
		# START PROMPTING SETUP HERE:
		# Create a pdlog-${name} setup in the namespace

		for i in "${NAMESPACE_RULES[@]}" ; do
			iptables -w -C $i -m comment --comment NS-${namespace} 2>/dev/null || iptables -w -I $i -m comment --comment NS-${namespace}
		done

		if [[ 1 != "$dontStartVPN" ]]; then # Start services with the net namespace created above

			# Allow DNS from the namespace and into to the namespace for the VPN inside it
			python "${config[DAEMON]}" --create "-s ${vethIfaceNSIP} -d ${vethIfaceIP} -p udp --dport 53 -i ${vethIface}" "/usr/bin/python3.11" "in" "allow" "0" "${DEFAULT_NAMESPACE}" "1"
			python "${config[DAEMON]}" --create "-d ${vethIfaceNSIP} -p udp --dport 1053 -m owner --uid-owner 0 -o ${vethIface}" "/usr/bin/python3.11" "out" "allow" "0" "${DEFAULT_NAMESPACE}" "1"

			case "${config[${namespace}_vpn_provider]}" in
				"${config[VPN1]}")
					if [[ -z "${config[${namespace}_route_via]}" ]]; then
						# Route the ss-local proxy bridge ip in the namespace
						# Allow the incoming connection from the netNS to default ss-local
						/usr/bin/sysctl -w net.ipv4.conf."${vethIface}".route_localnet=1
						
						if [[ 1 != "${config[${namespace}_ss_use_default]}" ]]; then
							/usr/bin/ip route add "${config[${namespace}_fh_ip]}" via "$LANGateway" dev "${config[LANInterface]}" metric 0 table main
							/usr/bin/ip netns exec "$namespace" /usr/bin/ip route add "${config[${namespace}_fh_ip]}" via "$vethIfaceIP" dev "$vethIfaceNS" metric 0 table main
						else
							python "${config[DAEMON]}" --create "-s ${vethIfaceNSIP} -d 127.0.0.1 -p ${config[${DEFAULT_NAMESPACE}_ss_proto]} --dport ${config[${DEFAULT_NAMESPACE}_ss_lport]}" "/usr/bin/ss-local" "in" "allow" "0" "${DEFAULT_NAMESPACE}" "1"
						fi
						# echo "${config[${namespace}_route_via]}"
					fi
				;;
				"${config[VPN2]}")
					echo -n
                ;;
            esac

			# Route this namespace's VPN via another: create a veth pair between them
			if [[ -n "${config[${namespace}_route_via]}" ]]; then
				/usr/bin/sysctl -w net.ipv4.conf."${vethIface}".route_localnet=1

				local otherNetNSIP=10.0.0.2
				
				local netNSIface="fh-tun"
				# Add the first hop tun to namespace, then its output in the route_via namespace
				# Normally, the veth-${namespace} would be forwarded, but not in the case of default namespace. So set $netNSIface pair to soemthing different that veth-${namespace}, which already exists in default
				# NAT the incoming from 10.0.0.1 to tun0 in the route_via netns in the FORWARD table
				# UPDATE HERE: ifname has to be at most 15 characters long
				/usr/bin/ip netns exec "$namespace" /usr/bin/ip link add "${netNSIface}" type veth peer name "${otherNetNSIface}"
    			/usr/bin/ip netns exec "$namespace" /usr/bin/ip address add "${netNSIP}"/30 dev "${netNSIface}"
			    /usr/bin/ip netns exec "$namespace" /usr/bin/ip link set "${netNSIface}" up
    	
    			# ip netns exec "$otherNetNS" ip link delete "veth-${namespace}" 2>/dev/null
				/usr/bin/ip netns exec "$namespace" /usr/bin/ip link set "${otherNetNSIface}" netns "$otherNetNS"
    			/usr/bin/ip netns exec "$otherNetNS" /usr/bin/ip address add "${otherNetNSIP}"/30 dev "${otherNetNSIface}"
    			/usr/bin/ip netns exec "$otherNetNS" /usr/bin/ip link set "${otherNetNSIface}" up

    			/usr/bin/ip netns exec "$namespace" /usr/bin/ip route add "${config[${group}_fh_ip]}" via "${otherNetNSIP}" dev "${netNSIface}" src "${netNSIP}" metric 0
    			# echo /usr/bin/ip route add "${config[${group}_fh_ip]}" via "${otherNetNSIP}" dev "${netNSIface}" src "${netNSIP}" metric 0

				for i in "${ROUTE_VIA_RULES[@]}" ; do
					/usr/bin/ip netns exec "$otherNetNS" iptables -w -C $i 2>/dev/null || /usr/bin/ip netns exec "$otherNetNS" iptables -w -I $i
				done
    			echo "${config[${namespace}_fh_ip]}"
			
			# Otherwise route it via the LANInterface, like with the sslocal above
			else
				echo "FINISH HERE"
			fi

			# Only need to start OpenVPN service for sslocal proxy vpn since services are connected
			restartServices "${config[${namespace}_ov_service]}"
		fi

		ret=$(parseOpenvpnSyslog "${config[${group}_ov_syslog]}" "${config[${group}_ov_service]}" "0")
		if [ $? -ne 0 ]; then
			echo $ret
			if [[ 1 == "$start" ]]; then # Tear down the stuff created
				start=0
			fi
		fi
		OVinterface=$(echo $ret | /usr/bin/cut -d' ' -f1)
		OVlocalIP=$(echo $ret | /usr/bin/cut -d' ' -f2)
		OVprefix=$(echo $ret | /usr/bin/cut -d' ' -f3)
		OVgateway=$(echo $ret | /usr/bin/cut -d' ' -f4)
		OVdns=$(echo $ret | /usr/bin/cut -d' ' -f5)
		echo $OVinterface $OVlocalIP $OVprefix $OVgateway $OVdns

		if [[ 1 == "$start" ]]; then # Could have been changed to stop the service if it failed to start
			if [[ 1 != "$dontStartVPN" ]]; then
				# Only load these rules if prompting for this namespace
				# Otherwise send DNS straight to VPN
				if [[ 1 == "${config[${namespace}_prompting]}" ]]; then
					# TODO: If this --namespace is required for a --group, then consider the group's options for CLEARDNS (to the VPN directly), DOH, or running socks in the namespace
					case "${config[${namespace}_dns]}" in
						doh)
							startStopDoH 1 "$namespace" "[ '${vethIfaceNSIP}:1053' ]"
						;;
						vpn|*)
							startStopDnsmasq 1 "$namespace" "$vethIfaceNS" "--port=1053 --server=${OVdns}#53 --bind-interfaces --except-interface=lo --no-resolv --bogus-priv --domain-needed --neg-ttl 180"
							# --log-facility=/var/log/prompt-and-split/netns-${namespace}-dnsmasq.log --log-queries"
						;;
					esac

					# Write hosts entry for prompter to send back into the netNS
					"$hostsEditScript" 1 "$namespace"-DNS "$vethIfaceNSIP"

					local RULES=(
						# MASQUERADE for connections coming into the netNS to exit via the VPN tunnel
						"POSTROUTING -t nat -o ${OVinterface} -s ${vethIfaceIP} -j MASQUERADE"
						# Instead of a forwarding DNS server process, NAT each packet to the default netNS where prompter.py runs. Added benefit of this is that it keeps the query's TID
						"POSTROUTING -t nat -o ${vethIfaceNS} -p udp --dport 53 -j SNAT --to-source ${vethIfaceNSIP}"
						"OUTPUT      -t nat -s 127.0.0.1      -p udp --dport 53 -j DNAT --to-destination ${vethIfaceIP}:53"
						"OUTPUT                               -p udp --dport 53 -m dns --watch-queries -j DROP"
					)
					for i in "${RULES[@]}" ; do
						/usr/bin/ip netns exec "$namespace" iptables -w -C $i 2>/dev/null || /usr/bin/ip netns exec "$namespace" iptables -w -A $i
					done

					# DNS packet NAT needs this
					/usr/bin/ip netns exec "$namespace" /bin/bash -c 'echo 1 > /proc/sys/net/ipv4/conf/veth9999/route_localnet'
				else
					echo "nameserver ${OVdns}" > "/etc/netns/${namespace}/resolv.conf"

					# For something only running a namespace, no default NS or main prompter, so DROP in FORWARD so that no leaks occur if openvpn drops and its routes drop
					iptables -w -t filter -P FORWARD DROP
				fi

				"$removeAddScript" 1 "namespace ${group}" "${RUNNING_FILE}"
			fi

			if [[ 1 == "${config[${namespace}_use_socks]}" ]]; then
				/usr/bin/ip netns exec "$namespace" iptables -w -t nat -A OUTPUT -s 127.0.0.1 -d 127.0.0.1 -o lo -p udp --dport 53 -m proc --path /usr/bin/microsocks -j DNAT --to-destination :1053
				/usr/bin/systemctl restart socks-proxy@"$namespace".service
				"$hostsEditScript" 1 "$namespace"-socks "${vethIfaceNSIP}"
			fi

			if [[ 1 == "${config[${namespace}_use_http_proxy]}" ]]; then
				# /usr/bin/ip netns exec "$namespace" iptables -w -t nat -D OUTPUT -s 127.0.0.1 -d 127.0.0.1 -o lo -p udp --dport 53 -m proc --path /usr/bin/microsocks -j DNAT --to-destination :1053
				startStopHttpProxy 1 "$namespace" "${vethIfaceNSIP}"
				# /usr/bin/systemctl restart tinyproxy@"$namespace".service
				"$hostsEditScript" 1 "$namespace"-http "${vethIfaceNSIP}"
			fi		
		fi
		"$CUSTOM_NOTIFY_SCRIPT" -u user "NetNS ${namespace}" "Started" -i /etc/my_icons/openvpn.png
	fi

	if [[ 1 != "$start" ]]; then

		if [[ 1 == "${config[${namespace}_use_http_proxy]}" ]]; then
			# "$hostsEditScript" 0 "$namespace"-socks "$vethNSIP"
			# /usr/bin/ip netns exec "$namespace" iptables -w -t nat -D OUTPUT -s 127.0.0.1 -d 127.0.0.1 -o lo -p udp --dport 53 -m proc --path /usr/bin/microsocks -j DNAT --to-destination :1053
			startStopHttpProxy 0 "$namespace" "${vethIfaceNSIP}"
			"$hostsEditScript" 0 "$namespace"-http "${vethIfaceNSIP}"
		fi

		if [[ 1 == "${config[${namespace}_use_socks]}" ]]; then
			"$hostsEditScript" 0 "$namespace"-socks "$vethNSIP"
			/usr/bin/ip netns exec "$namespace" iptables -w -t nat -D OUTPUT -s 127.0.0.1 -d 127.0.0.1 -o lo -p udp --dport 53 -m proc --path /usr/bin/microsocks -j DNAT --to-destination :1053
			/usr/bin/systemctl stop socks-proxy@"$namespace".service
		fi

		if [[ 1 != "$dontStartVPN" ]]; then
			/usr/bin/systemctl stop "${config[${namespace}_ov_service]}"
			/usr/bin/systemctl stop "${config[${namespace}_ss_service]}"
			# python "${config[DAEMON]}" --delete "-s ${vethIfaceNSIP} -d 127.0.0.1 -p udp --dport 53" "/usr/bin/python3.11" "in" "allow" "0" "default" "1"

			if [[ 1 == "${config[${namespace}_prompting]}" ]]; then
				"$hostsEditScript" 0 "$namespace"-DNS
			fi

			if [[ 1 == "${config[${group}_ss_use_default]}" ]]; then
				/usr/bin/python "${config[DAEMON]}" --delete "-s ${vethIfaceNSIP} -d 127.0.0.1 -p ${config[${DEFAULT_NAMESPACE}_ss_proto]} --dport ${config[${DEFAULT_NAMESPACE}_ss_lport]}" "/usr/bin/ss-local" "in" "allow" "0" "${DEFAULT_NAMESPACE}" "1"
			else
				/usr/bin/systemctl stop "${config[${namespace}_ss_service]}"
			fi

			if [[ -n "${config[${namespace}_route_via]}" ]]; then
				/usr/bin/ip netns exec "$otherNetNS" /usr/bin/ip link delete "$otherNetNSIface"
				for i in "${ROUTE_VIA_RULES[@]}" ; do
					/usr/bin/ip netns exec "$otherNetNS" iptables -w -D $i
				done
			fi
		fi

		if [[ 1 != "$persist" ]]; then # No not persist the existence of the netns and its routing
			for i in "${NAMESPACE_RULES[@]}" ; do iptables -w -D $i -m comment --comment NS-${namespace} ; done

			createNetNS "$start" "$namespace" "$vethIface"
		fi
		"$removeAddScript" 0 "namespace ${namespace}" $RUNNING_FILE

		"$CUSTOM_NOTIFY_SCRIPT" -u user "NetNS ${namespace}" "Stopped" -i /etc/my_icons/openvpn-off.png
	fi

	# UPDATE THIS
	# Run some iptables commands in the namespace, depending on the group
	# while IFS= read -r line; do
	# 	if [[ $group == $(echo $line | /usr/bin/awk -F' ' '{print $1}') ]]; then
	# 		line=$(echo $line | /usr/bin/awk -F' ' '{$1=""; print $0}')
	# 		eval ip netns exec "$namespace" iptables "$rules" $line -m comment --comment "from-startvpn.sh"
	# 	fi
	# done < "${CONFIG_DIR}/startvpn.namespaces"
}

function createBridge(){
	if [[ 1 == "$1" ]]; then
		# UPDATE HERE to send 1 to null as well
		if ! /usr/bin/ip link show "$2" 2>/dev/null ; then
			ip link add "$2" type bridge
			ip link set dev "$2" multicast off up
		fi
	else
		# Delete the bridge, but first delete all the taps that have it as their master
		for tap in $(/usr/bin/ip link show | /usr/bin/grep " master $2 " | /usr/bin/cut -d' ' -f2 | /usr/bin/rev | /usr/bin/cut -c2- | rev) ; do
			ip tuntap del "$tap" mode tap
		done
		ip link delete "$2" type bridge
	fi
}

function createIW(){
	# Note that card may not support WDS, so 'set 4addr on' for virtual interface means ensalving it to master bridge won't work
	# https://wiki.debian.org/BridgeNetworkConnectionsProxyArp
	# Basically depending on hardware, sometimes a bridged WiFi and ethernet won't work because of different frame packet types 
	if [[ 1 == "$1" ]]; then
		iw dev "$2" interface add "$3" type managed addr $(random_macaddr) #4addr on
		[[ 0 != $? ]] && exit $?
	else
		iw dev "$2" del
	fi
}

# function preTorRules(){
# 	# Rules to route incoming traffic on iface (from VMs on tap or containers on veth) to the Tor port
# 	# Use a PDLOG group (like the default one for the system) so that packets are prompted first
# 	local iface="$1"
# 	local command="$2"
# 	local group="$3"
# 	local namespace="$4"
# 	local command2="-I" ; [[ "$command" == "-D" ]] && command2="-D"
# 	TAP_CHAINS=(
# 		"${iface}-pre-MARK"
# 		"${iface}-pdlog"
# 		"${CHAIN_PREFIX}${group}-in"
# 	)
# 	# Mangle ${iface}-pdlog will do prompting for the packets. Nat will DNAT allowed ones to Tor
# 	TAP_RULES_INSERT=(
# 		"PREROUTING -t mangle -i $iface -j ${iface}-pdlog"
# 		"PREROUTING -t nat -m mark --mark ${config[TAPTOR_MARK]} -i $iface -p tcp -j DNAT --to-destination ${config[TRANSPROXYIP]}:${config[TRANSPROXYPORT]}"
# 	)
# 	TAP_RULES=(
# 		"${iface}-pre-MARK -t mangle -d 10.0.0.0/8,192.168.0.0/16,169.254.0.0/16,224.0.0.0/4,172.16.0.0/12 -j nontor-marker"
# 		"${iface}-pre-MARK -t mangle -m mark --mark ${config[NONTORMARK]} -j RETURN"
#         "${iface}-pre-MARK -t mangle -p tcp -j tor-marker"
# 	"${iface}-pdlog -t mangle -m connmark ! --mark 0 -j ACCEPT"
# 	"${iface}-pdlog -t mangle -j ${iface}-pre-MARK"
# 		"${CHAIN_PREFIX}${group}-in -t mangle -m connmark ! --mark 0 -j ACCEPT -m comment --comment top"
# 		"${CHAIN_PREFIX}${group}-in -t mangle -m connmark ! --mark 0 -j ACCEPT"
# 		"${CHAIN_PREFIX}${group}-in -t mangle -j PDLOG --log-uid --log-level $group"
# 	"${iface}-pdlog -t mangle -j ${CHAIN_PREFIX}${group}-in"
# 	"${iface}-pdlog -t mangle -j DROP"
# 	)
# 	if [[ "$command" != "-D" ]]; then for i in "${TAP_CHAINS[@]}" ; do iptables -t mangle -N $i ; done ; fi
# 	for i in "${TAP_RULES_INSERT[@]}" ; do iptables -t mangle $command2 $i -m comment --comment ${namespace} ; done
# 	for i in "${TAP_RULES[@]}" ; do        iptables -t mangle $command  $i -m comment --comment ${namespace} ; done
# 	if [[ "$command" == "-D" ]]; then
# 		for i in "${TAP_CHAINS[@]}" ; do iptables -t mangle -F $i ; iptables -t mangle -X $i ; done
# 	else
# 		echo reloading rules for $group
# 		python "${config[DAEMON]}" --reload-rules "$group"
# 	fi
# }

# Route incoming from a tap, like ones VMs use, to another interface, like wlan0 or tunnel to a server
# Prompting and a DoH server in another netNS
function ifaceRoute(){
	local start="$1"       # 1 to start, 0 to stop
	local iface="$2"       # incoming iface, likely a level 2 tap
	local routedIface="$3" # iface to NAT that tap to
	local group="$4"       # group name
	local SNAT="$5"        # 1 to use SNAT in nat POSTROUTING, caclulated from `ip link show | grep inet`

	# These do not have to be given if group name is given	
	local ifaceIP="$6"     # IP for tap iface and dnsmasq to listen on. dnsmasq upstreams DNS to localhost:53, which will prompt and upstream into the namespace
	local mark="$7"        # mark to use to route packets for `ip rule`. Should be unique!
	local groupNum="$8"    # number for the group to pass to pdlog log-level
	local gatewayIP="$9"   # IP for the default route of the route table for the tap iface
	local useNS="${10}"    # 0 means do not create the namespace, only the tap. Still need to 
		local namespace="${11}" # netNS created for DoH server and anything else manual. Name used for other stuff, like dnsmasq username
		local vethIP="${12}"    # IP for the veth in the default netNS
		local vethNSIP="${13}"  # IP for the veth in the namespace and what the DoH server in there listens on
	local removed="${14}"
	local useDoH="${15}"
	local useClearDNS="${16}"

	local comment="${group}"
	local iprouteTableFile="${TMP_DIR}/netNS-${group}-iprouteTable"

	if [[ 1 == "$start" ]] && [[ ! $(/usr/bin/ip link show $routedIface) || ! $(/usr/bin/ip link show $iface) ]]; then
		echo "Make sure both interfaces exist! Exitting."
		exit 1
	fi

	# mark 1 is NONTORMARK for system traffic, mark 2 is TORMARK for system traffic
	local reg=""
	# skip group 1 and 2 just to keep things in line with mark (although it doesn't have to be at all)
	if [[ -z "$ifaceIP" ]]; then
		reg="10\.([0-9]{1,3}\.){2}1"
		ifaceIP=$(newVar "$start" "$group" "$IP_FILE" "$reg" "/24")
		! echo "$ifaceIP" | /usr/bin/grep -q -E -- "^${reg}$" && echo "$ifaceIP" && exit 1
	fi

	if [[ -z "$mark" ]]; then
		reg="[0-9]{1,}"
		mark=$(newVar "$start" "$group" "$MARK_FILE" "$reg" "$(seq -s ' ' 3 250)")
		! echo "$mark" | /usr/bin/grep -q -E -- "^${reg}$" && echo "$mark" && exit 1
	fi

	if [[ -z "$groupNum" ]]; then
		reg="[0-9]{1,}"
		groupNum=$(newVar "$start" "$group" "$GROUP_FILE" "$reg" "$(seq -s ' ' 3 250)")
		! echo "$groupNum" | /usr/bin/grep -q -E -- "^${reg}$" && echo "$groupNum" && exit 1
	fi

	if [[ 1 == "$useNS" ]]; then
		if [[ -z "$vethIP" ]]; then
			reg="10\.([0-9]{1,3}\.){2}1"
			vethIP=$(newVar "$start" "$group" "$NSIP_FILE" "$reg" "/30")
			! echo "$vethIP" | /usr/bin/grep -q -E -- "^${reg}$" && echo "$vethIP" && exit 1
		fi
		GLOBAL[VETHIP]=$vethIP

		if [[ -z "$vethNSIP" ]]; then
			vethNSIP=$(lastOctet "$vethIP" 2)
		fi
		GLOBAL[VETHNSIP]=$vethNSIP

		if [[ -z "$namespace" ]]; then
			namespace="$group"
		fi

		createRoutedNetNS "$start" "$routedIface" "$namespace" "$mark" "veth-${namespace}" "$vethIP" "veth9999" "$vethNSIP" "$comment"
	fi

	local CHAINS=( "${iface}-pdlog" "${CHAIN_PREFIX}${group}-in" "${CHAIN_PREFIX}${group}-out" )
	local RULES=(
		# Allow the forwarding, mark for outgoing, iif for incoming packets
		"FORWARD -t filter -i ${routedIface} -o ${iface} -j ACCEPT"
		"FORWARD -t filter -m mark --mark ${mark} -j ACCEPT"

		# Allow through the mangle safety
		"safety -t mangle -m mark --mark ${mark} -s ${ifaceIP}/24 -o ${routedIface} -j RETURN"

		# mangle first checks pdlog prompter, then if allowed, marks the packet to be clean over clearnet
		"PREROUTING -t mangle -i ${iface} -j ${iface}-pdlog"

			# First group != 0 has to accept mark != 0 as first in ${CHAIN_PREFIX}${group}-in so that ssh locally in works
			"${CHAIN_PREFIX}${group}-in -t mangle -j PDLOG --log-uid --log-level ${groupNum}"
			"${CHAIN_PREFIX}${group}-in -t mangle -m connmark ! --mark 0 -j ACCEPT"
			"${CHAIN_PREFIX}${group}-in -t mangle -m connmark ! --mark 0 -j ACCEPT -m comment --comment top"
		"${iface}-pdlog -t mangle -j DROP"
		"${iface}-pdlog -t mangle -j ${CHAIN_PREFIX}${group}-in"
		"${iface}-pdlog -t mangle -j MARK --set-mark ${mark}"

		# Add a mangle output check so pdlog-wlan0-out works
		# Don't need to set mark because output should be routed properly to begin with based on the interface
		# Adding a DROP at the bottom of the pdlog-wlan0-out because anything at the end won't matter to the strict order of how rules are added
			"${CHAIN_PREFIX}${group}-out -t mangle -j DROP"
			"${CHAIN_PREFIX}${group}-out -t mangle -j PDLOG --log-uid --log-level ${groupNum}"
			"${CHAIN_PREFIX}${group}-out -t mangle -m connmark ! --mark 0 -j ACCEPT"
			# "${CHAIN_PREFIX}${group}-out -t mangle -m connmark ! --mark 0 -j ACCEPT -m comment --comment top"
		# This rule is special because -I takes index, but -D does not
		"OUTPUT 4 -t mangle -o ${iface} -j ${CHAIN_PREFIX}${group}-out"

		# MASQ out the LANInterface from the VMs on the incoming iface. Target is set RIGHT BELOW to MASQ or SNAT
		"POSTROUTING -t nat -o ${routedIface} -s ${ifaceIP}/24 -m mark --mark ${mark} -j"
	)

	if [[ 1 == "$SNAT" ]]; then
		RULES[-1]="$(echo "${RULES[-1]} SNAT --to-source $(ifaceAddress ${routedIface})")"
	else
		RULES[-1]="$(echo "${RULES[-1]} MASQUERADE")"
	fi

	if [[ 1 == "$start" ]]; then
		
		routeMarkInterface "$start" "$iprouteTableFile" "$routedIface" "$gatewayIP" "$mark" "$group"

		for i in "${CHAINS[@]}" ; do iptables -w -t mangle -N $i 2>/dev/null ; done
		for i in "${RULES[@]}" ; do
			iptables -w -C $i -m comment --comment $comment 2>/dev/null || iptables -w -I $i -m comment --comment $comment
		done

		if [[ 1 == "$useNS" ]]; then
			# UPDATE: Should check that the user is not doing clearDNS and DoH
			if [[ 1 == "$useDoH" ]]; then
				startStopDoH 1 "$namespace" "[ '${vethNSIP}:1053' ]"
			fi

			if [[ 1 == "$useClearDNS" ]]; then
				# Potentially get the upstream DNS server from the journalctl for the VPN service
				local upstream="${config[$(echo ${group} | tr '[:lower:]' '[:upper:]')_CLEARDNSUPSTREAM]}"
				if [[ -z "$upstream" ]]; then
					local requirednetns="${config[$(echo ${group} | tr '[:lower:]' '[:upper:]')_REQUIREDNS]}"
					local interface="${config[$(echo ${group} | tr '[:lower:]' '[:upper:]')_ARG]}"
					[[ -n $requirednetns ]] && upstream="$(/usr/bin/journalctl -b -t ${config[${requirednetns}_vpn_provider]}-${requirednetns} --grep ^PUSH: --no-pager | /usr/bin/grep -oE "DNS ${IPV4_REGEX}" | /usr/bin/cut -d ' ' -f2 | tail -1)"
					[[ -z $upstream ]] && upstream=$(/usr/bin/networkctl status "$interface" --no-pager --lines 0 | /usr/bin/grep "Gateway:" | /usr/bin/awk '{print $2}')
					[[ -z $upstream ]] && upstream=$(lastOctet $(/usr/bin/ip address show "$interface" | /usr/bin/grep "^[[:space:]]*inet " | /usr/bin/awk -F ' ' '{print $2}' | /usr/bin/cut -d'/' -f1) 1)
				fi
				startStopDnsmasq 1 "$namespace" "veth9999,lo" "--port=1053 --server=${upstream} --no-resolv --no-hosts --bogus-priv --domain-needed --neg-ttl 180"
				# --log-facility=/var/log/prompt-and-split/dnsmasq-${group}-NS.log --log-queries"
			fi

			if [[ 1 == "$useDoH" || 1 == "$useClearDNS" ]]; then
				local RULES=(
					# Instead of a forwarding DNS server process, NAT each packet to the default netNS where prompter.py runs. Added benefit of this is that it keeps the query's TID
					"POSTROUTING -t nat -o veth9999       -p udp --dport 53 -j SNAT --to-source ${vethNSIP}"
					"OUTPUT      -t nat -s 127.0.0.1      -p udp --dport 53 -j DNAT --to-destination ${vethIP}:53"
					"OUTPUT                               -p udp --dport 53 -m dns --watch-queries -j DROP"
				)
				for i in "${RULES[@]}" ; do
					/usr/bin/ip netns exec "$namespace" iptables -w -C $i 2>/dev/null || /usr/bin/ip netns exec "$namespace" iptables -w -A $i
				done
				
				/usr/bin/ip netns exec "$namespace" /bin/bash -c 'echo 1 > /proc/sys/net/ipv4/conf/veth9999/route_localnet'
				
				"$hostsEditScript" 1 "$group"-DNS "$vethNSIP"
			fi
		fi

	else
		if [[ 1 == "$useNS" ]]; then
			if [[ 1 == "$useDoH" ]]; then
				startStopDoH 0 "$namespace"
			fi
			if [[ 1 == "$useClearDNS" ]]; then
				startStopDnsmasq 0 "$namespace" "veth9999,lo"
			fi
			if [[ 1 == "$useDoH" || 1 == "$useClearDNS" ]]; then
				"$hostsEditScript" 0 "$group"-DNS
			fi
		fi

		iptables -w -D OUTPUT -t mangle -o ${iface} -j ${CHAIN_PREFIX}${group}-out -m comment --comment $comment

		for i in "${RULES[@]}" ; do iptables -w -D $i -m comment --comment $comment 2>/dev/null; done
		for i in "${CHAINS[@]}" ; do iptables -w -t mangle -F $i ; iptables -w -t mangle -X $i 2>/dev/null; done

		routeMarkInterface "$start" "$iprouteTableFile" "$routedIface" "$gatewayIP" "$mark" "$group"
	fi
}

function startStopGroup(){
	local start="$1"
	local group="$(echo $2 | tr '[:upper:]' '[:lower:]')"
	[[ "$group" =~ "-" ]] && echo "Group name cannot contain dashes. Exitting." && exit 1

	local iface="br-${group}"
	local strNS=$(echo ${group} | tr '[:lower:]' '[:upper:]')

	if [[ 1 == "$start" ]]; then
		# This area is too run too soon when CPU gov is on performance for interface to show up
		# TODO: spin for 5 seconds until iface shows up
        sleep 5
		! /usr/bin/ip link show dev "${config[${strNS}_ARG]}" 1>/dev/null && exit 1
		[[ -n ${config[${strNS}_REQUIREDNS]} ]] && ! /usr/bin/grep -q -- "^namespace ${config[${strNS}_REQUIREDNS]}" "$RUNNING_FILE" && echo "Group ${group} required --namespace ${config[${strNS}_REQUIREDNS]}" && exit 1
		/usr/bin/grep -q -- "^group ${group}$" "$RUNNING_FILE" && echo "Group ${group} looks already started" && exit 1
		/usr/bin/ip link show dev "$iface" 2>/dev/null && echo "Iface $iface already exists. Returning..." && exit 1
	else
		# Error here when stopping a group when there are no vars in the tmp dir
		# ! /usr/bin/grep "^group ${group}$" $RUNNING_FILE 1>/dev/null && echo "Group ${group} doesn't look started" && return

		# Simple check to prompt for stopping with a running VM apparently using the incoming interface
		if [[ 1 != $FORCE ]]; then
			for tap in $(/usr/bin/ip link show | /usr/bin/grep " master ${iface} " | /usr/bin/cut -d' ' -f2 | /usr/bin/rev | /usr/bin/cut -c2- | rev) ; do
				if /usr/bin/ps -C qemu-system-x86_64 -o args | /usr/bin/grep "ifname=${tap}[ ,]" ; then
					read -p "Are you sure you want to stop group ${group}? It looks like a VM might be using bridge ${iface}. " -n 1 -r
					echo ""
		    		[[ ! "$REPLY" =~ ^[Yy]$ ]] && exit 0
		    		break
				fi
			done
			# Stop hostapd running off this group
			if /usr/bin/grep -q -- "^hostapd ${group}$" "$RUNNING_FILE" ; then
				read -p "Are you sure you want to stop group ${group}? It looks like hostapd might be using it and will be stopped. " -n 1 -r
				echo ""
	    		[[ ! "$REPLY" =~ ^[Yy]$ ]] && exit 0
	    		startStopHostapd 0 "$group" "$group"_ap
			fi
		fi
	fi

	# UPDATE:
	# Because there is not a check that IP existing on system belongs to the interface being deleted, need to set
	# available to "" if 1 != "$start" or a new var will be written to the file for IPs, since they exist when being deleted.
	local reg="10\.([0-9]{1,3}\.){2}1"
	local ip=$(newVar "$start" "$group" "$IP_FILE" "$reg" "/24")
	! echo "$ip" | /usr/bin/grep -q -E -- "^${reg}$" && echo "Error with ${ip}" && return

	startStopDnsmasq 0 "default" "${iface},${group}_ap" 2>/dev/null

	if [[ 1 == "$start" ]]; then
		# Write the 10.2.0.0 (for /24), 10.2.0.1 (for /32), to /etc/hosts so that they can be picked up by prompter.py when reading and writing rules
		"$hostsEditScript" 1 "${group}"        "$(lastOctet "$ip" 0)"
		"$hostsEditScript" 1 "${group}"-router "$ip"

		createBridge "1" "$iface"

		/usr/bin/ip address add "$ip"/25 dev "$iface"
	fi

	[[ 1 == "$debug" ]] && echo "startStopGroup pre ifaceRoute"
	local gateway=""
	ifaceRoute \
"$start" \
"$iface" \
"${config[${strNS}_ARG]}" \
"$group" \
"1" \
"$ip" \
"" \
"" \
"$gateway" \
"${config[${strNS}_USENS]}" \
	"" \
	"" \
	"" \
	"" \
"${config[${strNS}_USEDOH]}" \
"${config[${strNS}_USECLEARDNS]}"
	[[ 1 == "$debug" ]] && echo "startStopGroup post ifaceRoute"

	local dnsmasqDHCPHostsfile="${TMP_DIR}/dhcp-${group}.conf"
	if [[ 1 == "$start" ]]; then
		/usr/bin/rm --force "$dnsmasqDHCPHostsfile" # Delete before start in case group did not stop previously (machine shutdown)
		
		# Change the IP addresses in the dnsmasq DHCP files so they are updated to the new IP scheme for when the persisten rules may be added
		# /usr/bin/sed -i "s/\([^,]*\),\(\([0-9]\{1,3\}\.\)\{3\}\)\([0-9]\{1,3\}\),\(.*\)/\1,$(lastOctet "$ip")\4,\5/g" "$dnsmasqDHCPHostsfile"
		# Add all these IPs to /etc/hosts
		# while IFS= read -r line; do
			# if [[ $line =~ ^[[:space:]]*# || $line == "" ]]; then continue ; fi
			# "$hostsEditScript" 1 $(echo $line | /usr/bin/awk -v g="$group" -F',' '{print $3"-"g" "$2}')
		# done < "$dnsmasqDHCPHostsfile"

		# local dnsmasqDHCPLeasesfile="${CONFIG_DIR}/dnsmasq.d/leases-${group}.leases"

		local useDHCP="${config[${strNS}_USEDHCP]}"
		#--dhcp-leasefile="$dnsmasqDHCPLeasesfile" \

		# User created rules get loaded into the chain for this group
		python "${config[DAEMON]}" --reload-rules "$group"
		
		if [[ 1 == "$useDHCP" ]]; then
			# local dnsString="--listen-address=${ip} --server=${config[DNSIP]}#${config[DNSPORT]} --no-resolv --no-hosts --bogus-priv --domain-needed --neg-ttl 180 --log-queries"
			# [[ 1 != "$useDNS" ]] && dnsString="--port 0"

			# Use first 126 IPs for local VMs on the bridge interface and reserve last 126 for devices on the hostapd access point interface
			local dhcpString="--no-ping --leasefile-ro --dhcp-hostsfile="$dnsmasqDHCPHostsfile" --dhcp-alternate-port=67,68 --log-dhcp"
			dhcpString="${dhcpString} \
			--dhcp-range=${iface},$(lastOctet $ip 2),$(lastOctet $ip 127),255.255.255.128 \
			--dhcp-option=${iface},6,$ip \
			--dhcp-range=${group}_ap,$(lastOctet $ip 130),$(lastOctet $ip 254),255.255.255.128 \
			--dhcp-option=${group}_ap,6,$ip"

			[[ 1 != "$useDHCP" ]] && dhcpString=

			# These appear to do nothing!
			# python "${config[DAEMON]}" --create "-m comment --comment until\ restart" "any" "out" "deny" "0" "$group" "1"
			# python "${config[DAEMON]}" --create "-d 224.0.0.22/32 -p igmp -m comment --comment until\ restart" "(no path)" "out" "deny" "0" "$group" "1"
			# python "${config[DAEMON]}" --create "-d 224.0.0.22/32 -p igmp -m comment --comment until\ restart" "any" "out" "deny" "0" "$group" "1"
			# python "${config[DAEMON]}" --create "-d 224.0.0.22/32 -m comment --comment until\ restart" "any" "out" "deny" "0" "$group" "1"
			# python "${config[DAEMON]}" --create "-p igmp -m comment --comment until\ restart" "any" "out" "deny" "0" "$group" "1"
			# echo about to start dnsmasq in 10 seconds
			# sleep 10
			# Daemon will not be able to create the file without 777 on /var/log/prompt-and-split/ so create it and 777 the file
			# local dhcplog="/var/log/prompt-and-split/dnsmasq-${group}.log"
			# touch "$dhcplog"
			# chmod 666 "$dhcplog"

			# touch "$dnsmasqDHCPHostsfile"
			# chmod 666 "$dnsmasqDHCPHostsfile"

			# This needs to be able to read the dhcp file. Is this grouping a security risk? UPDATE to ideally fix that
			startStopDnsmasq 1 "default" "${iface},${group}_ap" "--except-interface=lo --bind-dynamic --port 0 ${dhcpString}" "prompt-and-split"
			# --log-facility=${dhcplog} 
		fi

		# May be creating group after NS, so no need for this rule
		[[ -n ${GLOBAL[VETHNSIP]} ]] && python "${config[DAEMON]}" --create "-d ${GLOBAL[VETHNSIP]} -p udp --dport 1053 -m owner --uid-owner 0 -o veth-${group}" "/usr/bin/python3.11" "out" "allow" "0" "${DEFAULT_NAMESPACE}" "1"

		"$removeAddScript" 1 "group ${group}" $RUNNING_FILE

		if [[ 1 == "$UPDATE_ORPHANED_RUNNING_VMS" ]]; then
			for tap in $(/usr/bin/ip link show | /usr/bin/awk '$2 ~ "^qemu-" {print $0}' | /usr/bin/cut -d' ' -f2) ; do
				if /usr/bin/ps -C qemu-system-x86_64 -o args | /usr/bin/grep -q -- "ifname=${tap}[ ,]" ; then
					echo
					# read -p "Are you sure you want to stop group ${group}? It looks like a VM might be using bridge ${iface}. " -n 1 -r
					# echo ""
		    		# [[ ! "$REPLY" =~ ^[Yy]$ ]] && exit 0
		    		# break
				fi
			done
		fi
	else
		[[ -n ${GLOBAL[VETHNSIP]} ]] && python "${config[DAEMON]}" --delete "-d ${GLOBAL[VETHNSIP]} -p udp --dport 1053 -m owner --uid-owner 0 -o veth-${group}" "/usr/bin/python3.11" "out" "allow" "0" "${DEFAULT_NAMESPACE}" "1"
		# python "${config[DAEMON]}" --delete "-d 224.0.0.22/32 -p igmp -m comment --comment until\ restart" "(no path)" "out" "deny" "0" "$group" "1"

		# Clear /etc/hosts based on hosts file, then clear hosts file since IPs are generated dynamically
		[[ -f "$dnsmasqDHCPHostsfile" ]] && while IFS= read -r line; do
			[[ $line =~ ^[[:space:]]*# || -z $line ]] && continue
		 	/usr/bin/sed -i "s/^[^ ]\{1,\} $(echo $line | /usr/bin/awk -F ',' '{print $3}')$//g;/^[[:space:]]*$/d" /etc/hosts
		done < "$dnsmasqDHCPHostsfile"

		/usr/bin/rm --force "$dnsmasqDHCPHostsfile"

		"$hostsEditScript" 0 "${group}"
		"$hostsEditScript" 0 "${group}"-router

		createBridge "0" "$iface"

		# Delete the leftover group chains as well. The clearZeroRefChains exit function will clear any chains pointed to from these
		# for i in "${CHAIN_PREFIX}${group}-in" "${CHAIN_PREFIX}${group}-out" ; do iptables -w -t mangle -F $i ; iptables -w -t mangle -X $i ; done
		# Remove the IP addresses from the dnsmasq DHCP files so they are not persistent for the next time VMs are created
		# Write 0.0.0.0 to hosts file or remove them completely?
#		/usr/bin/rm --force "$TMP_DIR"/dnsmasq.d/leases-${group}.leases
#		/usr/bin/rm --force "$TMP_DIR"/dnsmasq.d/dhcp-${group}.conf

		"$removeAddScript" 0 "group ${group}" $RUNNING_FILE
	fi
}

# Start or stop hostapd routing to an interface. The hostapd interface uses x.y.z.128/25
function startStopHostapd(){
	local start="$1"
	local group="$2"
	local iface="$3"

	local hostapdPidFile="${TMP_DIR}/pid.hostapd-${group}-${iface}"
	local hostapdConfigFile="${CONFIG_DIR}/configs/hostapd.d/${group}.conf"

	# Get the router ip for this group and set the created virtual interface to lastoctet 2 of that
	local reg="10\.([0-9]{1,3}\.){2}1"
	local ip=$(newVar "0" "$group" "$IP_FILE" "$reg" "")
	! echo "$ip" | /usr/bin/grep -q -E -- "^${reg}$" && echo "$ip" && exit 1

	# Start hostapd on that incoming hostapd iface
	if [[ 1 == "$start" ]]; then
		if /usr/bin/grep -q --"^hostapd ${group}$" "$RUNNING_FILE" ; then echo "Hostapd ${group} looks already started" ; exit 1 ; fi
		if ! /usr/bin/grep -q -- "^group ${group}$" "$RUNNING_FILE" ; then 
			read -p "Group ${group} must be started before hostapd. Start it now? " -n 1 -r
			echo ""
    		[[ ! "$REPLY" =~ ^[Yy]$ ]] && exit 0

    		startStopGroup 1 "$group"
		fi

		! /usr/bin/ip link show "${config[$(echo $group | /usr/bin/tr '[:lower:]' '[:upper:]')_ARG]}" 1>/dev/null && exit 1 # Make sure the group's routed interface exists
		! /usr/bin/ip link show "br-${group}" 1>/dev/null && exit 1 # Make sure the group's bridge interface exists
		/usr/bin/ip link show "$iface" 2>/dev/null && exit 1 # Make sure the virtual interface doesn't already exist

		createIW "$start" "${config[LANInterface]}" "$iface"

		# This if statement check provided enough time for the interface to be ready to accept and ip address so soon after creation
		if /usr/bin/ip link show "$iface" 1>/dev/null ; then
			/usr/bin/ip address add "$(lastOctet $ip 129)"/25 dev "$iface"
		fi

		# Route the AP iface to the br interface of the group
		# Make sure to pass ip to ifaceRoute so it does not generate a new one (no switch for read/write passed to newVar)
		# Need the 'ip route default via ip' to be that of veth-$group
		ifaceRoute "$start" "$iface" "br-$group" "$group" "1" "$ip"
		
		/usr/bin/touch "$hostapdPidFile"
	    /usr/bin/hostapd -P "$hostapdPidFile" -i "$iface" -B "$hostapdConfigFile"

	    "$removeAddScript" 1 "hostapd ${group}" $RUNNING_FILE
		
	else

		if [ -f "$hostapdPidFile" ]; then
			kill "$(cat "$hostapdPidFile")" 2>/dev/null
			/usr/bin/rm --force "$hostapdPidFile"
		fi
		
		ifaceRoute "$start" "$iface" "wlan0" "$group" "1" "$ip"
		
		ip address delete "$(lastOctet $ip 129)"/25 dev "$iface"

		createIW "$start" "$iface"

		"$removeAddScript" 0 "hostapd ${group}" $RUNNING_FILE
	fi
}

function readConfigEqualsVal(){
	# gsub(/[[:blank:]]/, "", $2) ; 
	/usr/bin/awk -v pattern="$2" -F '=' '$1 ~ "^[[:blank:]]*"pattern"[[:blank:]]*$" {for(i=2;i<=NF;i++)printf("%s%s",$i,(i!=NF)?"=":ORS)}' $1
}

function sshspin(){
	local pid="$1"
	local user="$2"
	local ip="$3"
	local pwd="$4" # Could be a keyfile to use

	local lock="/tmp/sshspin.lock"
	local counter=0

	/usr/bin/touch "$lock"
	echo "Polling $ip for ssh access. Delete ${lock} to stop and exit."
	while [[ -f "$lock" && 0 == $( /usr/bin/ps -p "$pid" 1>&2>/dev/null ; echo $?) ]]; do
		if [[ "$pwd" =~ ^"/" ]]; then
			/usr/bin/ssh -i "$pwd" -p 22 -o "StrictHostKeyChecking=false" -o "UserKnownHostsFile=/dev/null" "$user"@"$ip" exit 2>/dev/null && break
		elif [[ -n "$pwd" ]]; then
			/usr/bin/sshpass -p "$pwd" ssh -p 22 -o "StrictHostKeyChecking=false" -o "UserKnownHostsFile=/dev/null" "$user"@"$ip" exit 2>/dev/null && break
		else
			/usr/bin/ssh -p 22 -o "StrictHostKeyChecking=false" -o "UserKnownHostsFile=/dev/null" "$user"@"$ip" exit 2>/dev/null && break
		fi
		counter=$(( counter + 1 ))
		[[ $(echo "$counter" % 100 | /usr/bin/bc) -eq 0 ]] && echo "Tried ${counter} times..."
	done

	if [[ ! -f "$lock" ]]; then
		echo "Something deleted the lock file ${lock}. Exitting."
		return 1
	else
		/usr/bin/rm --force "$lock"
	fi

	if ! /usr/bin/ps -p $pid 1>/dev/null ; then
		echo "Process died"
		return 1
	fi

	return 0
}

function generateNETWORKandROUTERandIPandMACfrom(){
	local dhcp_conf="$1"
	local hostname="$2"
	local group="$3"

	global[ROUTER]=$(newVar "0" "$group" "$IP_FILE" "10\.([0-9]{1,3}\.){2}1" "")
    ! echo ${global[ROUTER]} | /usr/bin/grep -q -E -- "10\.([0-9]{1,3}\.){2}1" && echo "Error with router: ${global[ROUTER]}" && exit 1
    global[NETWORK]=$(lastOctet "${global[ROUTER]}" 0)

	local IP=
	local MAC=$(random_macaddr)

	# echo ${global[ROUTER]} ${global[NETWORK]}
	# Get a new IP based if the hostname is not already in the hosts file
	if ! /usr/bin/cut -d ',' -f 3 "$dhcp_conf" | /usr/bin/grep -q -- "^${hostname}$" ; then
	    IP=$(echo "${global[NETWORK]}" "$dhcp_conf" | python3 -c '
import ipaddress
network,file=input().split()
ips=list(str(ip) for ip in ipaddress.IPv4Network("{}/24".format(network)))[2:-1]
with open(file, "r") as f:
    for line in f.readlines():
        if not line.strip() or "#" == line.lstrip()[0]:
            continue
        ips.remove(line.split(",")[1])
if len(ips):
    print(ips[0])
else:
    print("Used all 254 IPs!")
    exit(1)
')
		[[ -z "$IP" ]] && echo "ERROR with blank IP" && exit 1
	    echo ${MAC},${IP},${hostname} >> "$dhcp_conf"

	# Else set the IP and set the new random mac address
	else
	    IP=$(/usr/bin/awk -F ',' -v h="${hostname}" '$3 == h {print $2}' "$dhcp_conf")
	    MAC=$(/usr/bin/awk -F ',' -v h="${hostname}" '$3 == h {print $1}' "$dhcp_conf")
        # /usr/bin/sed -i "s/^.\{17\}\(,${IP},${HOSTNAME},2m\)$/${MAC}\1/g" "$dhcp_conf"
	fi
	
	global[MAC]="$MAC"
	global[IP]="$IP"

    /usr/bin/sudo pkill --signal SIGHUP dnsmasq
}

function HostRulesForGroup(){
	local start="$1"
	local group="$2"
	local command="--create" ; [[ 1 != "$start" ]] && command="--delete"

	# Allow DHCP
	/usr/bin/sudo python "${config[DAEMON]}" "$command" "-d ${global[NETWORK]}/24 -p udp -m udp --dport 68" "/usr/bin/dnsmasq" "out" "allow" "0" "$group" "1"
	/usr/bin/sudo python "${config[DAEMON]}" "$command" "-s ${global[NETWORK]}/24 -d ${global[ROUTER]}/32 -p udp -m udp --dport 67" "/usr/bin/dnsmasq" "in" "allow" "0" "$group" "1"
	/usr/bin/sudo python "${config[DAEMON]}" "$command" "-s 0.0.0.0/32 -d 255.255.255.255/32 -p udp -m udp --dport 67" "/usr/bin/dnsmasq" "in" "allow" "0" "$group" "1"

	# /usr/bin/sudo python "${config[DAEMON]}" "$command" "-d ${global[NETWORK]}/24 -p icmp -m owner --uid-owner nobody" "/usr/bin/dnsmasq" "out" "deny" "0" "0" "1"
    # /usr/bin/sudo python "${config[DAEMON]}" "$command" "-p igmp" "any" "in" "deny" "0" "$GROUP" "1"
	
	# Allow pacman in from VM
	/usr/bin/sudo python "${config[DAEMON]}" "$command" "-s ${global[NETWORK]}/24 -d ${global[ROUTER]}/32 -p tcp -m tcp --dport ${config[PACMANPORT]}" "/usr/bin/nginx" "in" "allow" "0" "$group" "1"	
}

# Make tor-browser access clearnet
CLEAN_TBB_USER_JS='''
user_pref("app.update.auto", false);
user_pref("network.dns.disabled", false);
user_pref("network.proxy.socks", "");
user_pref("network.proxy.socks_remote_dns", false);
'''

USER_JS='''
user_pref("privacy.resistFingerprinting", false);
user_pref("privacy.resistFingerprinting.letterboxing", true);
user_pref("privacy.resistFingerprinting.block_mozAddonManager", true);

//user_pref("keyword.enabled", false);
//user_pref("privacy.clearOnShutdown.history", true);
//user_pref("signon.management.page.breach-alerts.enabled", false);
//user_pref("signon.management.page.breachAlertUrl", "");
user_pref("browser.tabs.closeWindowWithLastTab", false);

user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);

// Strict cookie setting
user_pref("network.cookie.cookieBehavior", 5);
user_pref("browser.contentblocking.category", "strict");

user_pref("browser.urlbar.placeholderName", "DuckDuckGo");
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);

// Containers
user_pref("privacy.userContext.enabled", true);
user_pref("privacy.userContext.ui.enabled", true);
user_pref("privacy.userContext.newTabContainerOnLeftClick.enabled", true);

user_pref("browser.aboutConfig.showWarning", false);
user_pref("app.normandy.first_run", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);

user_pref("dom.security.https_only_mode", true);
user_pref("dom.security.https_only_mode_ever_enabled", true);

// Telemetry
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.hybridContent.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.sessions.current.clean", true);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.ping-centre.telemetry", false);
user_pref("devtools.onboarding.telemetry.logged", false);

// Other
user_pref("accessibility.force_disabled", 1); // claims to increase speed

user_pref("extensions.pocket.enabled", false);
user_pref("extensions.pocket.onSaveRecs", false);

'''

# Create or start a VM with a tap interface in the selected group
function startVM(){
	# TODO: set user and uid in VM to use/create
	# account for user that runs this function
	local CONFIG_FILE="${CONFIG_DIR}/guests/vms/${1}.conf"
	local GROUP="$2"
	[[ ! -f "$CONFIG_FILE" ]] && echo "No config file for this VM. Exitting" && exit 1
	[[ 0 == $EUID ]] && echo "Don't run as root" && exit 1

	local OS=$(readConfigEqualsVal $CONFIG_FILE os | /usr/bin/tr -d ' ')
	[[ -z $OS ]] && OS=arch
	[[ -f /var/lib/pacman/db.lck && "arch" == "$OS" ]] && echo "pacman is currently running elsewhere and VM may fail" && exit 1
	
	! /usr/bin/head --bytes=0 "$CONFIG_FILE" >/dev/null 2>&1 && echo "Cannot read config file ${CONFIG_FILE}. Exitting." && exit 1
	local REQUIRED_GROUP=$(readConfigEqualsVal $CONFIG_FILE required_group | /usr/bin/tr -d ' ')
	[[ -n "$REQUIRED_GROUP" && "$GROUP" != "$REQUIRED_GROUP" ]] && echo "Required group set to ${REQUIRED_GROUP}, not ${GROUP}. Exitting." && exit 1
	local BR="br-${GROUP}"
	! /usr/bin/ip link list "$BR" 1>/dev/null && echo "Group does not look active. Exitting." && exit 1

	local HOSTNAME=$(readConfigEqualsVal $CONFIG_FILE hostname | /usr/bin/tr -d ' ')
	/usr/bin/grep -E -q -- "^vm [0-9]{1,} ${HOSTNAME} ${GROUP}$" "$RUNNING_FILE" && echo "ERROR: This VM in this group is running already" && exit 1
	local DISK=$(readConfigEqualsVal $CONFIG_FILE disk | /usr/bin/tr -d ' ')
	[[ -z "$DISK" ]] && echo "Must specify disk in this VM's config file!" && exit 1
	local TEMP_DISK=$(readConfigEqualsVal $CONFIG_FILE temp_disk | /usr/bin/tr -d ' ')
	echo "$TEMP_DISK" | /usr/bin/grep -E -i -q -- "yes|y|true|1" && TEMP_DISK=1

	# UPDATE HERE
	# Another VM is modifying the disk used for copy on write
	# if [[ "" ]] && grep "^vm " "$RUNNING_FILE" 1>/dev/null ; then
		# $(ps -C qemu-system-x86_64 -o args | grep -o -- "-monitor unix:${TMP_DIR}/monitor-[a-z]*-" | /usr/bin/rev | /usr/bin/cut -d'-' -f2 | rev)
		# for vm in $(grep "^vm " "$RUNNING_FILE" | /usr/bin/cut -d ' ' -f3); do
			# if [[ "temp" == $(readConfigEqualsVal ${CONFIG_DIR}/guests/vms/${vm}.conf disk | /usr/bin/tr -d ' ') ]]; then
				# echo "Another VM is running that copy-on-write relies on temp disk being unmodified" && exit 1
			# fi
		# done
	# fi
	# Make sure local nginx is running to serve pacman packages
	
	local DISK_SIZE=$(readConfigEqualsVal $CONFIG_FILE disk_size | /usr/bin/tr -d ' ')
	if [[ -z "$DISK_SIZE" ]]; then
		case OS in
			windows) DISK_SIZE=60G ;;
			*) DISK_SIZE=6G ;;
		esac
	fi
	local FIRMWARE=$(readConfigEqualsVal $CONFIG_FILE firmware | /usr/bin/tr -d ' ')
	[[ -z "$FIRMWARE" ]] && FIRMWARE=bios
	local CPU=$(readConfigEqualsVal $CONFIG_FILE cpu | /usr/bin/tr -d ' ')
	[[ -z "$CPU" ]] && CPU=kvm64

	/usr/bin/sudo /usr/bin/systemctl start nginx || exit 1

	local pid= # pid used to wait for setting up VM to shutdown and later tracking the ready VM until its shutdown
	
	# Get multiple mount points (not implemented yet!)
	# local MOUNT=
	# while IFS= read -r line; do
	# 	[[ $line =~ ^[[:space:]]*# || -z $line ]] && continue
	# 	if [[ $line =~ ^[[:space:]]*mount[[:space:]]*= ]]; then
	# 		echo
	# 	fi
	# done < $CONFIG_FILE

	# # Mount a single ext4 file on host as /dev/sdb in guest
	# MOUNT=$(readConfigEqualsVal $CONFIG_FILE mount) # Disk name is first, destination location to mount is second
	# if [[ -n $MOUNT ]]; then
	# 	local MOUNT_SIZE=$(readConfigEqualsVal $CONFIG_FILE mount_size | /usr/bin/tr -d ' ')
	# 	[[ -z "$MOUNT_SIZE" ]] && MOUNT_SIZE=512M

	# 	local MOUNT_POINT=$(echo ${MOUNT} | /usr/bin/cut -d ' ' -f2 | /usr/bin/sed 's_/_\\/_g')
	# 	local MOUNT_NAME=$(echo ${MOUNT} | /usr/bin/cut -d ' ' -f1)
	# 	local MOUNT_DISK="${CONFIG_DIR}/guests/vms/mounts/mount-${MOUNT_NAME}.ext4"
	# 	local MOUNT_PWD
	# 	if [[ ! -f "$MOUNT_DISK" ]]; then
	# 		echo "${MOUNT_DISK} does not exist. Creating and formatting as ext4."
	# 		if ! fallocate -l "$MOUNT_SIZE" "$MOUNT_DISK" ; then
	# 			echo "Could not create ${MOUNT_DISK}. Exitting."
	# 			exit 1
	# 		fi
	# 		sudo chown root:"${config[FS_GROUP]}" "$MOUNT_DISK"
	# 		sudo chmod 660 "$MOUNT_DISK"
			
	# 		MOUNT_PWD=$(head /dev/urandom | /usr/bin/sha256sum | /usr/bin/cut -d ' ' -f1)
	# 		local SOME_RAND=$(head /dev/urandom | md5sum | /usr/bin/cut -d ' ' -f1)
	# 		echo "mount-${MOUNT_NAME} ${MOUNT_PWD}" >> "$LUKS_PASSWORD_FILE"
			
	# 		echo -n "${MOUNT_PWD}" | cryptsetup luksFormat "$MOUNT_DISK" && \
	# 			echo -n "${MOUNT_PWD}" | /usr/bin/sudo cryptsetup luksOpen "$MOUNT_DISK" "mount-${MOUNT_NAME}-${SOME_RAND}" && \
	# 			sudo mkfs.ext4 -q "/dev/mapper/mount-${MOUNT_NAME}-${SOME_RAND}" && \
	# 			sudo cryptsetup luksClose "mount-${MOUNT_NAME}-${SOME_RAND}" && \
	# 			echo "Created successfully"
	# 	# Get the password for the mount
	# 	else
	# 		! touch $MOUNT_DISK && echo "Cannot access ${MOUNT_DISK}. Exitting." && exit 1
	# 		MOUNT_PWD=$(grep "^mount-${MOUNT_NAME} " "$LUKS_PASSWORD_FILE" | tail -1 | /usr/bin/cut -d ' ' -f2)
	# 	fi
	# fi
	local PORTS=$(readConfigEqualsVal $CONFIG_FILE ports)
	local PACKAGES=$(readConfigEqualsVal $CONFIG_FILE packages)
	local AUTOSTART=$(readConfigEqualsVal $CONFIG_FILE autostart)

	local MEM=$(readConfigEqualsVal $CONFIG_FILE ram | /usr/bin/tr -d ' ')
	[[ -z $MEM ]] && MEM=4G
	local SMP=$(readConfigEqualsVal $CONFIG_FILE smp | /usr/bin/tr -d ' ')
	[[ -z $SMP ]] && SMP=1
	local VGA=$(readConfigEqualsVal $CONFIG_FILE vga | /usr/bin/tr -d ' ')
	[[ -z $VGA ]] && VGA=virtio
	local DISPLAY=$(readConfigEqualsVal $CONFIG_FILE display | /usr/bin/tr -d ' ')
	[[ -z $DISPLAY ]] && DISPLAY=spice
	local DGPU_PCI=$(readConfigEqualsVal $CONFIG_FILE dgpu_pci | /usr/bin/tr -d ' ')
	local DGPU_ROMFILE=$(readConfigEqualsVal $CONFIG_FILE dgpu_romfile | /usr/bin/tr -d ' ')
	local IGPU_UUID=$(readConfigEqualsVal $CONFIG_FILE igpu_uuid | /usr/bin/tr -d ' ')
	local IGPU_ROMFILE=$(readConfigEqualsVal $CONFIG_FILE igpu_romfile | /usr/bin/tr -d ' ')
	local GL=$(readConfigEqualsVal $CONFIG_FILE gl | /usr/bin/tr -d ' ')
	[[ -z $GL ]] && GL=on
	local RESOLUTION=$(readConfigEqualsVal $CONFIG_FILE resolution | /usr/bin/tr -d ' ')
	[[ -z $RESOLUTION ]] && RESOLUTION="3840x2160"
	local SOUND=$(readConfigEqualsVal $CONFIG_FILE sound | /usr/bin/tr -d ' ')
	[[ -z $SOUND ]] && SOUND="true"

	local DHCP_CONF="${TMP_DIR}/dhcp-${GROUP}.conf"
	# local DHCP_LEASES="${CONFIG_DIR}/dnsmasq.d/leases-${GROUP}.leases"
	local SETUP_FILE="${CONFIG_DIR}/configs/vm-setup-helper.sh"

	local TAP="$(echo qemu-$(echo ${HOSTNAME} ${GROUP} | md5sum) | /usr/bin/cut -c -15)"
	
	local CDROM=
	local cdrom_counter=0
	while IFS= read -r line; do
		[[ $line =~ ^[[:space:]]*# || -z $line ]] && continue
		if [[ $line =~ ^[[:space:]]*cdrom[[:space:]]*= ]]; then
			CDROM="$CDROM -drive media=cdrom,index=${cdrom_counter},file=$(echo $line | /usr/bin/awk -F '=' '{$1="" ; print $0}' | /usr/bin/sed 's/^[[:space:]]*//;s/[[:space:]]*$//')"
			cdrom_counter=$(( cdrom_counter + 1 ))
		fi
	done < $CONFIG_FILE

	local DISK_FORMAT="qcow2"
	local IMG="${CONFIG_DIR}/guests/vms/disks/disk-${DISK}.${DISK_FORMAT}"

	local COPYPASTE=1

	local RUNSETUP=0
	local QEMU_DISPLAY
	local QEMU_SOUND

	#-device virtio-scsi-pci,id=scsi0 \
	#-drive file=$IMG,if=none,format=raw,discard=unmap,aio=native,cache=none,id=someid \
	#-device scsi-hd,drive=someid,bus=scsi0.0
	#-vga qxl \
	#MESA_LOADER_DRIVER_OVERRIDE=i915
	#INTEL_DEBUG=norbc
	case "$DISPLAY" in
# device virtio-vga-gl
# virtio-vga / virtio-gpu is a paravirtual 3D graphics driver based on virgl. Currently a work in progress, supporting only very recent (>= 4.4) Linux guests with mesa (>=11.2) compiled with the option gallium-drivers=virgl.
# To enable 3D acceleration on the guest system select this vga with -device virtio-vga-gl and enable the opengl context in the display device with -display sdl,gl=on or -display gtk,gl=on for the sdl and gtk display output respectively. Successful configuration can be confirmed looking at the kernel log in the guest:
		vfio)
			VGA=none
			QEMU_DISPLAY="-display gtk,gl=on,zoom-to-fit=off,full-screen=off"
		;;
		sdl|gtk)
			QEMU_DISPLAY="-display ${DISPLAY},gl=${GL}"
			[[ "gtk" == "$DISPLAY" ]] && QEMU_DISPLAY="${QEMU_DISPLAY},full-screen=on"
			
			# Why does this not work anymore for copy paste?
			# Enable spice-vdagent copy/paste support, from https://www.kraxel.org/blog/2021/05/qemu-cut-paste/
			[[ "$COPYPASTE" == 1 ]] && QEMU_DISPLAY="""${QEMU_DISPLAY} \
-chardev qemu-vdagent,id=ch1,name=vdagent,clipboard=on,mouse=on \
-device virtio-serial-pci \
-device virtserialport,chardev=ch1,id=ch1,name=com.redhat.spice.0 \
"""

			case "$SOUND" in 
				true|yes|1) # Not tested yet
					QEMU_SOUND="""-device intel-hda -device hda-duplex,audiodev=audio0 \
-audiodev pa,id=audio0,out.mixing-engine=off,out.stream-name=test-out,in.stream-name=test-in"""
				;;
			esac
		;;
		spice|*)
			# Needed for spice?
			VGA=virtio

			# From online: True? Not tested yet
			# On Linux guests, the qxl and bochs_drm kernel modules must be loaded in order to gain a decent performance. 
			GL=on
			QEMU_DISPLAY="-spice unix=on,addr=/tmp/qemu-spice-${HOSTNAME}-${GROUP}.socket,gl=${GL},disable-ticketing=on,disable-agent-file-xfer=on,image-compression=off"
			if [[ $COPYPASTE == 1 ]]; then
				QEMU_DISPLAY="""${QEMU_DISPLAY},disable-copy-paste=off \
-chardev spicevmc,id=spicechannel0,name=vdagent \
-device virtio-serial-pci \
-device virtserialport,chardev=spicechannel0,id=spicechannel0,name=com.redhat.spice.0 \
"""
			else
				QEMU_DISPLAY="""${QEMU_DISPLAY},disable-copy-paste=on"
			fi

			case "$SOUND" in
				true|yes|1)
					# From https://askubuntu.com/questions/1314825/how-to-enable-sound-in-qemu
					QEMU_SOUND="""-device ich9-intel-hda,addr=1f.1 \
-audiodev pa,id=snd0 \
-device hda-output,audiodev=snd0"""
					# For pipewire?
					QEMU_SOUND="""-device ich9-intel-hda,id=sound0,bus=pcie.0,addr=0x1b \
-device hda-duplex,id=sound0-codec0,bus=sound0.0,cad=0 \
-global ICH9-LPC.disable_s3=1 \
-global ICH9-LPC.disable_s4=1"""
					# Basically, audiodev pa is the host's PulseAudio, and device hda-output is a line-out device on the guest (which is routed through audiodev=snd0). ...And hda-output requires an ich9-intel-hda bus to connect to.
					QEMU_SOUND="""-device ich9-intel-hda,bus=pcie.0,addr=0x1b \
-audiodev pa,id=hda,server=unix:/run/user/1000/pulse/native \
-device hda-micro,audiodev=hda"""
# For windows?
# -device ich9-intel-hda,bus=pcie.0,addr=0x1b -device hda-micro,audiodev=hda -audiodev pa,id=hda,server=unix:/run/user/1000/pulse/native,out.frequency=44100'/>
					# Options taken from quickemu. DOES NOT work for Firefox, which requires Pulseaudio, but VLC does work
					QEMU_SOUND="""-device intel-hda -device hda-duplex,audiodev=audio0 \
-audiodev spice,id=audio0,out.mixing-engine=off"""
				;;
			esac
		;;
	esac
	# 	QEMU_SOUND="""-device intel-hda -device hda-duplex,audiodev=audio0 \
	# -audiodev pa,id=audio0,out.mixing-engine=off,out.stream-name=test-out,in.stream-name=test-in"""

		# Create tap under this bridge
	/usr/bin/sudo /usr/bin/ip tuntap add "$TAP" mode tap user user
	/usr/bin/sudo /usr/bin/ip link set "$TAP" master "$BR"
	/usr/bin/sudo /usr/bin/ip link set "$TAP" up
	! /usr/bin/ip link show "$TAP" 1>/dev/null && echo "$TAP interface does not exist." && exit 1

	# Make sure permissions are OK for files touched by low priv daemons
	if [[ ! -f "$DHCP_CONF" ]]; then
	    /usr/bin/touch "$DHCP_CONF"
	    # chown :"${config[FS_GROUP]}" "$DHCP_CONF"
	    /usr/bin/chmod 666 -- "$DHCP_CONF"
	fi
	
	generateNETWORKandROUTERandIPandMACfrom "$DHCP_CONF" "$HOSTNAME" "$GROUP"
	local NETWORK=${global[NETWORK]}
	local ROUTER=${global[ROUTER]}
	local IP=${global[IP]}
	local MAC=${global[MAC]}
	# echo $NETWORK $ROUTER $IP $MAC && exit

	[[ $COPYPASTE == 1 ]] && PACKAGES="spice-vdagent ${PACKAGES}"

	# Add the audio codec and map it to a host audio backend id
	# QEMU_SOUND="-device ich9-intel-hda -audiodev pa,id=hda -device hda-output,audiodev=hda"

	# -audiodev pa,id=snd0 -device ich9-intel-hda -device hda-output,audiodev=snd0
	# <model type='qxl' ram='65536' vram='65536' vgamem='16384' heads='1' primary='yes'/> 
	
	# INTEL_DEBUG=norbc
	# MESA_LOADER_DRIVER_OVERRIDE=i915
	    # <qemu:arg value='host,kvm=off,'/>

	[[ "host" == "$CPU" ]] && CPU="host,kvm=off,+hypervisor,+invtsc,l3-cache=on,migratable=no,hv_passthrough"
	# [[ "host" == "$CPU" ]] && CPU="host,kvm=off,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time,hv_vendor_id=Genuine-Intel"
# host,migratable=on,hv-time=on,hv-relaxed=on,hv-vapic=on,hv-spinlocks=0x1fff,hv-vendor-id=123412341234,kvm=off

	local QEMU_BASE="qemu-system-x86_64 \
-enable-kvm \
-machine q35,smm=on,vmport=off,hpet=off \
-global kvm-pit.lost_tick_policy=discard \
-global ICH9-LPC.disable_s3=1 \
-global ICH9-LPC.disable_s4=1 \
-global PIIX4_PM.disable_s3=1 \
-global PIIX4_PM.disable_s4=1 \
-cpu ${CPU} \
-smp cores=${SMP},threads=2,sockets=1 \
-m ${MEM} \
-device virtio-balloon \
-vga ${VGA} \
${QEMU_DISPLAY} \
${QEMU_SOUND} \
-object rng-random,id=rng0,filename=/dev/urandom \
-device virtio-rng-pci,rng=rng0 \
-rtc base=utc,clock=host,driftfix=slew \
-no-fd-bootchk \
-monitor unix:${TMP_DIR}/monitor-${HOSTNAME}-${GROUP},server,nowait \
-D ${TMP_DIR}/qemu-${HOSTNAME}.log \
"
# -device qemu-xhci,id=spicepass \
# -chardev spicevmc,id=usbredirchardev1,name=usbredir \
# -device usb-redir,chardev=usbredirchardev1,id=usbredirdev1 \
# -chardev spicevmc,id=usbredirchardev2,name=usbredir \
# -device usb-redir,chardev=usbredirchardev2,id=usbredirdev2 \
# -chardev spicevmc,id=usbredirchardev3,name=usbredir \
# -device usb-redir,chardev=usbredirchardev3,id=usbredirdev3 \
# -device pci-ohci,id=smartpass \
# -device usb-ccid \
# -chardev spicevmc,id=ccid,name=smartcard \
# -device ccid-card-passthru,chardev=ccid \
# -device usb-ehci,id=input \
# -device usb-kbd,bus=input.0 \
# -k en-us \
# -device usb-tablet,bus=input.0 \
# -realtime mlock=off \
# -boot strict=on

	if [[ "uefi" == "$FIRMWARE" ]]; then
		! /usr/bin/pacman -Q edk2-ovmf 1>/dev/null && echo "pacman package edk2-ovmf is required for UEFI!" && exit 1

		# UPDATE to move this to /tmp/ is TEMP_DISK is set!
		local nvram="${CONFIG_DIR}/guests/vms/disks/nvram-${HOSTNAME}.fd"
		if [[ ! -f "$nvram" ]]; then
			/usr/bin/sudo /usr/bin/cp -- /usr/share/edk2-ovmf/x64/OVMF_VARS.fd "$nvram"
			/usr/bin/sudo /usr/bin/chown root:"${config[FS_GROUP]}" -- "$nvram"
			/usr/bin/sudo /usr/bin/chmod 0660 -- "$nvram"
		fi

		QEMU_BASE="$QEMU_BASE \
-global driver=cfi.pflash01,property=secure,value=on \
-drive if=pflash,format=raw,unit=0,file=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd,readonly=on \
-drive if=pflash,format=raw,unit=1,file=${nvram} \
"
	fi

# -device ide-cd,drive=drive-iso,id=cd1,bootindex=1 \
# -device ahci,id=achi0 \
# bus=achi0.0
# rombar=0 ???
# you should add "display=off" option when you create VM without dma-buf.
# This should be combined with the above DMA-BUF configuration in order to also display everything that happens before the guest Intel driver is loaded (i.e. POST, the firmware interface, and the guest initialization).
# -device vfio-pci,host=01:00.0,bus=root.1,addr=00.0,x-pci-sub-device-id=0x07b1,x-pci-sub-vendor-id=0x1028,multifunction=on,romfile=MyGPU.rom \
# -sandbox on,obsolete=deny,elevateprivileges=deny,spawn=deny,resourcecontrol=deny \

	local QEMU_NIC="-nic tap,ifname=${TAP},mac=${MAC},script=no,downscript=no,model=virtio-net-pci"
	
	# socat - UNIX-CONNECT:${TMP_DIR}/monitor-${HOSTNAME}-${GROUP}
	# echo system_powerdown | socat - UNIX-CONNECT:${TMP_DIR}/monitor-${HOSTNAME}-${GROUP}

	# Create a new image for the disk if the given hostname is new, or if it is one meant to be ephmeral
	if [[ ! -f "$IMG" ]]; then
	    /usr/bin/sudo /usr/bin/qemu-img create -f "$DISK_FORMAT" -o preallocation=metadata "$IMG" "$DISK_SIZE"
	    /usr/bin/sudo /usr/bin/chown 0:"${config[FS_GROUP]}" -- "$IMG"
	    /usr/bin/sudo /usr/bin/chmod 0660 -- "$IMG"
	    echo "Created a fresh disk at ${IMG}"
	    RUNSETUP=1
	else
	    RUNSETUP=0
	fi

	# Beware that that chains for this group may not have been loaded yet because the group number might have changed?
	HostRulesForGroup 1 "$GROUP"
	
	# Allow ssh to VM
	/usr/bin/sudo python "${config[DAEMON]}" --create "-d ${global[IP]}/32 -p tcp -m tcp --dport 22 -m owner --uid-owner 1000" "/usr/bin/ssh" "out" "allow" "0" "$GROUP" "1"
	
	# Generate an ssh key per disk only at runtime. No password on key. ssh askpass program opens, so override env var (also security benefit?)
	local SSH_KEY_PATH="${CONFIG_DIR}/guests/vms/disks/ssh-key-${DISK}"
	if [[ ! -f "$SSH_KEY_PATH" ]]; then
		echo -e "\n\n" | SSH_ASKPASS="" /usr/bin/sudo /usr/bin/ssh-keygen -t ed25519 -f "$SSH_KEY_PATH"
		[[ $? -ne 0 ]] && echo "Error generating ssh-key for disk!" && exit 1
		
		/usr/bin/sudo /usr/bin/chown 0:"${config[FS_GROUP]}" -- "$SSH_KEY_PATH"
		/usr/bin/sudo /usr/bin/chown 0:"${config[FS_GROUP]}" -- "$SSH_KEY_PATH".pub
		/usr/bin/sudo /usr/bin/chmod 0440 -- "$SSH_KEY_PATH"
		/usr/bin/sudo /usr/bin/chmod 0440 -- "$SSH_KEY_PATH".pub
	fi

	if [[ "arch" == "$OS" && 1 == "$RUNSETUP" ]]; then
		[[ ! -f "$SETUP_FILE" ]] && echo "Error ${SETUP_FILE} is missing!" && exit 1
		echo "No disk detected. Running first time setup..."
		echo "Set the root password to a"

	    /usr/bin/sudo /usr/bin/python "${config[DAEMON]}" --create "-s ${IP}/32" "any" "in" "deny" "0" "$GROUP" "1"

		# Default to Arch Linux install
		[[ -z $CDROM && "windows" != "$OS" ]] && CDROM="-cdrom $(ls -tr -- /home/user/.isos/archlinux-*-x86_64.iso | /usr/bin/tail -1)"

		${QEMU_BASE} -drive file="$IMG",index=0,cache=writeback,media=disk ${CDROM} ${QEMU_NIC} &
		
		pid=$!

		sshspin "$pid" "root" "$IP" "a"

		local SSH_KEY="$(/usr/bin/cat "$SSH_KEY_PATH".pub | /usr/bin/cut -d' ' -f1-2 | /usr/bin/sed 's/\//\\\//g')"
		/usr/bin/sed "s/^SSH_KEY[[:space:]]*=.*/SSH_KEY=\"${SSH_KEY}\"/" "${SETUP_FILE}" | \
		/usr/bin/sed "s_^PACMANSERVER[[:space:]]*=.*_PACMANSERVER=\"https://ROUTER:${config[PACMANPORT]}/archlinux/\"_" | \
		/usr/bin/sshpass -p "a" ssh -p 22 -o "StrictHostKeyChecking=false" -o "UserKnownHostsFile=/dev/null" root@"$IP" \
			"> /setup.sh ; \
			echo ${ROUTER} ROUTER > /etc/hosts ; \
			/bin/bash /setup.sh"
		# TODO: respond when setup.sh fails

		# Script spins here waiting for VM to run the setup and shutdown...
		/usr/bin/tail --follow /dev/null --pid="$pid"

	    # VM is shutdown and setup, running and ready to vnc into, so remove restrictive rules
	    /usr/bin/sudo /usr/bin/python "${config[DAEMON]}" --delete "-s ${IP}/32" "any" "in" "deny" "0" "$GROUP" "1"
	fi

	# Make sure this VM is configured in the host's ssh user config
	# TODO: make this check if file contains all these lines in order instead ro reduce disk writes
	if ! /usr/bin/grep -q -- "^Host ${HOSTNAME}$" ~/.ssh/config ; then
		echo """Host ${HOSTNAME}
	StrictHostKeyChecking false
	UserKnownHostsFile /dev/null
	IdentityFile ${SSH_KEY_PATH}""" >> ~/.ssh/config
	else
		# Find the instance of Host $HOSTNAME and remove it until the next line of "^Host .*"
		local tmpFile=$(/usr/bin/mktemp)
		[[ $? -ne 0 ]] && echo "Error creating tmp file, line $LINENO!" && exit 1
		local tmpFile1=$(/usr/bin/mktemp)
		[[ $? -ne 0 ]] && echo "Error creating tmp file, line $LINENO!" && exit 1

		echo """	StrictHostKeyChecking false
	UserKnownHostsFile /dev/null
	IdentityFile ${SSH_KEY_PATH}""" >> "$tmpFile"

		/usr/bin/awk "/^Host ${HOSTNAME}/{f=1;print;while (getline < \""${tmpFile}"\"){print}}/^Host /{f=0}!f" ~/.ssh/config > "$tmpFile1"
		/usr/bin/cat "$tmpFile1" > ~/.ssh/config
		/usr/bin/rm --force "$tmpFile"
	fi

	###########################
	## Final Storage Section ##
	###########################

	# After possibly creating and setting up new "temp" disk, make CoW qemu-image clone of "temp" disk to /tmp/
	# To-do: Mechanism to update disk-temp.img with VM only meant for that (so no custom packages or scripts that could pollute it)
	if [[ 1 == "$TEMP_DISK" ]]; then
		local base_disk_mod_time=$(/usr/bin/stat "$IMG" --format=%Y)
		
		# User should delete temporary disk if they do not want to reuse it on next VM startup
		local TEMP_IMG="/tmp/disk-${HOSTNAME}-${GROUP}.${base_disk_mod_time}.${DISK_FORMAT}"
		
		# Make sure base disk has not been updated since the CoW temp disk was made
		for file in /tmp/* ; do
			if [[ "$file" =~ "^/tmp/disk-${HOSTNAME}-${GROUP}\." && "$file" != "$TEMP_IMG" ]]; then
				echo "ERROR: Looks like an old disk exists at ${file} and base disk has been updated!"
				exit 1
			fi
		done
		[[ ! -f "$TEMP_IMG" ]] && /usr/bin/qemu-img create -f "$DISK_FORMAT" -o backing_file="$IMG",backing_fmt="$DISK_FORMAT" -- "$TEMP_IMG"
		IMG="$TEMP_IMG"
	fi
	
	# Any isos and the main disk. Is scsi=off needed?
	QEMU_BASE="${QEMU_BASE} \
		${CDROM} \
		-device virtio-blk-pci,scsi=off,drive=drive0 \
		-drive id=drive0,if=none,format=${DISK_FORMAT},cache=writeback,media=disk,discard=unmap,file=${IMG} \
	"
	
	# [[ -n $MOUNT ]] && QEMU_BASE="${QEMU_BASE} -drive file=${MOUNT_DISK},index=1,format=raw,cache=writeback,media=disk"

	###################
	## VFIO Section ##
	###################	

	if [[ "vfio" == "$DISPLAY" ]]; then
		if [[ -n "$IGPU_UUID" ]]; then
			[[ ! -d "/sys/bus/mdev/devices/${IGPU_UUID}" ]] && echo "iGPU UUID does not exist. Exitting." && exit 1
			for mod in "mdev" "kvmgt" ; do
				! /usr/bin/lsmod | /usr/bin/grep -q -- "^${mod} " && echo "Load kernel module ${mod}" && exit 1
			done

			QEMU_BASE="${QEMU_BASE} \
-device vfio-pci,id=igpu0,sysfsdev=/sys/bus/mdev/devices/${IGPU_UUID},display=on,x-igd-opregion=on,ramfb=on,driver=vfio-pci-nohotplug"
			
			if [[ -n "$IGPU_ROMFILE" ]]; then
				[[ ! -f "${IGPU_ROMFILE}" ]] && echo "iGPU romfile does not exist. Exitting." && exit 1
				QEMU_BASE="${QEMU_BASE},romfile=${IGPU_ROMFILE}"
			fi
		fi
		if [[ -n "$DGPU_PCI" ]]; then
			# [[ ! -d "/sys/bus/mdev/devices/${dGPU_UUID}" ]] && echo "iGPU UUID does not exist. Exitting." && exit 1

			QEMU_BASE="${QEMU_BASE} -device vfio-pci,id=dgpu0,host=${DGPU_PCI}"
			
			if [[ -n "$DGPU_ROMFILE" ]]; then
				[[ ! -f "${DGPU_ROMFILE}" ]] && echo "dGPU romfile does not exist. Exitting." && exit 1
				QEMU_BASE="${QEMU_BASE},romfile=${DGPU_ROMFILE}"
			fi
		fi
	fi

	###################
	## Samba Section ##
	###################

	# Keep an encrypted directory on the host that is shared and mounted to the VM
	local VM_SMB_ENC_MOUNTS_DIR=${CONFIG_DIR}/guests/vms/smb_enc_mounts
	/usr/bin/sudo /usr/bin/mkdir -p -- "$VM_SMB_ENC_MOUNTS_DIR"
	/usr/bin/sudo /usr/bin/chown 0:0 "$VM_SMB_ENC_MOUNTS_DIR"
	/usr/bin/sudo /usr/bin/chmod 0700 "$VM_SMB_ENC_MOUNTS_DIR"
	
	local VM_SMB_ENC_MOUNTS_TMP_DIR=/srv/prompt-and-split/samba
	/usr/bin/sudo /usr/bin/mkdir -p -- "$VM_SMB_ENC_MOUNTS_TMP_DIR"
	/usr/bin/sudo /usr/bin/chown 0:0 -- "$VM_SMB_ENC_MOUNTS_TMP_DIR"
	/usr/bin/sudo /usr/bin/chmod 0770 -- "$VM_SMB_ENC_MOUNTS_TMP_DIR"
	/usr/bin/sudo /usr/bin/chmod a+rx -- "$VM_SMB_ENC_MOUNTS_TMP_DIR"
	
	local SMB_MOUNT_CMD=""
	local SMB_UMOUNT_CMD=""
	local smb_service=smb-prompt-and-split-${1}.service
	local smb_service_path="/run/systemd/system/${smb_service}"
	local smb_config_file="${TMP_DIR}"/smb-vm-$1.config
	local smb_service_port=$(openPort)
	[[ "windows" == "$OS" ]] && smb_service_port=445
	local smb_service_pid_dir=${TMP_DIR}/samba-pids/${smb_service}/
	/usr/bin/sudo /usr/bin/mkdir -p -- "${smb_service_pid_dir}"

	while IFS= read -r line; do
		if [[ $line =~ ^[[:space:]]*smb_mount[[:space:]]*= ]]; then
			local SMB_MOUNT=$(echo $line | /usr/bin/awk -F '=' '{for(i=2;i<=NF;i++)printf("%s%s",$i,(i!=NF)?"=":ORS)}' | /usr/bin/sed 's/^[[:space:]]*//')
			
			if [[ -n $SMB_MOUNT ]]; then
				# Create a temporary user based on VM name
				/usr/bin/sudo /usr/bin/groupadd dummy-group
				local MOUNT_USER=prompt-and-split-${HOSTNAME}
				local MOUNT_USER_PWD=a
				/usr/bin/sudo /usr/bin/useradd --no-create-home --shell /usr/bin/nologin --group dummy-group -- "$MOUNT_USER"
				local MOUNT_UID=$(/usr/bin/getent passwd "$MOUNT_USER" | /usr/bin/cut -d':' -f3)
				echo -e "${MOUNT_USER_PWD}\n${MOUNT_USER_PWD}" | /usr/bin/sudo /usr/bin/smbpasswd -a "$MOUNT_USER"
				
				local MOUNT_NAME=$(echo ${SMB_MOUNT} | /usr/bin/cut -d ' ' -f1)
				local MOUNT_POINT=$(echo ${SMB_MOUNT} | /usr/bin/cut -d ' ' -f2 | /usr/bin/sed 's_/_\\/_g')
				
				/usr/bin/sudo /usr/bin/mkdir -p -- "${VM_SMB_ENC_MOUNTS_DIR}/${MOUNT_NAME}/"
				/usr/bin/sudo /usr/bin/chown "${MOUNT_USER}":dummy-group "${VM_SMB_ENC_MOUNTS_DIR}/${MOUNT_NAME}/"
				/usr/bin/sudo /usr/bin/chmod 0700 "${VM_SMB_ENC_MOUNTS_DIR}/${MOUNT_NAME}/"

				local MOUNT_PWD=$(/usr/bin/grep "^smb-mount-${MOUNT_NAME} " "$LUKS_PASSWORD_FILE" | /usr/bin/tail -1 | /usr/bin/cut -d ' ' -f2)
				[[ -z $MOUNT_PWD ]] && MOUNT_PWD=$(/usr/bin/head /dev/urandom | /usr/bin/sha256sum | /usr/bin/cut -d ' ' -f1) && echo "smb-mount-${MOUNT_NAME} ${MOUNT_PWD}" >> "$LUKS_PASSWORD_FILE"

				echo -n "$MOUNT_PWD" | /usr/bin/sudo /usr/bin/gocryptfs -init "${VM_SMB_ENC_MOUNTS_DIR}/${MOUNT_NAME}"
				
				if ! /usr/bin/sudo /usr/bin/mount | /usr/bin/grep -q -- "^${VM_SMB_ENC_MOUNTS_DIR}/${MOUNT_NAME} on ${VM_SMB_ENC_MOUNTS_TMP_DIR}/${MOUNT_NAME} type fuse.gocryptfs " ; then
					/usr/bin/sudo /usr/bin/mkdir -p -- "${VM_SMB_ENC_MOUNTS_TMP_DIR}/${MOUNT_NAME}/"
					/usr/bin/sudo /usr/bin/chown 0:0 -- "${VM_SMB_ENC_MOUNTS_TMP_DIR}/${MOUNT_NAME}/"
					/usr/bin/sudo /usr/bin/chmod 0000 -- "${VM_SMB_ENC_MOUNTS_TMP_DIR}/${MOUNT_NAME}/"
				
					if ! echo "$MOUNT_PWD" | /usr/bin/sudo /usr/bin/gocryptfs -force_owner "${MOUNT_UID}:${MOUNT_UID}" "${VM_SMB_ENC_MOUNTS_DIR}/${MOUNT_NAME}/" "${VM_SMB_ENC_MOUNTS_TMP_DIR}/${MOUNT_NAME}/" ; then
						echo "Failed to mount. Exitting..."
						exit 1
					fi
				fi

				# Create the mount point, chown it properly if it is deep in a directory, smb mount to it
				[[ -z $SMB_MOUNT_CMD ]] && \
					SMB_MOUNT_CMD="for d in \$(echo ${MOUNT_POINT} | /usr/bin/awk -F '/' '{for(i=1;i<=NF;i++){for(j=1;j<=i;j++){ printf(\"%s%s\",\$j,(j!=NF)?FS:ORS)} if(i!=NF){ printf(\"\n\") } } }'); do [[ ! -d \"\${d}\" ]] && install -d -m 0755 -o user -g user \"\${d}\" ; done ; /usr/bin/mount -t smb3 -o uid=user,gid=user,username=${MOUNT_USER},password=${MOUNT_USER_PWD},noatime,port=${smb_service_port} //${ROUTER}/${MOUNT_NAME} ${MOUNT_POINT} " || \
					SMB_MOUNT_CMD="$SMB_MOUNT_CMD ; for d in \$(echo ${MOUNT_POINT} | /usr/bin/awk -F '/' '{for(i=1;i<=NF;i++){for(j=1;j<=i;j++){ printf(\"%s%s\",\$j,(j!=NF)?FS:ORS)} if(i!=NF){ printf(\"\n\") } } }'); do [[ ! -d \"\${d}\" ]] && install -d -m 0755 -o user -g user \"\${d}\" ; done ; /usr/bin/mount -t smb3 -o uid=user,gid=user,username=${MOUNT_USER},password=${MOUNT_USER_PWD},noatime,port=${smb_service_port} //${ROUTER}/${MOUNT_NAME} ${MOUNT_POINT} "

				SMB_UMOUNT_CMD="${VM_SMB_ENC_MOUNTS_TMP_DIR}/${MOUNT_NAME}/ ${SMB_UMOUNT_CMD}"
			fi
		fi
	done < $CONFIG_FILE
	if [[ -n $SMB_MOUNT_CMD ]]; then
		# Generate port for new smb service

		echo """[global]
	access based share enum = yes
	security = user
	follow symlinks = no
#   wide links = yes
#   unix extensions = no
	server min protocol SMB3_00
	server smb encrypt = required
	client smb encrypt = required
# Disable printer sharing
	load printers = no
	printing = bsd
	printcap name = /dev/null
	disable spoolss = yes
	show add printer wizard = no
#	netbios name = NETBIOS_NAME
	workgroup = WORKGROUP
#	interfaces = 192.168.1.1/8
	server string = Samba Server
	# log file = /var/log/samba/%m.log
	# max log size = 50
	server role = standalone server
	dns proxy = no
	pid directory = ${smb_service_pid_dir}
[${MOUNT_NAME}]
    # comment = Samba share
    path = /srv/prompt-and-split/samba/${MOUNT_NAME}/
    browseable = no
    writable = yes
    read only = no
    guest ok = no
    public = no
    create mask = 0660
    force create mode = 0660
    directory mask = 0770
    force directory mode = 0770
    forceuser = ${MOUNT_USER}
    forcegroup = ${MOUNT_USER}
    valid users = ${MOUNT_USER}
    locking = yes
""" | /usr/bin/sudo /usr/bin/tee "${smb_config_file}" 1>/dev/null

		echo """[Unit]
Description=Samba SMB Daemon
After=network.target nmb.service winbind.service

[Service]
Type=notify
PIDFile=${TMP_DIR}/${smb_service}.pid
LimitNOFILE=16384
EnvironmentFile=-/etc/conf.d/samba
ExecStart=/usr/bin/smbd -p ${smb_service_port} --foreground --no-process-group --configfile=${smb_config_file} $SMBDOPTIONS
ExecReload=/bin/kill -HUP $MAINPID
LimitCORE=infinity

[Install]
WantedBy=multi-user.target
""" | /usr/bin/sudo /usr/bin/tee "$smb_service_path" 1>/dev/null

	else
		SMB_MOUNT_CMD="/usr/bin/echo -n"
	fi

	####################
	## Launch Section ##
	####################
	
	$QEMU_BASE $QEMU_NIC &

	pid=$!

	[[ "$pid" -le 0 ]] && echo "qemu failed to start" && exit 1

	# Write the pid to the running file with the hostname and group name
	"$removeAddScript" 1 "vm ${pid} ${HOSTNAME} ${GROUP}" $RUNNING_FILE

	# Allow DNS from the VM
    /usr/bin/sudo /usr/bin/python "${config[DAEMON]}" --create "-d ${ROUTER}/32 -s ${IP}/32 -p udp --dport 53 -m comment --comment until\ restart" "/usr/bin/python3.11" "in" "allow" "0" "$GROUP" "1"

	if [[ -n $SMB_MOUNT_CMD ]]; then
		/usr/bin/sudo python "${config[DAEMON]}" --create "-d ${ROUTER}/32 -s ${IP}/32 -p tcp --dport ${smb_service_port} -m comment --comment until\ restart" "/usr/bin/smbd" "in" "allow" "0" "$GROUP" "1"
		/usr/bin/sudo /usr/bin/systemctl daemon-reload
		/usr/bin/sudo /usr/bin/systemctl restart "${smb_service}"
	fi

	if [[ "arch" == "$OS" ]]; then 
		if ! sshspin "$pid" "root" "$IP" "$SSH_KEY_PATH" ; then
			"$removeAddScript" 0 "vm ${pid} ${HOSTNAME} ${GROUP}" $RUNNING_FILE
			exit 1
		fi
	fi

	# UPDATE: Allow setting some firefox user.js options from config
	# while IFS= read -r line; do
	# 	[[ $line =~ ^[[:space:]]*# || -z $line ]] && continue
	# 	if [[ $line =~ ^[[:space:]]*ffuserjs[[:space:]]*= ]]; then
	# 		echo
	# 	fi
	# done < $CONFIG_FILE

	# umount arg1 must have no spaces!
	SMB_UMOUNT_CMD=$(echo $SMB_UMOUNT_CMD | /usr/bin/sed 's_^[[:space:]]*__;s_[[:space:]]*$__')
	# Wait for VM pid to stop and remove it from the running file. This is run as root so it isn't accidentally killed, however note that the later execution has to be root or in the prompt-and-split group to edit the file and root or the original user to send a notification
	(smb_service=$smb_service smb_service_port=$smb_service_port SMB_UMOUNT_CMD="$SMB_UMOUNT_CMD" removeAddScript=$removeAddScript hostsEditScript=$hostsEditScript PID=$pid HOSTNAME=$HOSTNAME IP=$IP ROUTER=$ROUTER GROUP=$GROUP RUNNING_FILE=$RUNNING_FILE DAEMON="${config[DAEMON]}" CONFIG_DIR=$CONFIG_DIR TAP=$TAP \
		/usr/bin/sudo -E /bin/bash -c ' \
		/usr/bin/tail --follow /dev/null --pid "${PID}" && \
	    /usr/bin/python "${DAEMON}" --delete "-d ${ROUTER}/32 -s ${IP}/32 -p udp --dport 53                  -m comment --comment until\ restart" "/usr/bin/python3.11" "in"  "allow" "0" "$GROUP" "1" ; \
		/usr/bin/python "${DAEMON}" --delete "-d ${ROUTER}/32 -s ${IP}/32 -p tcp --dport ${smb_service_port} -m comment --comment until\ restart" "/usr/bin/smbd"       "in"  "allow" "0" "$GROUP" "1" ; \
		/usr/bin/python "${DAEMON}" --delete "-d ${IP}/32 -p tcp -m tcp --dport 22 -m owner --uid-owner 1000"                                     "/usr/bin/ssh"        "out" "allow" "0" "$GROUP" "1" ; \
		/usr/bin/python "${DAEMON}" --delete-rules-file-for-group "${CONFIG_DIR}/guests/rules/${HOSTNAME}.rules" "${GROUP}" ; \
		/usr/bin/python "${DAEMON}" --delete-dns-ip-rules-for-source-and-group "$HOSTNAME" "${GROUP}" ; \
		/usr/bin/systemctl stop "$smb_service" && \
		/usr/bin/umount --lazy "${SMB_UMOUNT_CMD}" ; \
		"$hostsEditScript" 0 "${HOSTNAME}" && \
		/usr/bin/ip link delete "${TAP}" && \
		"$removeAddScript" 0 "vm ${PID} ${HOSTNAME} ${GROUP}" "${RUNNING_FILE}" && \
		/etc/my_scripts_root/notify-send_all_users -u user "VM ${HOSTNAME} shutdown" "group ${GROUP}" -i /etc/my_icons/arch.png \
		' \
	) </dev/null &>/dev/null &
	# TODO: make this give errors to a file somewhere

	# Send information to the VM before starting the graphical session:
	# copy the most recent user.js into the VM.
	# Always update the router in /etc/hosts
    # This is needed because something overwrites /mnt/etc/resolv.conf
    # Make sure the right mounts exist
	# Start a graphical session by spinning in .bash_profile until magic file is created before startx
	# --ignore updating kernel so that module problems don't occur without a reboot
	# todo: wait until /dev/sdb is registered on guest

	if [[ "arch" == "$OS" ]]; then

		# Add the resolution and spice agent start to .xinitrc if it does not already exist
		local resline="xrandr --output \$(xrandr --current | head -2 | tail -1 | /usr/bin/cut -d \" \" -f1) --mode ${RESOLUTION}"
		local spiceline="spice-vdagent \&" # Need slash for /usr/bin/sed to not interpret it. Grep trailing slash error is piped to /dev/null
		# local pulseline="pulseaudio \&"
		local guest_user_xinitrc=/home/user/.xinitrc
		# QEMU_AUDIO_DRV=pa and QEMU_PA_SERVER=/run/user/1000/pulse/native

		# Ignore updating host ignored packages as they might be out of date
		local IgnorePkg=$(for b in $(grep "^[[[:space:]]*IgnorePkg[ =]" /etc/pacman.conf | /usr/bin/cut -d= -f2) ; do echo -n $b, ; done)
		[[ -n $IgnorePkg ]] && IgnorePkg="--ignore $IgnorePkg"

		# UPDATE HERE
		if [[ $only_update == 1 ]]; then
			ssh -i "$SSH_KEY_PATH" -p 22 -o "StrictHostKeyChecking=false" -o "UserKnownHostsFile=/dev/null" root@"$IP" "echo ${ROUTER} ROUTER > /etc/hosts ; echo nameserver ${ROUTER} > /etc/resolv.conf ; pacman -Sy --noconfirm --needed archlinux-keyring ; pacman -Su --noconfirm --needed ${IgnorePkg}"
		else
			echo "$USER_JS" | ssh -i "$SSH_KEY_PATH" -p 22 -o "StrictHostKeyChecking=false" -o "UserKnownHostsFile=/dev/null" root@"$IP" "cat > /root/user.js ; ( /usr/bin/grep -q -- '${resline}' '${guest_user_xinitrc}' || /usr/bin/sed -i '1s/^/${resline}\n/' ${guest_user_xinitrc} ) ; echo ${ROUTER} ROUTER > /etc/hosts ; echo nameserver ${ROUTER} > /etc/resolv.conf ; ${SMB_MOUNT_CMD} ; mkdir -p /etc/pacman.d/hooks/ ; echo \"$hook\" > /etc/pacman.d/hooks/tor-browser.hook ; pacman -Sy --noconfirm --needed archlinux-keyring ; pacman -Su ${PACKAGES} --noconfirm --needed --ignore linux,linux-lts,linux-zen,linux-hardened ${IgnorePkg}; [[ '${PACKAGES}' =~ pulseaudio ]] && (su - user --shell=/bin/sh -c 'export XDG_RUNTIME_DIR=/run/user/\$(id -u) ; /usr/bin/systemctl --user enable --now pulseaudio') ; [[ '${COPYPASTE}' == 1 ]] && (systemctl enable --now spice-vdagentd ; /usr/bin/grep -q '${spiceline}' '${guest_user_xinitrc}' 2>/dev/null || /usr/bin/sed -i '1s/^/${spiceline}\n/' ${guest_user_xinitrc}) ; su - user -c 'mkdir -p /home/user/.config/i3/ && /usr/bin/sed \"/i3-config-wizard/d\" /etc/i3/config > /home/user/.config/i3/config && /usr/bin/sed -i \"/^bar {[[:space:]]*$/a mode hide\" /home/user/.config/i3/config && echo -e \"default_floating_border none\\ndefault_border none\" >> /home/user/.config/i3/config' && [[ -n '${AUTOSTART}' ]] && for p in \\"${AUTOSTART}\\" ; do echo exec --no-startup-id exec \$p >> /home/user/.config/i3/config ; done ; touch /tmp/startx ; [[ -n '${PORTS}' ]] && (for f in /proc/sys/net/ipv4/conf/en*/route_localnet ; do echo 1 > \$f ; done ; for p in \\"${PORTS}\\" ; do iptables -w -t nat -A PREROUTING -d ${IP} -p tcp --dport \$p -j DNAT --to-destination 127.0.0.1 ; iptables -w -t nat -A PREROUTING -d ${IP} -p udp --dport \$p -j DNAT --to-destination 127.0.0.1 ; done)"
		fi
	fi

	"$hostsEditScript" 1 "${HOSTNAME}" "$IP"

	# Add iptables rules for this VM for the group based on it's config file. DNS rules already in place with a source checker that polls /etc/hosts
	/usr/bin/sudo python "${config[DAEMON]}" --add-rules-file-for-group "${CONFIG_DIR}/guests/rules/${HOSTNAME}.rules" "$GROUP"

	# Create the IP rules that come from DNS rules
	/usr/bin/sudo python "${config[DAEMON]}" --add-dns-ip-rules-for-source-and-group "$HOSTNAME" "$GROUP"
}

function routeIP(){
	local start="$1"
	local source="$2"
	local inIface="$3"
	local iface="$4"

	local reg="[0-9]{1,}"
	local mark=$(newVar "$start" "routeIP-${source}" "$MARK_FILE" "$reg" "$(seq -s ' ' 3 250)")
	! echo "$mark" | /usr/bin/grep -q -E -- "^${reg}$" && echo "ERROR with mark $mark" && exit 1
	
	local table=$(newVar "$start" "routeIP-${source}" "$TABLE_FILE" "$reg" "$(seq -s ' ' 3 250)")
	! echo "$table" | /usr/bin/grep -q -E -- "^${reg}$" && echo "ERROR with table $table" && exit 1

	local gateway=$(/usr/bin/ip route show | /usr/bin/grep --only-matching -E "via ${IPV4_REGEX} dev ${iface}" | head -1 | /usr/bin/cut -d' ' -f2)
	if [[ -z "$gateway" ]]; then
		echo "Could not get gateway from a 'via IP dev IFACE'"
		gateway=$(lastOctet $(ifaceAddress ${iface}) "1")
	fi
	# echo $mark $table $gateway

	local ROUTE_RULES=(
		"PREROUTING  -t nat    -s ${source} -i ${inIface}                      -j MARK --set-mark ${mark}"
		"FORWARD     -t filter -s ${source} -o ${iface} -i ${inIface}          -j ACCEPT"
		"FORWARD     -t filter -d ${source} -i ${iface} -o ${inIface}          -j ACCEPT"
		"POSTROUTING -t nat    -s ${source} -o ${iface} -m mark --mark ${mark} -j MASQUERADE"
	)

	local I=$(fromBottom 1 filter FORWARD)

	if [[ 1 == "$start" ]]; then
		
		/usr/bin/ip route add default via "$gateway" dev "$iface" table "$table"
		
		[[ -z "$(/usr/bin/ip rule list fwmark ${mark} table ${table})" ]] && /usr/bin/ip rule add fwmark "$mark" table "$table"
		
		for i in "${ROUTE_RULES[@]}" ; do
			iptables -w $1 -I $i -m comment --comment "route-${source}"
		done

		while iptables -t filter -D FORWARD -i "${inIface}" -s "${source}" -j DROP 2>/dev/null; do echo -n ; done
	else
		if ! iptables -t filter -C FORWARD -i "${inIface}" -s "${source}" -j DROP 2>/dev/null; then
			iptables -t filter -I FORWARD "$I" -i "${inIface}" -s "${source}" -j DROP
		fi

		for i in "${ROUTE_RULES[@]}" ; do
			iptables -w $1 -D $i -m comment --comment "route-${source}"
		done

		[[ -z "$(/usr/bin/ip rule list fwmark ${mark} table ${table})" ]] && /usr/bin/ip rule add fwmark "$mark" table "$table"

		/usr/bin/ip route del default via "$gateway" dev "$iface" table "$table"
	fi
}

# Basicly the same as startVM, except with Linux Containers (LXC)
# /var/lib/lxc/ exists on a Btrfs filesystem for CoW containers
function startLXC(){
	local BASE_LXC_NAME="base"
	local LXC_DIR="/var/lib/lxc/"

	[[ "btrfs" != "$(/usr/bin/df --output=fstype /var/lib/lxc/ | /usr/bin/tail -1)" ]] && echo "${LXC_DIR} should be a btrfs filesystem. Exitting." && exit 1
	[[ -f /var/lib/pacman/db.lck ]] && echo "pacman is currently running elsewhere and LXC may fail" && exit 1
	/usr/bin/grep -q -- "^lxc ${1} " "$RUNNING_FILE" && echo "LXC ${1} looks already started" && exit 1

	local CONFIG_FILE="${CONFIG_DIR}/guests/lxcs/${1}.conf"
	local GROUP="$2"

	local REQUIRED_GROUP=$(readConfigEqualsVal $CONFIG_FILE required_group | /usr/bin/tr -d ' ')
	[[ -n "$REQUIRED_GROUP" && "$GROUP" != "$REQUIRED_GROUP" ]] && echo "Required group set to ${REQUIRED_GROUP}, not ${GROUP}. Exitting." && exit 1
	! /usr/bin/ip link list "br-${GROUP}" 1>/dev/null && echo "Group does not look active. Exitting." && exit 1

	local HOSTNAME=$(readConfigEqualsVal $CONFIG_FILE hostname | /usr/bin/tr -d ' ')
	[[ -z "$HOSTNAME" ]] && echo "No hostname set in lxc config. Using ${1}" && HOSTNAME="$1"
	local PACKAGES=$(readConfigEqualsVal $CONFIG_FILE packages | /usr/bin/sed 's/^[[:space:]]*//;s/ $//')
	local USERS=$(readConfigEqualsVal $CONFIG_FILE users | /usr/bin/sed 's/^[[:space:]]*//')
	local SOUND=$(readConfigEqualsVal $CONFIG_FILE sound | /usr/bin/sed 's/^[[:space:]]*//')
	echo "$SOUND" | /usr/bin/grep -E -i -q -- "yes|y|true|1" && SOUND=1

	local DHCP_CONF="${TMP_DIR}/dhcp-${GROUP}.conf"

	/usr/bin/touch -- "$DHCP_CONF"
	/usr/bin/chmod 0666 -- "$DHCP_CONF"

	# Process the base container without a config file
	[[ "$BASE_LXC_NAME" == "$1" ]] && HOSTNAME=$BASE_LXC_NAME
	local LXC_CONFIG_FILE="${LXC_DIR}/${HOSTNAME}/config"

	/usr/bin/lxc-stop -n "$HOSTNAME" 2>/dev/null
	/usr/bin/lxc-wait -n "$HOSTNAME" -s "STOPPED"

	# Create the new container from the base container and update the config file with mounts
	if [[ ! -d "${LXC_DIR}/${HOSTNAME}/" ]]; then
		if [[ "$BASE_LXC_NAME" != "$1" ]]; then
			/usr/bin/lxc-copy -n "$BASE_LXC_NAME" -N "$HOSTNAME" -B btrfs --snapshot -l DEBUG
		else
			echo "Create container 'base'"
		fi
	fi
	
	# Remove old mounts from the config file
 	/usr/bin/sed -i '/^[[:space:]]*#\?[[:space:]]*lxc\.mount\.entry[[:space:]]*=/d' "$LXC_CONFIG_FILE"

	IFS=$'\n'
	for mount in $(readConfigEqualsVal "$CONFIG_FILE" mount | /usr/bin/sed 's/^[[:space:]]*//'); do
		local add="lxc.mount.entry = ${mount}"
		! /usr/bin/grep -q -- "^${add}$" "$LXC_CONFIG_FILE" && echo "$add" >> "$LXC_CONFIG_FILE"
	done

	# Use gocryptfs to mount encrypted host dirs unecrypted on the host
	if /usr/bin/grep -q -- "^enc_mount[[:space:]]*=" "$CONFIG_FILE" ; then
		PACKAGES="gocryptfs $PACKAGES"

		local MOUNT_TMP=
		# Run gocryptfs outside the container because of failure in container to load and then use fuse kernel module
		for enc_mount in $(readConfigEqualsVal "$CONFIG_FILE" enc_mount | /usr/bin/sed 's/^[[:space:]]*//'); do
			local MOUNT_NAME=$(echo $enc_mount | /usr/bin/awk '{print $1}') # What to use to access the password
			local MOUNT_HOST=$(echo $enc_mount | /usr/bin/awk '{print $2}' | /usr/bin/sed 's_/$__g')
			local MOUNT_CONTAINER=$(echo $enc_mount | /usr/bin/awk '{print $3}' | /usr/bin/sed 's_/$__g')
			
			# If config just gives name and where to mount in container, look in hosts/lxcs/enc_mounts/ dir for folder with that name
			if [[ -z $MOUNT_CONTAINER ]]; then
				MOUNT_CONTAINER="$MOUNT_HOST"

				local MOUNTS_DIR=${CONFIG_DIR}/guests/lxcs/enc_mounts/
				/usr/bin/mkdir -p -- "$MOUNTS_DIR"
				/usr/bin/chown 0:0 -- "$MOUNTS_DIR"
				/usr/bin/chmod 0700 -- "$MOUNTS_DIR"

				MOUNT_HOST=${MOUNTS_DIR}/${MOUNT_NAME}/
				[[ ! -d $MOUNT_HOST ]] && echo "Dir ${MOUNT_HOST} does not exist. Exitting." && exit 1 
				
				local MOUNT_TMP_DIR=${CONFIG_DIR}/guests/lxcs/enc_mounts_tmp/
				/usr/bin/mkdir -p -- "$MOUNT_TMP_DIR"
				/usr/bin/chown 0:0 -- "$MOUNT_TMP_DIR"
				/usr/bin/chmod 0700 -- "$MOUNT_TMP_DIR"

				MOUNT_TMP=${MOUNT_TMP_DIR}/${MOUNT_NAME}/
				/usr/bin/mkdir -p -- "$MOUNT_TMP"
			else
				MOUNT_TMP=${MOUNT_HOST}-$(echo "$MOUNT_HOST" | /usr/bin/md5sum | /usr/bin/cut -d ' ' -f1)
				/usr/bin/mkdir -p -- "$MOUNT_TMP"
			fi
			local MOUNT_PWD=$(/usr/bin/grep -- "^mount-${MOUNT_NAME} " "$LUKS_PASSWORD_FILE" | /usr/bin/tail -1 | /usr/bin/cut -d ' ' -f2)
			[[ -z $MOUNT_PWD ]] && MOUNT_PWD=$(/usr/bin/head /dev/urandom | /usr/bin/sha256sum | /usr/bin/cut -d ' ' -f1) && echo "mount-${MOUNT_NAME} ${MOUNT_PWD}" >> "$LUKS_PASSWORD_FILE" && echo "Must gocryptfs -init the folder with password in LUKS file. Exitting" && exit 1

			# gocryptfs mount "hides" a user:user dir behind root or creting files in the mount does not work, so user can read the encrypted files all the time and the unencryped when the lxc is running
			echo "$MOUNT_PWD" | /usr/bin/gocryptfs -force_owner 1000:1000 "$MOUNT_HOST" "$MOUNT_TMP" || exit 1
			/usr/bin/chown user:user "$MOUNT_TMP"

			local add="lxc.mount.entry = ${MOUNT_TMP} ${MOUNT_CONTAINER} none bind,create=dir 0 0"
			! /usr/bin/grep -q -- "^${add}$" "$LXC_CONFIG_FILE" && echo "$add" >> "$LXC_CONFIG_FILE"
		done
	fi
	unset IFS

	# Overwrite network configuration every time, since it could change based on group
	generateNETWORKandROUTERandIPandMACfrom "$DHCP_CONF" "$HOSTNAME" "$GROUP"
	local NETWORK=${global[NETWORK]}
	local IP=${global[IP]}
	local MAC=${global[MAC]}

	# For some reason the order of these network options LXC matters!!!
	local k=( "lxc.uts.name" "lxc.net.0.type" "lxc.net.0.link" "lxc.net.0.flags" "lxc.net.0.hwaddr" "lxc.net.0.veth.mode" "lxc.apparmor.profile" ) #"lxc.net.0.veth.pair" )
	local v=( "${HOSTNAME}"  "veth"           "br-${GROUP}"    "up"              "${global[MAC]}"   "bridge"              "unconfined"           ) #"${HOSTNAME}-veth"    )
	for i in "${!k[@]}"; do
		if ! /usr/bin/grep -q -E -- "^[[:space:]]*#?${k[i]}[= ]" "$LXC_CONFIG_FILE" ; then
			echo "${k[i]} = ${v[i]}" >> "$LXC_CONFIG_FILE"
		else
			/usr/bin/sed -i "s/^[[:space:]]*#\?${k[i]}[= ].*/${k[i]} = ${v[i]}/" "$LXC_CONFIG_FILE"
		fi
	done

	# Update here to a single function that sets keys and values
	k=( "DNS"               "FallbackDNS"       "MulticastDNS" "LLMNR" )
	v=( "${global[ROUTER]}" "${global[ROUTER]}" "no"           "no"    )
	for i in "${!k[@]}"; do
		/usr/bin/sed -i "s/^[[:space:]]*#\?${k[i]}[[:space:]]*=.*/${k[i]}=${v[i]}/" "${LXC_DIR}/${HOSTNAME}/rootfs/etc/systemd/resolved.conf"
	done

	# Add sound support
	local soundLine="lxc.mount.entry = /run/user/1000/pulse tmp/pulse/ none bind,optional,create=dir"
	! /usr/bin/grep -q -- "^${soundLine}$" "$LXC_CONFIG_FILE" && echo "$soundLine" >> "$LXC_CONFIG_FILE"

	local pacmanConf="${LXC_DIR}/${HOSTNAME}/rootfs/etc/pacman.conf"

	# Add the host for ROUTER, the changed DNS server, and the pacman info
	# Do the same for the pacman configs on real disk
	echo "Server = https://ROUTER:${config[PACMANPORT]}/archlinux/" > "${LXC_DIR}/${HOSTNAME}/rootfs/etc/pacman.d/mirrorlist"
	/usr/bin/sed -i 's_^[[:space:]]*#\?[[:space:]]*XferCommand[[:space:]]*=.*$_XferCommand = /usr/bin/curl -k -L -C - -f -o %o %u_' "$pacmanConf"
	/usr/bin/sed -i 's_^[[:space:]]*#\?[[:space:]]*CacheDir[[:space:]]*=.*$_CacheDir = /tmp/pacman-cache/_' "$pacmanConf" # CacheDir is probably commented out by default

	# Change journald and pacman to write to ram
	/usr/bin/sed -i 's_^[[:space:]]*#\?[[:space:]]*Storage[[:space:]]*=.*$_Storage=volatile_' "${LXC_DIR}/${HOSTNAME}/rootfs/etc/systemd/journald.conf"
	/usr/bin/sed -i 's_^[[:space:]]*#\?[[:space:]]*LogFile[[:space:]]*=.*$_LogFile=/run/pacman.log_' "$pacmanConf"

	# Add multilib and custom AUR pacman repo to pacman.conf so LXC can get AUR packages built on host
	local tmpFile=$(/usr/bin/mktemp)
	[[ $? -ne 0 ]] && echo "Error creating tmp file, line $LINENO!" && exit 1
	for repo in """[aur-repo]
SigLevel = Never
Server = https://ROUTER:${config[PACMANPORT]}/archlinux/aur/""" """[multilib]
Include = /etc/pacman.d/mirrorlist
"""
	do
		echo "$repo" > "$tmpFile"
		! BcontainsA "$tmpFile" "$pacmanConf" && cat "$tmpFile" >> "$pacmanConf"
	done
	/usr/bin/rm --force "$tmpFile"

	# Allow DHCP, pacman, and ssh
	HostRulesForGroup 1 "$GROUP"

	# Add ROUTER to /etc/hosts in container
	"$hostsEditScript" 1 ROUTER "${global[ROUTER]}" "${LXC_DIR}/${HOSTNAME}/rootfs/etc/hosts"

	# Add IP to /etc/hosts in container
	"$hostsEditScript" 1 "${HOSTNAME}" "${global[IP]}"

	# Allow prompter.py's dns out and in
	python "${config[DAEMON]}" --create "-d ${GROUP}-dns -p udp --dport 1053 -m owner --uid-owner 0 -m comment --comment until\ restart" "/usr/bin/python3.11" "out" "allow" "0" "0" "1"
	
	python "${config[DAEMON]}" --create "-d ${global[IP]}/32 -p tcp -m tcp --dport 22 -m owner --uid-owner 1000" "/usr/bin/ssh" "out" "allow" "0" "${GROUP}" "1"
	python "${config[DAEMON]}" --create "-s ${IP}/32 -d ${global[ROUTER]}/32 -p udp --dport 53 -m comment --comment until\ restart" "/usr/bin/python3.11" "in" "allow" "0" "${GROUP}" "1"
	python "${config[DAEMON]}" --add-rules-file-for-group "${CONFIG_DIR}/guests/rules/${HOSTNAME}.rules" "$GROUP"
	python "${config[DAEMON]}" --add-dns-ip-rules-for-source-and-group "$HOSTNAME" "$GROUP"

	/usr/bin/lxc-start -n "$HOSTNAME" --logfile /dev/stdout
	/usr/bin/lxc-wait -n "$HOSTNAME" -s "RUNNING"

	"$removeAddScript" 1 "lxc ${1} ${GROUP}" "$RUNNING_FILE"
	/usr/bin/lxc-wait -n "$HOSTNAME" -s STOPPED && (\
		/usr/bin/umount "$MOUNT_TMP" ; \
		python "${config[DAEMON]}" --delete-dns-ip-rules-for-source-and-group "$HOSTNAME" "$GROUP" ; \
		python "${config[DAEMON]}" --delete-rules-file-for-group "${CONFIG_DIR}/guests/rules/${HOSTNAME}.rules" "$GROUP" ; \
		python "${config[DAEMON]}" --delete "-s ${IP}/32 -d ${global[ROUTER]}/32 -p udp --dport 53 -m comment --comment until\ restart" "/usr/bin/python3.11" "in" "allow" "0" "${GROUP}" "1" ; \
		python "${config[DAEMON]}" --delete "-d ${IP}/32 -p tcp -m tcp --dport 22 -m owner --uid-owner 1000" "/usr/bin/ssh" "out" "allow" "0" "${GROUP}" "1"
		"$removeAddScript" 0 "lxc ${1} ${GROUP}" "$RUNNING_FILE" ; \
		"$hostsEditScript" 0 "${HOSTNAME}" ; \
		/etc/my_scripts_root/notify-send_all_users -u user "LXC ${HOSTNAME} shutdown" "group ${GROUP}" -i /etc/my_icons/lxc.png ; \
	) &

	# Wait for network to come online before pacman
	while ! /usr/bin/lxc-attach -n "$HOSTNAME" -- networkctl list eth0 2>/dev/null | /usr/bin/awk '$4 ~ "^routable$"' | /usr/bin/grep -q -- "routable" ; do
		sleep .1
	done

	/usr/bin/systemctl start nginx
	/usr/bin/lxc-attach -n "$HOSTNAME" -- /usr/sbin/sh -c "pacman -Rnsc --noconfirm $(echo $PACKAGES_REMOVE | tr ',' ' ') 2>/dev/null ; pacman -Sy --noconfirm --needed archlinux-keyring && pacman -Su --noconfirm --needed ${PACKAGES}"
	# Make sure user is in /etc/lxc/lxc-usernet: user veth lxcbr0 10

	# Mount any encrypted folders
	# The create=dir may create dirs as root if they don't exist. Need to change back to user:user in $HOME
	# IFS=$'\n'
	# for enc_mount in $(readConfigEqualsVal "$CONFIG_FILE" enc_mount | /usr/bin/sed 's/^[[:space:]]*//'); do
	# 	# Get password
	# 	MOUNT_NAME=$(echo $enc_mount | /usr/bin/awk '{print $1}') # What to use to access the password
	# 	# MOUNT_DIR=$(echo $enc_mount | /usr/bin/cut -d ' ' -f2) # Host encrypted folder
	# 	MOUNT_POINT=$(echo $enc_mount | /usr/bin/awk '{print $3}' | /usr/bin/sed 's_/$__g') # Where to mount the folder in the container

	# 	MOUNT_PWD=$(grep "^mount-${MOUNT_NAME} " "$LUKS_PASSWORD_FILE" | tail -1 | /usr/bin/cut -d ' ' -f2)
	# 	[[ -z $MOUNT_PWD ]] && MOUNT_PWD=$(head /dev/urandom | shasum -a 256 | /usr/bin/cut -d ' ' -f1) && echo "mount-${MOUNT_NAME} ${MOUNT_PWD}" >> "$LUKS_PASSWORD_FILE"

	# 	# Create a temporary dir to mount unencrypted and mount to real dir
	# 	MOUNT_TMP=/${MOUNT_POINT}-$(echo ${MOUNT_POINT}|/usr/bin/md5sum|cut -d' ' -f1)

	# 	lxc-attach -n "$HOSTNAME" -- sh -c "su - user -c 'mkdir -p /${MOUNT_POINT} ; echo ${MOUNT_PWD} | gocryptfs /${MOUNT_TMP} /${MOUNT_POINT}'"
	# done
	# unset IFS

	for user in $USERS ; do
		# Directory may be owned by root because of a mount with option create=dir
		echo """
/usr/bin/useradd --shell /bin/bash --home-dir /home/${user}/ --create-home -- ${user}
echo -e 'a\na' | /usr/bin/passwd -- ${user}
/usr/bin/rm --force /script.sh
""" > "${LXC_DIR}/${HOSTNAME}/rootfs/script.sh"
		/usr/bin/lxc-attach -n "$HOSTNAME" -- /bin/bash /script.sh

		echo """
export TERM=linux
export DISPLAY=:0
""" > "${LXC_DIR}/${HOSTNAME}/rootfs/home/${user}/.bashrc"
		[[ 1 == "$SOUND" ]] && echo "export PULSE_SERVER=/tmp/pulse/native" >> "${LXC_DIR}/${HOSTNAME}/rootfs/home/${user}/.bashrc"
		
		echo """
[[ -f ~/.bashrc ]] && . ~/.bashrc
""" > "${LXC_DIR}/${HOSTNAME}/rootfs/home/${user}/.bash_profile"
		/usr/bin/lxc-attach -n "$HOSTNAME" -- /usr/sbin/sh -c "/usr/bin/chown ${user}:${user} -R /home/${user}/"
	done
}

function initRules(){
	# makeMarkers
	# for mod in nfnetlink nfnetlink_log nf_pdlog xt_PDLOG xt_dns xt_proc ; do modprobe "$mod"; done
	# protect "-I"
	# [[ 1 == "$debug" ]] && echo "Set protective rules"

	ret=$(parseOpenvpnSyslog "${config[${DEFAULT_NAMESPACE}_ov_syslog]}" "${config[${DEFAULT_NAMESPACE}_ov_service]}" "1")
	[[ "$?" != 0 ]] && echo "$ret" && exit 1
	config[${DEFAULT_NAMESPACE}_ov_interface]=$(echo "$ret" | /usr/bin/cut -d' ' -f1)
	config[${DEFAULT_NAMESPACE}_ov_localIP]=$(echo "$ret" | /usr/bin/cut -d' ' -f2)
	config[${DEFAULT_NAMESPACE}_ov_prefix]=$(echo "$ret" | /usr/bin/cut -d' ' -f3)
	config[${DEFAULT_NAMESPACE}_ov_gateway]=$(echo "$ret" | /usr/bin/cut -d' ' -f4)
	config[${DEFAULT_NAMESPACE}_ov_dns]=$(echo "$ret" | /usr/bin/cut -d' ' -f5)
	"$hostsEditScript" 1 openvpn-dns "${config[${DEFAULT_NAMESPACE}_ov_dns]}"
	[[ 1 == "$debug" ]] && echo "Started default VPN"

	# UPDATE HERE to be cleaner
	dns_file="${TMP_DIR}/dns_ip" && touch "$dns_file" && chown root:root "$dns_file" && chmod 444 "$dns_file"
	echo "${config[${DEFAULT_NAMESPACE}_ov_dns]}" > "$dns_file"

	# filter FORWARD
	iptables -w -t filter -A FORWARD -j LOG --log-prefix "FORWARD " --log-uid
	[[ 1 == "$debug" ]] && echo "filter FORWARD"

	# March 23, 2022 no longer using ipset to save cycles by not hashing every single packet
	if [[ $USE_SAFETY -ne 0 ]]; then
		add_safety filter OUTPUT
		add_safety mangle POSTROUTING
	fi

	# filter OUTPUT
	iptables -w -t filter -A OUTPUT -m connmark ! --mark 0 -j ACCEPT
	iptables -w -t filter -A OUTPUT -o lo -j ACCEPT
	iptables -w -t filter -A OUTPUT ! -d "${config[${DEFAULT_NAMESPACE}_fh_ip]}" -j NFLOG --nflog-group "$NFLOGOUTPUTGROUP" --nflog-threshold 1
	iptables -w -t filter -A OUTPUT -j DROP
	[[ 1 == "$debug" ]] && echo "filter OUTPUT"

	# filter INPUT
	iptables -w -t filter -A INPUT -m connmark ! --mark 0 -j ACCEPT
	iptables -w -t filter -A INPUT -i lo -j ACCEPT
	iptables -w -t filter -A INPUT -j NFLOG --nflog-group "$NFLOGINGROUP" --nflog-threshold 1
	iptables -w -t filter -A INPUT -j DROP
	[[ 1 == "$debug" ]] && echo "filter INPUT"

	# nat OUTPUT
	iptables -w -t nat -A OUTPUT -m mark --mark "${config[TORMARK]}" -p tcp -j DNAT --to-destination "${config[TRANSPROXYIP]}":"${config[TRANSPROXYPORT]}"
	[[ 1 == "$debug" ]] && echo "nat OUTPUT"

	# mangle INPUT
	iptables -w -t mangle -A INPUT -m connmark ! --mark 0 -j ACCEPT
	iptables -w -t mangle -A INPUT -i lo -j ACCEPT
		iptables -w -t mangle -N $DEFAULT_IN
		iptables -w -t mangle -A $DEFAULT_IN -m connmark ! --mark 0 -j ACCEPT
		iptables -w -t mangle -A $DEFAULT_IN -j PDLOG --log-uid --log-level 0
	iptables -w -t mangle -A INPUT -j $DEFAULT_IN
	iptables -w -t mangle -A INPUT -j DROP
	[[ 1 == "$debug" ]] && echo "mangle INPUT"

	# mangle OUTPUT
	iptables -w -t mangle -A OUTPUT -m connmark ! --mark 0 -j ACCEPT
	iptables -w -t mangle -A OUTPUT -p udp --dport "${config[DNSPORT]}" -m dns --watch-queries -j DROP
	iptables -w -t mangle -A OUTPUT -o lo -j ACCEPT
		# Rules to mark for routing over not tor, including the initial first hop bridge's program
		iptables -w -t mangle -A mangle-out-MARK -m proc --path "${config[${DEFAULT_NAMESPACE}_fh_path]}" -m owner --uid-owner "${config[${DEFAULT_NAMESPACE}_fh_owner]}" -p "${config[${DEFAULT_NAMESPACE}_fh_proto]}" --dport "${config[${DEFAULT_NAMESPACE}_fh_port]}" -s "${LANIP}" -d "${config[${DEFAULT_NAMESPACE}_fh_ip]}" -o "${config[LANInterface]}" -j nontor-marker
		iptables -w -t mangle -A mangle-out-MARK -d 10.0.0.0/8,192.168.0.0/16,169.254.0.0/16,224.0.0.0/4,172.16.0.0/12 -j nontor-marker
		#Anything else goes through TOR only if from vpnIP source. Some stuff I think leaked once for tor-marker
		iptables -w -t mangle -A mangle-out-MARK -m mark --mark "${config[NONTORMARK]}" -j RETURN
		iptables -w -t mangle -A mangle-out-MARK -p tcp -s "${config[${DEFAULT_NAMESPACE}_ov_gateway]}"/16 -o "${config[${DEFAULT_NAMESPACE}_ov_interface]}" -j tor-marker
	iptables -w -t mangle -A OUTPUT -j mangle-out-MARK

		# Prompting and filtering rules (rules added from prompts using userspace daemon)
		iptables -w -t mangle -N $DEFAULT_OUT
		iptables -w -t mangle -A $DEFAULT_OUT -m connmark ! --mark 0 -j ACCEPT
		iptables -w -t mangle -A $DEFAULT_OUT -j PDLOG --log-uid --log-level 0
	iptables -w -t mangle -A OUTPUT -j $DEFAULT_OUT
	iptables -w -t mangle -A OUTPUT -j DROP
	[[ 1 == "$debug" ]] && echo "mangle OUTPUT"
	echo "Set new firewall"

	#routes="$(/usr/bin/ip route show table main)"
	#if [[ ! $(echo "$routes" | grep "^0.0.0.0/1 via ${config[default_ov_gateway]} dev ${config[default_ov_interface]} metric ${config[default_ov_metric]}") || \
	#      ! $(echo "$routes" | grep "^128.0.0.0/1 via ${config[default_ov_gateway]} dev ${config[default_ov_interface]} metric ${config[default_ov_metric]}") || \
	#      ! $(echo "$routes" | grep "${config[default_fh_ip]} via ${LANGateway} dev ${LANInterface}") ]]; then
	#	echo "Routing not set properly. Exiting..."
	#	exit 1
	#else
	#	echo "Set routes"
	#fi

	/usr/bin/chattr -i /etc/resolv.conf
	echo "nameserver ${config[DNSIP]}" > /etc/resolv.conf
	/usr/bin/chattr +i /etc/resolv.conf
	# if [[ $(wc -l /etc/resolv.conf | /usr/bin/cut -d" " -f1) -ne 1 ]] || ! grep "nameserver ${config[DNSIP]}" /etc/resolv.conf 1>/dev/null ; then
	# 	echo "/etc/resolv.conf Error. Exitting" ; exit 1
	# else
	# 	echo "Set /etc/resolv.conf"
	# fi

	# Add rules with python prompter script
	while IFS= read -r line; do
		[[ $line =~ ^[[:space:]]*# || -z $line ]] && continue
		# Last args are pid, group, enabled
		eval python "${config[DAEMON]}" --create $line "0" "${DEFAULT_NAMESPACE}" "1"
	done < ${CONFIG_DIR}/rules/rules.txt
	[[ 1 == "$debug" ]] && echo "Loaded python rules"

	# Add static iptables -w rules
	while IFS= read -r line; do
		[[ $line =~ ^[[:space:]]*# || -z $line ]] && continue
		eval iptables -w $line
	done < ${CONFIG_DIR}/rules/iptables.txt
	[[ 1 == "$debug" ]] && echo "Loaded iptables rules"

	# UPDATE THIS
	# Add static routes
	# while IFS= read -r line; do
	# 	[[ $line =~ ^[[:space:]]*# || -z $line ]] && continue
	# 	eval ip route $line
	# done < ${CONFIG_DIR}/startvpn.routes
	# [[ 1 == "$debug" ]] && echo "Set static routes"

	local createdPersistGroup=0 # If any groups say to persist, like tun0 used for a Tor browser VM
	local createdPersistNS=0    # If any pairs say to persist, set up their rules
	for key in "${!config[@]}" ; do
		# echo $key ${config[$key]}
		if [[ "$key" =~ ^.*_BOOT$ && 1 == "${config[${key}]}" ]]; then
			local group=$(echo "$key" | /usr/bin/rev | /usr/bin/cut --characters 6- | /usr/bin/rev )
			[[ 1 == "$debug" ]] && echo "Stopping group ${group}"
			startStopGroup 0 "$group"
			[[ 1 == "$debug" ]] && echo "Starting group ${group}"
			startStopGroup 1 "$group"
			[[ 1 == "$debug" ]] && echo "Started group ${group}"
			createdPersistGroup=1
		elif [[ "$key" =~ ^.*_persist$ && 1 == "${config[${key}]}" ]]; then
			local namespace=$(echo "$key" | /usr/bin/rev | /usr/bin/cut --characters 9- | /usr/bin/rev )
			[[ 1 == "$debug" ]] && echo "Starting namespace ${namespace}"
			writeServicesForName 1 "$namespace"
			startStopNamespace 0 "$namespace" 1 1
			# start=1,namespace,dontStartVPN=1,persist=1
			startStopNamespace 1 "$namespace" 1 1
			[[ 1 == "$debug" ]] && echo "Started namespace ${namespace}"
			createdPersistNS=1
		fi
	done
	[[ 1 == "$debug" && 1 == "$createdPersistNS" ]] && echo "Created persistent namespaces"
	[[ 1 == "$debug" && 1 == "$createdPersistGroup" ]] && echo "Created persistent groups"
}

# Stop all the things in the running file (namespaces, groups, hostapds)
# Hostapds rely on groups, groups might rely on namespaces
# Read in reverse order so when a namespace or group relies on another it is stopped first
function stopRunning(){
	local tmp=$(/usr/bin/mktemp)
	[[ $? -ne 0 ]] && echo "Error creating tmp file, line $LINENO!" && exit 1
	/usr/bin/tac "$RUNNING_FILE" > "$tmp"

	[[ 1 == "$debug" ]] && echo -e "\nStopping hostapds"
	while IFS= read -r line; do
		[[ $line =~ ^hostapd ]] && startStopHostapd 0 "$(echo $line | /usr/bin/cut -d' ' -f2)"
	done < "$tmp"

	[[ 1 == "$debug" ]] && echo -e "\nStopping groups"
	while IFS= read -r line; do
		[[ $line =~ ^group ]] && startStopGroup 0 "$(echo $line | /usr/bin/cut -d' ' -f2)"
	done < "$tmp"

	[[ 1 == "$debug" ]] && echo -e "\nStopping namespaces"
	while IFS= read -r line; do
		var=$(echo $line | /usr/bin/cut -d' ' -f2)
		if [[ $line =~ ^namespace ]]; then
			writeServicesForName 0 "$var"
			startStopNamespace 0 "$var" 0 "${config[${var}_persist]}"
		fi
	done < "$tmp"

	/usr/bin/rm --force "$tmp"
}

function setLANinfo(){
	local iface="${config[LANInterface]}"

	while ! networkctl status "$iface" --lines 0 | /usr/bin/grep -q -- "^[[:space:]]*State: routable (configured)$" ; do
		sleep .1
	done

	LANIP=$(networkctl status "$iface" --lines 0 | /usr/bin/grep "^[[:space:]]*Address: " | /usr/bin/awk '{print $2}')
	LANGateway=$(networkctl status "$iface" --lines 0 | /usr/bin/grep "^[[:space:]]*Gateway: " | /usr/bin/awk '{print $2}')

	LANSubnet=$(/usr/bin/ip address show "$iface" | /usr/bin/awk '$1 ~ "^inet$" {print $2}')
	! echo $LANSubnet | /usr/bin/grep -q -E -- "^${IPV4_REGEX}/[0-9]{1,2}$" && echo "Bad LAN Subnet: ${LANSubnet}" # && exit 1
}

while [ $# -gt 0 ]; do
	case "$1" in
	--services) # Needed when connecting for the first time for the main ss-local and OpenVPN instances.
		! /usr/bin/ip link show "${config[LANInterface]}" 1>/dev/null && exit 1
		clearEverything
		makeMarkers

		for mod in nfnetlink nfnetlink_log ip_set $MODULES ; do modprobe "$mod" ; done

		# Add the Tor IPs to the ipset if non existant using the systemd path file above. See `tor-browser.rules` in sample-config-dir/guests/rules/ for iptables ipset syntax
		/usr/bin/ipset list tor 1>/dev/null || touch /var/lib/tor/cached-microdesc-consensus

		writeServicesForName 1 "${DEFAULT_NAMESPACE}"

		setLANinfo

		protect "-I"
		[[ 1 == "$debug" ]] && echo "Set protective rules"
		
		/usr/bin/ip route add "${config[${DEFAULT_NAMESPACE}_fh_ip]}" via "$LANGateway" dev "${config[LANInterface]}" metric 0 table main
		for i in "${INITIAL_SERVICES[@]}" ; do
			restartServices "$i"
			sleep 1
		done
		initRules
		protect "-D"

		echo "Done with --services"
		/etc/my_scripts_root/notify-send_all_users "Done with --services"
		exit 0
	;; --services-stop)
		# Forcefully stop all hostapd, namespace, and groups (note that the user prompts are still there)
		FORCE=1
		stopRunning
		writeServicesForName 0 "${DEFAULT_NAMESPACE}"
		for i in "${INITIAL_SERVICES[@]}" "tor.service" ; do /usr/bin/systemctl stop $i ; done
		exit 0
	;;
	--stop-running)
		stopRunning
		exit 0
	;;
	--namespace|--namespace-stop)
		# Start another ss-local, openvpn, and netNS to run programs in (aren't routed over Tor and have separate VPN exit IP)
		# /usr/bin/sudo ip netns exec nontor1 /usr/bin/sudo -u "user" program arg1 arg2
		[[ -z "$2" ]] && echo "Must give namespace from config file" && exit 1
		if [[ "--namespace" == "$1" ]]; then
			writeServicesForName 1 "$2"
			# echo "${config[${2}_vpn_provider]}" "${config[${2}_vpn_account]}"
			startStopNamespace 1 "$2" 0 "${config[${2}_persist]}"
		else
			writeServicesForName 0 "$2"
			startStopNamespace 0 "$2" 0 "${config[${2}_persist]}"
		fi
		exit 0
	;;
	--group|--group-stop)
		[[ -z "$2" ]] && echo "Must select a group" && exit 1
		if [[ "--group" == "$1" ]]; then startStopGroup 1 "$2" ; else startStopGroup 0 "$2" ; fi
		exit 0
	;;
	--hostapd|--hostapd-stop)
		[[ -z "$2" ]] && echo "Must select a group" && exit 1
		if [[ "--hostapd" == "$1" ]]; then startStopHostapd 1 "$2" "$2"_ap ; else startStopHostapd 0 "$2" "$2"_ap ; fi
		exit 0
	;;
	--vm) # Create a VM (should be done as non-root, but need /usr/bin/sudo for creating tap interfaces)
		[[ -z "$2" ]] && echo "Must select a hostname" && exit 1
		[[ -z "$3" ]] && echo "Must select a group" && exit 1
		startVM "$2" "$3"
		exit 0
	;;
	--lxc) # Create a Linux Container, like --vm, in group
		[[ -z "$2" ]] && echo "Must select a hostname" && exit 1
		[[ -z "$3" ]] && echo "Must select a group" && exit 1

		# Make sure base container exists on btrfs
		# Make a copy of it for a new container
		startLXC "$2" "$3"
		exit 0
	;;
	--route|--route-stop) # Allow a host to route its internet traffic to this machine to a namespace
		[[ -z "$2" ]] && echo "Must select a source" && exit 1
		[[ -z "$3" ]] && echo "Must select a source interface" && exit 1
		[[ -z "$4" ]] && echo "Must select a dest interface" && exit 1
		if [[ "--route" == "$1" ]]; then routeIP 1 "$2" "$3" "$4" ; else routeIP 0 "$2" "$3" "$4" ; fi
		exit 0
	;;
	--test)
		getTable
		exit 0
		;;
	*)
		echo "startvpn.sh: Unknown argument!"
		exit 1
	;;
	esac
done
