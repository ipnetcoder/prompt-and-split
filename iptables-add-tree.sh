#!/bin/bash
# Copy some iptables-extensions into a clean iptables source dir

dest="iptables/"
/usr/bin/cp --force --recursive iptables-tree/* "$dest"
