#!/bin/bash
# Compile custom kernel on $server (ssh+scp) then install it on $vm, $server, or locally

set -e
##########################
# CHNAGE THESE VARIABLES #
##########################
# Names "$server" and "$vm" are based on entries in ~/.ssh/config so 'ssh $server' must work
# "$server" is meant to give the possibility for a remote build server, but adding a local entry with ssh listening locally works as well
# "$vm" is meant as an install destination for testing, not a build server. It is not necessary to have and mostly unmaintained and commented out below
# "$comm" is meant to either be "ssh ..." to build on a server or "bash -c" to build locally
server=build-server
vm=vm
comm="ssh $server"
#comm="bash -c"

# Directory on the local machine that linux-"${ver}".tar.xz is stored in
compressedSource="/home/user/.kernels/"

# $serverBase is directory on server to store the uncompressed kernel source tree. linux-"${ver}".tar.xz is sent to $serverCompressedBase first
# The uncompressed kernel source tree needs > 10GB of space
serverBase="/home/user/"
serverCompressedBase="/tmp/"

# Local directory for kernel and initramfs to be sent on install
bootDir="/boot/"

# Where on the server modules are installed to. Make sure this includes $ver in it for 'headers_install' source option below
modulesPath="/tmp/"
##########################

# Version of the kernel being used. Set with arg2
ver=

# Make rsync work with local filesystem if $comm does not start with "ssh"
rsyncServer="${server}:"
[[ ! $comm =~ ssh ]] && rsyncServer=""

PV_OPTS=

for exe in "ssh" "scp" "pv" "sudo" "wget" "rsync" "zstd" "mkinitcpio" "grub-mkconfig" "python" "find" "xargs"; do
	! which $exe > /dev/null 2>&1 && echo "Install ${exe} or add it to PATH" && exit 1
done

# Copy all the modified local source files to the build server's kernel source tree
function remote_add_init(){
	[[ $EUID -eq 0 ]] && echo "Don't run this as root" && exit 1

	# Copy the compressed kernel source tree to server's /tmp/ and extract it to $serverBase if not done already
	if $comm "file ${serverBase}/linux-${ver}" | grep -q -- "(No such file or directory)" && [[ $EUID -ne 0 ]]; then
		echo "linux-${ver} directory not found on ${server} in ${serverBase}. Adding."
		if $comm "file /tmp/linux-${ver}.tar.xz" | grep -q -- "(No such file or directory)" ; then
			echo "linux-${ver}.tar.xz directory not found on ${server} in ${serverBase}. Adding."
			image="$compressedSource"/linux-"${ver}".tar.xz
			/usr/bin/pv -s $(du -sh "$image" | awk '{print $1}') $PV_OPTS "$image" | $comm "cat > ${serverCompressedBase}/linux-${ver}.tar.xz" #xz was not working over stdin for some reason, so must scp
		fi
		$comm "cd /tmp/ && tar xvfJ linux-${ver}.tar.xz -C ${serverBase} >/dev/null"
		echo "Finished expanded compressed source tree on ${server} in ${serverBase}"
	fi

	# Make sure the config in the tree directory matches the version
	config=tree/.config

	# Used to keep multiple version's configs in one directory
	/usr/bin/cp -f configs/${ver} $config

	if ! grep -q -- "Linux/x86 $ver Kernel Configuration" "$config" ; then
		echo "No appropriate config file found! Exitting."
		exit 1
	fi
}

# Apply changes for this project (separating this from init option allows an optional clean kernel build+install)
function remote_add_copy(){
    # Push added files to server filesystem
    /usr/bin/rsync -azP tree/ "${rsyncServer}""${serverBase}/linux-${ver}/"

    # Copy mod-tree to server filesysyem and run it
    cat ./mod-tree.sh | $comm "cat > /tmp/mod-tree.sh"

    $comm "/bin/bash /tmp/mod-tree.sh ${serverBase}/linux-${ver}/ && \
	    cd ${serverBase}/linux-${ver}/ && \
        make olddefconfig 1>/dev/null ; \
        /bin/rm /tmp/mod-tree.sh"
}

# Build kernel
function remote_add_make(){
	[[ $EUID -eq 0 ]] && echo "Don't run this as root" && exit 1
	$comm "cd ${serverBase}/linux-${ver} && time make -j\$(nproc) bzImage 2>&1 ; echo \$?"
}

# Build modules
function remote_add_modules(){
	[[ $EUID -eq 0 ]] && echo "Don't run this as root" && exit 1
	$comm "cd ${serverBase}/linux-${ver} && time make -j\$(nproc) modules 2>&1 ; echo \$?"
}

# Install modules to /tmp/lib/modules/ on server
function remote_add_modules_install(){
	[[ $EUID -eq 0 ]] && echo "Don't run this as root" && exit 1
	$comm "mkdir -p ${modulesPath}/${ver}"
	$comm "cd ${serverBase}/linux-${ver} && (export INSTALL_MOD_PATH=${modulesPath}/${ver} ; time make -j\$(nproc) modules_install 2>&1) ; echo \$?"
}

# Transfer all modules and kernel from server to local. Run this as root.
function remote_add_install(){
	[[ $EUID -ne 0 ]] && echo "Need root privs to install locally" && exit 1

	# Server's modules were installed with /tmp/ as root directory
#	/usr/bin/rm --force /lib/modules/${ver} && \
	set -e
	/usr/bin/rsync -azP --no-owner --no-group "${rsyncServer}${modulesPath}/${ver}/lib/modules/${ver}" /usr/lib/modules/
	echo "All modules installed"
	/usr/bin/ln --symbolic --force /usr/lib/modules/${ver} /lib/modules/
	echo "Symlinked /lib/modules/${ver} to real /usr/lib/modules/${ver}"

	# Copy vmlinuz here to $bootDir
	$comm "cd ${serverBase}/linux-${ver}/ && cat ${serverBase}/linux-${ver}/\$(make -s image_name)" > "$bootDir"/vmlinuz-"$ver"
	mkinitcpio --kernel "$ver" --config /etc/mkinitcpio.conf --generate "$bootDir"/initramfs-"$ver".img
	grub-mkconfig -o "$bootDir"/grub/grub.cfg
	[[ $? -ne 0 ]] && echo "ERROR mkinitcpio or grub-mkconfig failed!" && exit 1
}

# Install everthing to the vm. Needs to be after `make modules_install` when all mods are in [/tmp/]/lib/modules/$ver
#elif [[ "install_vm" == "$1" ]]; then
#	[[ $EUID -eq 0 ]] && echo "Don't run this as root" && exit 1
#	set -e
#	if [[ -z "$2" ]]; then
#		ssh $vm "rm -rdf /lib/modules/${ver}"
#		ssh $server "cd /tmp/lib/modules/ && tar zcf - ${ver}" | \
#			pv -s $($comm "du -sh /tmp/lib/modules/${ver}" | awk '{print $1}') $PV_OPTS | \
#			ssh $vm 'tar zxf - -C /lib/modules/'
#	fi
#	echo "Copying kernel image"
#	ssh $server "cat ${serverBase}/linux-${ver}/arch/x86/boot/bzImage" | pv $PV_OPTS | \
#	ssh $vm "> ${bootDir}/vmlinuz-${ver} && mkinitcpio -k ${bootDir}/vmlinuz-${ver} -c /etc/mkinitcpio.conf -g ${bootDir}/initramfs-${ver}.img && grub-mkconfig -o ${bootDir}/grub/grub.cfg"
#	set +e

# Export the headers to /tmp/
# /tmp/ file already likely exists from the modules export, so a sub directory for this command must be used
function remote_add_headers(){
	[[ $EUID -eq 0 ]] && echo "Don't run this as root" && exit 1
	dir="/tmp/headers_install-${ver}/"
	$comm "/usr/bin/rm -rfd ${dir} && mkdir ${dir} && cd ${serverBase}/linux-${ver} && make -j\$(nproc) headers_install ARCH=$(uname -m) INSTALL_MOD_STRIP=1 INSTALL_HDR_PATH=${dir} 2>&1"
}

# Install the headers to the local machine from server:/tmp"
function remote_add_headers_install(){
	[[ $EUID -ne 0 ]] && echo "Run this as root" && exit 1

	if [[ ! -f PKGBUILD ]]; then
		echo "About to download PKGBUILD from Github for the ArchLinux package 'linux' in order to use its script for installing headers"
		wget https://raw.githubusercontent.com/archlinux/svntogit-packages/packages/linux/trunk/PKGBUILD
		[[ $? -ne 0 ]] && echo "Could not download PKGBUILD!" && exit 1
	fi

	# Run only the function '_package-headers' from a PKGBUILD for package 'linux-headers' on the server
	sed -n '/^_package-headers/, /^}/p' PKGBUILD | \
	sed '1 s_^_#!/bin/bash\n_ i' | \
	sed '$ a _package-headers' | \
	$comm " \
		cd ${serverBase}/linux-${ver}/ && \
		cat > install.sh && \
		chmod +x install.sh && \
		export pkgdir=${modulesPath}/${ver} && \
		export _srcname=${serverBase}/linux-${ver}/ && \
		echo ${ver} > version && \
		./install.sh && \
		/usr/bin/rm --force install.sh \
	"

	# Copy /tmp/usr/lib/modules/${ver}/build to local
	rsync -azP --no-owner --no-group "${rsyncServer}${modulesPath}/${ver}/usr/lib/modules/${ver}/build" /usr/lib/modules/${ver}
}

# Compile single modules
#elif [[ "mod" == "$1" ]]; then
#	[[ $EUID -eq 0 ]] && echo "Don't run this as root" && exit 1
##	for mod in "net/netfilter/nf_pdlog.c" "net/netfilter/xt_dns.c" "net/netfilter/xt_PDLOG.c" "net/netfilter/xt_proc.c"
##	for mod in "drivers/test/test.c"
#	for mod in "drivers/test/socket_process_lsm_helper.c"
#	do
#		o=$(echo "$mod" | rev | cut -d '/' -f1 | rev | sed 's/\(.*\)\.c$/\1.o/g')
#		ko=$(echo "$mod" | rev | cut -d '/' -f1 | rev | sed 's/\(.*\)\.c$/\1.ko/g')
#		module=$(echo $o | rev | cut -c3- | rev)
#		final="/lib/modules/${ver}/kernel/$(echo "$mod" | rev | cut -d '/' -f2- | rev)/"
#		echo $o $ko $final $module
#
#		# Copy the file to the server. UPDATE HERE to get location from add-tree.sh ?
#		scp tree/${mod} ${server}:${serverBase}/linux-${ver}/${mod}
#
#		# Clear dir /tmp/mod/ and output built modules there
#		ssh $server "rm -rfd /tmp/mod ; mkdir /tmp/mod && cd /tmp/mod && echo -e \"obj-m = $o\nall:\n\tmake -C ${serverBase}/linux-${ver} M=\$(pwd) modules\n\" > Makefile && cp ${serverBase}/linux-${ver}/${mod} . && make ; echo \$?"
#
#		# Install the module locally
#		if [[ "$2" == "local" ]]; then
#			/usr/bin/scp ${server}:/tmp/mod/"${ko}" /tmp/ && \
#			/usr/bin/zstd --force /tmp/"${ko}" && \
#			/usr/bin/sudo /usr/bin/chown root:root /tmp/"${ko}".zst && \
#			/usr/bin/sudo /usr/bin/chmod 644 /tmp/"${ko}".zst && \
#			/usr/bin/sudo /usr/bin/mv --force /tmp/"${ko}".zst "$final"
##			sudo rmmod "$module" ; sudo modprobe "$module"
#
#		# Install to the vm
#		elif [[ "$2" == "vm" ]]; then
#			ssh $server "cd /tmp/mod && cat $ko" | ssh $vm "> ${final}/${ko} && zstd --force ${final}/${ko}"
#
#		# Install on build server. Needs root there
#		elif [[ "$2" == "server" ]]; then
#			ssh $server "zstd --force /tmp/mod/$ko && mv -f /tmp/mod/${ko}.zst $final && chown root:root $final/$ko.zst"
#
#		elif [[ -n "$2" ]]; then
#			echo "Unknown option"
#		fi
#	done

# Assuming no other functions with these names
if declare -F | awk '{print $3}' | grep -q -- "^remote_add_${1}$" ; then
	ver=$2
	[[ -z $ver ]] && echo "arg2 must be the version of the kernel used. Exitting." && exit 1

	remote_add_$1
else
	echo """
Unknown option! These options should be run in order:

init:            Move kernel source and config to desired location
copy:            (optional) make this project's changes to the source tree. Not specificing builds a clean upstream kernel
make:            builds the kernel
modules:         builds the kernel modules
modules_install: installs the modules to a temporary location
install:         (run as root) installs the kernel and modules to final system location
headers:         (optional) copies kernel headers required for single module compilation, like what dkms needs, to a temporary location
headers_install  (optional, run as root) moves headers to final system location
"""
	exit 1
fi
