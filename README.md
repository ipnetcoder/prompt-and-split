## Prompts for network activity tied to processes. Split/route traffic for processes and VMs with prompting.

## Note that this readme does not do a great job explaining and is incomplete

### Features:
- Iptables extension to match packets based on creating process (calculated in kernel, requires compiling kernel)
- Iptables extension to match packets based on hostname or domain wildcard, using kernelspace DNS table
- No traffic ever escapes or enters the machine without the user creating an allow rule
- All network connections are drop and prompted using a userspace script to create an ACL
- All DNS queries are prompted and dropped to create an ACL, so no queries are ever leaked unnecessarily
- Create routing rules for each connection and DNS queries per process to go over Tor, other proxies, or VPNs running in network namespaces
- Easily provision VMs, LXCs, and hostapd instances with prompting and routing for their traffic
- Limitations:
	- This readme and installation guide are lacking and time consumer and meant for advanced users.
	- This code and kernel are meant to run in on an Arch Linux system
	- The code modifications made to the kernel source tree are using sed and awk, not a patch
	- The start.sh script intends to default route all (allowed) traffic to one of 2 paid VPN services and fails if connection/authentication to one of them fails. No intention yet to move away from this yet.
	- Any blocked queries are not queued and sent out if prompt is allowed making generating ACL initially lengthy.

- Feature inspirations: Little Snitch, Qubes, ip route/rule, "split tunneling", sandboxing
