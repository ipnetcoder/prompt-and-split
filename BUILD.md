- Be using Arch Linux
- Clone this repo
- Obtain and verify a .tar.xz kernel source tree from https://kernel.org
- Read and change varaibles at the top of `remote-add.sh` to reflect the kernel version, directories and your ssh setup. Move compressed kernel source to location specified. Make the uncompressed iptables source tree exist in this repo as a directory named iptables.
- `./remote-add.sh init` to copy files and make changes to the source tree
- `./remote-add.sh make` to compile kernel image
- `./remote-add.sh modules` to compile kernel modules
- `./remote-add.sh modules_install` to install the modules to /tmp/
- `./remote-add.sh install` (as root, note possible ssh user config changes) to 1) install the kernel image and modules to the local filesystem's "$bootDir" and /lib/modules/ and 2) run mkinitcpio and grub-mkconfig

- clone https://git.netfilter.org/iptables/ for the iptables userspace code. Can also download a tar from https://netfilter.org/projects/iptables/downloads.html and PGP verify with `gpg --verify`
- `./iptables-add-tree.sh` to copy custom modules into the source tree
- In the iptables repo: 
	- `autoconf` then `./autogen.sh`
	`./configure --prefix=/usr --sbindir=/sbin --disable-nftables --enable-libipq --with-xtlibdir=/lib/xtables && make` to configure build iptables
	- `make install` as root to install the iptables modules locally
- Reboot, select the new custom kernel in grub, see USAGE.md
