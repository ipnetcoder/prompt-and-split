#!/usr/bin/python
# For iptables layout:
	# Use the -t mangle table since it is right after conntrack and before the others

	#First restore mark. -j CONNMARK --restore-mark
	#-m mark ! --mark 0 -j
	#If mark not 0, send to be allowed or whatever else
	#If 0:
		# Sort by process to a separate chain for that process. An allow and Drop chain, allow first, then Drop
		# In that process chain, linearly check for deniable attribute rules. -j DROP
		# In that process chain, linearly check for allowable atrribute rules. -j CONNMARK --set-xmark 1

		# Then at the end -j CONNMARK --save-mark
		# Then if mark is not 0, allow since the packet matched one of the allow rules
		# Now -j PDLOG to tell userspace that a packet didn't match!
		# Could immediately -j DROP now to avoid hitting any other chain checks or anything
	#If not 0, then allow

	# When deleting an allow rule, delete the -j CONNMARK --set-xmark target, then from terminal run 
	# conntrack -U -m number -m 0 so that now any conntracked connection with that mark is now 0 
	# When deleting a delete rule, just remove it from userspace so it will now appear in prompts

	# In order to create rules until a process quits, insert a chain with -m proc --pid 123 -j out-{}-allow-pid at the begining of the out-{}-allow chain.
	# Doesn't matter if some later all the time rules get inserted before it.

# Some default types and values:
	# Packet rules:
PACKET_NO_PATH = ""
PACKET_NO_PATH_STR = "(no path)" # String used in the packet rule editor
PACKET_ANY_PATH = "any"
PACKET_ERROR_PATH = "error"
		# process == "" means --no-path, so probably a kernel packet or kernel failed to get path
		# process == "any" means any path, so don't call -m proc
		# process == "/.../program" is assumed otherwise and -m proc --path is used
		# pid == 0 means all pids
		# pid != 0 is assumed otherwise for a specific integer pid 
LOG_DEFAULT_VALUE = None
		# The default value set for fields of a -j LOG string

	# DNS rules:
		# process is same as above
		# pid == 0 means all pids
		# pid == -1 means the rule is meant to apply to all pids, but will go away when the daemon restarts
		# pid == 123 means the rule applies to that pid until the pid_active sweeper sees that pid is no longer in use

		# xt_dns gets uid and gid in the same way as -j LOG and logs this in the proc tid file.
		# If kernel wasn't able to get the uid/gid (the packet's socket had no associated file), these values are used
DNS_NO_UID = None
DNS_NO_GID = None
PID_ALL = 0
MATCH_ALL_QNAMES = "*."

DNS_EXPIRE_NEVER = 0 # DNS rule never expires
DNS_EXPIRE_RESTART = -1 # Expiration time set to this to only work until restart
# DNS_SOURCE_ANY = "any"

# When creating a DNS rule, what should the default allowed UID be?
# This is also used for 
DNS_DEFAULT_UID = 1000

UID_ALL = -1
GID_ALL = -1
	# uid and gid are usigned integers, so -1 will be used in ACLs to mean match any

SOURCE_DEFAULT = "localhost"
SOURCE_DEFAULT_ADDR = "127.0.0.1"
ALL_SOURCES = None # Used from special dns routing

HOSTS_FILES = ["/etc/hosts"]
# Read the files in HOSTS_FILES at startup
READ_HOSTS = True
# Dict of name to IP address from hosts files
HOSTS_DATA = {}
# If READ_HOSTS is true, poll means read each one for each query
POLL_HOSTS = True

# fix input so user can give input when there is no active prompt and it is separate from each parser function
# Root daemon and user level UI: How to communicate?
	# Root daemon should completely control rule expiration to not add an extra iptables module for checks
	# Client ask root to List (DNS/IP) rules:
		# Write that command to first line, send signal SIGUSR1 to root daemon
		# Server writes out pickel info and sends signal SIGUSR1 to client
		# Format is process, io, allowdeny, rule string
	# Delete/Add rules
		# Write that command to first line and the rule to the second, send SIGUSR1 to root daemon

# Set DNS to hashing rather than all that parsing in xt_dns.c
# Sweep for pid expirations
	# Start a timer for expirations, create a sweep for startup
# Fix kernel DNS matching regexes
	# First include end matching
# Check if just added rule would take care of any of the rules in the current Q and remove them
	# Hard to check?
# Split this program more and don't rely on globals as much
# Fix issue of adding stuff to program while it is running

# Must get the group. Groups allow different instances to run, so 0 for IN+OUTPUT, 1 for PREROUTING
# This means separate instances with separate rulesets can run for individual tap adapaters in PREROUTING
# For example I use group 1 for one local VM and group 2 for an Internet Sharing setup with a second device
# Although there isn't process info from those systems, I still get the benefit of monitoring and blocking specific traffic,
# all in a separate prompter from my local system so there is no interference ()

IPV4_REGEX = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
DISMISS_RESPONSES = ("d", "da")

# Used to properly add and call things in hostname_process_acl
def dns_dict(source, group_str, pid, uid, arg1=None, decision=None, enabled=None, expiration_time=None):
	ret = dict()

	if None != decision:
		ret["decision"] = decision
	if None != enabled:
		ret["enabled"] = enabled
	if 0 != expiration_time and None != expiration_time:
		ret["expiration_time"] = expiration_time
	if None != source:
		ret["source"] = source
	if None != group_str:
		ret["group_str"] = group_str
	if pid != PID_ALL and pid != None:
		ret["pid"] = pid
	if uid != UID_ALL and uid != None:
		ret["uid"] = uid

	return ret

import sys
CONFIG_DIR = "/etc/prompt-and-split/"
MEMORY_MODIFICATION_FILE = "/run/prompt-and-split/python_memory"
sys.path.insert(0, CONFIG_DIR)

from configs.config import *

# Set the specific dns query that is allowed through that tunnel, so can check VPN with curl, but use ok otherwise
# Remember to also set a Tor routing exemption in iptables rules!
# Sort by process, then uid, then list of dicts with 'qname' regex and 'addr' to that query send to
from configs.special_dns_config import *

# Processes to use the cmdline arguments (like python running a certain script)
# The actual process (ie python) will be overwritten by the first argument in the cmdline file in /proc/
# PROCESS_CHECK_CMDLINE = ( "/usr/bin/python3" "/usr/bin/python3.10")
PROCESS_CHECK_CMDLINE = ()

# First line_num used for dns rules displayed
LINE_NUM_START = 0

# These are some hard coded values:
# The value used from -j PDLOG --log-level. This corresponds to all of this group's process allow/deny chains
# GROUP = sys.argv[1]
# Name that corresponds to GROUP so that process allow/deny chains are stored in CHAIN_PREFIX + group + "-in" or "-out"
CHAIN_PREFIX = "pdlog-"

# Regex to match all possible groups in chain names
INT_GROUP_REGEX = "[0-9]{3}"
LEN_INT_GROUP_REGEX=3
STR_GROUP_REGEX="[a-z0-9]{1,}"

# Default group is the one that is used for system stuff, not tap/tun interfaces
GROUP_DEFAULT_INT = 0
GROUP_DEFAULT_STR = "default"
GROUP_DEFAULT = (GROUP_DEFAULT_STR, GROUP_DEFAULT_INT, str(GROUP_DEFAULT_INT))
GROUP_MAX = 255

GROUP_STR_ERROR = "error"

# If the group != 0 and the connection is incoming, the desination address will be used in addition to the source
# For example this means a VM on tap1 making an "incoming" ping can be filtered on the VM source as well as its Internet destination
GROUP_NON_ZERO_INCOMING_IMPLIES_USE_DESTINATION_TOO = True

# If the group != 0 and the connection is incoming, then use the source no matter what, even if the destination is a hostname
GROUP_NON_ZERO_INCOMING_IMPLIES_USE_SOURCE_IP = True

# What each column separated by "|"
IP_FOREVER_FILE_COLUMNS = {
"separator": "|",
"string": 0,
"process": 1,
"io": 2,
"allowdeny": 3,
"pid": 4,
"group": 5,
"enabled": 6,
}

# import tkinter as tk
import concurrent.futures, threading, subprocess, signal, os, pwd, tempfile, copy, psutil
import itertools, pickle, operator, time, datetime
import ipaddress, re, hashlib, socket, dnslib, random, socketserver

transactionIDMap = {}

# Cache is key header to value answer packet and time that answer was created (to allow discard after DNS_CACHE_TTL)
DNS_CACHE = {}
DNS_CACHE_LOCK = threading.Lock()
DNS_FLOOD_CACHE = {}
DNS_FLOOD_CACHE_LOCK = threading.Lock()

dns_special_routing_lock = threading.Lock()

tid_map_lock = threading.Lock()

# Line numbers are used when printing rules to group them together. Therefore it isn't that important to make that fast to access
# Dict {process: dict {pid: 4-tuple for allow/deny rules
# Allow/deny hostname is dict {hostname: (expiraction, enabled, line)
# Allow/deny pattern is list of tuple (pattern, expiration, enabled, line)
# Expiration for rules are stored as the value for the equal dicts and as a second object for a 2-tuple in the lists
process_hostname_acl_lock = threading.Lock()
process_hostname_acl = dict() # '/usr/bin/dig': {0: ({}, [('(.*\\.)?example\\.com', 999999999999, True)], {}, [])} Allow any pid of dig to query *.example.com
# An empty pid entry in process_hostname_acl
PROCESS_HOSTNAME_ACL_PID_EMPTY = ({}, [], {}, [])

Q = list()
Q_lock = threading.Lock()
prompt_queue_condition = threading.Condition(Q_lock)

# mappings between starting strings in the log line and the keys for a rule
LOG_ENTRIES = {
	"IN=":    "iif", 
	"OUT=":   "oif",
	"SRC=":   "src",
	"DST=":   "dst",
	"PROTO=": "proto",
	"SPT=":   "spt",
	"DPT=":   "dpt",
	"UID=":   "uid",
	"GID=":   "gid"
}

def pad_zeroes(number, length) -> str:
	return "0" * (length-len(str(number))) + str(number)
def all_zeros(string) -> bool:
	if "" == string:
		return False
	for _ in string:
		if _ != "0":
			return False
	return True
def is_int(string) -> bool:
	if isinstance(string, int):
		return True
	try:
		int(string)
		return True
	except:
		return False
	

def special_dns_config_hostname_in_group(hostname, group) -> bool:
	group_to_key = {"@vm": "vm", "@lxc": "lxc"}
	# UPDATE HERE to get file path from common bash and oython config file
	file = "/run/prompt-and-split/running"
	try:
		with open(file, "r") as f:
			for line in f.readlines():
				line = line.split()
				if "@vm" == group and 4 == len(line) and group_to_key[group] == line[0] and hostname == line[2]:
					return True
				elif "@lxc" == group and 3 == len(line) and group_to_key[group] == line[0] and hostname == line[1]:
					return True
	except:
		print("ERROR opening " + file)
	return False

CHARS_BEFORE_CHAIN = 2 # io[0], allowdeny[0]
def chain_name_from_path_pid(io, allowdeny, process, pid, group_str) -> str:
	# This makes it easy to find an out deny chain as [io][ad]GROUP...
	chain = io[0] + allowdeny[0] + hashlib.blake2s(group_str.encode() + io.encode() + allowdeny.encode() + process.encode() + str(pid).encode()).hexdigest()[CHARS_BEFORE_CHAIN:IPTABLES_CHAIN_MAX]
	return chain

def command_run(command) -> str:
	return subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode()

# Used previously to get all used marks in order to generate a new one not already in use
def existing_marks() -> set:
	return {1}
	# ret = command_run("iptables-save -t mangle | grep \"^-A [io][ad]\" | grep -o \"0x[0-9a-fA-F]*/0xffffffff\" | sed 's/0x\\([0-9a-fA-F]*\\)\\/0x[fF]\\{8\\}/\\1/g'")
	# return set([int(mark, 16) for mark in ret.split() if mark.strip()])

def new_mark() -> int:
	return 1
	# marks = existing_marks()
	# for i in range(1, int("FFFFFFFF", 16)):
	# 	if i not in marks: return i
	# print("Error getting an unused mark!")
	# return -1

# Read a config value that would be accessed in the bash startvpn.sh with "${config[key]}"
def read_config_value(key):
	for file in (CONFIG_DIR + "/namespaces.conf", CONFIG_DIR + "/groups.conf", CONFIG_DIR + "/config.conf"):
		with open(file, "r") as f:
			for line in f.readlines():
				if line and line.lstrip()[0] != "#":
					split = line.split("=", maxsplit=1)
					if split[0].strip(" '\"") == key:
						return split[1].strip(" '\"")
	return None

# Convert group (maybe type str) into an integer
def int_group(group):

	# This one isn't found anywhere in the config file
	if group in GROUP_DEFAULT:
		return GROUP_DEFAULT_INT

	if is_int(group):
		return int(group)

	# Read from the numbers config file to get the group number for a namespace string
	try:
		with open(GROUP_NUMBERS_CONFIG_FILE, "r") as f:
			for line in f.readlines():
				line = line.split()
				if 2 == len(line) and line[0] == group:
					return int(line[1])
	except:
		pass

	# Don't print this error because int_group is called when parsing all lines of IP_FOREVER_FILE
	# print("No int group found for", group)
	return None

def str_group(group):

	# Keep it at "0" not "default" for readability
	if group in GROUP_DEFAULT:
		return GROUP_DEFAULT_STR

	if not is_int(group):
		return group

	group = int(group) # Could be passed something like "2"

	with open(GROUP_NUMBERS_CONFIG_FILE, "r") as f:
		for line in f.readlines():
			line = line.strip().split()
			if 2 == len(line) and is_int(line[1]) and int(line[1]) == group:
				return line[0]
	
	print("No str group found for", group)
	return GROUP_STR_ERROR

def pid_cleaner():
	# Look for OLD pid rules and chains and delete them

	proc_pids = set()
	for pid in os.listdir("/proc/"):
		if re.fullmatch("[0-9]{1,}", pid):
			proc_pids.add(pid)

	for line in command_run("iptables-save -t mangle | grep pid").splitlines():
		if re.fullmatch("-A [io][ad]{}[0-9a-f]{{{}}} -m proc --pid [0-9]{{1,}} -j [io][ad]{}[0-9a-f]{{{}}}".format(INT_GROUP_REGEX, IPTABLES_CHAIN_MAX - CHARS_BEFORE_CHAIN, INT_GROUP_REGEX, IPTABLES_CHAIN_MAX - CHARS_BEFORE_CHAIN), line):
			# "-A od0bad26f -m proc --pid 172001 -j od00498b1"
			split = line.split()

			# If the pid doesn't exist anymore, delete the chain and the rule and reset conntrack entries
			if split[5] not in proc_pids:
				chain = split[-1]
				command_run("iptables -w -t mangle -F {}".format(chain))
				command_run("iptables -w -t mangle -X {}".format(chain))
				command_run("iptables -w -t mangle -D {}".format(line[2:]))

def _read_pair_from_hosts(searchIndex, var, file) -> str:
	ret = None
	try:
		with open(file, "r") as f:
			for line in f.readlines():
				if line and not line.lstrip().startswith("#"):
					split = line.split()
					if 2 == len(split) and var == split[searchIndex]:
						ret = split[not searchIndex] # Get all and return the last matching line
	except:
		print("ERROR opening " + file)

	return var if None == ret else ret

# Return the IP from a hosts file
# UPDATE: What if IP is 0.0.0.0 and so in multiple lines?
def read_ip_from_hosts(hostname, file="/etc/hosts") -> str:
	return _read_pair_from_hosts(1, hostname, file)

# Return the hostname for this IP from /etc/hosts if it is there
def read_hostname_from_hosts(ip, file="/etc/hosts") -> str:
	return _read_pair_from_hosts(0, ip, file)

# Load all hosts files into HOSTS_DATA
def load_hosts_files():
	for file in HOSTS_FILES:
		with open(file, "r") as f:
			for line in f.readlines():
				split = line.split() if (line := line.strip()) and "#" != line[0] else []
				if 2 == len(split):
					# Recursively get to an IP if it has multiple chained hostnames
					try:
						ipaddress.ip_address(split[0])
					except:
						temp = split[0]
						while True:
							# print(split[0], type(split[0]))
							split[0] = read_ip_from_hosts(temp)
							if split[0] == temp:
								break
							temp = split[0]
					finally:
						HOSTS_DATA[split[1]] = split[0]


# def iptables_extract_creation_time(rule):
# 	if " -m comment --comment " in rule:
# 		creation_time = rule.split(" -m comment --comment ")[-1].split()[0]
# 		# print(creation_time)
# 		return creation_time.strip("\""), rule.replace(" -m comment --comment " + creation_time, "")
# 	else: return "0", rule

# sub_label("www.example.com", 2) == "example.com"
def sub_label(hostname, level) -> str:
	return "".join([x + "." for x in hostname.split(".")[-level:]])[:-1]

# Given a matrix, print each column nicely
def grid(columns, space=1):
	column_max_string_lens = [max([len(row) for row in column]) for column in columns]
	for i in range(0, max([len(column) for column in columns])):
		for column in columns:
			column_index = columns.index(column)
			if i < len(column): print(column[i] + " "*(column_max_string_lens[column_index] - len(column[i]) + space), end="")
			else: print(" "*(column_max_string_lens[column_index] + space), end="")
		print()

def seconds_from_string(string, count) -> int:
	if   string == "m": return count * 60
	elif string == "h": return count * 60 * 60
	elif string == "d": return count * 60 * 60 * 24
	elif string == "w": return count * 60 * 60 * 24 * 7
	else:               return 0

# Get group string from the source IP. Interface should start with "br-"
def group_from_source(source, printError=False):
	if source in (SOURCE_DEFAULT, SOURCE_DEFAULT_ADDR):
		return GROUP_DEFAULT_STR
	
	interface = command_run("ip route get {}".format(read_ip_from_hosts(source))).split()[2]
	# Interface should be named based on the group
	for prefix in ("br-", "veth-"):
		if interface.startswith(prefix):
			return interface[len(prefix):]
	# Interface names for APs
	for suffix in ["_ap"]:
		if interface.endswith(suffix):
			return interface[:len(suffix)]

	if True == printError:
		print("ERROR: Could not discern group from {}, returning error".format(source))

	return GROUP_STR_ERROR

def iptables_exists(string, process, io, allowdeny, pid, group_str, enabled) -> bool:
	chain_name_pid = chain_name_from_path_pid(io=io,allowdeny=allowdeny,process=process,pid=pid,group_str=group_str)

	# end_match = "\\.* -j DROP$" if "deny" == allowdeny else ""
	# time_match = " -m comment --comment {}".format(("\"{}\"".format(creation_time), "0")["0" == creation_time]) if creation_time else ""
	# if "" == command_run("iptables-save -t mangle | grep -E \"^-A {}{}{}".format(chain_name_pid, string, time_match, end_match)):
		# return True

	if "deny" == allowdeny:
		return "" == command_run("iptables -w -t mangle -C {} {} {}".format(chain_name_pid, string, "-j DROP" if enabled else "")) # + " -m comment --comment "+ creation_time))
	elif "" == command_run("iptables -w -t mangle -C {} {} {}".format(chain_name_pid, string, "-j CONNMARK --set-xmark 1" if enabled else "")): # + " -m comment --comment " + creation_time, mark)):
		return True
	return False

def iptables_run(string, failOnError = True, printErrors = True) -> bool:
	for line in string.splitlines():
		strip = line.strip()
		if not strip or "#" == strip[0]:
			continue
		# print(line)
		output = command_run(line)
		if "" != output:
			if printErrors:
				print("iptables_run ERROR:")
				print(" "*4 + line)
				print(" "*4 + output)
			if failOnError:
				return False
	return True

# Return whether something like "-p TCP --dport 443" matches "-m tcp --dport 443 --protocol tcp"
# Instead of parsing, create a blank chain and try to add the rule twice using the kernel's logic for -C option
def iptables_rule_matches(a, b) -> bool:
	chain = str(random.randint(9999, 99999))
	iptables_run("iptables -w -N {}".format(chain))
	iptables_run("iptables -w -A {} {}".format(chain, a))
	ret = command_run("iptables -w -C {} {}".format(chain, b))
	iptables_run("iptables -w -F {}".format(chain))
	iptables_run("iptables -w -X {}".format(chain))
	return "" == ret

# Delete a rule and try to remove it from IP_FOREVER_FILE on disk
def del_ip_rule(string, process, io, allowdeny, pid, group_str, enabled) -> bool:
	lines = list()
	with open(IP_FOREVER_FILE, "r") as f:
		lines = [line.strip() for line in f.readlines() if line.strip()]

	# Only write all the old rules to the file if the rule being deleted was there (aka avoid disk io for a rule kept in memory)
	new_file = ""
	write_new = False 
	for line in lines:
		
		s = [temp.strip() for temp in line.split(IP_FOREVER_FILE_COLUMNS["separator"])]
		if len(s) != len(IP_FOREVER_FILE_COLUMNS) - 1:
			print("Bad line in {}:".format(IP_FOREVER_FILE), line)
			continue

		# Add the rules to the new_file if it does NOT match the one wanting to be deleted
		if not (process == s[IP_FOREVER_FILE_COLUMNS["process"]] and io == s[IP_FOREVER_FILE_COLUMNS["io"]] and allowdeny == s[IP_FOREVER_FILE_COLUMNS["allowdeny"]] and pid == int(s[IP_FOREVER_FILE_COLUMNS["pid"]]) and group_str == s[IP_FOREVER_FILE_COLUMNS["group"]] and enabled == int(s[IP_FOREVER_FILE_COLUMNS["enabled"]]) and iptables_rule_matches(s[IP_FOREVER_FILE_COLUMNS["string"]], string)):
			new_file += line + "\n"
		
		# Rule wanting to be deleted did match one on disk, so we DO need to write all the old rules to the disk, minus that one
		else:
			write_new = True

	if True == write_new:
		with open(IP_FOREVER_FILE, "w") as f:
			f.write(new_file + "\n")
	
	# Make process same (for chain name generation) as in add_ip_rule()
	if PACKET_NO_PATH_STR == process:
		process = PACKET_NO_PATH

	chain_name_pid = chain_name_from_path_pid(process=process, pid=pid, io=io, allowdeny=allowdeny, group_str=group_str)
	full_rule = "-D {} ".format(chain_name_pid) + string + (" -j {}".format("CONNMARK --set-xmark 1" if "allow" == allowdeny else "DROP") if True == enabled else "")
	ret = iptables_run("iptables -w -t mangle " + full_rule)

	# Also delete the DNS rule if this ip rule looked like one that would have been created
	# 2 cases: Simple port 443 rule or custom group rule
	# if True == IP_DEL_IMPLIES_DNS_DEL:
		# pass

	# TODO: Fix this because this messes up all the "incoming" packets from things that already have connections established!
	# Flush conntrack so that rule will have to go to pdlog again
	# command_run("conntrack -F")
	return ret

# Todo, do better checking here if the rule exists in the file with the above checker
def save_ip_rule(string, process, io, allowdeny, pid, group_str, enabled, replaceIPs=True) -> None:
	sep = IP_FOREVER_FILE_COLUMNS["separator"]
	save_file = IP_FOREVER_FILE
	
	# Replace any IP addresses for ones that are listed in the hosts files.
	# This is useful for rules for VMs so that they are not tied to an IP address
	# If VM with hostname is in group tap-tor, we don't want to tie that VM to an IP address, but rather the hostname
	hostname = False
	if True == replaceIPs:
		
		# IPs are only going to be after "-s", "--source", "-d", or "--destination". Split and reconstruct string is the safest way
		# This includes things like 10.2.0.0/24 being replaced as group/24
		split = string.split()
		def cond(s,i,arr):
			if i > 0 and arr[i-1] in ("--destination", "--source", "-s", "-d") and re.fullmatch(IPV4_REGEX+"(/[0-9]{1,2})?", s):
				hostname_from_hosts = read_hostname_from_hosts(s.split("/")[0] if "/" in s else s)
				nonlocal hostname
				if hostname != hostname_from_hosts and arr[i-1] in ("--source", "-s"):
					hostname = hostname_from_hosts
				return hostname_from_hosts + (("/" + s.split("/")[1]) if "/" in s else "")
			else:
				return s

		string = "".join(cond(word, i, split) + " " for i,word in enumerate(split))[:-1]

	# group = str_group(group)

	# Take anything that is sourced to a VM and write that to a hostname.rules file instead of the global FOREVER_IP_FILE
	# This means that that vm specific rules file will be loaded when the vm loads, with a changable group at load time
	if False != hostname and "in" == io and PACKET_NO_PATH == process:
		save_file = CONFIG_DIR + "/hosts/rules/{}.rules".format(hostname) #hostname[:-len(group) - 1] # When hostname has group

	print(string , process , io , allowdeny , str(pid) , group_str , str(int(enabled)), save_file)
	add = string + sep + process + sep + io + sep + allowdeny + sep + str(pid) + sep + group_str + sep + str(int(enabled)) + "\n"
	
	try:
		with open(save_file, "r") as f:
			for line in f.readlines():
				if line.strip() == add.strip():
					add = False
					break
	# File might not exist yet
	except:
		pass

	try:
		if False != add:
			if not os.path.exists(save_file):
				command_run("/usr/bin/touch {}".format(save_file))
				print(123)
				# TODO: read the group from the config.conf file
				os.chown(save_file, 0, int(command_run("/usr/bin/getent group prompt-and-split | /usr/bin/cut -d':' -f3")))

			with open(save_file, "a") as f:
				f.write(add)

	except Exception as e:
		print("ERROR appending", save_file)
		print(str(e))
		pass

# Add a rule for a process and optionally its specific pid.
# The group chain (pdlog1-in and pdlog1-out) are created at all rule creations so if group 1 rules are added, they are properly
# added to pdlog1-in and not in a 0 references chain
def add_ip_rule(string, process, io, allowdeny, pid, group_str, enabled, saveToDisk=False, createGroupChains=False) -> bool:
	if allowdeny not in ("allow", "deny") or io not in ("in", "out"):
		print("allowdeny must be 'allow' or 'deny' AND io must be 'in' or 'out'", allowdeny, type(allowdeny), io, type(io))
		return False
	
	if PACKET_NO_PATH == process or PACKET_NO_PATH_STR == process.strip() or not process.strip():
		process = PACKET_NO_PATH
		match_condition = "-m proc --no-path"
	elif PACKET_ANY_PATH == process:
		match_condition = ""
	else:
		match_condition = "-m proc --path {}".format(process)
	
	# Index to insert per process jumps to their chains.
	out_start_count = 1
	in_start_count = 1
	
	# pdlog3-in uses a mark != 0 ACCEPT at the start of the chain to make ssh work, since there is a DENY ALL rule in there
	if GROUP_DEFAULT_STR != group_str:
		in_start_count += 1
	
	# if createGroupChains:
		# iptables_run("iptables -t mangle -N {}{}-{}".format(CHAIN_PREFIX, group, io), printErrors = False)

	def create_and_point_to_chain(new_chain, old_chain, matches, failOnError=True) -> bool:
		iptables_run("""
iptables -w -t mangle -N {}
iptables -w -t mangle -I {} {} -j {}""".format(new_chain, old_chain, matches, new_chain), failOnError=failOnError, printErrors=False)

	# The specific rule existing also needs the link for its process from pdlog[0-7] chain to the per process chain
	# Simply checking that the per-process chain exists isn't enough. We need to add it and check that the pointing rule exists

	# The chain for all the rules for this process, for all pids
	chain_name_all_pid = chain_name_from_path_pid(io=io, allowdeny=allowdeny, process=process, pid=PID_ALL, group_str=group_str)

	chain = "{}{}-{}".format(CHAIN_PREFIX, group_str, io)

	# Create the chain for the process and create the "-m proc --path ... -j chain_name_all_pid in pdlog[0-7]-in/out chain" 
	if "allow" == allowdeny:
		# Make sure the link from the pdlog chain to the per process chain exists
		old_chain = chain + " {}".format(in_start_count if "in" == io else out_start_count)
		
		c = "iptables -w -t mangle -C {} {} -j {}".format(chain, match_condition, chain_name_all_pid)
		
		# Pointing rule does not exist, so create the chain if it isn't there and create the pointing rule
		if "" != command_run(c):
			create_and_point_to_chain(chain_name_all_pid, old_chain, match_condition, failOnError = False)

	else:
		# Add the process deny chain rules after all the accept rules
		accept_count_command = "iptables -w -t mangle -vnL {} | awk '$3 ~ \"{}a\" {{print $0}}' | wc -l".format(chain, io[0])

		# Only add 1 for groups not 0, since these have a connmark check after the accept rules, but group 0 doesn't
		deny_count = int(command_run(accept_count_command)) + (in_start_count if "in" == io else out_start_count) + 1
		
		old_chain = chain + " {}".format(deny_count)

		c = "iptables -w -t mangle -C {} {} -j {}".format(chain, match_condition, chain_name_all_pid)

		# Pointing rule does not exist, so create the chain if it isn't there and create the pointing rule
		if "" != command_run(c):
			create_and_point_to_chain(chain_name_all_pid, old_chain, match_condition)

	# The chain from the per-process chain for this specific pid. This is redundant if pid is PID_ALL, but is needed if it is not
	chain_name_single_pid = chain_name_all_pid if PID_ALL == pid else chain_name_from_path_pid(io=io, allowdeny=allowdeny, process=process, pid=pid, group_str=group_str)

	if pid > 0:
		match_condition = "-m proc --pid " + str(pid)

		# Agian check that the pointing rule from the per-process chain to the pid chain exists
		if "" != command_run("iptables -w -t mangle -C {} {} -j {}".format(chain_name_all_pid, match_condition, chain_name_single_pid)):
			create_and_point_to_chain(chain_name_single_pid, chain_name_all_pid, match_condition)

	# Now that all the proper chains are confirmed to exist, check that the final rule does not exist
	if iptables_exists(string=string,process=process,io=io,allowdeny=allowdeny,pid=pid,group_str=group_str,enabled=enabled):
		# print(string, "Exists")
		return True
	
	string_target = string + ((" -j CONNMARK --set-xmark 1" if "allow" == allowdeny else " -j DROP ") if True == enabled else "")
	
	ret = iptables_run("iptables -w -t mangle -I {} {}".format(chain_name_single_pid, string_target))
	# print(ret, "iptables -w -t mangle -I {} {}".format(chain_name_single_pid, string_target))

	if True == ret and True == saveToDisk:
		save_ip_rule(string, process, io, allowdeny, pid, group_str, enabled)

	return ret

# Parse iptables-save -t mangle for the rules this prompter created and print them in a readable way.
def print_packet_rules(returnArray=False, stdout=True):
	# Dicts for chain to path and rules in it as well as the temporary pid rules for processes
	chain_to_process = {}

	pid_regex    = re.compile("^-A [io][ad][0-9a-f]{{{}}} -m proc --pid ".format(IPTABLES_CHAIN_MAX - CHARS_BEFORE_CHAIN))
	chain_regex  = re.compile("^-A [io][ad][0-9a-f]{{{}}}".format(IPTABLES_CHAIN_MAX - CHARS_BEFORE_CHAIN))
	path_regex   = re.compile("^-A {}{}-(in|out) -m proc --path ".format(CHAIN_PREFIX, STR_GROUP_REGEX))
	nopath_regex = re.compile("^-A {}{}-(in|out) -m proc --no-path ".format(CHAIN_PREFIX, STR_GROUP_REGEX))
	allpath_regex = re.compile("^-A {}{}-(in|out) -j [io][ad][0-9a-f]{{{}}}".format(CHAIN_PREFIX, STR_GROUP_REGEX, IPTABLES_CHAIN_MAX - CHARS_BEFORE_CHAIN))

	# First get all the path and nopath chains, which should set the group for the [io][ad] chain they point to
	iptables_save_mangle_rules = command_run("iptables-save -t mangle").splitlines()[::-1]
	for line in iptables_save_mangle_rules:
		chain = line.split(" -j ")[-1].strip()

		# Look for path or no path pointing to the chains with those rules. The chain name starts with [io][ad] for (in|out)(allow|deny)
		if path_regex.match(line):
			split = line.split()
			path = split[5]
			group_str = split[1][len(CHAIN_PREFIX):split[1].rindex("-")]
			chain_to_process[chain] = {"path": path, "group": group_str, "pid": PID_ALL, "rules": []}

		elif nopath_regex.match(line):
			append_chain = line.split()[1]
			group_str = append_chain[len(CHAIN_PREFIX):append_chain.rindex("-")]
			chain_to_process[chain] = {"path": PACKET_NO_PATH_STR, "group": group_str, "pid": PID_ALL, "rules": []}
		
		elif allpath_regex.match(line):
			append_chain = line.split()[1]
			group_str = append_chain[len(CHAIN_PREFIX):append_chain.rindex("-")]
			chain_to_process[chain] = {"path": PACKET_ANY_PATH, "group": group_str, "pid": PID_ALL, "rules": []}
		
	# Now get the possible pid regexes
	for line in iptables_save_mangle_rules:
		# Case of "-m proc --pid X -j " pointing to yet another [io][ad] chain
		# Get the process path from the first chain this is being added to, that has already been set before since 'p' is after 'i' and 'o'
		if pid_regex.match(line):
			split = line.split()
			chain = split[-1]
			chain_to_process[chain] = {"path": chain_to_process[split[1]]["path"], "group": chain_to_process[split[1]]["group"], "pid": split[5], "rules": chain_to_process[chain]["rules"] if chain in chain_to_process else list()}

		elif chain_regex.match(line):
			chain = line.split(maxsplit=3)[1]
			if chain in chain_to_process:
				chain_to_process[chain]["rules"].append(line)
			# May not have entered the pid_regex yet for a certain chain based on alphabetical order, so add a place holder
			# Some 0 reference chains may get added here. Need to disregard them later
			else:
				chain_to_process[chain] = {"path": None, "group": None, "pid": None, "rules": [line]}

	# Loop over all the collected information and display it cleanly
	# Print Process path, then the temporary rules, then the permanent. Drop rules before allow.

	# Create a mapping from process -> its chains
	process_to_chains = {}
	for chain, info in chain_to_process.items():
		if info["path"] in process_to_chains:
			process_to_chains[info["path"]].append(chain)
		else:
			process_to_chains[info["path"]] = [chain]
	# print(chain_to_process) ; print(process_to_chains)

	if True == returnArray:
		return chain_to_process, process_to_chains

	string = ""
	for path, chains in process_to_chains.items():

		# Make sure one of the chains has rules in it
		for chain in chains:
			if chain_to_process[chain]["rules"] and None != chain_to_process[chain]["path"]:
				string += path + "\n"
				chains_remove = list()
				for chain_inner in chains: #Print the "Until Restart" or "Forever" rules:
					pid = chain_to_process[chain_inner]["pid"]
					group = chain_to_process[chain_inner]["group"]
					group_string = "group {} ".format(group) if group != GROUP_DEFAULT_STR else ""

					if PID_ALL == pid:
						for rule in chain_to_process[chain_inner]["rules"]:
							rule_and_target = rule[4 + IPTABLES_CHAIN_MAX:].split("-j ")
							string += " "*2 + ("0 " if 1 == len(rule_and_target) else "") + ("Deny " if "d" == chain_inner[1] else "Allow")+" "+ ("out" if "o" == chain_inner[0] else "in") +" "+group_string+rule_and_target[0]+ "\n"

						# Clear the None pids so no check has to be done in the next loop
						chains_remove.append(chain_inner)
				for chain_inner in chains_remove:
					chains.remove(chain_inner)

				for chain_inner in chains: # Print Until process quit PID rules:
					pid = chain_to_process[chain_inner]["pid"]
					group = chain_to_process[chain_inner]["group"]
					group_string = "group {} ".format(group) if group != GROUP_DEFAULT_STR else ""

#					if PID_ALL != pid: #This isn't needed because of the above removal
					if chain_to_process[chain_inner]["rules"]:
						string += " "*2 + "> PID=" + pid + "\n"
					for rule in chain_to_process[chain_inner]["rules"]:
						rule_and_target = rule[4 + IPTABLES_CHAIN_MAX:].split("-j ")
						string += " "*4 + ("0 " if 1 == len(rule_and_target) else "") + ("Deny " if "d" == chain_inner[1] else "Allow")+" "+("out" if "o" == chain_inner[0] else "in") +" "+group_string+rule_and_target[0] + "\n"
				# Break out of looking to see if there were any rules for this process since there were
				break
	
	if stdout:
		print(string.rstrip())
	else:
		return string.rstrip()

def edit_dns_special_routing(addOrDel, process, uid, qname, source, addr) -> None:
	if process in ("''", "\"\"", "None", PACKET_NO_PATH_STR):
		process = PACKET_NO_PATH

	if not is_int(uid):
		print("ERROR: uid {} must be an int".format(uid))
		return

	if source in ("\"\"", "''", "None"):
		source = ALL_SOURCES

	addr = addr.split(":")
	if 2 != len(addr) or not is_int(addr[1]):
		print("ERROR: addr {} must contains a colon with a port".format(addr))
		return

	uid = int(uid)
	addr = (addr[0], int(addr[1]))

	with dns_special_routing_lock:
		if True == addOrDel:
			if process not in DNS_SPECIAL_ROUTING:
				DNS_SPECIAL_ROUTING[process] = {}
			
			if uid not in DNS_SPECIAL_ROUTING[process]:
				DNS_SPECIAL_ROUTING[process][uid] = []
			
			for rule in DNS_SPECIAL_ROUTING[process][uid]:
				if qname == rule['qname'] and source == rule['source'] and addr == rule['addr']:
					return

			DNS_SPECIAL_ROUTING[process][uid].append({"qname": qname, "source": source, "addr": addr})

		else:
			if process not in DNS_SPECIAL_ROUTING:
				# print("ERROR: not in DNS_SPECIAL_ROUTING", process, uid, qname, source, address)
				return
			
			if uid not in DNS_SPECIAL_ROUTING[process]:
				# print("ERROR: not in DNS_SPECIAL_ROUTING", process, uid, qname, source, address)
				return

			for rule in DNS_SPECIAL_ROUTING[process][uid]:
				if qname == rule['qname'] and source == rule['source'] and addr == rule['addr']:
					DNS_SPECIAL_ROUTING[process][uid].remove(rule)
					if 0 == len(DNS_SPECIAL_ROUTING[process][uid]):
						DNS_SPECIAL_ROUTING[process].pop(uid)
					if 0 == len(DNS_SPECIAL_ROUTING[process]):
						DNS_SPECIAL_ROUTING.pop(process)
					break

def log_packet_connection(connection, starting=False) -> None:
	with open(PACKET_PROMPTER_LOG_FILE, "at") as f:
		if not starting:
			# First remove the LOG_ENTRIES from connection, since these are already in the "log" value and more readable in that form
			temp = connection.copy()
			for remove in LOG_ENTRIES.values():
				temp.pop(remove)
			f.write("{} {}\n".format(str(datetime.datetime.utcnow())[:-7], str(temp)))
		else:
			f.write("\n{} Starting\n".format(str(datetime.datetime.utcnow())[:-7]))


def new_connection(hostname, process, pid, conn_type, group, log="",arg1=None) -> dict:
	connection = {}
	for value in LOG_ENTRIES.values():
		connection[value] = LOG_DEFAULT_VALUE

	connection["hostname"] = hostname
	connection["process"] = process
	connection["pid"] = pid
	connection["type"] = conn_type
	connection["group"] = group
	connection["log"] = log
	connection["arg1"] = arg1

	connection["ref"] = True
	# This is the process path gueseed based on the listening sockets.
	# Can't use the original "process" field since it will mess up adding things to the queue
	connection["in_process"] = ""
	return connection

# Add an entry to the queue if it is not already there based on some conditions
# For outgoing connection queue, look at: process, dest, proto, dpt
# Other values don't matter and aren't shown in the prompts
def prompt_queue_add_check(connection, buffer_queue) -> bool:

	# For outgoing connections:
		# Disregard source port
		# Disregard source address and only consider the destination address and possibly hostname if it exists in the connection
		# Consider destination address to a hostname if applicable
	
	# For incoming connections:
		# Disregard source port
		# Disregard destination address
		# Don't convert source address to anything, since it is probably not in our DNS list
	
	global Q
	
	# Make sure the properties of this new entry prompt are not already in the Q
	if connection["type"] == "packet":
		for entry in itertools.chain(Q, buffer_queue):

			# Assume outgoing connection by default.
			# Use the hostname for matching, if it exists.
			# Consider the case where the IP is in the Q with no hostname, then the same IP connection comes in, but with a hostname.
			# Any decision made in the past is ignored, as well as the possibility of the user responding to both, because this program assumes the 
			# kernel DNS watcher has 100% uptime and success.
			connection_address = [connection["dst"], connection["hostname"]][connection["hostname"] != ""]
			entry_address      = [     entry["dst"],      entry["hostname"]][     entry["hostname"] != ""]

			# Incoming connection. Consider the source IP addresses for comparison
			if "" != connection["iif"]:
				connection_address = connection["src"]
				entry_address      =      entry["src"]

			if  connection["type"]    == entry["type"] \
			and connection_address    == entry_address \
			and connection["process"] == entry["process"] \
			and connection["dpt"]     == entry["dpt"] \
			and connection["proto"]   == entry["proto"] \
			and connection["uid"]     == entry["uid"] \
			and connection["gid"]     == entry["gid"] \
			and connection["oif"]     == entry["oif"] \
			and connection["iif"]     == entry["iif"] \
			and connection["group"]   == entry["group"]:
				return False
	elif "dns" == connection["type"]:
		for entry in itertools.chain(Q, buffer_queue):
			if  connection["type"] == entry["type"] \
			and connection["process"] == entry["process"] \
			and connection["hostname"] == entry["hostname"]:
				return False
	else:
		print("Connection type error in prompt_queue_add_check()") 
		return False

	return True

def packet_editor(showChanges=False):
	# Open EDITOR with the rule in it
	# Compare the second with the original, remove gone rules and add new ones

	# Need to parse rules as path, then rules, 4 spaces in, or "> PID 123" and rules 8 spaces in
	def packet_editor_parser(string):
		INIT_CUR_PATH = 0 # This type int will never match from the parsed strings
		INIT_CUR_PID = INIT_CUR_PATH
		cur_path = INIT_CUR_PATH
		cur_pid = INIT_CUR_PID

		ret = set()
		for line in string.splitlines():
			
			# Rule for the current pid
			if line.startswith(" "*4):
				if INIT_CUR_PATH == cur_path or INIT_CUR_PID == cur_pid:
					print("Error no cur_path or cur_pid when parsing:", line)
					break

				ret.add((cur_path, int(cur_pid), line.strip()))
			
			# Rule for that process and all pids
			elif line.startswith(" "*2):
				if INIT_CUR_PATH == cur_path:
					print("Error no cur_path when parsing:", line)
					break
				if line.startswith(" "*2 + "> PID="):
					cur_pid = line[2+6:].strip()
				else:
					ret.add((cur_path, PID_ALL, line.strip()))
			
			elif "/" == line[0] or PACKET_NO_PATH_STR == line.rstrip():
				cur_pid = INIT_CUR_PID
				cur_path = line.strip()
				cur_path = PACKET_NO_PATH if PACKET_NO_PATH_STR == cur_path else cur_path

			elif PACKET_ANY_PATH == line.rstrip():
				cur_pid = INIT_CUR_PID
				cur_path = PACKET_ANY_PATH
			
			else:
				print("ERROR parsing IP rules. Bad line", line)
				continue

		return ret

	def packet_editor_line_parser(rule):
		split = rule.split()
		
		disabled = True if "0" == split[0] else False
		allowdeny = split[disabled].lower()
		io = split[1 + disabled].lower()

		if len(split) > (2 + disabled) and "group" == split[2 + disabled].lower():
			group_str = str_group(split[3 + disabled])
			string_start = 4 + disabled

		else: 
			group_str = GROUP_DEFAULT_STR
			string_start = 2 + disabled

		return allowdeny, io, group_str, not disabled, "".join([word + " " for word in split[string_start:]])

	old_rules_string = print_packet_rules(returnArray=False, stdout=False)
	print(old_rules_string)
	old_rules = packet_editor_parser(old_rules_string)
	
	# display_rules = list()
	# for process in process_to_chains:
		# for chain in process_to_chains[process]:
			# pid = chain_to_process[chain][1]
			# for rule in chain_to_process[chain][2]:
				# display_rules.append(("{} {} {}".format(process, pid, rule[4 + IPTABLES_CHAIN_MAX:].split(" -j ")[0]), (process, pid, rule)))
	# print(old_rules_string)
	EDITOR = os.environ.get('EDITOR','nano')

	with tempfile.NamedTemporaryFile(suffix=".tmp") as tf:
		tf.write(old_rules_string.encode())
		tf.flush()
		subprocess.call([EDITOR, tf.name])

		tf.seek(0)
		new_rules = packet_editor_parser(tf.read().decode())

		all_success = True
		counter = 0
		for process, pid, rule in new_rules.difference(old_rules):
			allowdeny, io, group_str, enabled, string = packet_editor_line_parser(rule)

			# print(allowdeny, io, group_str, enabled, string)
			
			# Only save to disk if it does not end in "until restart"
			ret = add_ip_rule(string=string,process=process,io=io,allowdeny=allowdeny,pid=pid,group_str=group_str,enabled=enabled,saveToDisk="until restart" not in string)

			if True == showChanges:
				message = "Added:  " if ret else "Failed to add:\n"
				print(message, process, pid, rule)
				if False == ret:
					all_success = False
			counter += 1
		if False == all_success:
			print("\nOne or more of {} rules failed to be added!\n".format(counter))
		else:
			print("All {} rules added successfully\n".format(counter))

		all_success = True
		counter = 0
		for process, pid, rule in old_rules.difference(new_rules):

			allowdeny, io, group_str, enabled, string = packet_editor_line_parser(rule)
			ret = del_ip_rule(string=string,process=process,io=io,allowdeny=allowdeny,pid=pid,group_str=group_str,enabled=enabled)
			if True == showChanges:
				message = "Removed:" if ret else "Failed to remove:\n"
				print(message, process, pid, rule)
				if False == ret:
					all_success = False
		if False == all_success:
			print("\nOne or more of {} rules failed to be removed!\n".format(counter))
		else:
			print("All {} rules removed successfully\n".format(counter))
	'''
	class Editor(object):
		def __init__(self, master):
			self.master = master
			self.listbox = tk.Listbox(master)
			self.scrollbar = tk.Scrollbar(master)
			tk.Label(master, text="IP Rules").grid(row=0, column=0, columnspan=2)
			self.listbox.grid(row=1, column=0, sticky='new',columnspan=2)
			tk.Button(master, text="DNS Rules").grid(row=2,column=0, sticky='w')
			tk.Button(master, text="Packet Rules").grid(row=2,column=1, sticky='w')
			self.master.rowconfigure(1, weight=1)
			self.master.columnconfigure(0, weight=1)

			self.listbox.config(selectmode=tk.BROWSE, exportselection=0, height = -1, yscrollcommand = self.scrollbar.set)
			self.listbox.config(bg = 'white') #, font = ( 'courier', 12 ) )
			self.scrollbar.config(command = self.listbox.yview)
			# self.scrollbar.pack(side = tk.RIGHT, fill = tk.BOTH)
			# self.scrollbar.grid(side = tk.RIGHT, fill = tk.BOTH)

			# Map each displayed rule to its chain for easy deletion
			# Get all the rules and add a disaply string as well as an identification object
			chain_to_process, process_to_chains = print_packet_rules(array=True)
			# {'chain0': [process, pid, [rule0, rule1,...]], 'chain1': ...}, {'process0':[chain0, chain1,...], 'process1': ...}
			# Go through each chain and build the 

			# Stores (printed rule, (process, pid, io, allow/deny, creation_time))
			self.display_rules = list()

			for process in process_to_chains:
				for chain in process_to_chains[process]:
					pid = chain_to_process[chain][1]
					for rule in chain_to_process[chain][2]:
						# creation_time, rule = iptables_extract_creation_time(rule[4 + IPTABLES_CHAIN_MAX:].split(" -j ")[0])

						# self.display_rules.append(("{} {} {}".format(process, pid, rule), (process, pid, ("in", "out")["o" == chain[0]], ("deny", "allow")["a" == chain[1]], creation_time)))
						self.display_rules.append(("{} {} {}".format(process, pid, rule[4 + IPTABLES_CHAIN_MAX:].split(" -j ")[0]), (process, pid, rule)))

			for rule,key in self.display_rules:
				self.listbox.insert(tk.END, rule)

			self.listbox.focus_set()
			self.selection = 0
			self.listbox.select_set(self.selection)
			self.listbox.event_generate("<<ListboxSelect>>")

			for key in ("<d>", "<BackSpace>", "<Delete>"):
				self.listbox.bind(key, self.deleteSelected)
			self.listbox.bind("<q>", self.close)
			self.listbox.bind("<e>", self.edit)
		
		def close(self, event):
			self.master.destroy()
		
		# def edit(self, event):
		# 	# Create a pop-up
		# 	ret = ""
		# 	def popup(string):
		# 	    win = tk.Toplevel()
		# 	    win.wm_title("Edit Rule")
		# 	    t = Text(win)
		# 		t.insert(0.0, self.)
		# 		t.grid(row=0,column=0)
		# 	    # l = tk.Label(win, text="Input")
		# 	    # l.grid(row=0, column=0)
		# 		b = ttk.Button(win, text="Okay", command=win.destroy)
		# 		b.grid(row=1, column=0)

		def deleteSelected(self, event):
			selections = self.listbox.curselection()
			for i in selections:
				displayed = self.listbox.get(i)
				for rule,key in self.display_rules:
					if rule == displayed:
						print(displayed)
						print(key)
						# del_ip_rule(string=key[0],process=key[0],io=key[2],allowdeny=key[3],pid=key[4],full_rule=key[5])

			self.listbox.selection_clear(0,self.listbox.size()-1)
			# Need to delete by content of entry, not index!!1
			for i in selections: self.listbox.delete(i)

	window = tk.Tk()
	window.title("Rule Viewer")
	obj = Editor(window)
	window.mainloop()
	'''
def print_dns_rules():
	global process_hostname_acl_lock
	ret = ""
	with process_hostname_acl_lock:
		global process_hostname_acl
		for process in process_hostname_acl:
			ret += (process if process != PACKET_NO_PATH else PACKET_NO_PATH_STR) + "\n"

			for decision_string, decision_value in (("  Allow:\n", True), ("  Deny:\n", False)):
				ret += decision_string
				lines_for_decision_string = False

				# Sort the allow/deny by source, then by pattern
				b = [(pattern, rule) for pattern,pattern_rules in process_hostname_acl[process].items() for rule in pattern_rules if decision_value == rule["decision"]]
				a = sorted(b, key=lambda patt_rule_pair: (patt_rule_pair[1]["source"] if ("source" in patt_rule_pair[1] and None != patt_rule_pair[1]["source"]) else "", patt_rule_pair[0]))
				
				for pattern, rule in a:
					lines_for_decision_string = True

					ret += " "*4

					if "enabled" in rule and False == rule["enabled"]:
						ret += "0 "

					if "source" in rule and None != rule["source"]:
						ret += "{} ".format(rule["source"])
					
					if "uid" in rule:
						try:
							u = pwd.getpwuid(rule["uid"]).pw_name
						except:
							u = rule["uid"]
						ret += "-u {} ".format(u)

					ret += "{}".format(pattern)

					if "expiration_time" in rule:
						ret += " until {}".format("restart" if DNS_EXPIRE_RESTART == rule["expiration_time"] else rule["expiration_time"])

					ret += "\n"

				if False == lines_for_decision_string:
					ret = ret[:-len(decision_string)]

	return ret

def dns_editor_line_parser(line):
	# Expecting line: "[0] [source] [-u uid/user] pattern [pattern] ... [pattern] [until restart]"
	# source is matched out by not having an asterisk or a dot. All patterns must have an asterisk or dot.
	# uid is parsed by looking for a "-u" then taking the next string

	split = line.split()

	enabled = False if "0" == split[0] else True

	after_enabled = split[1 if not enabled else 0]

	# Make sure after_enabled is not a user and not a pattern (which has a dot or astrisck)
	# UPDATE: Note that this will mess up if source is an IP address
	source = after_enabled if ("-u" != after_enabled and re.fullmatch("[^\.\*]{1,}", after_enabled)) else None

	uid = DNS_NO_UID
	for i, v in enumerate(split):
		if "-u" == v:
			if i + 1 < len(split):
				uid = split[i + 1]
				try:
					uid = pwd.getpwnam(uid).pw_uid
				except:
					if not is_int(uid):
						print("ERROR:", uid, "doesn't apper to be a user")
					# return
			else:
				print("ERROR in line:", line)
				return

			break

	# Expecting "until restart/EpochTimeStamp" or nothing
	if len(split) > 2 and split[-2] == "until":
		expiration_time = split[-1]
		if "restart" == expiration_time:
			expiration_time = DNS_EXPIRE_RESTART
	else:
		expiration_time = None

	start_index = (1 if not enabled else 0) + (1 if None != source else 0) + (2 if None != uid else 0)
	# print(line, split[start_index : None if None == expiration_time else -2])
	return enabled, source, uid, expiration_time, split[start_index : None if None == expiration_time else -2] # Note this must be None, not 0

# Return rules based on a formatted string
def dns_editor_parser(string):
	cur_path = cur_pid = cur_decision = CUR_DEFAULT = None
	ret = set()

	for line in string.splitlines():

		# Get the rule for the current pid
		if line.startswith(" "*6):
			if CUR_DEFAULT == cur_path or CUR_DEFAULT == cur_pid or CUR_DEFAULT == cur_decision:
				print("ERROR with 6 space DNS line", line)
				continue
			enabled, source, uid, expiration_time, patterns = dns_editor_line_parser(line.strip())
			
			if None == enabled:
				print("ERROR parsing 6 space DNS line", line)
				continue
			
			for pattern in patterns:
				ret.add((cur_path, cur_pid, cur_decision, enabled, source, uid, expiration_time, pattern))

		# Get the rule, or pid to start reading rules from
		elif line.startswith(" "*4):
			if CUR_DEFAULT == cur_path or CUR_DEFAULT == cur_decision:
				print("ERROR with 4 space DNS line", line)
				continue

			if line[4:].startswith("> PID="):
				# pid_line_num = LINE_NUM_START
				cur_pid = line[4+6:].strip()
			else:
				enabled, source, uid, expiration_time, patterns = dns_editor_line_parser(line.strip())

				if None == enabled:
					print("ERROR parsing 4 space DNS line", line)
					continue
				
				for pattern in patterns:
					ret.add((cur_path, None, cur_decision, enabled, source, uid, expiration_time, pattern))
				
		# Line starting with "Allow:" or "Deny:"
		elif line.startswith(" "*2):
			if CUR_DEFAULT == cur_path:
				print("ERROR with 2 space DNS line", line)
				continue

			cur_decision = True if "Allow:" == line.strip() else False

		# Get the path or skip
		elif line:
			if "/" == line[0] or PACKET_NO_PATH_STR == line.strip():
				cur_pid = CUR_DEFAULT
				cur_path = line.strip()
				cur_path = PACKET_NO_PATH if PACKET_NO_PATH_STR == cur_path else cur_path
		
			else:
				print("ERROR parsing DNS rules. Bad line", line)
				continue
		else:
			continue

	return ret

# Write valid DNS rules to disk based on what is in memory.
# Assumes lock is held for process_hostname_acl already!
def save_dns_to_disk() -> None:
	with open(DNS_FOREVER_FILE, "w") as f:
		f.write(print_dns_rules())

# Remove then add rules based on remove and add sets. createIP applies to adding dns rule
def dns_bulk_add(add, saveToDisk, createIP):
	all_success = True

	for process, pid, decision, enabled, source, uid, expiration_time, pattern in add:
		# In the case that these DNS rules are loade when no groups are active (so no entries in /etc/hosts) group_str will just be default when it should not be
		rule = dns_dict(decision=decision, enabled=enabled, expiration_time=expiration_time, source=source, group_str=None, pid=pid, uid=uid)
		ret = add_dns_rule(process=process, pattern=pattern, rule=rule, saveToDisk=False, createIP=createIP)
		if False == ret:
			all_success = False
	if True == all_success:
		# print("SUCCESS: All DNS added and removed successfully.")
		pass
	
	if True == saveToDisk:
		save_dns_to_disk()

# Read rules from disk, parse it, and return those rules in format
def read_dns_from_disk(loadUntilRestart=False, createIP=False) -> None:
	with open(DNS_FOREVER_FILE, "r") as f:
		if loadUntilRestart == False:
			rules = "".join([line for line in f.readlines() if not line.endswith("until restart\n")])
			dns_bulk_add(add = dns_editor_parser(rules), saveToDisk = False, createIP=createIP)
		else:
			dns_bulk_add(add = dns_editor_parser(f.read()), saveToDisk = False, createIP=createIP)

# Display a graphical editor for DNS rules to be edited from
def dns_editor(createIP = True):

	with tempfile.NamedTemporaryFile(suffix=".tmp") as tf:
		tf.write(print_dns_rules().encode())
		tf.flush()
		subprocess.call([os.environ.get('EDITOR','nano'), tf.name])
		tf.seek(0)

		global process_hostname_acl_lock
		global process_hostname_acl
		with process_hostname_acl_lock:
			process_hostname_acl.clear()


		dns_bulk_add(add = dns_editor_parser(tf.read().decode()), saveToDisk=True, createIP = createIP)
		
def parser_helper(all_string, domain_level_string):

	# This function helps with if{ if{} else{} } else{} type calls where else code is repeated
	def parser_helper_inner(string, index, numbers, connection_type, time_type, connection_time):

		# Did not specify a domain level, so [-index] must be the connection type
		if string[-index] in numbers:

			connection_type = string[-index]

			# Specified a time type
			if string[-index - 1] in "mhdw":
				time_type = string[-index - 1]
				if is_int(string[:-index - 1]):
					connection_time = string[:-index - 1]
				else:
					print("time \"{}\" not an integer".format(string[:-index-1]))
					return False

			# Did not specifiy a time type, so [-index - 1:] must be a connection time
			elif is_int(string[-index - 1]) and len(string[:-index]) == 1:
				connection_time = string[-index - 1]

			else:
				return False
		else:
			return False

		return connection_type, time_type, connection_time

	while True:

		string = input("")
		# In the case that string is "", [-1] will throw an error
		if "" == string:
			continue

		''' ["[1] Forever"         , "[1] Any Connection"                                               , "[y] Allow"],
		    ["[2] Until Quit"      , "[2] {} port {}".format(proto, port)                               , "[n] Drop "],
		    ["[3] Until Restart"   , "[3] Only {}".format(destination)                                               ],
		    ["[1m] [3h] [7d] [52w]", "[4] Only {} and {} port {}".format(destination, proto, port)                   ], '''
		# Easiest to read backwards:
		# First section can be [123], ended by another number or as many numbers until [mhdw]
		# Then [1234] or [34] followed by [a-m] to indicate different domain levels
		# Finally [yn]

		# Dismiss the rule and continue to next connection prompt in the queue
		if "d" == string:
			return "d"

		# Dismiss all the rules in the Q
		elif "da" == string:
			return "da"

		# Clear DNS cache
		elif "cc" == string:
			with DNS_CACHE_LOCK:
				DNS_CACHE.clear()
			print("Cleared DNS cache\n")
			return "cc"

		# Print more information about what is allowed
		elif string in ["h", "help", "info", "i"]:
			# Code for more info display and help
			'''
			(no path), likely for listening process /usr/bin/dnsmasq iif lxcbr0
			From:  1.1.1.1         and UDP port 68
			To:    example.com      and UDP port 67 
			ip:    172.12.12.12
			iif:   lxcbr0
			PID:   54362
			User:  user
			Group: user
			'''
			continue

			# Fix this
			print("More Commands:")
			print("In second place type 3 or 4 followed by a letter from a-z")
			print("to specify matching the number of labels after the TLD")
			print("3a or 4a for 'www.domain.tld' matches '*.domain.tld' and 'domain.tld'")

			'''	
			#only show domains if the destination was a hostname
			hostname = connection["hostname"]
			if hostname != "":
				for i in range(2,2+26):
					domain = sub_label(hostname, i)
					if domain != sub_label(hostname, i - 1):
						print("[3{}] Only domain {}".format(chr(i + 95), domain))
					else: break

				for i in range(2,2+26):
					domain = sub_label(hostname, i)
					if domain != sub_label(hostname, i - 1):
						print("[4{}] Only domain {} and {} port {}".format(chr(i + 95), domain, proto, port))
					else: break
			'''
			_ = input("Press any key to continue...")
			continue

		# User wants to give a custom command.
		# Should be in the more info mode for this to work
		elif "c" == string[0]:
			continue

		# Editor Mode: Start a tkinter GUI to view and edit IP and DNS rules
		elif "e" == string:
			packet_editor(showChanges = True)
			continue

		elif "ed" == string:
			dns_editor(createIP=False)
			continue

		# Case where something was wrong
		elif string[-1] not in "yn":
			return None

		yn              = "y" == string[-1]
		domain_level    = "" # "abcdefghijklm"
		time_type       = "" # "mhdw"
		connection_time = "" # int to go with time_type, or "123" for forever, quit, restart
		connection_type = "" # "1234" for any, proto+port, hostname/domain, hostname/domain and proto+port

		# User specified a domain level, so don't match the full hostname
		if string[-2] in "abcdefghijklm":
			if len(string) < 4: 
				print("ERROR: length")
				continue
			domain_level = string[-2]

			# Call the helper on index -3 to look for "3" or "4", since domain_level can only be specified with those
			_ = parser_helper_inner(string, 3, domain_level_string, connection_type, time_type, connection_time)
			if not _:
				print("ERROR: parser")
				continue
			connection_type, time_type, connection_time = _

		# Did not specify a domain level, so index -2 must be the connection type
		else:
			if len(string) < 3:
				print("ERROR: length")
				continue

			_ = parser_helper_inner(string, 2, all_string, connection_type, time_type, connection_time)
			if not _:
				print("ERROR: parser")
				continue
			connection_type, time_type, connection_time = _

		if False:
			print("yn:             ", yn)
			print("domain_level:   ", domain_level)
			print("time_type:      ", time_type)
			print("connection_time:", connection_time)
			print("connection_type:", connection_type)

		return yn, domain_level, time_type, connection_time, connection_type

def del_dns_rule(process, pattern, rule) -> bool:
	
	global process_hostname_acl_lock
	global process_hostname_acl
	
	process_hostname_acl_lock.acquire()
	if process in process_hostname_acl:
		if pattern in process_hostname_acl[process]:
			process_hostname_acl[process][pattern].remove(rule)
			process_hostname_acl_lock.release()
			save_dns_to_disk()
			return True

	return False

# Stolen from https://stackoverflow.com/questions/2301789/how-to-read-a-file-in-reverse-order
def reverse_readline(filename, buf_size=8192):
	"""A generator that returns the lines of a file in reverse order"""
	with open(filename) as fh:
		segment = None
		offset = 0
		fh.seek(0, os.SEEK_END)
		file_size = remaining_size = fh.tell()
		while remaining_size > 0:
			offset = min(file_size, offset + buf_size)
			fh.seek(file_size - offset)
			buffer = fh.read(min(remaining_size, buf_size))
			remaining_size -= buf_size
			lines = buffer.split('\n')
			# The first line of the buffer is probably not a complete line so
			# we'll save it and append it to the last line of the next buffer
			# we read
			if segment is not None:
				# If the previous chunk starts right from the beginning of line
				# do not concat the segment to the last line of new chunk.
				# Instead, yield the segment first 
				if buffer[-1] != '\n':
					lines[-1] += segment
				else:
					yield segment
			segment = lines[0]
			for index in range(len(lines) - 1, 0, -1):
				if lines[index]:
					yield lines[index]
		# Don't yield None if the file was empty
		if segment is not None:
			yield segment

def ip_rule_for_dns(del_or_add, process, pattern, rule, group_str) -> bool:
	# print(pattern, group_str, rule, process, process in DNS_YES_IMPLIES_IP_YES_CUSTOM)

	ip_string_base = ""
	ip_process = process

	# Replace the "*" with the group based on the pid of the dnsmasq instance running and what bridge interface name it listens on
	if process in DNS_YES_IMPLIES_IP_YES_CUSTOM:
		rules_dict = DNS_YES_IMPLIES_IP_YES_CUSTOM[process]
		
		if "source" in rule and None != rule["source"]:
			ip_string_base += " --source {} ".format(rule["source"])

		# Change these from what was supplied to this function to what was in the config
		group_str = rules_dict["group"].replace("*", group_str)
		
		# How to handle arg1 in this case?
		ip_process = rules_dict["path"]

		proto_ports = rules_dict["proto_ports"]
		direction = rules_dict["direction"]

	else:
		proto_ports = DNS_YES_IMPLIES_IP_YES_PROTO_PORTS
		direction = "out"

	ret = True
	for proto, port in proto_ports:
		ip_string = ip_string_base

		if MATCH_ALL_QNAMES == pattern:
			ip_string += " -p {} --dport {} ".format(proto, port)

		elif "*" in pattern:
			# Remove "*." from the start of the pattern to match the iptables syntax
			ip_string += " -p {} --dport {} -m dns --dns-dst --domain {} ".format(proto, port, pattern.replace("*.", "", 1))

		else:
			ip_string += " -p {} --dport {} -m dns --dns-dst --hostname {} ".format(proto, port, pattern)

		# Need to see if in a group or a process in a namespace (which shows as a group)
		# PACKET_NO_PATH should indicate coming from a VM or LXC host
		if group_str in GROUP_DEFAULT or (group_str not in GROUP_DEFAULT and PACKET_NO_PATH != process):
			if "uid" in rule:
				if UID_ALL != rule["uid"]:
					if DNS_NO_UID != rule["uid"]:
						ip_string += " -m owner --uid-owner {} ".format(rule["uid"])
					else:
						print("ERROR: Bad uid", rule["uid"])
				else:
					print("ERROR: unknown uid", rule["uid"])

		ip_string += " -m comment --comment from\ dns"

		if True == del_or_add:
			ret &= add_ip_rule(string=ip_string,process=ip_process,io=direction,allowdeny="allow",pid=rule["pid"] if "pid" in rule else PID_ALL,group_str=group_str,enabled=rule["enabled"],saveToDisk=False)
		else:
			ret &= del_ip_rule(string=ip_string,process=ip_process,io=direction,allowdeny="allow",pid=rule["pid"] if "pid" in rule else PID_ALL,group_str=group_str,enabled=rule["enabled"])
	
	return ret

def ip_rules_for_dns_for_source(del_or_add, source, group_str):
	counter = 0
	global process_hostname_acl_lock
	with process_hostname_acl_lock:
		global process_hostname_acl

		for process in process_hostname_acl:
			for pattern in process_hostname_acl[process]:
				for rule in process_hostname_acl[process][pattern]:
					# Mathcing source OR process with no path, no source (implying all VMs can connect to this domain), and adding so as not to delete rules needed for other VMs in that group still operating
					if (("source" in rule and rule["source"] == source) or ("source" not in rule and PACKET_NO_PATH == process and del_or_add == True)) and ("decision" in rule and True == rule["decision"]):
						ip_rule_for_dns(del_or_add, process, pattern, rule, group_str)
						counter += 1

	print("Added" if del_or_add else "Deleted", counter, "rules")

def add_dns_rule(process, pattern, rule, saveToDisk=False, createIP=True) -> bool:

	# If group is GROUP_STR_ERROR, group may not be up (or groups.txt not exist yet), and in any case wouldn't create rule on disk
	# No source implies default group/namespace
	if "group_str" in rule and None != rule["group_str"]:
		group_str = rule["group_str"]
	else:
		group_str = group_from_source(rule["source"]) if "source" in rule else GROUP_DEFAULT_STR

	# Opinionated action:
	# Do not add rule with a specific uid if the user is a temporary user (like from the systemd DynamicUser option)
	if "uid" in rule:
		passwd_file = "/etc/passwd"
		try:
			with open(passwd_file, "r") as f:
				if is_int(rule["uid"]):
					if int(rule["uid"]) not in [int(line.split(":")[2]) for line in f.readlines()]:
						print("Deleting uid", rule["uid"])
						del rule["uid"]
				else:
					if rule["uid"] not in [int(line.split(":")[0]) for line in f.readlines()]:
						print("Deleting uid", rule["uid"])
						del rule["uid"]

		except Exception as e:
			# TODO: What to do if cannot open this system file?
			print("ERROR: Cannot open {} with Exception {}".format(passwd_file, e))
			return False

	if True == createIP and \
		("decision" in rule and True == rule["decision"]) and \
		[] != DNS_YES_IMPLIES_IP_YES_PROTO_PORTS and process not in DNS_DONT_CREATE_IP_RULE and GROUP_STR_ERROR != group_str and \
		not ("source" not in rule and PACKET_NO_PATH == process):
		# Do not create ip rule if path is "(no path)" and there is no source because rule means all VMs can connect to this domain
		ret = ip_rule_for_dns(True, process, pattern, rule, group_str)
		if True != ret:
			print("Failed to add the IP rule for that DNS rule:")
			print(process, pattern, rule)
		else:
			# print("Added IP rule")
			pass

	# Add the DNS rule
	global process_hostname_acl_lock
	with process_hostname_acl_lock:
		global process_hostname_acl

		if process not in process_hostname_acl:
			process_hostname_acl[process] = dict()

		if pattern not in process_hostname_acl[process]:
			process_hostname_acl[process][pattern] = list()
		
		# source = read_hostname_from_hosts(source) if SOURCE_DEFAULT != source else source
		process_hostname_acl[process][pattern].append(rule)
		
	if True == saveToDisk:
		save_dns_to_disk()

	return True

# Allow input and parse a response to a dns connection prompt
# The rule is simply inserted into the in memory ACL
def parse_dns_response(connection) -> bool:

	# Get the parsed variables and passin the string for all possible middle values, then just the ones with a possible domain_level letter
	_ = parser_helper("12", "2")
	if not isinstance(_, tuple):
		if _ not in DISMISS_RESPONSES:
			print("Bad input")
		return _

	allowdeny, domain_level, time_type, connection_time, connection_type = _
	# 10m2n (False, '', 'm', '10', '2')
	# 32y (True, '', '', '3', '2')
	# print(connection)
	# print(_)

	# creation_time = str(time.time())
	pid = PID_ALL
	ret = False

	expiration_time = DNS_EXPIRE_NEVER
	# No expiration time, so either Forever (add to disk to be read at next startup), Until Quit (rule with pid), or Until Restart (only add to memory)
	if "" == time_type:
		# Forever
		# if "1" == connection_time:
			# expiration_time = DNS_EXPIRE_NEVER
		# Until Quit. Track this pid
		if "2" == connection_time:
			pid = connection["pid"]
		# Unil Restart. -1 is treated as never write to disk, but also match all pids
		elif "3" == connection_time:
			expiration_time = DNS_EXPIRE_RESTART

	else:
		expiration_time = time.time() + seconds_from_string(time_type, int(connection_time))

	# User gave a domain_level, so create the matching re pattern and add it into the pattern list
	if domain_level:
		pattern = "*." + sub_label(connection["hostname"], "abcdefghijklm".index(domain_level) + 2)
		
		# pattern = dns_pattern_transform(pattern, stdout = False)

		# pattern = "(.*\\.)?" + sub_label(connection["hostname"], "abcdefghijklm".index(domain_level) + 2).replace(".", "\\.")
	elif "1" == connection_type:
		pattern = MATCH_ALL_QNAMES
	else:
		pattern = connection["hostname"]

	saveToDisk = (DNS_EXPIRE_NEVER == expiration_time or (is_int(expiration_time) and expiration_time > time.time()))

	rule = dns_dict(decision=allowdeny, enabled=True, expiration_time=expiration_time, source=connection["src"], group_str=None, pid=pid, uid=connection["uid"], arg1=connection["arg1"])
	ret = add_dns_rule(process=connection["process"], pattern=pattern, rule=rule, saveToDisk=True, createIP=True)
	return ret

# User input and parse a response to a packet connection prompt
# Rule is created in iptables format and inserted into the scheme already setup
def parse_packet_response(connection) -> bool:
	# Get the parsed variables and passin the string for all possible middle values, then just the ones with a possible domain_level letter
	_ = parser_helper(all_string = "1234", domain_level_string = "34")
	if not isinstance(_, tuple): return _
	allowdeny, domain_level, time_type, connection_time, connection_type = _
	# print(_)
	
	group = connection["group"]

	string = ""
	time_string = ""
	interface_string = "" # Filter on the outoging or incoming interface to avoid routing changing and things 

	if "" != time_type:
		# -m time --datestop 2020-01-01T00:00:00
		_ = datetime.datetime.fromtimestamp(int(datetime.datetime.utcnow().timestamp() + seconds_from_string(time_type, int(connection_time))))
		time_string = "-m time --datestop {}-{}-{}T{}:{}:{}".format(_.year, pad_zeroes(_.month, 2), pad_zeroes(_.day, 2), pad_zeroes(_.hour, 2), pad_zeroes(_.minute, 2), pad_zeroes(_.second, 2))
	
	#defualt to hostname, unless domain was specified
	srcdest = ""
	if "" != connection["hostname"]:
		srcdest = "-m dns --dns-dst --hostname " + connection["hostname"]

	# For example: "*.example.com" matches "a.n.y.thing.example.com" and "example.com"
	if "" != domain_level:
		# If domain level was specified and there was no hostname only an IP address, continue 
		if "" == connection["hostname"]:
			print("can only specify label level for hostnames, not ip addresses. Using whole address")
			print("Fix code here!")
			return False
		else:
			# TODO: Add the GROUP_NON_ZERO_INCOMING_IMPLIES_USE_DESTINATION_TOO here and maybe fix a source having a hostname,
			# that's a little unordinary
			srcdest = "-m dns --dns-dst --domain " + sub_label(connection["hostname"], "abcdefghijklm".index(domain_level) + 2)

	# Use IP addresses if there was no hostname in the connection
	if "" == connection["hostname"]:
		# Set desination or source address based on the IN interface
		if "" != connection["iif"]:
			srcdest += " --source " + connection["src"]
		else:
			srcdest += " --destination {} -o {} ".format(connection["dst"], connection["oif"])

	
	if True == GROUP_NON_ZERO_INCOMING_IMPLIES_USE_SOURCE_IP and "" != connection["iif"]:
	
		if "--source" not in srcdest: # May have been added from above
			srcdest += " --source " + connection["src"]

		# Last check is that we aren't also filtering desintation hostname so we don't use --dns-dst and -d
		if True == GROUP_NON_ZERO_INCOMING_IMPLIES_USE_DESTINATION_TOO and group not in GROUP_DEFAULT and "" == connection["hostname"]:
			srcdest += " --destination " + connection["dst"]

	# Set the protocol string
	protocol_string = "-p {}".format(connection["proto"])

	# Map target LOG's integers to human readable strings. only TCP, UDP, and ICMP were logged as strings
	if "2" == connection["proto"]:
		protocol_string = "-p IGMP"

	# Set port rule to follow protocol, only if port is present
	port_string = "" if not connection["dpt"] else "--dport {}".format(connection["dpt"])

	# Set this rule for just this user
	user_string = "" if ("" == connection["oif"] or connection["uid"] in ("", None)) else "-m owner --uid-owner {}".format(connection["uid"])

	# Any connection
	if "1" == connection_type:
		string = "{} {}".format(interface_string, user_string)

	# Any connection with that protocol and port
	elif "2" == connection_type:
		string = "{} {} {} {} {}".format(interface_string, protocol_string, port_string, user_string, time_string)

	# Any connection with that hostname
	elif "3" == connection_type:
		string = "{} {} {} {}".format(interface_string, srcdest, user_string, time_string)

	# Any connection with that hostname and protocol and port
	elif "4" == connection_type:
		string = "{} {} {} {} {} {}".format(interface_string, protocol_string, port_string, srcdest, user_string, time_string)

	# If the packet is incoming and it will go to a process listening on a socket, first the socket has no process
	# If the in_process was set, then this is the case and use that for creating the rule, not the "process" key which is empty
	# so that it would no mess up the deduping in the add to queue function
	process = connection["in_process"] if "" != connection["in_process"] else connection["process"]

	# PID is only used as not None for "Until Quit" option
	pid = PID_ALL

	# creation_time = str(time.time())

	if process in PROCESS_CHECK_CMDLINE and None != connection["arg1"]:
		print("arg1 process found:", process, arg1)
		string += " --arg1 " + connection["arg1"]

	saveToDisk = False
	# No time in minutes, hours, etc. was specified, so connection_time was 1,2,3 for concepts rather than units
	if "" == time_type:
		# Forever.
		if "1" == connection_time:
			saveToDisk = True

		# Until Quit.
		elif "2" == connection_time:

			# Until Quit requires PID
			# That also reuires userspace (or kernel, but unlikely) to track when the process dies to delete the rule for when the pid is reused
			if not is_int(connection["pid"]) or connection["pid"] <= 0:	
				# Todo: Get the in_pid for here
				print("Error with the PID. Cannot make PID based rule.")
				print(connection)
				return False

			pid = connection["pid"]

		# Until Restart
		elif "3" == connection_time:
			string += " -m comment --comment until\ restart"

	ret = add_ip_rule(string=string,process=process,io="out" if "" != connection["oif"] else "in",allowdeny="allow" if allowdeny else "deny",pid=pid,group_str=str_group(group),enabled=True,saveToDisk=saveToDisk)

	if True != ret:
		print("Failed to create rule")
		print(string)
	return ret

def get_listening(version, protocol, address, port, interface):
	interface1= interface
	version1  = version
	protocol1 = protocol
	address1  = address
	port1     = port

	''' Runs ss -tulpne and parses
	Netid     State      Recv-Q     Send-Q                  Local Address:Port            Peer Address:Port     Process
	udp       UNCONN     0          0                           127.0.0.1:53                   0.0.0.0:*         users:(("dnsmasq",pid=1357765,fd=6))
	udp       UNCONN     0          0                   192.168.1.2%wlan0:68                   0.0.0.0:*         users:(("systemd-network",pid=1344581,fd=19))
	tcp       LISTEN     0          32                          127.0.0.1:53                   0.0.0.0:*         users:(("dnsmasq",pid=1357765,fd=7))
	'''
	# Return if one of these addresses is destined for the other (they match or either is all 0's)
	def listening_ip_match(address, address1) -> bool:
		address = ipaddress.ip_address(address)
		address1 = ipaddress.ip_address(address1)
		if address.version != address1.version: return False
		if address == ipaddress.IPv4Address(0) or address1 == ipaddress.IPv4Address(0) or address == ipaddress.IPv6Address(0) or address1 == ipaddress.IPv6Address(0): return True
		return address == address1

	for line in command_run("/usr/bin/ss -tulpn").split("\n")[1:-1]:

		line      = line.split()
		protocol  = line[0]
		address   = line[4].split(":")[0].split("%")[0]
		port      = line[4].split(":")[1]
		version   = ipaddress.ip_address(address).version
		interface = "" if "%" not in line[4] else line[4].split("%")[1].split(":")[0]
		# Strip until the the rightmost comma is removed, get whats before that after the rightmost equals
		pid      = line[6].rstrip(")0123456789fd=")[:-1]
		pid      = pid[pid.rindex("=") + 1:]
		exe      = command_run("/usr/bin/readlink -f /proc/{}/exe".format(pid)).rstrip("\n")
		# print(str(version), str(version1));print(protocol.lower(), protocol1.lower());print(address, address1);print(str(port), str(port1))

		# Match all the values for the listenign socket
		if listening_ip_match(address, address1) and str(port) == str(port1) and protocol.lower() == protocol1.lower() and (interface == "" or interface == interface1) and str(version) == str(version1):
			return exe, pid

	return "", "-1"

# Called when new connections are added to the Q from reading the /proc/ entries or the named pipes for DNS prompts
# It also calls itself in a loop until Q is empty
def prompter(stop_event):
	global Q
	global Q_lock
	global prompt_queue_condition

	while True:
		with Q_lock:
			while 0 == len(Q):
				prompt_queue_condition.wait()
				
			# if stop_event.is_set(): break

			# Get first connection for read only access, until it is responded to and the ref count can go to 0
			connection = None
			for _ in Q:
				if True == _["ref"]:
					connection = _
					break

		# All the stuff in the queue was already prompted, but just waiting to be dumped out in the /proc/ reader function
		if not connection:
			continue

		protocol    = connection["proto"]
		pid         = connection["pid"]
		uid         = connection["uid"]
		port        = connection["dpt"]
		process     = connection["process"]
		arg1        = connection["arg1"]
		hostname    = connection["hostname"]
		server      = connection["dst"] if "" == hostname else hostname # Use the IP if there is no hostname associated with it
		source      = connection["src"]
		port_string = "port {}".format(port) if port else ""
		# group       = connection["group"]
		group       = group_from_source(source)

		# None for type "dns", but version doesn't matter in that case
		version = ipaddress.ip_address(connection["dst"]).version if None != connection["dst"] else 4
		
		print()
		print(time.strftime("%H:%M:%S"))

		if "packet" == connection["type"]:

			group_string = " group {}".format(str_group(group)) if group not in GROUP_DEFAULT else ""
			uid_string = " uid-owner {}".format(uid) if LOG_DEFAULT_VALUE != uid else "no uid owner"

			# Incoming and desinted for a listening socket
			if PACKET_NO_PATH == process and "" != connection["iif"]:
				listening_exe, listening_pid = get_listening(version=version, protocol=protocol, address=connection["dst"], port=port, interface=connection["iif"])
				
				if PACKET_NO_PATH != listening_exe and True == USE_LISTENING_PROCESS_GUESS_FOR_IPTABLES:
					print("{}, likely for listening process {} iif {}{}".format(PACKET_NO_PATH_STR, listening_exe, connection["iif"], group_string))
					connection["in_process"] = listening_exe
				else:
					print("(no listening process)" if PACKET_NO_PATH == process else process, "iif {}{}".format(connection["iif"], group_string))
			else:
				# Log this in a separate file? All are logged in the same file.
				if process.endswith(" (deleted)"):
					print("Skipping (deleted) process for", process, "\n")
					
					with Q_lock:
						connection["ref"] = False
					continue
					
				# Normal connection prompt
				elif PACKET_NO_PATH != process:
					print((arg1 + " via ") if None != arg1 else "" + process + group_string + uid_string)
				
				else:
					print(connection["log"], arg1)
					print("ERR (no path, likely kernel)")


			protocol_string = protocol
			if protocol == "2":
				protocol_string = "IGMP"

			# Outgoing connection prompt
			if "" != connection["oif"]:
				print("wants to connect to {} and {} {} {}".format(server, protocol_string, port_string, connection["oif"]))
			
			# Incoming connection prompt
			elif "" != connection["iif"]:
				host = read_hostname_from_hosts(source)
				if host != source:
					print("connection from host {} to {} and {} {}".format(host, server, protocol_string, port_string))
				else:
					print("connection from {} to {} and {} {}".format(source, server, protocol_string, port_string))

			else:
				print("Error with no \"oif\" or \"iif\"")
				print(connection)

			rows = [
				["[1] Forever"         , "[1] Any Connection"                                               , "[y] Allow"],
				["[2] Until Quit"      , "[2] {} {}".format(protocol_string, port_string)                   , "[n] Deny "],
				["[3] Until Restart"   , "[3] Only {}".format(server)                                                    ],
				["[1m] [3h] [7d] [52w]", "[4] Only {} and {} {}".format(server, protocol_string, port_string)            ],
			]
			grid([[row[i] for row in rows if i < len(row)] for i in range(0,max([len(row) for row in rows]))])

			# Parses user input and creates iptables entry
			ret = parse_packet_response(connection)
			if False == ret:
				print("Error creating rule!")

		elif "dns" == connection["type"]:
			# if process.endswith(" (deleted)"):
				# continue

			uid_string = "uid-owner {}".format(uid) if LOG_DEFAULT_VALUE != uid else "no uid owner"

			# Consider the case of a --namespace sending DNS query, which will have source from "/var/run/prompt-and-split/nsips.txt"

			# If no path and source is not in SOURCES_ALL, it is probably a VM, so make prompt slightly different
			if PACKET_NO_PATH == process and SOURCE_DEFAULT != source:
				host = read_hostname_from_hosts(source)
				print("{} group {}".format(("host " + host if host != source else source), group))
			# Should be from --namespace
			elif GROUP_DEFAULT_STR != group:
				print(process, uid_string, "in namespace {}".format(group))
			else:
				print(process, uid_string)

			print("is trying to make a DNS query for", hostname)

			first_level = sub_label(hostname, 2)
			rows = [
				["[1] Forever"         , "[1]  Any Query"                                               , "[y] Allow"],
				["[2] Until Quit"      , "[2]  Hostname " + hostname                                    , "[n] Deny "],
				["[3] Until Restart"   , "[2a] Pattern " + "*." + first_level],
				["[1m] [3h] [7d] [52w]", ""],
			]

			# If the hostname had 3 or more labels then display more options to give the user the idea of how to specify the different levels with letters
			next_level = sub_label(hostname, 3)
			if first_level != next_level:
				rows[3][1] = "[2b] Pattern " + "*." + next_level

			grid([[row[i] for row in rows if i < len(row)] for i in range(0,max([len(row) for row in rows]))])

			# print(connection)
			ret = parse_dns_response(connection)
			
			if True == ret:
				print("Added DNS rule")
			elif ret in DISMISS_RESPONSES:
				# Should go on to the with Q_lock: part
				pass
			else:
				print("Error creating DNS rule, ret ", ret)

		else:
			print("ERROR connection[\"type\"] error for:", connection)
			continue

		#Once user responds, the rule can no longer exist in the Q (which prevented others from coming in from the kernel buffer)
		with Q_lock:
			if "da" == ret:
				print("Cleared {} prompts".format(len(Q)))
				Q.clear()
			else:
				connection["ref"] = False

def reader(stop_event):
	global Q
	global Q_lock
	global prompt_queue_condition

	# Clean pids about once in ten minutes minute
	if random.randint(0,600) == 0:
		pid_cleaner()

	while not stop_event.is_set():

		proc_path_values = 4
		split = None
		# print(1)
		with open(PROC_PATH, "r") as f:
			split = [line.strip() for line in f.readlines()]

		# print(2)
		if 0 != len(split) % proc_path_values:
			print("Error with process info from kernel. Check journalctl!")
			print(split)
			print()

		# print(3)
		#Parse for "IN= " or "OUT= ", then "SRC=", "DST=", "PROTO=", "SPT=", "DPT="
		#Anything that doesn't give a full 4 sections, caused by kernel FIFO filling up and only adding part of a string, should be ignored here
		added_list = list()
		Q_lock.acquire()
		# print(4)
		for i in range(0, len(split) // proc_path_values, proc_path_values):

			arg1 = None
			log,hostname,process,pid = split[i:i+proc_path_values]
			if process in PROCESS_CHECK_CMDLINE and "0" != pid:
				
				# SECURITY NOTE: the second argument of cmdline might be a symlink, not an actual file
				try:
					with open("/proc/{}/cmdline".format(pid), "rb") as f:
						arg1 = f.read().split(b'\x00')[1].decode()
				except Exception as e:
					print("ERROR: getting cmdline arg1 for", process, pid)
					print(str(e))

			# process = arg1
			group = int(log[0])
			log = log[1:]
			# print(log,hostname,process,pid)

			connection = new_connection(hostname=hostname,process=process,pid=int(pid),conn_type="packet",group=group,log=log,arg1=arg1)
			for entry in log.split():
				for key in LOG_ENTRIES:
					if entry.startswith(key) and connection[LOG_ENTRIES[key]] == None:
						connection[LOG_ENTRIES[key]] = entry[len(key):]
						break
			
			#Check that this connection can be added to the Q, but don't actually add it until removing this with ref == True
			if prompt_queue_add_check(connection, added_list):
				added_list.append(connection)
				log_packet_connection(connection)
				# print("added", connection)

		# Remove anything that has been responded to in the prompt (which may have been up while things were then read from the kernel buffer
		# and we still wanted to exist until after the next read from kernel, which is guarenteed safety due to the lock).
		# So the check to add was done before removing these already responded to connection prompts
		# Q = [entry for entry in Q if entry["ref"] == True]
		# print(5)
		for connection in Q:
			if False == connection["ref"]:
				Q.remove(connection)
		# print(6)
		if added_list:
			Q.extend(added_list)
			prompt_queue_condition.notify()
		# print(7)
		Q_lock.release()
		# print(8)
		time.sleep(PROC_READ_SLEEP)

def exit_iptables():
# 	iptables_run("""
# iptables -t mangle -F
# iptables -t mangle -X
# """)
	pass

# Update the transaction id to process mapping by reading from the /proc/ file
def update_tid_map():
	split = None
	with open(TID_PROC_FILE, "r") as f:
		split = [line.strip() for line in f.readlines()]

	if 0 != len(split) % 6:
		print("Error with DNS info from kernel. Check journalctl!", split)
		print()
		return

	global transactionIDMap #Lock is set by the one function calling update_tid_map(), not right here
	for i in range(0, len(split), 6):

		error, tid, pid, uid, gid, process = split[i:i+6]
		# if "1" == error:
			# print("Error reported by kernel about this DNS info. Check journalctl!", split[i : i + 6])
			# continue

		transactionIDMap[int(tid)] = (process, int(pid), int(uid), int(gid), time.time())
	# print(transactionIDMap)

# Buffer dns_log messages so that 5 done at once get likely batched together
# Each message gets added as the key pointing to value: a counter and latest time it was called
# Each call to the log function will look for keys older than 1 second and write them to the buffer
# In addiion, a 1 second loop will also flush so that any gaps in calls does not prevent a message from being logged until then
dns_log_buffer = dict()
# DNS_LOG_FIRST_QUERY_STR = "FIRST QUERY:"
# dns_log_buffer_instant_print = [DNS_LOG_FIRST_QUERY_STR]
dns_log_lock = threading.Lock()

def dns_log(*args):
	FLUSH_SECONDS = 1

	# Was this used at some point?
	# def format(s) -> str:
		# return (" {}".format(s))*(1 if s else 0)

	def flush() -> None:
		try:
			with open(DNS_LOG_FILE, "a") as f:
				# Need to confirm, are these FIFO ordered?
				for key in list(dns_log_buffer):
					if dns_log_buffer[key][1] < time.time() - FLUSH_SECONDS:
						times = dns_log_buffer[key][0]
						f.write(str(datetime.datetime.now())[:-7] + key + (" {} times".format(times) if times > 1 else "") + "\n")
						del dns_log_buffer[key]

		except Exception as e:
			print("LOG OPEN ERROR")
			print(str(e))

	global dns_log_lock
	with dns_log_lock:
		if not (1 == len(args) and True == args[0]):
			msg = "".join([" " + str(arg) for arg in args])	

			if msg in dns_log_buffer:
				dns_log_buffer[msg][0] += 1
			else:
				now_time = time.time()
				# if msg in [" " + x for x in dns_log_buffer_instant_print]:
					# now_time -= (2*FLUSH_SECONDS)
				dns_log_buffer[msg] = [1, now_time]

		flush()

def qname_block(qname) -> bool:
	# if '.' not in qname:
		# return True
	if '.' == qname[-1]:
		return True
	if qname.endswith(".in-addr.arpa"):
		return True
	if re.fullmatch("([0-9]{1,3}\.){3}[0-9]{1,3}", qname):
		return True
	return False

# Looks up if this hostname is allowed for this proces and if not prompts the user and blocks unitl user responds
# Maybe deny new impediately, but prompt user to add it to the DNS ACL?
def allow_qname_for_process(qname, qtype, process, query, pid) -> int:
	# print(uid, gid, qname, qtype, process, pid, source)

	# General qname blocking
	if qname_block(qname) or "AAAA" == dnslib.QTYPE[qtype]:
		# dns_log("REJECT " + dnslib.QTYPE[qtype], qname, process, pid)
		return 1

	# This prevents the case where the named pipe gets a tid not in the tid -> process map, so False is returned immediaiely
	# Could happen from lattency, corruption, or other packet related problems.
	# This would happen so rarely it is better to check after regular checks, but before prompting
	if PACKET_ERROR_PATH == process:
		# This likely comes from failure or kernel gets AAAA query and doesn't parse it
		print("ERROR: Got DNS query for {}, type {}, from pid {} with no process\n".format(qname, qtype, pid))
		return 1

	global process_hostname_acl_lock
	global process_hostname_acl
	
	# print(process_hostname_acl)
	with process_hostname_acl_lock:
		if process in process_hostname_acl:

			patterns = process_hostname_acl[process]

			# Return None implies the rule did not match, otherwise a decision is returned
			def check_dns_match(rules):
				for rule in rules:
					match = True
					if "source" in rule:
						rule["source"] = read_hostname_from_hosts(rule["source"])
						# if "group" in rule:
							# rule["group"] = group_from_source(rule["source"])

					# print(qname, rule, query)
					for condition in rule:
						if condition in query and rule[condition] != query[condition]:
							match = False
							break
					
					if True == match:
						return 0 if True == rule["decision"] else 1

				return None

			# UPDATE for better matching here. Should check full first, but then check all *... for a rule match
			# Check *.domain1.tld, then *.domain2.domain1.tld, ... then full hostname
			for special_qname in (qname, MATCH_ALL_QNAMES):
				if special_qname in patterns:
					decision_or_no_match = check_dns_match(patterns[special_qname])
					if None != decision_or_no_match:
						return decision_or_no_match
			i = 2
			sub = None
			old_sub = None
			while True:
				sub = "*." + sub_label(qname, i)

				# All possibilities of *.domain2.domain1.tld are exhausted
				if sub == old_sub:
					# Check last case all DNS qnames are blocked:
					if "*" in patterns:
						decision_or_no_match = check_dns_match(patterns["*"])
						if None != decision_or_no_match:
							return decision_or_no_match
					break
				
				if sub in patterns:
					decision_or_no_match = check_dns_match(patterns[sub])
					if None != decision_or_no_match:
						return decision_or_no_match
			
				old_sub = sub
				i += 1

	# Prompt the user with the new DNS request
	# Aquire lock and try to add a new prompt to the prompting queue
	conn = new_connection(hostname=qname, process=process, pid=query["pid"] if "pid" in query else None, conn_type="dns", group=None)#query["group_str"])
	conn["uid"] = query["uid"] if "uid" in query else None
	# conn["gid"] = gid
	conn["src"] = query["source"]

	global Q
	global Q_lock
	global prompt_queue_condition

	with Q_lock:
		if prompt_queue_add_check(conn, []):
			Q.append(conn)
			# print(conn)
			prompt_queue_condition.notify()

	# The user was prompted, so reject the query immediately until they may make a decision to allow
	return 2

# Set match_group if only load rules from file that match that group
# Set force_group if load all rules from file and force change the group to force_group
def reload_rules(del_or_add, file, match_group=None, force_group=None) ->bool:

	if None != match_group:
		group = match_group
	elif None != force_group:
		group = force_group
	else:
		print("ERROR: must specify one of match_group or force_group")
		return False

	group_int = int_group(group)
	group_str = str_group(group)
	print("Loading rules for group", group_str, group_int)
	if None == group:
		return False

	if True == del_or_add:
		iptables_run("iptables -w -t mangle -N {}{}-{}".format(CHAIN_PREFIX, group_str, "in"), printErrors=False)
		iptables_run("iptables -w -t mangle -N {}{}-{}".format(CHAIN_PREFIX, group_str, "out"), printErrors=False)

	if os.path.isfile(file):
		# try:
		with open(file, "r") as f:
			created_groups = list()
			for rule in [line.strip() for line in f.readlines() if line.strip() and "#" != line.lstrip()[0]]:
				rule = rule.split(IP_FOREVER_FILE_COLUMNS["separator"])
				
				if len(IP_FOREVER_FILE_COLUMNS) - 1 != len(rule):
					print("Error parsing {} line:".format(file), rule)
				else:
					if None != match_group and rule[IP_FOREVER_FILE_COLUMNS["group"]] != group_str:
						continue
					
					# Otherwise group matches or force the group to be group_str
					if True == del_or_add:
						add_ip_rule(string=rule[IP_FOREVER_FILE_COLUMNS["string"]],process=rule[IP_FOREVER_FILE_COLUMNS["process"]],io=rule[IP_FOREVER_FILE_COLUMNS["io"]],allowdeny=rule[IP_FOREVER_FILE_COLUMNS["allowdeny"]],pid=int(rule[IP_FOREVER_FILE_COLUMNS["pid"]]),group_str=group_str,enabled=int(rule[IP_FOREVER_FILE_COLUMNS["enabled"]]))
					else:
						del_ip_rule(string=rule[IP_FOREVER_FILE_COLUMNS["string"]],process=rule[IP_FOREVER_FILE_COLUMNS["process"]],io=rule[IP_FOREVER_FILE_COLUMNS["io"]],allowdeny=rule[IP_FOREVER_FILE_COLUMNS["allowdeny"]],pid=int(rule[IP_FOREVER_FILE_COLUMNS["pid"]]),group_str=group_str,enabled=int(rule[IP_FOREVER_FILE_COLUMNS["enabled"]]))

class listener_thread(threading.Thread):
	def __init__(self, threadID, name, listen_addr):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		self.listen_addr = listen_addr
		self.UDPServerObject = None
	def run(self):
		try:
			threading_object = socketserver.ThreadingUDPServer(self.listen_addr, MyDatagramRequestHandler)
			self.UDPServerObject = threading_object
			# self.UDPServerObject.asd()
			self.UDPServerObject.serve_forever(poll_interval=.5) # Default poll_interval (to check for shutdown) is .5 as of July 2022
		except Exception as e:
			print("ERROR with thread run()", str(e), self.listen_addr, self.name)
	def stop(self):
		if self.UDPServerObject: # Avoid case where this is None for some reason (not started?)
			try:
				self.UDPServerObject.shutdown() 
			except Exception as e:
				print("ERR", str(e))

def hash_dns(packet):
	return str(packet.header) + str(packet.q)

# Subclass the DatagramRequestHandler
class MyDatagramRequestHandler(socketserver.DatagramRequestHandler):

	global tid_map_lock
	global transactionIDMap
	global DNS_SPECIAL_ROUTING
	global dns_special_routing_lock

	def handle(self):
		# print(-2)
		source = self.client_address[0]
		# datagram = self.rfile
		packet = dnslib.DNSRecord.parse(self.rfile.read())
		qname = str(packet.q.qname)[:-1]
		tid = packet.header.id
		qtype = packet.q.qtype
		# print(source, qname, tid)
		# print(-1)
		def send_reject():
			# print(qname, qtype, tid)
			send_reject_packet = dnslib.DNSRecord(dnslib.DNSHeader(qr=1, rcode=dnslib.RCODE.reverse['REFUSED'], id=tid), q=dnslib.DNSQuestion(qname, qtype=qtype))
			self.wfile.write(send_reject_packet.pack())
			# print("sent reject")

		# print(0)
		# Call update_tid_map() to update the association of this packet with a process
		with tid_map_lock:
			# print(1)
			update_tid_map()
			# print(2)

			if tid in transactionIDMap:
				info = transactionIDMap.pop(tid)
			else:
				info = (PACKET_NO_PATH, PID_ALL, UID_ALL, GID_ALL, time.time())
				# print("ERROR: tid {} did not appear in tid list".format(tid))
			# info pop the other for DNS queries from incoming PREROUTING chain from the VMs
			# note that in the kernel for incoming packets, process is reported as the destination, so no use putting a --watch-queries in the br-* PREROUTNIG chains

		process = info[0]
		pid = int(info[1])
		uid = int(info[2])
		gid = int(info[3])
		group_str = group_from_source(source)
		source_hostname = read_hostname_from_hosts(source)
		query_dict = dns_dict(pid=pid, uid=uid, source=source_hostname, group_str=None)
		# print(packet)

		# SECURITY NOTE:
		# Returning an address for a hosts file entry WITHOUT prompting. DNS prompting was made to help stop useless leaky upstream queries
		# This could leak private ip to name mappings to a program WITHOUT ANY PROMPTING. This should be updated and made an option for prompting
		if True == READ_HOSTS:
			if True == POLL_HOSTS:
				load_hosts_files()

			if qname in HOSTS_DATA:
				# temp = dnslib.DNSRecord(dnslib.DNSHeader(qr=1,aa=1,ra=1,id=tid), q=dnslib.DNSQuestion(qname), a=dnslib.RR(qname,rdata=dnslib.A(HOSTS_DATA[qname])))
				temp = dnslib.DNSRecord(dnslib.DNSHeader(qr=1,rd=1,ra=1,id=tid), q=dnslib.DNSQuestion(qname, qtype=qtype), a=dnslib.RR(qname,ttl=60,rdata=dnslib.A(HOSTS_DATA[qname])))
				self.wfile.write(temp.pack())
				dns_log("Allow:", qname, "PROC="+process, "UID={}".format(uid), "GRP={}".format(group_str), "SRC={}".format(source_hostname), "PID={}".format(pid))
				return

		# Case where 
		# if GROUP_STR_ERROR == group_str:
			# group_str = group_from_source(self.server.server_address[0])
			# print(123)
		# print(process, group_str, source, qname)

		# Got a crash at one point because of this. Not sure if just randomly malformed packets
		if qtype not in dnslib.QTYPE.forward:
			print("\n\n\n==========================")
			print("ERROR with qtype", qtype)
			print(qname, "PROC="+process, "UID={}".format(uid), "GRP={}".format(group_str), "SRC={}".format(source_hostname), "PID={}".format(pid))
			print(packet)
		
		decision = allow_qname_for_process(qname=qname,qtype=qtype,process=process,query=query_dict,pid=pid)
		first_query = True if 2 == decision else False

		if "AAAA" != dnslib.QTYPE[qtype]:
			s = ("FIRST QUERY: " if True == first_query else "") + ("Allow:" if 0 == decision else "REJECT:") + (" {}".format(dnslib.QTYPE[qtype]) if "A" != dnslib.QTYPE[qtype] else "")
			dns_log(s, qname, "PROC="+process, "UID={}".format(uid), "GRP={}".format(group_str), "SRC={}".format(source_hostname), "PID={}".format(pid))
		
		if 0 != decision:
			send_reject()
			return
		# print(process, group_str, source, qname)

		# If query not cached, send it upstream to internet
		packet.header.id = 0 # All cache items have the same
		cache_key = hash_dns(packet)

		now = time.time()

		# Unlock after aquiring regular cache lock so response will be guarenteed in regular cache for small case of when reponse is removed from flood cache, but not yet in regualr cache
		DNS_CACHE_LOCK.acquire()

		# Clean the cache every once in a while
		# if random.randint(0,20) == 0:
		# 	cache_remove = list()
		# 	for item in DNS_CACHE.items():
		# 		if now - item[1][1] > DNS_CACHE_TTL:
		# 			cache_remove.append(item[0])
		# 	for item in cache_remove:
		# 		DNS_CACHE.pop(item, None)

		if cache_key not in DNS_CACHE or now - DNS_CACHE[cache_key][1] > DNS_CACHE_TTL:
			# Sockets may be appended to flood cache slightly out of order if they come in at the same time very quickly
			DNS_CACHE_LOCK.release()

			# Check if the qname is in the outgoing cache, meaning the internet was already asked and we don't want to send multiple queries
			continue_with_remote_query = False
			with DNS_FLOOD_CACHE_LOCK:
				if cache_key not in DNS_FLOOD_CACHE:
					continue_with_remote_query = True
					DNS_FLOOD_CACHE[cache_key] = list()

				# Add wfile and tid to list to be sent a response. When upstream comes back, go through and send it to all local threads
				# If local socket does not get answer fast enough, it will resend a query with the SAME TID as the first time. This will get added too. Which to respone to, the first, latest, or all?
				DNS_FLOOD_CACHE[cache_key].append((self.wfile, tid))

			# Cannot return here as then self.wfile is released (socket goes away) so future writes to it (in the loop below) with answer 
			# won't go through. time.sleep() seems to make all threads go to sleep, not just this one, so spin until the write() or error
			if False == continue_with_remote_query:
				# UPDATE: No lock here is OK, just for reads?
				while cache_key in DNS_FLOOD_CACHE:
					pass
				return

			# print(group_str, source_hostname, source_hostname)
			# Send the local query to the internet
			remote_server = None

			# Send some dns requests to an address other than the default based on the process path
			# Mainly used for VMs' queries ith no path coming in on the bridge devices
			with dns_special_routing_lock:
				if process in DNS_SPECIAL_ROUTING:
					config_used_uid = None

					# UPDATE here because UID_ALL may be in config, but explicit uid may ALSO be there. which to use?
					# Also what about different source?
					if UID_ALL in DNS_SPECIAL_ROUTING[process]:
						config_used_uid = UID_ALL
					elif uid in DNS_SPECIAL_ROUTING[process]:
						config_used_uid = uid

					if None != config_used_uid:
						for check_dict in DNS_SPECIAL_ROUTING[process][config_used_uid]:
							source_match = check_dict['source'] in (source, source_hostname, ALL_SOURCES) or special_dns_config_hostname_in_group(source_hostname, check_dict['source'])
							# print(source_match, source, source_hostname, ALL_SOURCES)
							if re.fullmatch(check_dict['qname'], qname) and source_match:
								dest = check_dict['addr'][0]
								try: # If address given is text (like a hostname) this fails and will try to resolve with /etc/hosts.
									ipaddress.ip_address(dest)
								except: # If it is not an IP and not in /etc/hosts (function returns same string passed in), return an error
									host = dest.replace("*", group_str)
									resolved_dest = read_ip_from_hosts(host)
									if host != resolved_dest:
										remote_server = (resolved_dest, check_dict['addr'][1])
									else:
										print("Error in DNS_SPECIAL_ROUTING. No IP in /etc/hosts for", host)
										return
								else:
									remote_server = check_dict['addr']
								break

			if remote_server == None:
				# UPDATE to really check if group is a --namespace
				# If group is not default and source is a *-DNS, then query is probably from a process in a netNS, so route back to there
				if GROUP_DEFAULT_STR != group_str and source_hostname.endswith("-DNS"):
					remote_server = (source, 1053)
				else:
					remote_server = DNS_REMOTE

			# print(remote_server)
			remote_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			remote_socket.settimeout(5)
			packet.header.id = tid
			try:
				# print(remote_server)
				remote_socket.sendto(packet.pack(), remote_server)

			except Exception as e: # Failure to send. If failure based on the python3.9 packet not being allowed, for some reason the whole script freezes
				print(str(e))
				print("ERR Remote send failure to", remote_server)
				dns_log("ERR Remote send failure to {}".format(remote_server), "", "", "")

				# Remove everything from the outgoing cache as they will never get an answer from internet
				with DNS_FLOOD_CACHE_LOCK:
					DNS_FLOOD_CACHE.pop(cache_key)

				# send_reject()
				return

			# Get the response
			try:
				data = remote_socket.recv(2048)
			except: # Timeout
				dns_log("ERR Timeout to {}".format(remote_server), qname, process, pid)

				with DNS_FLOOD_CACHE_LOCK:
					DNS_FLOOD_CACHE.pop(cache_key)

				# send_reject()
				return

			try:
				packet = dnslib.DNSRecord.parse(data)
			except: # Malformed DNS Response, even according to Wireshark in some case I looked at
				dns_log("ERROR response for", qname)

				with DNS_FLOOD_CACHE_LOCK:
					DNS_FLOOD_CACHE.pop(cache_key)

				return

			a_records_for_kernel_table = str(packet.q.qname).rstrip(".") + ","
			
			found_a_record = False
			for rr in packet.rr:
				# Removed "qname == rr.rname" because need to account for CNAMEs better
				# Should log cnames here to a verbose log file
				if dnslib.QTYPE.A == rr.rtype and dnslib.CLASS.reverse["IN"] == rr.rclass and rr.rdata:
					found_a_record = True
					a = str(rr.rdata).split(".")
					a_records_for_kernel_table += pad_zeroes(str(int(a[3])*(256**3)+int(a[2])*(256**2)+int(a[1])*(256**1)+int(a[0])), 10)

			# Send the answers for the qname to the dns kernel module
			if True == found_a_record:
				with open(DNS_TABLE_PATH, "w") as f:
					f.write(a_records_for_kernel_table)
			else:
				with DNS_FLOOD_CACHE_LOCK:
					DNS_FLOOD_CACHE.pop(cache_key)

				# print("ERROR: Found no A record in", str(packet))
				return

			# Add the internet answer to the dns answer cache along with the current time
			with DNS_CACHE_LOCK:
				DNS_CACHE[cache_key] = (packet.pack(), time.time())

			# Send the internet answer locally to all sockets that asked for it in time between first ask and receiving an internet answer
			with DNS_FLOOD_CACHE_LOCK:
				for local_thread_wfile_and_tid in DNS_FLOOD_CACHE[cache_key]:
					packet.header.id = local_thread_wfile_and_tid[1]
					local_thread_wfile_and_tid[0].write(packet.pack())

				DNS_FLOOD_CACHE.pop(cache_key)

		# Return what was in the cache
		else:
			packet = dnslib.DNSRecord.parse(DNS_CACHE[cache_key][0])
			DNS_CACHE_LOCK.release()
			packet.header.id = tid
			self.wfile.write(packet.pack())

# Read a file and do action based on content
def memory_modification_handler():
	with open(MEMORY_MODIFICATION_FILE, "r") as f:
		for line in f.readlines():
			if "" == line.strip():
				continue

			line = line.split()
			if line[0] == "add-dns-special-routing":
				if 6 != len(line):
					print("ERROR: --add-dns-special-routing requires 5 arguments: process, uid, qname, source, address")
					continue
				edit_dns_special_routing(addOrDel=True,process=line[1],uid=line[2],qname=line[3],source=line[4],addr=line[5])
			
			elif line[0] == "del-dns-special-routing":
				if 6 != len(line):
					print("ERROR: --del-dns-special-routing requires 5 arguments: process, uid, qname, source, address")
					continue
				edit_dns_special_routing(addOrDel=False,process=line[1],uid=line[2],qname=line[3],source=line[4],addr=line[5])

			elif line[0] in ("show-dns-special-routing", "print-dns-special-routing"):
				print("DNS_SPECIAL_ROUTING:")

				for process in DNS_SPECIAL_ROUTING:
					print(process + ": {")
					for uid in DNS_SPECIAL_ROUTING[process]:
						print(" "*4 + str(uid) + ": [", end="")
						print(DNS_SPECIAL_ROUTING[process][uid], end="")
						print("]")
					print("}")

def signal_handler(signal, frame):
	sys.exit(0)

def SIGUSR1_handler(signal, frame):
	memory_modification_handler()
	pass

if __name__ == "__main__":
	command_run("/usr/bin/touch {}".format(MEMORY_MODIFICATION_FILE))

	signal.signal(signal.SIGUSR1, SIGUSR1_handler)

	# Change readable "(no path)" to "" in memory
	for x in (DNS_YES_IMPLIES_IP_YES_CUSTOM, DNS_SPECIAL_ROUTING):
		if PACKET_NO_PATH_STR in x:
			x[PACKET_NO_PATH] = x.pop(PACKET_NO_PATH_STR)

	# Handy to make a required first arg
	start_index = 1
	if len(sys.argv) > start_index:

		# Print all the rules in kernel with the hash to identify it by for deletion
		if sys.argv[start_index] in ["list", "l", "-l", "--list", "list-all", "--list-all"]:
			read_dns_from_disk(loadUntilRestart=False, createIP=False)
			print(print_dns_rules())
			print_packet_rules()
			exit()

		if sys.argv[start_index] in ["list-dns", "--list-dns"]:
			read_dns_from_disk(loadUntilRestart=False, createIP=False)
			print(print_dns_rules())
			exit()

		elif sys.argv[start_index] in ["--edit", "e"]:
			packet_editor(showChanges=True)
			exit()

		# Add rules. Meant for a network connection script to allow NTP and VPN initial connections
		elif sys.argv[start_index] in ["--add", "-a", "--create", "-c"]:
			if len(sys.argv) != start_index + 1 + len(IP_FOREVER_FILE_COLUMNS) - 1:
				print("Wrong number of args for add_ip_rule()")
				exit(1)
			# creation_time = str(time.time())
			# if sys.argv[6] == "None": sys.argv[6] = None
			# if sys.argv[3] == "None": sys.argv[3] = None
			ret = add_ip_rule(string=sys.argv[start_index+1], process=sys.argv[start_index+2], io=sys.argv[start_index+3], allowdeny=sys.argv[start_index+4], pid=int(sys.argv[start_index+5]), group_str=str_group(sys.argv[start_index+6]), enabled=int(sys.argv[start_index+7]))
			exit(0 if ret else 1)

		elif sys.argv[start_index] in ["--del", "-d", "--delete"]:
			if len(sys.argv) != start_index + 1 + len(IP_FOREVER_FILE_COLUMNS) - 1:
				print("Wrong number of args for del_ip_rule()")
				exit(1)

			ret = del_ip_rule(string=sys.argv[start_index+1], process=sys.argv[start_index+2], io=sys.argv[start_index+3], allowdeny=sys.argv[start_index+4], pid=int(sys.argv[start_index+5]), group_str=str_group(sys.argv[start_index+6]), enabled=int(sys.argv[start_index+7]))
			exit(0 if ret else 1)


		# Completely overwrite the dns.pickle file on disk. Probably don't use while the script is running. Only for fixing broken stuff
		# Current dns.pickle is moved to backup first, then given a blank file to add to
		elif sys.argv[start_index] == "--overwrite-dns-on-disk":
			command_run("mv {} /tmp/dns.pickle.backup.{}".format(DNS_FOREVER_FILE, time.time()))
			dns_editor(createIP = False)
			exit()

		# Read the pickle file on disk and print it
		elif sys.argv[start_index] == "--print-rules-on-disk":
			print_packet_rules(returnArray=False, stdout=True)
			exit()

		# Reload all the rules from the rules file
		elif sys.argv[start_index] == "--reload-rules":
			reload_rules(True, file=IP_FOREVER_FILE, match_group=sys.argv[start_index + 1] if len(sys.argv) > start_index + 1 else GROUP_DEFAULT_INT)
			exit()

		# Load rules file, like the global one, but replace all the groups with given one
		elif sys.argv[start_index] == "--add-rules-file-for-group":
			if len(sys.argv) != start_index + 3:
				print("ERROR: --add-rules-file-for-group requires file path and group")
				exit(1)
			reload_rules(del_or_add=True, file=sys.argv[start_index+1], force_group=sys.argv[start_index+2])
			exit()

		elif sys.argv[start_index] == "--delete-rules-file-for-group":
			if len(sys.argv) != start_index + 3:
				print("ERROR: --delete-rules-file-for-group requires file path and group")
				exit(1)
			reload_rules(del_or_add=False, file=sys.argv[start_index+1], force_group=sys.argv[start_index+2])
			exit()

		# Create IP rules from DNS ones for this host's (source's) DNS rules
		# These should not have been created when reading all rules from disk initially because the group_str would be an error
		elif sys.argv[start_index] == "--add-dns-ip-rules-for-source-and-group":
			if len(sys.argv) != start_index + 3:
				print("ERROR: --add-dns-ip-rules-for-source-and-group requires 2 arguments: source string and group string")
				exit(1)
			
			# Go through DNS rules in memory and create IP rules for the ones with the given source
			read_dns_from_disk(createIP=False)
			ip_rules_for_dns_for_source(True, sys.argv[start_index + 1], sys.argv[start_index + 2])
			exit()

		elif sys.argv[start_index] == "--delete-dns-ip-rules-for-source-and-group":
			if len(sys.argv) != start_index + 3:
				print("ERROR: --delete-dns-ip-rules-for-source-and-group requires 2 arguments: source string and group string")
				exit(1)
			
			read_dns_from_disk(createIP=False)
			ip_rules_for_dns_for_source(False, sys.argv[start_index + 1], sys.argv[start_index + 2])
			exit()

		else:
			print("Unknown argument!")
			exit(1)

	# It is a list so it is easily iterable and we can remove things from the middle
	stop_event = threading.Event()

	# Create the IP
	if os.path.isfile(DNS_FOREVER_FILE):
		# UPDATE HERE: what if --services failed, files got created, and IP rules did not get created?
		# Prevent rules from dns being set multiple times
		set_dns_file = "/run/prompt-and-split/set_dns"
		if os.path.isfile(set_dns_file):
			read_dns_from_disk(createIP=False)
		else:
			read_dns_from_disk(createIP=True)
			command_run("echo > " + set_dns_file)
	else:
		print("Failed to open dns rules at", DNS_FOREVER_FILE)

	if READ_HOSTS:
		load_hosts_files()

	reload_rules(del_or_add=True, file=IP_FOREVER_FILE, match_group = GROUP_DEFAULT_INT)

	print("Starting threads")
	log_packet_connection(None, starting=True)

	executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
	command_run("/usr/bin/cat {} > /dev/null".format(PROC_PATH)) #Clear out any previous potential buildup
	executor.submit(reader,   stop_event) # Producer
	executor.submit(prompter, stop_event) # Consumer

	command_run("/usr/bin/cat {} > /dev/null".format(TID_PROC_FILE))
	listening_dict = dict()
	# UPDATE HERE to make this read from config file like the bash one does
	LANInterface = command_run("/usr/bin/ip address show | /usr/bin/grep -B2 -- '192\\.168\\.' | /usr/bin/head -1 | /usr/bin/awk '{{print $2}}'").rstrip().replace(":","")
	for index, listen_iface in enumerate(("lo", LANInterface)):
		listen_port = 53
		listen_address = command_run("/usr/bin/ip address show dev {} | /usr/bin/grep -- '^[[:space:]]*inet ' | /usr/bin/head -1 | /usr/bin/awk -F ' ' '{{print $2}}' | cut -d'/' -f1".format(listen_iface)).rstrip()
		thread = listener_thread(index + 1, listen_iface, (listen_address, listen_port))
		thread.start()
		listening_dict[listen_iface] = ((listen_address, listen_port), thread)
		dns_log("Started on {}:{} iface {}".format(listen_address, listen_port, listen_iface))

	# print(HOSTS_DATA)
	command_run("/etc/my_scripts_root/notify-send_all_users 'Promtper Started'")
	# Spin off threads for the default lo listener as well as any other interfaces:
	# "br-" for VMs, "veth-" for namespaces
	while True:
		
		# Piggyback on the loop here to flush the dns log in a loop
		dns_log(True)

		ifaces = psutil.net_if_addrs()
		for iface_name in ifaces:
			if (iface_name.startswith("br-") or iface_name.startswith("veth-")) and iface_name not in listening_dict:
				# Interface needs to have an AF_INET address, not just AF_PACKET (layer 2 MAC address). Can the address be None or something?
				for snicaddr in ifaces[iface_name]:
					if socket.AddressFamily.AF_INET == snicaddr.family:
						listen_port = 53
						listen_addr = (snicaddr.address, listen_port)
						thread = listener_thread(len(listening_dict) + 1, iface_name, listen_addr)
						thread.start()
						listening_dict[iface_name] = (listen_addr, thread)
						dns_log("Started on {}:{} iface {}".format(snicaddr.address, listen_port, iface_name))
						break

		for iface_name in list(listening_dict):
			if iface_name not in ifaces:
				listening_dict[iface_name][1].stop()
				del listening_dict[iface_name]
				dns_log("Stopped on {}".format(iface_name), "", "", "")

		time.sleep(1)