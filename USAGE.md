# Usage guide for after BUILD.md is followed

## Containers Examples
### Running a GUI application with a shared XWayland socket:
Security Note: The host runs Wayland, so the container GUI application is in the same security scope as anything run with the host's XWayland, which includes any other graphical applciations run in other containers.

I use this command as root to run a popular private messenger's GUI application in a container under a tun0 namespace (which is the default tun device in the default namespace). Something about lxc-attach in the background failing means the root terminal needs to be filled with it in the foreground. Quit it (triggering `lxc-stop name` and then the container cleanup in startvpn.sh) with Ctrl-Q in the application's window.

\# (lxc-info name | grep "^State:[[:space:]]*RUNNING$" 1>/dev/null || startvpn.sh --lxc name tun0) && lxc-attach --clear-env -n name -- sh -c 'cd /tmp/ ; sudo -u user DISPLAY=:0 name-package > /dev/null 2>&1' && lxc-stop name
